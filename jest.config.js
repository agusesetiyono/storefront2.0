// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
  preset: 'ts-jest',
  clearMocks: true,
  coverageDirectory: 'coverage',
  testEnvironment: 'jsdom',
  collectCoverageFrom: [
    '**/*.{ts,tsx}',
    '!<rootDir>/src/pages/Component/**',
    '!<rootDir>/src/index.tsx',
    '!<rootDir>/node_modules/',
    '!<rootDir>/build/',
  ],
  resetMocks: false,
  setupFiles: ['jest-localstorage-mock'],
  setupFilesAfterEnv: ['./jest.setup.js'],
  transform: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/fileTransformer.js',
  },
  moduleNameMapper: {
    '^@assets/(.*)': '<rootDir>/src/assets/$1',
    '^@components/(.*)': '<rootDir>/src/components/$1',
    '^@hooks/(.*)': '<rootDir>/src/hooks/$1',
    '^@interfaces/(.*)': '<rootDir>/src/interfaces/$1',
    '^@pages/(.*)': '<rootDir>/src/pages/$1',
    '^@utils/(.*)': '<rootDir>/src/utils/$1',
    '^@mocks/(.*)': '<rootDir>/src/__mocks__/$1',
    '^.+\\.(css|scss)$': '<rootDir>/src/__mocks__/style-mock.js',
  },
  globals: {
    'ts-jest': {
      isolatedModules: true,
    },
  },
};
