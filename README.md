# Storefront 2.0

Microfe for storefront editor in smartseller.

## Test Coverage
[![Coverage Status](http://opencov.smartseller.co.id/projects/3/badge.svg)](http://opencov.smartseller.co.id/projects/3)

## Development

Make sure you're aleready install [NodeJS](https://nodejs.org/en/) and [Yarn](https://yarnpkg.com/en/). After that run this several command.

1. Add project
```bash
$ git clone git@bitbucket.org:agusesetiyono/storefront2.0.git
$ cd storefront2.0
$ cp .env.sample .env
```
2. Install dependencies
```bash
$ yarn
```
3. Run
```bash
$ yarn dev
```

### Stand-alone

- Update all these variables according to the css used in [app.smartseller.co.id](https://app.smartseller.co.id):
```
SMARTSELLER_VENDOR_CSS=https://app.smartseller.co.id/assets/dist/vendor-d01fca0c40.css
SMARTSELLER_MAIN_CSS=https://app.smartseller.co.id/assets/dist/style-fbf5cd82f3.css
SMARTSELLER_TAILWIND_CSS=https://app.smartseller.co.id/assets/dist/tailwind-aa20f1de16.css
```
- Open your browser
```bash
http://localhost:3000/storefront-editor
```

### From layout container ([ngorder2017](https://bitbucket.org/agusesetiyono/ngorder2017/branch/feature/smartseller-storefront2.0))

- Run ngorder2017
- Add env variable to ngorder2017
```
STOREFRONT_MICRO_FE_HOST=[storefront2.0 host]
```
- Open your browser (ngorder2017 app)
```
http://localhost/storefront-editor
```