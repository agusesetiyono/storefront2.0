const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');
require('dotenv').config();

const resolveAlias = require('./tsconfig-webpack-alias');

const {
  PORT,
  ASSET_URL,
  SMARTSELLER_VENDOR_CSS,
  SMARTSELLER_MAIN_CSS,
  SMARTSELLER_TAILWIND_CSS,
} = process.env;
const config = {
  module: {
    rules: [
      {
        test: /\.(ts|tsx)?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.(js|jsx)$/,
        use: {
          loader: 'babel-loader',
        },
        exclude: /node_modules/,
      },
      {
        test: /\.(svg|png|jpg|gif)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'images',
          },
        },
      },
      {
        test: /\.s?css$/i,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
    ],
  },
  entry: './src/index.tsx',
  output: {
    path: path.join(__dirname, '/build'),
    filename: '[name]-[fullhash].js',
    chunkFilename: '[id]-[chunkhash].js',
    publicPath: ASSET_URL,
  },
  devServer: {
    historyApiFallback: true,
    port: PORT,
    hot: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET',
    },
  },
  plugins: [
    new WebpackManifestPlugin({
      fileName: 'asset-manifest.json',
      publicPath: ASSET_URL,
      generate: (seed, files, entrypoints) => {
        const manifestFiles = files.reduce((manifest, file) => {
          manifest[file.name] = file.path;
          return manifest;
        }, seed);
        const entrypointFiles = entrypoints.main.filter(
          (fileName) => !fileName.endsWith('.map'),
        );

        return {
          files: manifestFiles,
          entrypoints: entrypointFiles,
        };
      },
    }),
  ],
  resolve: {
    alias: resolveAlias(),
    extensions: ['.tsx', '.ts', '.js'],
  },
};

module.exports = (_, argv) => {
  const isInDevelopment = argv.mode === 'development';

  const htmlTemplate = isInDevelopment ? 'development-container' : 'index';
  const htmlWebpackPluginOptions = {
    template: `./public/${htmlTemplate}.html`,
    ...(isInDevelopment
      ? {
          vendorCSS: SMARTSELLER_VENDOR_CSS,
          mainCSS: SMARTSELLER_MAIN_CSS,
          tailwindCSS: SMARTSELLER_TAILWIND_CSS,
        }
      : {}),
  };
  config.plugins.push(new HtmlWebpackPlugin(htmlWebpackPluginOptions));
  config.plugins.push(
    new webpack.DefinePlugin({
      'process.env.ASSET_URL': JSON.stringify(process.env.ASSET_URL),
      'process.env.SENTRY_DSN': JSON.stringify(process.env.SENTRY_DSN),
      'process.env.APP_ENV': JSON.stringify(process.env.APP_ENV),
      'process.env.SMARTSELLER_API_URL': JSON.stringify(
        process.env.SMARTSELLER_API_URL,
      ),
      'process.env.SMARTSELLER_SITE_EXT': JSON.stringify(
        process.env.SMARTSELLER_SITE_EXT,
      ),
      'process.env.STOREFRONT_TOKEN_KEY': JSON.stringify(
        process.env.STOREFRONT_TOKEN_KEY,
      ),
      'process.env.TRACKER_CLIENT_KEY': JSON.stringify(
        process.env.TRACKER_CLIENT_KEY,
      ),
      'process.env.SMARTSELLER_URL': JSON.stringify(
        process.env.SMARTSELLER_URL,
      ),
    }),
  );

  return config;
};
