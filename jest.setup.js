process.env.STOREFRONT_TOKEN_KEY = 'tokenkey';
process.env.SMARTSELLER_API_URL = 'http://localhost:3001';
process.env.SMARTSELLER_SITE_EXT = '.smartseller.co.id';

const localStorageMock = {
  getItem: jest.fn(),
  setItem: jest.fn(),
  removeItem: jest.fn(),
  clear: jest.fn(),
};

global.fetch = jest.fn();
global.console.error = jest.fn();
global.URL.createObjectURL = jest.fn();
global.localStorage = localStorageMock;
global.CONFIG = {
  token:
    '{"ct":"v7N6QpV\\/WOA9b1ZGhfBOiA==","iv":"e3dc9f7021632f045bb76dfbd68b86b2","s":"b060b85cd18b7c94"}',
};
global.tracker = {
  isBySuperuser: false,
  trackerUrl: 'https://tracker-staging.smartseller.co.id',
};

jest.setTimeout(30000);

jest.mock('@utils/tracker', () => ({ sendTracker: jest.fn() }));
