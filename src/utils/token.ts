import CryptoJS from 'crypto-js';
import config from '@utils/config';

// Reference https://github.com/brainfoolong/cryptojs-aes-php
const CryptoJSAesJson = {
  parse: function (jsonStr: string) {
    const j = JSON.parse(jsonStr);
    const cipherParams = CryptoJS.lib.CipherParams.create({
      ciphertext: CryptoJS.enc.Base64.parse(j.ct),
    });
    if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv);
    if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s);
    return cipherParams;
  },
};

export function getBearerToken(): string {
  const tokenConfig: string = global.CONFIG.token;
  if (!tokenConfig) return '';

  const decryptedToken: string = CryptoJS.AES.decrypt(
    tokenConfig,
    config.tokenKey,
    {
      format: CryptoJSAesJson,
    },
  ).toString(CryptoJS.enc.Utf8);
  const token = JSON.parse(decryptedToken);
  return `Bearer ${token}`;
}

export const generateRandomString = (length: number): string => {
  let result = '';
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return result;
};

export default {
  getBearerToken,
  generateRandomString,
};
