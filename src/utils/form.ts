import type { AxiosError } from 'axios';
import type { FormikErrors, FormikValues } from 'formik';
import { IResponseError } from '@interfaces/query';

export const setFormErrors = (
  error: AxiosError<any, any>,
  setErrors: (errors: FormikErrors<FormikValues>) => void,
) => {
  const errorResponse: IResponseError = error.response?.data || {};
  let dataErrors = {};
  errorResponse?.errors?.map((error) => {
    dataErrors = { ...dataErrors, [error.field]: error.message };
  });

  setErrors(dataErrors);
};

export const setFormFieldError = (
  field: string,
  error: AxiosError<any, any>,
  setFieldError: (field: string, value: string | undefined) => void,
) => {
  const errorResponse: IResponseError = error.response?.data || {};
  const dataErrors = errorResponse?.errors?.find(
    (error) => error.field === field,
  );

  setFieldError(field, dataErrors?.message);
};

export const ALPHA_NUMERIC_DASH_REGEX = /^[a-z0-9-]+$/i;
export const ALPHA_NUMERIC_REGEX = /^[a-z0-9]+$/i;
