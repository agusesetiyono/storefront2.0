import { IResponseError } from '@interfaces/query';
import { AxiosError } from 'axios';

export const getErrorMessage = (error: AxiosError<any, any>) => {
  const errorResponse: IResponseError = error.response?.data;
  const lastErrorIndex = errorResponse.errors.length - 1;

  return errorResponse.errors.reduce((messages, curr, index) => {
    return (messages += `${curr.message}${
      index === lastErrorIndex ? '' : ', '
    }`);
  }, '');
};
