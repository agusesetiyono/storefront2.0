import dayjs from 'dayjs';
require('dayjs/locale/id');

export const formatDate = (date: Date | string, format?: string) =>
  dayjs(date).locale('id').format(format);

export const dateOnly = 'D-M-YYYY'; //1-3-2022
export const shortDate = 'DD-MM-YYYY'; //01-03-2022
export const longDate = 'DD MMMM YYYY'; //01 Maret 2022
export const dateWithTime = 'D-M-YYYY HH:mm'; //1-3-2022 00:00
export const shortDateWithTime = 'DD-MM-YYYY HH:mm'; //01-03-2022 00:00
export const longDateWithTime = 'DD MMMM YYYY HH:mm'; //01 Maret 2022 00:00

export const setSecondToZero = (date: string): string =>
  `${date.slice(0, -7)}00.000Z`;
