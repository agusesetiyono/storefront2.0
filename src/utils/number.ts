export const formatNumber = (value?: string) => {
  if (!value) {
    return undefined;
  }

  const stringNumber = value.replace(/[\D_]+/g, '');
  return stringNumber ? Number(stringNumber) : undefined;
};

export const formatCurrency = (
  value?: number,
  showCurrency = true,
  currency = 'Rp',
  locale = 'id',
): string => {
  if (value !== undefined) {
    const formattedValue = value.toLocaleString(locale);
    return showCurrency ? `${currency} ${formattedValue}` : formattedValue;
  }
  return '';
};
