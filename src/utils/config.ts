const appConfig = {
  apiUrl: process.env.SMARTSELLER_API_URL,
  smartSellerUrl: process.env.SMARTSELLER_URL,
  siteExt: process.env.SMARTSELLER_SITE_EXT,
  tokenKey: process.env.STOREFRONT_TOKEN_KEY,
};

export default appConfig;
