import {
  useMutation,
  useQuery,
  useQueryClient,
  UseQueryOptions,
} from 'react-query';
import type { QueryFunctionContext } from 'react-query/types/core/types';
import { AxiosError, AxiosResponse } from 'axios';

import { IResponseError, IResponseSuccess, QueryKeyT } from '@interfaces/query';
import toast from '@utils/toast';
import { getBearerToken } from '@utils/token';
import { api } from './api';

const refetchErrorCodes = [
  11127, // Manage pages: data updated / Delete slider: slider already deleted
  11128, // Manage pages: page not found
  11142, // Manage coupons: expired coupon
];

export const fetcher = <T, T2>({
  queryKey,
  pageParam,
}: QueryFunctionContext<QueryKeyT, T2>): Promise<T> => {
  const [url, params] = queryKey;
  return api
    .get<T>(url, { params: { ...params, pageParam } })
    .then((res) => res.data);
};

export const useFetch = <T>(
  url: string | null,
  params?: object,
  config?: UseQueryOptions<T, Error, T, QueryKeyT>,
) => {
  const context = useQuery<T, Error, T, QueryKeyT>(
    [url!, params, 'get'],
    ({ queryKey, meta }) => fetcher<T, typeof params>({ queryKey, meta }),
    {
      enabled: !!url && !!getBearerToken(),
      ...config,
    },
  );

  return context;
};

const useGenericMutation = <T, S>(
  func: (data: T | S) => Promise<AxiosResponse<S>>,
  url: string,
  method: string,
  params?: object,
  updater?: ((oldData: T, newData: S) => T) | undefined,
) => {
  const queryClient = useQueryClient();
  const queryKey = [url, params, method];

  return useMutation<AxiosResponse, AxiosError, T | S>(func, {
    onMutate: async (data) => {
      await queryClient.cancelQueries(queryKey);

      const previousData = queryClient.getQueryData(queryKey);

      queryClient.setQueryData<T>(queryKey, (oldData) => {
        return updater ? updater(oldData!, data as S) : (data as T);
      });

      return previousData;
    },
    onError: (err, _, context) => {
      const errorResponse: IResponseError = err.response
        ?.data as IResponseError;
      queryClient.setQueryData([url, params], context);

      if (errorResponse?.meta.http_status > 500) {
        onErrorMutate(err);
      }
    },
    onSettled: async () => {
      await queryClient.invalidateQueries(queryKey);
    },
  });
};

export const useDelete = <T>(
  url: string,
  params?: object,
  updater?: (oldData: T, id: string | number) => T,
) => {
  return useGenericMutation<T, string | number>(
    (id) => api.delete(`${url}/${id}`),
    url,
    'delete',
    params,
    updater,
  );
};

export const usePost = <T, S>(
  url: string,
  params?: object,
  updater?: (oldData: T, newData: S) => T,
) => {
  return useGenericMutation<T, S>(
    (data) => api.post<S>(url, data),
    url,
    'post',
    params,
    updater,
  );
};

export const usePut = <T, S>(
  url: string,
  params?: object,
  updater?: (oldData: T, newData: S) => T,
) => {
  return useGenericMutation<T, S>(
    (data) => api.put<S>(url, data),
    url,
    'put',
    params,
    updater,
  );
};

export const usePatch = <T, S>(
  url: string,
  params?: object,
  updater?: (oldData: T, newData: S) => T,
) => {
  return useGenericMutation<T, S>(
    (data) => api.patch<S>(url, data),
    url,
    'patch',
    params,
    updater,
  );
};

export const onSuccessMutate = (
  data: AxiosResponse<any, any>,
  refetch?: () => void,
  onRefetch?: () => void,
) => {
  const successResponse: IResponseSuccess<any> = data.data;
  toast.success(successResponse.message);
  if (refetch) {
    refetch();
    onRefetch && onRefetch();
  }
};

export const onErrorMutate = (
  error: AxiosError<any, any>,
  refetch?: () => void,
  forceRefetch?: boolean,
  onRefetch?: () => void,
) => {
  const errorResponse: IResponseError = error.response?.data;
  let isRefetch = false;
  errorResponse.errors.forEach((error) => {
    if (!isRefetch) {
      isRefetch = refetchErrorCodes.includes(error.code);
    }
    toast.danger(error.message);
  });

  if ((forceRefetch || isRefetch) && refetch) {
    void refetch();
    onRefetch && onRefetch();
  }
};
