import React from 'react';
import { sendTracker } from '@utils/tracker';

interface IDashboardTabUrlMapping {
  path: string;
  tab: string;
}

interface ILinkTracker {
  event: React.MouseEvent;
  url: string;
  actionType: string;
  actionDetails?: string;
}

interface IDashboardTracker {
  actionType: string;
  actionDetails?: string;
}

const evn = 'ngorder_sf_dashboard_actions';

const DASHBOARD_TAB_URL_TRACKER_MAPPING: IDashboardTabUrlMapping[] = [
  {
    path: 'domain',
    tab: 'domain',
  },
  {
    path: 'custom-website',
    tab: 'custom_website',
  },
  {
    path: 'manage-pages',
    tab: 'atur_halaman',
  },
  {
    path: 'marketing-tools/seo',
    tab: 'marketing_seo',
  },
  {
    path: 'marketing-tools/meta-pixel',
    tab: 'marketing_metapixel',
  },
  {
    path: 'marketing-tools/google-analytics',
    tab: 'marketing_analytics',
  },
  {
    path: 'coupons',
    tab: 'kupon',
  },
  {
    path: '/setting_frontstore/general',
    tab: 'setting',
  },
  {
    path: 'marketing-tools',
    tab: 'marketing_seo',
  },
];

export const getTabName = (pathname) =>
  DASHBOARD_TAB_URL_TRACKER_MAPPING.find(({ path }) =>
    pathname.replace('/storefront-editor/settings/', '').startsWith(path),
  )?.tab || pathname;

export const sendDashboardTracker = ({
  actionType,
  actionDetails,
}: IDashboardTracker) => {
  return sendTracker({
    evn,
    active_tab: getTabName(window.location.pathname),
    action_type: actionType,
    ...(actionDetails ? { action_details: actionDetails } : {}),
  });
};

export const sendClickTabTracker = (targetPath) =>
  sendDashboardTracker({
    actionType: 'click_tab',
    actionDetails: getTabName(targetPath),
  });

export const sendLinkTracker = ({
  event,
  url,
  actionType,
  actionDetails,
}: ILinkTracker) => {
  event.preventDefault();
  return sendDashboardTracker({ actionType, actionDetails })?.finally(() => {
    window.location.href = url;
  });
};
