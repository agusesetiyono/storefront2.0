import { sendTracker } from '@utils/tracker';

export enum WebBuilderAction {
  Visit = 'visit',
  EditSetting = 'edit_setting',
  ClickEntry = 'click_entry',
  ClickToggle = 'click_toggle',
  AddEntry = 'add_entry',
  EditEntry = 'edit_entry',
  DeleteEntry = 'delete_entry',
  ReorderEntry = 'reorder_entry',
  Error = 'error',
}

interface IBuilderPanelUrlMapping {
  path: string;
  panel: string;
}

const evn = 'ngorder_sf_builder_actions';
const BASE_PATH = '/storefront-editor/settings/custom-website/builder';

const BUILDER_PANEL_URL_MAPPING: IBuilderPanelUrlMapping[] = [
  {
    path: '/section/header',
    panel: 'section_header',
  },
  {
    path: '/section/banner',
    panel: 'section_banner',
  },
  {
    path: '/section/slider',
    panel: 'section_slider',
  },
  {
    path: '/section/product-list',
    panel: 'section_custom_product_list',
  },
  {
    path: '/section/text',
    panel: 'section_text',
  },
  {
    path: '/theme/config',
    panel: 'theme_general',
  },
  {
    path: '/theme/font',
    panel: 'theme_font',
  },
  {
    path: '/theme/color',
    panel: 'theme_color',
  },
  {
    path: '/theme',
    panel: 'customize_theme',
  },
  {
    path: '/section',
    panel: 'customize_section',
  },
];

const _getActivePanel = (pathname) =>
  BUILDER_PANEL_URL_MAPPING.find(({ path }) =>
    pathname.replace(BASE_PATH, '').startsWith(path),
  )?.panel || 'default';

export const sendWebBuilderTracker = (
  action_type: WebBuilderAction,
  action_details?: string,
) => {
  return sendTracker({
    evn,
    active_panel: _getActivePanel(window.location.pathname),
    action_type,
    ...(action_details ? { action_details } : {}),
  });
};
