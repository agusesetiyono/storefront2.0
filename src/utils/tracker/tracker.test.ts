import axios from '@mocks/axios';
import { API_URL, getLastCalled } from '@utils/test-helper';
import { sendTracker } from '.';

axios.onPost(`${API_URL.tracker}/e`).reply(200);
// revert mocking tracker module from global config in jest.setup.js
jest.mock('.', () => ({
  __esModule: true,
  ...jest.requireActual('.'),
}));

afterEach(() => {
  axios.history.post = [];
});

describe('Tracker utils', () => {
  test('Should send tracker correctly', async () => {
    await sendTracker({
      evn: 'test_evn',
      value: 10,
    });
    expect(getLastCalled(axios, 'post').data).toEqual(
      '{"evn":"test_evn","value":10}',
    );
  });

  test.each([true, false])(
    'Should not call tracker, isBySuperuser: %s',
    async (isBySuperuser) => {
      global.tracker = isBySuperuser
        ? {
            isBySuperuser: true,
            trackerUrl: 'https://tracker-staging.smartseller.co.id',
          }
        : undefined;

      await sendTracker({
        evn: 'test_evn',
        value: 10,
      });
      expect(getLastCalled(axios, 'post')).toEqual(undefined);
    },
  );
});
