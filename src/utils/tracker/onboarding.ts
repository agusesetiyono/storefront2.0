import { sendTracker } from '@utils/tracker';

const evn = 'ngorder_sf_onboarding_funnel_actions';

interface IOnboardingTracker {
  onBoardingStep: string;
  actionType: string;
  actionDetails?: string;
  targetUrl?: string;
}

export const sendOnboardingTracker = ({
  onBoardingStep,
  actionType,
  actionDetails,
  targetUrl,
}: IOnboardingTracker) => {
  return sendTracker({
    evn,
    onboarding_step: onBoardingStep,
    action_type: actionType,
    ...(actionDetails ? { action_details: actionDetails } : {}),
    ...(targetUrl ? { target_url: targetUrl } : {}),
  });
};
