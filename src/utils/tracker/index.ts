import axios from 'axios';

export const sendTracker = (data) => {
  if (!globalThis.tracker || globalThis.tracker.isBySuperuser)
    return Promise.resolve();

  const config = {
    headers: {
      'client-key': process.env.TRACKER_CLIENT_KEY as string,
      'X-User-Agent': 'Web-App',
    },
  };
  return axios.post(
    `${globalThis.tracker.trackerUrl}/e`,
    {
      ui: globalThis.tracker.ui,
      ...data,
    },
    config,
  );
};
