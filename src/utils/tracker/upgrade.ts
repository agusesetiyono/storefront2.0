import { sendTracker } from '@utils/tracker';

interface IUpgradeTracker {
  actionType: string;
  actionDetails?: string;
  targetUrl?: string;
}

const evn = 'ngorder_sf_upgrade_funnel_actions';

export const sendUpgradeTracker = ({
  actionType,
  actionDetails,
  targetUrl,
}: IUpgradeTracker) => {
  return sendTracker({
    evn,
    action_type: actionType,
    ...(actionDetails ? { action_details: actionDetails } : {}),
    ...(targetUrl ? { target_url: targetUrl } : {}),
  });
};
