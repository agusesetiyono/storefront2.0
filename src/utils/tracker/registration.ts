import { sendTracker } from '@utils/tracker';

const evn = 'ngorder_sf_bukastore_funnel_action';

interface IRegistrationTracker {
  actionType: string;
  targetUrl?: string;
}

export const sendRegistrationTracker = ({
  actionType,
  targetUrl,
}: IRegistrationTracker) => {
  const email =
    (globalThis.email as string) || (globalThis.USER?.email as string);

  return sendTracker({
    evn,
    action_type: actionType,
    bl_identity: JSON.stringify({
      user_id: '',
      email,
    }),
    ui: globalThis.tracker.ui,
    ...(targetUrl ? { target_url: targetUrl } : {}),
  });
};
