import { sendTracker } from '@utils/tracker';

const evn = 'ngorder_sf_activate_funnel_actions';

interface IActivationTracker {
  actionType: string;
  actionDetails?: string;
  targetUrl?: string;
  isResubscribe?: boolean;
}

interface ILinkTracker {
  event: React.MouseEvent;
  url: string;
  actionType: string;
  actionDetails?: string;
  isResubscribe?: boolean;
}

export const sendActivationTracker = ({
  actionType,
  actionDetails,
  targetUrl,
  isResubscribe,
}: IActivationTracker) => {
  return sendTracker({
    evn,
    action_type: actionType,
    ...(actionDetails ? { action_details: actionDetails } : {}),
    ...(targetUrl ? { target_url: targetUrl } : {}),
    ...(isResubscribe ? { is_resubscribe: isResubscribe } : {}),
  });
};

export const sendLinkTracker = ({
  event,
  url,
  actionType,
  actionDetails,
  isResubscribe,
}: ILinkTracker) => {
  event.preventDefault();
  return sendActivationTracker({
    actionType,
    actionDetails,
    isResubscribe,
    targetUrl: url,
  })?.finally(() => {
    window.location.href = url;
  });
};
