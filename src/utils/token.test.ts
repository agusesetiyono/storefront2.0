import { getBearerToken } from './token';

describe('Token utils', () => {
  test('Should decrypt token correctly', () => {
    expect(getBearerToken()).toEqual('Bearer sampletoken');
  });

  test('Should return empty token', () => {
    global.CONFIG = {
      token: '',
    };
    expect(getBearerToken()).toEqual('');
  });
});
