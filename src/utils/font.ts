export const getFontName = (font: string) => font.replace(/\+/g, ' ');

const getFontStyle = (style?: string) => {
  const isItalic = style?.includes('italic');
  const size = isItalic ? style?.replace('italic', '') : style;
  return {
    isItalic,
    size,
  };
};

export const getFontDisplayName = (font: string, style?: string) => {
  const { size, isItalic } = getFontStyle(style);
  return `${getFontName(font)} ${getFontSizeName(size)} ${
    isItalic ? 'Italic' : ''
  }`;
};

export const getFontProperties = (
  font: string,
  style?: string,
): { fontFamily: string; fontWeight?: string; fontStyle?: string } => {
  const { size, isItalic } = getFontStyle(style);
  return {
    fontFamily: getFontName(font),
    ...(size && { fontWeight: size }),
    ...(isItalic && { fontStyle: 'italic' }),
  };
};

export const getFontSizeName = (size?: string) => {
  const numSize = Number(size);
  switch (true) {
    case numSize === 100:
      return 'Thin';
    case numSize === 200:
      return 'Extra Light';
    case numSize === 300:
      return 'Light';
    case numSize === 400:
      return '';
    case numSize === 500:
      return 'Medium';
    case numSize === 600:
      return 'Semi Bold';
    case numSize === 700:
      return 'Bold';
    case numSize === 800:
      return 'Extra Bold';
    case numSize === 900:
      return 'Black';
    default:
      return '';
  }
};
