import axios from 'axios';
import { getBearerToken } from '@utils/token';

const instance = axios.create({
  headers: {
    // Get global config variable from ngorder container
    authorization: getBearerToken(),
    'Accept-Language': 'id',
  },
});

export const api = {
  get: <T>(url: string, params?: object) => instance.get<T>(url, { ...params }),
  post: <T>(url: string, data: any) => instance.post<T>(url, data),
  put: <T>(url: string, data: any) => instance.put<T>(url, data),
  patch: <T>(url: string, data: any) => instance.patch<T>(url, data),
  delete: <T>(url: string) => instance.delete<T>(url),
};
