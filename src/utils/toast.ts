type TToast = 'success' | 'info' | 'warning' | 'danger';

interface IToastPosition {
  x: 'left' | 'center' | 'right';
  y: 'top' | 'center' | 'bottom';
}

interface IToast {
  show: (
    type: TToast,
    message: string | undefined,
    duration?: number,
    position?: IToastPosition,
  ) => void;
}

const notif = (window as any).Notification as IToast;
const defaultDuration = 7000;
const defaultPosition = { x: 'center', y: 'top' } as IToastPosition;

const defaultToast = (type: TToast, message?: string) => {
  const duration =
    ((window as any).DEFAULT_TOAST_DURATION as number) || defaultDuration;
  notif.show(type, message || '', duration, defaultPosition);
};

const toast = {
  success: (message?: string) => defaultToast('success', message),
  danger: (message?: string) => defaultToast('danger', message),
  warning: (message?: string) => defaultToast('warning', message),
  info: (message?: string) => defaultToast('info', message),
};

export default toast;
