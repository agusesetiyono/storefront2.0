import React from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { render, RenderResult } from '@testing-library/react';
import MockAdapter from 'axios-mock-adapter';
import { AxiosRequestConfig } from 'axios';
import Router from '../Router';

interface IRenderWithRouteParams {
  route?: string;
}

export const renderWithRoute = ({
  route = '/storefront-editor',
}: IRenderWithRouteParams = {}): RenderResult => {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        cacheTime: 0,
        retry: false,
      },
    },
  });
  window.history.pushState({}, 'Test page', route);
  return render(
    <QueryClientProvider client={queryClient}>
      <Router />,
    </QueryClientProvider>,
  );
};

export const getLastCalled = (
  axios: MockAdapter,
  method: string,
): AxiosRequestConfig => {
  const methodHistory = axios.history[method];
  return methodHistory[methodHistory.length - 1];
};

export const getLastCalledTimes = (
  histories: AxiosRequestConfig[],
  url: string,
): number => {
  return histories.filter((history) => history.url === url)?.length || 0;
};

export const BASE_API_URL = `${process.env.SMARTSELLER_API_URL}`;
export const API_URL = {
  storefronts: `${BASE_API_URL}/storefronts`,
  domains: `${BASE_API_URL}/domains`,
  pages: `${BASE_API_URL}/pages`,
  coupons: `${BASE_API_URL}/coupons`,
  customerCategories: `${BASE_API_URL}/customers/categories`,
  marketingTools: `${BASE_API_URL}/marketing-tools`,
  sections: `${BASE_API_URL}/sections`,
  productList: `${BASE_API_URL}/sections/product-list`,
  searchProduct: `${BASE_API_URL}/searches/products`,
  text: `${BASE_API_URL}/sections/text`,
  sliders: `${BASE_API_URL}/sliders`,
  banners: `${BASE_API_URL}/banners`,
  header: `${BASE_API_URL}/storefronts/themes/header`,
  settingThemes: `${BASE_API_URL}/storefronts/themes/settings`,
  themeColors: `${BASE_API_URL}/storefronts/themes/colors`,
  userTheme: `${BASE_API_URL}/storefronts/themes/me`,
  updateFonts: `${BASE_API_URL}/storefronts/themes/fonts`,
  tracker: 'https://tracker-staging.smartseller.co.id',
  registerUser: `${process.env.SMARTSELLER_URL}/storefront-editor/register-bukalapak`,
};
