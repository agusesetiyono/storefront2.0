import React, { createContext, ReactNode, useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { Spinner } from 'react-bootstrap';

import type { ISubscriptionContext } from '@interfaces/storefronts';
import { useGetSubscription } from '@hooks/api/storefronts';
import useOnboarding from '@hooks/useOnboarding';

export const SubscribeContext = createContext<ISubscriptionContext>(null!);

const SubscribeProvider = ({ children }: { children: ReactNode }) => {
  const { pathname } = useLocation();
  const navigate = useNavigate();
  const { isLoading, subscription, refetchSubscription } = useGetSubscription();
  const { isCompleted } = useOnboarding();
  const { isActive, version } = subscription || {};

  useEffect(() => {
    if (!isLoading) {
      if (pathname === '/register') return;

      if (pathname !== '/' && !isActive) {
        if (pathname.includes('custom-website/builder')) {
          window.location.href = 'storefront-editor';
        }
        return navigate('/');
      } else if (isActive) {
        if (pathname !== '/update' && !isCompleted) {
          return navigate('/onboarding');
        } else if (
          (['/', '/onboarding'].includes(pathname) && isCompleted) ||
          (pathname === '/update' && version === '2.0')
        ) {
          return navigate('/settings/domain');
        }
      }
    }
  }, [isLoading, pathname, isActive, isCompleted]);

  return (
    <SubscribeContext.Provider value={{ subscription, refetchSubscription }}>
      {isLoading ? (
        <div className="flex align-items-center justify-content-center h-screen">
          <Spinner animation="border" variant="primary" className="mt-88" />
        </div>
      ) : (
        children
      )}
    </SubscribeContext.Provider>
  );
};

export default SubscribeProvider;
