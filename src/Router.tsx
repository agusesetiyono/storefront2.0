import React from 'react';
import { Col, Row } from 'react-bootstrap';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from 'react-router-dom';

import { PromptContext } from '@hooks/usePrompt';
import { ROUTE_BASE_NAME } from '@utils/constants';

import SettingLayout from '@components/Layouts/SettingLayout';
import MarketingToolsLayout from '@components/Layouts/MarketingToolsLayout';
import WebBuilderLayout from '@components/Layouts/WebBuilderLayout';

import ScrollToTop from '@components/ScrollToTop';
import LoadingState from '@components/LoadingState';
import SubscribeProvider from './Provider';

const Activation = React.lazy(() => import('@pages/Activation'));
const Onboarding = React.lazy(() => import('@pages/Onboarding'));
const UpdateVersion = React.lazy(() => import('@pages/UpdateVersion'));
const Domain = React.lazy(() => import('@pages/Domain'));
const CustomWebsite = React.lazy(() => import('@pages/CustomWebsite'));
const ManagePages = React.lazy(() => import('@pages/ManagePages'));
const ManagePagesForm = React.lazy(() => import('@pages/ManagePages/Form'));
const Coupons = React.lazy(() => import('@pages/Coupons'));
const CouponsForm = React.lazy(() => import('@pages/Coupons/Form'));
const SEO = React.lazy(() => import('@pages/MarketingTools/SEO'));
const MetaPixel = React.lazy(() => import('@pages/MarketingTools/MetaPixel'));
const GoogleAnalytics = React.lazy(
  () => import('@pages/MarketingTools/GoogleAnalytics'),
);
const WebBuilderHomepage = React.lazy(
  () => import('@pages/WebBuilder/Homepage'),
);
const WebBuilderSection = React.lazy(() => import('@pages/WebBuilder/Section'));
const WebBuilderHeader = React.lazy(() => import('@pages/WebBuilder/Header'));
const WebBuilderSlider = React.lazy(() => import('@pages/WebBuilder/Slider'));
const SliderForm = React.lazy(() => import('@pages/WebBuilder/Slider/Form'));
const WebBuilderBanner = React.lazy(() => import('@pages/WebBuilder/Banner'));
const BannerForm = React.lazy(() => import('@pages/WebBuilder/Banner/Form'));
const ProductListForm = React.lazy(
  () => import('@pages/WebBuilder/ProductList'),
);
const TextForm = React.lazy(() => import('@pages/WebBuilder/Text'));
const WebBuilderTheme = React.lazy(() => import('@pages/WebBuilder/Theme'));
const ConfigForm = React.lazy(() => import('@pages/WebBuilder/Config'));
const FontForm = React.lazy(() => import('@pages/WebBuilder/Font'));
const ColorForm = React.lazy(() => import('@pages/WebBuilder/Color'));
const Registration = React.lazy(() => import('@pages/Registration'));

const NormalLayout = ({ children }) => (
  <Row>
    <Col lg={12}>{children}</Col>
  </Row>
);

const AppRouter = () => (
  <Router basename={ROUTE_BASE_NAME}>
    <ScrollToTop>
      <SubscribeProvider>
        <Routes>
          <Route
            path="/"
            element={
              <React.Suspense fallback={<LoadingState />}>
                <NormalLayout>
                  <Activation />
                </NormalLayout>
              </React.Suspense>
            }
          />
          <Route
            path="/onboarding"
            element={
              <React.Suspense fallback={<LoadingState />}>
                <NormalLayout>
                  <Onboarding />
                </NormalLayout>
              </React.Suspense>
            }
          />
          <Route
            path="/register"
            element={
              <React.Suspense fallback={<LoadingState />}>
                <Registration />
              </React.Suspense>
            }
          />
          <Route
            path="/update"
            element={
              <React.Suspense fallback={<LoadingState />}>
                <NormalLayout>
                  <UpdateVersion />
                </NormalLayout>
              </React.Suspense>
            }
          />
          <Route path="/settings" element={<SettingLayout />}>
            <Route
              path="domain"
              element={
                <React.Suspense fallback={<LoadingState />}>
                  <Domain />
                </React.Suspense>
              }
            />
            <Route
              path="custom-website"
              element={
                <React.Suspense fallback={<LoadingState />}>
                  <CustomWebsite />
                </React.Suspense>
              }
            />
            <Route
              path="manage-pages"
              element={
                <React.Suspense fallback={<LoadingState />}>
                  <ManagePages />
                </React.Suspense>
              }
            />
            {['manage-pages/create', 'manage-pages/edit/:id'].map((path) => {
              return (
                <Route
                  path={path}
                  element={
                    <React.Suspense fallback={<LoadingState />}>
                      <PromptContext>
                        <ManagePagesForm />
                      </PromptContext>
                    </React.Suspense>
                  }
                  key={path}
                />
              );
            })}
            <Route path="marketing-tools" element={<MarketingToolsLayout />}>
              <Route path="" element={<Navigate to="seo" replace />} />
              <Route
                path="seo"
                element={
                  <React.Suspense fallback={<LoadingState />}>
                    <SEO />
                  </React.Suspense>
                }
              />
              <Route
                path="meta-pixel"
                element={
                  <React.Suspense fallback={<LoadingState />}>
                    <MetaPixel />
                  </React.Suspense>
                }
              />
              <Route
                path="google-analytics"
                element={
                  <React.Suspense fallback={<LoadingState />}>
                    <GoogleAnalytics />
                  </React.Suspense>
                }
              />
            </Route>
            <Route
              path="coupons"
              element={
                <React.Suspense fallback={<LoadingState />}>
                  <Coupons />
                </React.Suspense>
              }
            />
            {['coupons/create', 'coupons/edit/:id'].map((path) => {
              return (
                <Route
                  path={path}
                  element={
                    <React.Suspense fallback={<LoadingState />}>
                      <PromptContext>
                        <CouponsForm />
                      </PromptContext>
                    </React.Suspense>
                  }
                  key={path}
                />
              );
            })}
          </Route>
          <Route
            path="/settings/custom-website/builder"
            element={<WebBuilderLayout />}
          >
            <Route
              path=""
              element={
                <React.Suspense fallback={<LoadingState />}>
                  <WebBuilderHomepage />
                </React.Suspense>
              }
            />
            <Route
              path="section"
              element={
                <React.Suspense fallback={<LoadingState />}>
                  <WebBuilderSection />
                </React.Suspense>
              }
            />
            <Route
              path="section/header"
              element={
                <React.Suspense fallback={<LoadingState />}>
                  <PromptContext>
                    <WebBuilderHeader />
                  </PromptContext>
                </React.Suspense>
              }
            />
            <Route
              path="section/slider"
              element={
                <React.Suspense fallback={<LoadingState />}>
                  <WebBuilderSlider />
                </React.Suspense>
              }
            />
            {['section/slider/create', 'section/slider/:id'].map((path) => {
              return (
                <Route
                  path={path}
                  element={
                    <React.Suspense fallback={<LoadingState />}>
                      <PromptContext>
                        <SliderForm />
                      </PromptContext>
                    </React.Suspense>
                  }
                  key={path}
                />
              );
            })}

            <Route
              path="section/banner"
              element={
                <React.Suspense fallback={<LoadingState />}>
                  <WebBuilderBanner />
                </React.Suspense>
              }
            />
            {['section/banner/create', 'section/banner/:id'].map((path) => {
              return (
                <Route
                  path={path}
                  element={
                    <React.Suspense fallback={<LoadingState />}>
                      <PromptContext>
                        <BannerForm />
                      </PromptContext>
                    </React.Suspense>
                  }
                  key={path}
                />
              );
            })}
            <Route
              path="section/product-list/:id"
              element={
                <React.Suspense fallback={<LoadingState />}>
                  <PromptContext>
                    <ProductListForm />
                  </PromptContext>
                </React.Suspense>
              }
            />
            <Route
              path="section/text/:id"
              element={
                <React.Suspense fallback={<LoadingState />}>
                  <PromptContext>
                    <TextForm />
                  </PromptContext>
                </React.Suspense>
              }
            />
            <Route
              path="theme"
              element={
                <React.Suspense fallback={<LoadingState />}>
                  <WebBuilderTheme />
                </React.Suspense>
              }
            />
            <Route
              path="theme/config"
              element={
                <React.Suspense fallback={<LoadingState />}>
                  <PromptContext>
                    <ConfigForm />
                  </PromptContext>
                </React.Suspense>
              }
            />
            <Route
              path="theme/font"
              element={
                <React.Suspense fallback={<LoadingState />}>
                  <FontForm />
                </React.Suspense>
              }
            />
            <Route
              path="theme/color"
              element={
                <React.Suspense fallback={<LoadingState />}>
                  <ColorForm />
                </React.Suspense>
              }
            />
          </Route>
          <Route path="*" element={<Navigate to="/" replace />} />
        </Routes>
      </SubscribeProvider>
    </ScrollToTop>
  </Router>
);

export default AppRouter;
