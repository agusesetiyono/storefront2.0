import React, { useState } from 'react';
import { AxiosError, AxiosResponse } from 'axios';
import { useBoolean } from 'usehooks-ts';
import { onSuccessMutate, onErrorMutate } from '@utils/reactQuery';

import Modal from '@components/Modal';
import Button from '@components/Button';

import type { UseMutationResult } from 'react-query/types/react/types';

interface IUseDeleteItemProps {
  refetch?: () => void;
  deleteItem?: () => UseMutationResult;
  confirmDeleteAction?: () => void;
  onRefetch?: () => void;
  modalTitle: string;
  modalContent: string;
  afterSuccess?: (idToDelete: number | string) => void;
  afterError?: (error: AxiosError) => void;
}

const useDeleteItem = ({
  refetch,
  deleteItem,
  confirmDeleteAction,
  onRefetch,
  modalTitle,
  modalContent,
  afterSuccess,
  afterError,
}: IUseDeleteItemProps) => {
  const deleteItemMutation = deleteItem && deleteItem();
  const [idToDelete, setIdToDelete] = useState<number | string>('');
  const {
    value: isConfirmDelete,
    setTrue: showConfirmDelete,
    setFalse: closeConfirmDelete,
  } = useBoolean(false);

  const onDelete = (id?: number) => {
    setIdToDelete(id || '');
    showConfirmDelete();
  };

  const confirmDelete = () => {
    deleteItemMutation &&
      deleteItemMutation.mutate(idToDelete, {
        onSuccess: (data: AxiosResponse) => {
          onSuccessMutate(data, refetch, onRefetch);
          afterSuccess && afterSuccess(idToDelete);
        },
        onError: (error: AxiosError) => {
          onErrorMutate(error, refetch, false, onRefetch);
          afterError && afterError(error);
        },
        onSettled: closeConfirmDelete,
      });
    confirmDeleteAction && confirmDeleteAction();
    closeConfirmDelete();
    setIdToDelete('');
  };

  const ModalDeleteComponent = () => {
    return (
      <Modal
        show={isConfirmDelete}
        title={modalTitle}
        variant="confirmation"
        action={[
          <Button
            variant="primary"
            onClick={closeConfirmDelete}
            data-testid="button-cancel-delete"
          >
            Tidak
          </Button>,
          <Button
            variant="outline-primary"
            onClick={confirmDelete}
            data-testid="button-confirm-delete"
          >
            Ya, hapus
          </Button>,
        ]}
      >
        <p dangerouslySetInnerHTML={{ __html: modalContent }}></p>
      </Modal>
    );
  };

  return {
    isConfirmDelete,
    onDelete,
    ModalDeleteComponent,
  };
};

export default useDeleteItem;
