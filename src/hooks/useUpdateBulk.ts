import { useEffect } from 'react';
import { useBoolean } from 'usehooks-ts';
import { AxiosError, AxiosResponse } from 'axios';
import useDebounceEffect from '@hooks/useDebounceEffect';
import useCallAPIOnRouteLeave from '@hooks/useCallAPIOnRouteLeave';
import { onSuccessMutate, onErrorMutate } from '@utils/reactQuery';

export interface IUseUpdateBulkProps<RecordType = unknown> {
  data: RecordType[];
  isCancelDelay?: boolean;
  refetch: () => void;
  onSuccess?: () => void;
  onError?: (error: AxiosError) => void;
  onRefetch?: () => void;
  updateAction: () => Promise<AxiosResponse>;
}

const useUpdateBulk = <RecordType>({
  data,
  isCancelDelay,
  refetch,
  onSuccess,
  onRefetch,
  updateAction,
  onError,
}: IUseUpdateBulkProps<RecordType>) => {
  const updateDelay = ((window as any).DEFAULT_SORT_DELAY as number) || 0;
  const {
    value: hasPendingUpdate,
    setTrue: startUpdate,
    setFalse: endUpdate,
  } = useBoolean(false);

  const update = () =>
    updateAction()
      .then((data: AxiosResponse) => {
        onSuccessMutate(data);
        onSuccess && onSuccess();
      })
      .catch((error: AxiosError) => {
        onErrorMutate(error, refetch, true, onRefetch);
        onError && onError(error);
      })
      .finally(endUpdate);

  // immediately update, when user want to leave page but still have pending update action
  useCallAPIOnRouteLeave(update, hasPendingUpdate);

  // immediately update, when user do cancelling action
  useEffect(() => {
    if (isCancelDelay && hasPendingUpdate) {
      void update();
    }
  }, [isCancelDelay, hasPendingUpdate]);

  // update order every (x) ms after user updating data
  useDebounceEffect(
    () => {
      if (!isCancelDelay && hasPendingUpdate) {
        void update();
      }
    },
    [data, isCancelDelay, hasPendingUpdate],
    updateDelay,
  );

  return {
    startUpdate,
  };
};

export default useUpdateBulk;
