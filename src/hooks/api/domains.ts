import config from '@utils/config';
import { usePost } from '@utils/reactQuery';

const baseUrl = `${config.apiUrl}/domains`;
const apiRoutes = {
  postValidate: `${baseUrl}/validate`,
  postSave: `${baseUrl}`,
};

export const useValidateDomain = () => usePost(apiRoutes.postValidate);
export const useSaveDomain = () => usePost(apiRoutes.postSave);
