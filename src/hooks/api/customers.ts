import { UseQueryOptions } from 'react-query';
import config from '@utils/config';
import { useFetch } from '@utils/reactQuery';
import type { ICategoriesStatus } from '@interfaces/customers';
import { QueryKeyT } from '@interfaces/query';

const baseUrl = `${config.apiUrl}/customers`;
const apiRoutes = {
  categories: `${baseUrl}/categories`,
};

export const useGetCategories = (
  opts?: UseQueryOptions<
    ICategoriesStatus,
    Error,
    ICategoriesStatus,
    QueryKeyT
  >,
) => useFetch<ICategoriesStatus>(apiRoutes.categories, undefined, opts);
