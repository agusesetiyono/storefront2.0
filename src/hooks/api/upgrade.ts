import config from '@utils/config';
import { usePost } from '@utils/reactQuery';

const baseUrl = `${config.apiUrl}/storefronts`;
const apiRoutes = {
  upgrades: `${baseUrl}/upgrades`,
};

export const useUpgradeVersion = () => usePost(apiRoutes.upgrades);
