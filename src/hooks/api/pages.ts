import { useState } from 'react';
import config from '@utils/config';
import {
  useDelete,
  useFetch,
  usePatch,
  usePut,
  usePost,
} from '@utils/reactQuery';
import type {
  IPagesResponseStatus,
  IPageResponseStatus,
  IPage,
} from '@interfaces/pages';

const baseUrl = `${config.apiUrl}`;
const apiRoutes = {
  pages: `${baseUrl}/pages`,
  reorderPages: `${baseUrl}/pages/orders`,
};

export const useGetPages = () => {
  const [pages, setPages] = useState<IPage[]>([]);
  const { isFetching, isError, refetch } = useFetch<IPagesResponseStatus>(
    apiRoutes.pages,
    undefined,
    {
      refetchOnWindowFocus: false,
      onSuccess: (data) => {
        const pages = data?.data;
        setPages(pages);
      },
    },
  );

  return {
    isFetching,
    isError,
    pages,
    refetchGetPages: () => void refetch(),
  };
};

export const useCreatePage = () => usePost(apiRoutes.pages);
export const useUpdatePage = (id) => usePatch(`${apiRoutes.pages}/${id}`);
export const useGetPage = (id, onSuccess, onError) =>
  useFetch<IPageResponseStatus>(`${apiRoutes.pages}/${id}`, undefined, {
    refetchOnWindowFocus: false,
    onSuccess,
    onError,
  });
export const useReorderPages = () => usePut(apiRoutes.reorderPages);
export const useDeletePage = () => useDelete<IPage[]>(apiRoutes.pages);
export const useUpdateStatusPage = (id: number) =>
  usePatch(`${apiRoutes.pages}/${id}/status`);
