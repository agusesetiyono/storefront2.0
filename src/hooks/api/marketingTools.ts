import config from '@utils/config';
import { useFetch, usePut } from '@utils/reactQuery';
import type {
  IGoogleAnalyticsResponse,
  ISEOResponse,
  IMetaPixelResponse,
} from '@interfaces/marketingTools';

const baseUrl = `${config.apiUrl}/marketing-tools`;
const apiRoutes = {
  seo: `${baseUrl}/seo`,
  metaPixel: `${baseUrl}/meta-pixel`,
  googleAnalytics: `${baseUrl}/google-analytics`,
};

export const useGetSEO = (onSuccess) => {
  const { isFetching } = useFetch<ISEOResponse>(apiRoutes.seo, undefined, {
    refetchOnWindowFocus: false,
    onSuccess,
  });

  return {
    isFetching,
  };
};

export const useUpdateSEO = () => usePut(apiRoutes.seo);

export const useGetMetaPixel = (onSuccess) => {
  const { isFetching } = useFetch<IMetaPixelResponse>(
    apiRoutes.metaPixel,
    undefined,
    {
      refetchOnWindowFocus: false,
      onSuccess,
    },
  );

  return {
    isFetching,
  };
};

export const useGetGoogleAnalytics = (onSuccess) => {
  const { isFetching } = useFetch<IGoogleAnalyticsResponse>(
    apiRoutes.googleAnalytics,
    undefined,
    {
      refetchOnWindowFocus: false,
      onSuccess,
    },
  );

  return {
    isFetching,
  };
};

export const useUpdateMetaPixel = () => usePut(apiRoutes.metaPixel);
export const useUpdateGoogleAnalytics = () => usePut(apiRoutes.googleAnalytics);
