import { useState } from 'react';
import config from '@utils/config';
import { useFetch, usePost } from '@utils/reactQuery';
import { IResponseStatus, ISubscription } from '@interfaces/storefronts';
import { formatDate } from '@utils/datetime';

const baseUrl = `${config.apiUrl}/storefronts`;
const apiRoutes = {
  getStatus: `${baseUrl}/status`,
  postSubscription: `${baseUrl}/subscriptions`,
};
const NOT_ACTIVE_STOREFRONT_TYPES: string[] = ['none', 'premium'];

export const defaultSubscription: ISubscription = {
  owner_name: '',
  storefront_type: '',
  storefront_expired_date: '',
  active_domain: '',
  domain: '',
  subdomain: '',
  isActive: false,
  isExpired: false,
  version: '',
};

export const useGetSubscription = () => {
  const [subscription, setSubscription] =
    useState<ISubscription>(defaultSubscription);

  const { isLoading, refetch } = useFetch<IResponseStatus>(
    apiRoutes.getStatus,
    undefined,
    {
      onSuccess: (data) => {
        const subsData = data?.data;
        const { storefront_type, storefront_expired_date } = subsData || {};

        const expiredDate = new Date(storefront_expired_date || '');
        const isExpired = Boolean(expiredDate < new Date());
        const isActive = Boolean(
          !NOT_ACTIVE_STOREFRONT_TYPES.includes(storefront_type) && !isExpired,
        );

        setSubscription({
          ...subsData,
          storefront_expired_date: formatDate(expiredDate, 'DD MMMM YYYY'),
          isExpired,
          isActive,
        });
      },
    },
  );

  return {
    isLoading,
    subscription,
    setSubscription,
    refetchSubscription: () => void refetch(),
  };
};

export const useSubmitSubscription = () => usePost(apiRoutes.postSubscription);
