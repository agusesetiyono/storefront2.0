import { useState } from 'react';

import config from '@utils/config';
import { useFetch, usePut } from '@utils/reactQuery';
import { api } from '@utils/api';
import {
  ESectionType,
  IResponseProductList,
  IResponseSections,
  ISections,
  IResponseText,
} from '@interfaces/sections';
import type {
  ISideMenuGroup,
  ISideMenuItem,
} from '@components/WebBuilder/SideMenu/sideMenuWebBuilder.types';
import { IResponseProducts } from '@interfaces/products';

const baseUrl = `${config.apiUrl}`;
const apiRoutes = {
  sections: `${baseUrl}/sections`,
  updateSections: `${baseUrl}/sections/orders`,
  productList: `${baseUrl}/sections/product-list`,
  searchProduct: `${baseUrl}/searches/products`,
  text: `${baseUrl}/sections/text`,
};

const mapSectionList = (section: ISections) => {
  const { id, status, type } = section;

  const defaultSection: ISideMenuItem = {
    title: '',
    id,
    isActive: status === 'active',
    canToggle: true,
  };

  switch (type) {
    case ESectionType.BANNER:
      return {
        ...defaultSection,
        title: 'Banner',
        description:
          'Dengan Banner, kamu bisa menampilkan promosi menarik berupa gambar dan teks.',
        url: 'banner',
      };
    case ESectionType.SLIDER:
      return {
        ...defaultSection,
        title: 'Slider',
        description:
          'Dengan Slider, kamu bisa menampilkan beberapa banner secara bergantian.',
        url: 'slider',
      };
    case ESectionType.CATEGORY:
      return {
        ...defaultSection,
        title: 'Kategori',
        description: 'Tambah kategori produk untuk memudahkan pengelompokan.',
      };
    case ESectionType.PRODUCT_ALL:
      return {
        ...defaultSection,
        title: 'Semua Produk',
        description: 'Tampilkan semua produk di section ini.',
      };
    case ESectionType.PRODUCT_POPULAR:
      return {
        ...defaultSection,
        title: 'Produk Populer',
        description: 'Tampilkan produk-produk terbaikmu dengan section ini.',
      };
    case ESectionType.PRODUCT_BEST_SELLER:
      return {
        ...defaultSection,
        title: 'Terlaris',
        description: 'Tampilkan produk-produk paling laris di section ini.',
      };
    case ESectionType.PRODUCT_CUSTOM:
      return {
        ...defaultSection,
        title: 'Custom Product List',
        description:
          'Pilih atau custom daftar produk yang ingin ditampilkan di homepage pada section ini.',
        url: `product-list/${id}`,
      };
    case ESectionType.TEXT:
      return {
        ...defaultSection,
        title: 'Teks',
        description:
          'Tampilkan teks yang menarik dan kamu bisa atur penempatannya.',
        url: `text/${id}`,
      };
    default:
      return defaultSection;
  }
};

export const useGetSections = () => {
  const [sideMenu, setSideMenu] = useState<ISideMenuGroup[]>([]);

  const { isFetching, isError, refetch } = useFetch<IResponseSections>(
    apiRoutes.sections,
    undefined,
    {
      refetchOnWindowFocus: false,
      onSuccess: (res) => {
        setSideMenu([
          {
            title: 'Bagian Atas',
            menus: [
              {
                title: 'Header',
                description:
                  'Pastikan gambar yang kamu pilih untuk header, menggambarkan identitas atau profil tokomu ya.',
                url: 'header',
              },
            ],
          },
          {
            title: 'Komponen',
            menus: res.data.map((section) => mapSectionList(section)),
            canSort: true,
          },
          {
            title: 'Bagian Bawah',
            menus: [
              {
                title: 'Footer',
                description:
                  'Menampilkan ringkasan semua halaman di website kamu.',
                isDisabled: true,
              },
            ],
          },
        ]);
      },
    },
  );

  return {
    sideMenu,
    setSideMenu,
    isFetching,
    isError,
    refetchSections: () => void refetch(),
  };
};

export const useUpdateSections = (
  sections: { id: number; status: 'active' | 'inactive' }[],
) => api.put(apiRoutes.updateSections, { sections });

export const useGetProductList = (
  id?: string,
  onSuccess?: (res: IResponseProductList) => void,
) =>
  useFetch<IResponseProductList>(`${apiRoutes.productList}/${id}`, undefined, {
    refetchOnWindowFocus: false,
    enabled: !!id,
    onSuccess,
  });

export const useSearchProduct = (
  keyword?: string,
  onSuccess?: (res: IResponseProducts) => void,
) =>
  useFetch<IResponseProducts>(
    apiRoutes.searchProduct,
    { keyword },
    {
      refetchOnWindowFocus: false,
      enabled: Boolean(keyword && keyword?.length >= 3),
      onSuccess,
    },
  );

export const useUpdateProductList = () => usePut(apiRoutes.productList);

export const useGetText = (id, onSuccess) =>
  useFetch<IResponseText>(`${apiRoutes.text}/${id}`, undefined, {
    refetchOnWindowFocus: false,
    onSuccess,
  });

export const useUpdateText = () => usePut(apiRoutes.text);
