import config from '@utils/config';
import { useFetch, usePatch } from '@utils/reactQuery';
import type { IHeaderResponse } from '@interfaces/header';

const baseUrl = `${config.apiUrl}`;
const apiRoutes = {
  header: `${baseUrl}/storefronts/themes/header`,
};

export const useGetHeader = (onSuccess) => {
  const { isFetching, isError, refetch } = useFetch<IHeaderResponse>(
    apiRoutes.header,
    undefined,
    {
      refetchOnWindowFocus: false,
      onSuccess,
    },
  );

  return {
    isFetching,
    isError,
    refetchHeader: () => void refetch(),
  };
};

export const useUpdateHeader = () => usePatch(apiRoutes.header);
