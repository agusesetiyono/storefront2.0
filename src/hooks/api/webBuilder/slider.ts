import { useState } from 'react';
import config from '@utils/config';
import { useFetch, useDelete, usePost, usePut } from '@utils/reactQuery';
import { api } from '@utils/api';
import type {
  IResponseSliders,
  ISlider,
  IResponseSlider,
} from '@interfaces/sliders';
import type { ISideMenuGroup } from '@components/WebBuilder/SideMenu/sideMenuWebBuilder.types';

const baseUrl = `${config.apiUrl}`;
const apiRoutes = {
  sliders: `${baseUrl}/sliders`,
  slidersOrders: `${baseUrl}/sliders/orders`,
};

export const useGetSliders = () => {
  const [sideMenu, setSideMenu] = useState<ISideMenuGroup[]>([{ menus: [] }]);
  const { isFetching, isError, refetch } = useFetch<IResponseSliders>(
    apiRoutes.sliders,
    undefined,
    {
      refetchOnWindowFocus: false,
      onSuccess: (res) => {
        setSideMenu([
          {
            menus: res.data.map(({ title, image, id }) => ({
              title: title || 'Slider',
              image,
              url: `${id}`,
              id,
              canDelete: true,
            })),
            canSort: true,
          },
        ]);
      },
    },
  );

  return {
    sideMenu,
    setSideMenu,
    isFetching,
    isError,
    refetchSliders: () => void refetch(),
  };
};

export const useReorderSliders = (slider_ids: (number | undefined)[]) =>
  api.patch(apiRoutes.slidersOrders, { slider_ids });

export const useDeleteSlider = () => useDelete<ISlider[]>(apiRoutes.sliders);

export const useCreateSlider = () => usePost(apiRoutes.sliders);

export const useGetSlider = (id, onSuccess, onError) =>
  useFetch<IResponseSlider>(`${apiRoutes.sliders}/${id}`, undefined, {
    refetchOnWindowFocus: false,
    onSuccess,
    onError,
  });

export const useUpdateSlider = (id) => usePut(`${apiRoutes.sliders}/${id}`);
