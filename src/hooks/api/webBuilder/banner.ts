import { useState } from 'react';
import config from '@utils/config';
import { useFetch, usePost, usePut, useDelete } from '@utils/reactQuery';
import type { IResponseBanners, IResponseBanner } from '@interfaces/banners';
import type { ISideMenuGroup } from '@components/WebBuilder/SideMenu/sideMenuWebBuilder.types';

const baseUrl = `${config.apiUrl}`;
const apiRoutes = {
  banners: `${baseUrl}/banners`,
};

export const useGetBanners = () => {
  const [sideMenu, setSideMenu] = useState<ISideMenuGroup[]>([{ menus: [] }]);
  const { isFetching, isError, refetch } = useFetch<IResponseBanners>(
    apiRoutes.banners,
    undefined,
    {
      refetchOnWindowFocus: false,
      onSuccess: (res) => {
        setSideMenu([
          {
            menus: res.data.map(({ title, image, id }) => ({
              title: title || 'Banner',
              image,
              url: `${id}`,
              id,
              canDelete: true,
            })),
            canSort: false,
          },
        ]);
      },
    },
  );

  return {
    sideMenu,
    setSideMenu,
    isFetching,
    isError,
    refetchBanners: () => void refetch(),
  };
};

export const useCreateBanner = () => usePost(apiRoutes.banners);

export const useDeleteBanner = () => useDelete(apiRoutes.banners);

export const useGetBanner = (id, onSuccess, onError) =>
  useFetch<IResponseBanner>(`${apiRoutes.banners}/${id}`, undefined, {
    refetchOnWindowFocus: false,
    onSuccess,
    onError,
  });

export const useUpdateBanner = (id) => usePut(`${apiRoutes.banners}/${id}`);
