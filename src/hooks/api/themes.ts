import { useState } from 'react';
import config from '@utils/config';
import { useFetch, usePatch } from '@utils/reactQuery';
import type {
  ISelectedThemeResponseStatus,
  IThemesResponseStatus,
  ITheme,
  IThemeData,
} from '@interfaces/themes';

const baseUrl = `${config.apiUrl}/storefronts`;
const apiRoutes = {
  themes: `${baseUrl}/themes`,
  userTheme: `${baseUrl}/themes/me`,
  settings: `${baseUrl}/themes/settings`,
  colors: `${baseUrl}/themes/colors`,
  fonts: `${baseUrl}/themes/fonts`,
};

export const useGetThemes = () => {
  const [themes, setThemes] = useState<ITheme[]>([]);
  const [themesData, setThemesData] = useState<IThemeData>({});
  const { isFetching, isError, refetch } = useFetch<IThemesResponseStatus>(
    apiRoutes.themes,
    undefined,
    {
      refetchOnWindowFocus: false,
      onSuccess: (data) => {
        const themes = data.data;
        setThemes(themes);
        setThemesData(
          themes.reduce((prev, theme) => {
            return {
              ...prev,
              [theme['id']]: theme,
            };
          }, {}),
        );
      },
    },
  );

  return {
    isFetching,
    isError,
    refetch,
    themesData,
    themes,
  };
};

export const useGetSelectedTheme = () => {
  const [selectedThemeId, setSelectedThemeId] = useState<number>(0);
  const [savedThemeId, setSavedThemeId] = useState<number>(0);

  const { isFetching, isError, refetch } =
    useFetch<ISelectedThemeResponseStatus>(apiRoutes.userTheme, undefined, {
      refetchOnWindowFocus: false,
      onSuccess: (data) => {
        setSelectedThemeId(data.data.theme_id);
        setSavedThemeId(data.data.theme_id);
      },
    });

  return {
    isFetching,
    isError,
    refetch,
    selectedThemeId,
    savedThemeId,
    setSelectedThemeId,
    setSavedThemeId,
  };
};

export const useUpdateSelectedTheme = () => usePatch(apiRoutes.themes);

export const useGetTheme = (onSuccess) => {
  const { isFetching, isError, refetch } =
    useFetch<ISelectedThemeResponseStatus>(apiRoutes.userTheme, undefined, {
      refetchOnWindowFocus: false,
      onSuccess,
    });

  return {
    isFetching,
    isError,
    refetchSettingTheme: () => void refetch(),
  };
};

export const useUpdateTheme = () => usePatch(apiRoutes.settings);

export const useUpdateColors = () => usePatch(apiRoutes.colors);

export const useUpdateFonts = () => usePatch(apiRoutes.fonts);
