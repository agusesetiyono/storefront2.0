import { IResponseUser } from '@interfaces/users';
import { useFetch } from '@utils/reactQuery';

const baseUrl = 'https://reqres.in/api';
const apiRoutes = {
  getUser: `${baseUrl}/users`,
};

export const useGetProfileById = (id: number | null) => {
  const { data } = useFetch<IResponseUser>(`${apiRoutes.getUser}/${id}`);

  return {
    user: data?.data,
  };
};
