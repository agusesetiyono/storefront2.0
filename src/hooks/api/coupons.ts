import { useState } from 'react';
import config from '@utils/config';
import { useFetch, usePost, usePatch, usePut } from '@utils/reactQuery';
import { api } from '@utils/api';
import type {
  ICouponsResponseStatus,
  ICouponResponseStatus,
  ICoupon,
} from '@interfaces/coupons';
import type { IMeta } from '@interfaces/meta';

const baseUrl = `${config.apiUrl}`;
const apiRoutes = {
  coupons: `${baseUrl}/coupons`,
};

const mapCouponList = (dataList: any[]) =>
  dataList.map((data) => ({
    ...data,
    nominal_max: data.nominal_max || 0,
    discount: data.discount || 0,
    id: data.id || '',
    start_date: data.start_date || '',
    end_date: data.end_date || '',
    target_customer_category: data.target_customer_category?.map(
      (target) => target.name,
    ),
  }));

export const useGetCoupons = ({ limit, offset }) => {
  const [coupons, setCoupons] = useState<ICoupon[]>([]);
  const [metaCoupons, setMetaCoupons] = useState<IMeta>();
  const { isFetching, isError, refetch } = useFetch<ICouponsResponseStatus>(
    apiRoutes.coupons,
    { offset, limit },
    {
      refetchOnWindowFocus: false,
      onSuccess: (data) => {
        const coupons = mapCouponList(data?.data);
        const meta = data?.meta;
        setCoupons(coupons);
        setMetaCoupons(meta);
      },
    },
  );

  return {
    isFetching,
    isError,
    coupons,
    metaCoupons,
    refetchGetCoupons: () => void refetch(),
  };
};

export const useGetCouponByCode = (code: string) =>
  api.get<ICouponsResponseStatus>(apiRoutes.coupons, { params: { code } });

export const useCreateCoupon = () => usePost(apiRoutes.coupons);

export const useGetCoupon = (id, onSuccess, onError) =>
  useFetch<ICouponResponseStatus>(`${apiRoutes.coupons}/${id}`, undefined, {
    refetchOnWindowFocus: false,
    onSuccess,
    onError,
  });

export const useUpdateCoupon = (id) => usePut(`${apiRoutes.coupons}/${id}`);

export const useUpdateStatusCoupon = (id: number) =>
  usePatch(`${apiRoutes.coupons}/${id}/status`);
