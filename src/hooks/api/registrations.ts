import config from '@utils/config';
import { usePost } from '@utils/reactQuery';

const apiRoutes = {
  register: `${config.smartSellerUrl}/storefront-editor/register-bukalapak`,
};

export const useRegisterUser = () => usePost(apiRoutes.register);
