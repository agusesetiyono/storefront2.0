import { useNavigate } from 'react-router-dom';
import { useLocalStorage } from 'usehooks-ts';

import useOnboarding from './useOnboarding';
import useSubscription from './useSubscription';

const useUpgrade = () => {
  const { refetchSubscription } = useSubscription();
  const { skipOnboarding } = useOnboarding();
  const navigate = useNavigate();

  const upgradeKey = 'upgradeStatus';
  const [upgradeStatus, setUpgradeStatus] = useLocalStorage<
    'success' | undefined
  >(upgradeKey, undefined);

  const handleSuccessUpgrade = () => {
    skipOnboarding(false);
    setUpgradeStatus('success');
    refetchSubscription();
    navigate('/settings/domain');
  };

  return {
    upgradeStatus,
    successUpgrade: handleSuccessUpgrade,
    resetUpgrade: () => setUpgradeStatus(undefined),
  };
};

export default useUpgrade;
