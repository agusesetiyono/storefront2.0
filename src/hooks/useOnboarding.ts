import { useLocalStorage } from 'usehooks-ts';

import { EOnboardingSteps } from '@interfaces/onboarding';
import toast from '@utils/toast';
import { sendOnboardingTracker } from '@utils/tracker/onboarding';

const useOnboarding = () => {
  const onboardingKey = 'onboardingStep';
  const maxStep = EOnboardingSteps.SHARE;
  const [activeStep, setActiveStep] = useLocalStorage(onboardingKey, 1);
  const isCompleted = Boolean(activeStep > maxStep);

  const handleNextOnboarding = () => {
    setActiveStep(activeStep + 1);
    if (activeStep === maxStep) {
      toast.success(`Berhasil membuat website`);
    }
    setTimeout(() => {
      const nextOnboardingSection = document.getElementById(
        `form-wizard-${activeStep + 1}`,
      );
      nextOnboardingSection?.scrollIntoView({
        behavior: 'smooth',
      });
    }, 50);
    if (activeStep !== 2) {
      void sendOnboardingTracker({
        onBoardingStep: activeStep.toString(),
        actionType: 'step_success',
      });
    }
  };

  const handleSkipOnboarding = (showToast = true) => {
    setActiveStep(maxStep + 1);
    if (showToast) {
      toast.success(`Berhasil membuat website`);
      void sendOnboardingTracker({
        onBoardingStep: activeStep.toString(),
        actionType: 'skip_onboarding',
      });
    }
  };

  return {
    activeStep,
    isCompleted,
    nextOnboarding: handleNextOnboarding,
    skipOnboarding: handleSkipOnboarding,
    setOnboardingStep: setActiveStep,
  };
};

export default useOnboarding;
