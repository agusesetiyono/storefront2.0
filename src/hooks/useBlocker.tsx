import { useContext, useEffect, ContextType } from 'react';
import { History } from 'history';
import {
  Navigator as BaseNavigator,
  UNSAFE_NavigationContext as NavigationContext,
} from 'react-router-dom';
import type { Blocker, Transition } from 'history';

interface INavigator extends BaseNavigator {
  block: History['block'];
}

type TNavigationContextWithBlock = ContextType<typeof NavigationContext> & {
  navigator: INavigator;
};

/**
 * @source https://github.com/remix-run/react-router/commit/256cad70d3fd4500b1abcfea66f3ee622fb90874
 */
const useBlocker = (blocker: Blocker, when = true) => {
  const { navigator } = useContext(
    NavigationContext,
  ) as TNavigationContextWithBlock;

  useEffect(() => {
    if (!when) {
      return;
    }

    const unblock = navigator.block((tx: Transition) => {
      const autoUnblockingTx = {
        ...tx,
        retry() {
          unblock();
          tx.retry();
        },
      };

      blocker(autoUnblockingTx);
    });

    return unblock;
  }, [navigator, blocker, when]);
};

export default useBlocker;
