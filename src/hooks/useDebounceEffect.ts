import { useEffect, useState } from 'react';

const useDebounceEffect = (action: () => void, deps: any[], delay?: number) => {
  const [firstLoad, setFirstLoad] = useState<boolean>(true);

  useEffect(() => {
    const timer = setTimeout(() => {
      !firstLoad && action();
      setFirstLoad(false);
    }, delay || 500);

    return () => {
      clearTimeout(timer);
    };
  }, [...deps, delay]);
};

export default useDebounceEffect;
