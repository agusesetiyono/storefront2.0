import { useOutletContext } from 'react-router-dom';
import { IWebBuilderContext } from '@interfaces/webBuilder';

const useWebBuilder = () => {
  return useOutletContext<IWebBuilderContext>();
};

export default useWebBuilder;
