import { useCallback } from 'react';
import useBlocker from '@hooks/useBlocker';
import type { Transition } from 'history';

const useCallAPIOnRouteLeave = (
  apiToCall: () => Promise<void>,
  when: boolean,
) => {
  const blocker = useCallback(
    (tx: Transition) => {
      void apiToCall();
      tx.retry();
    },
    [apiToCall],
  );

  return useBlocker(blocker, when);
};

export default useCallAPIOnRouteLeave;
