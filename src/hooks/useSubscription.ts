import { useContext } from 'react';
import { SubscribeContext } from '../Provider';

const useSubscription = () => {
  return useContext(SubscribeContext);
};

export default useSubscription;
