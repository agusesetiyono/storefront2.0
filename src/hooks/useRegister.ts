import { useLocalStorage } from 'usehooks-ts';

import { EOnboardingSteps } from '@interfaces/onboarding';
import { sendRegistrationTracker } from '@utils/tracker/registration';

import useOnboarding from './useOnboarding';

const useRegister = () => {
  const { setOnboardingStep } = useOnboarding();

  const registerKey = 'register_storefront';
  const [registerMessage, setRegisterMessage] = useLocalStorage<string>(
    registerKey,
    '',
  );

  const handleSuccessRegister = (targetUrl: string) => {
    setOnboardingStep(EOnboardingSteps.DOMAIN);
    setRegisterMessage('Selamat, kamu berhasil registrasi dan login.');
    sendRegistrationTracker({
      actionType: '7_login_registration_success',
    }).finally(() => {
      window.location.href = targetUrl;
    });
  };

  const handleResetRegisterInfo = () =>
    window.localStorage.removeItem(registerKey);

  return {
    registerMessage,
    successRegister: handleSuccessRegister,
    resetRegisterInfo: handleResetRegisterInfo,
  };
};

export default useRegister;
