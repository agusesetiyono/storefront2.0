import { createContext } from 'react';
import type { TPromptContextValue } from './prompt.types';

export const Context = createContext<TPromptContextValue>(() =>
  Promise.resolve(),
);
