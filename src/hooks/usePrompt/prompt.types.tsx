export type TPromptContextValue = () => Promise<void>;

export interface IPromptContextProps {
  children: JSX.Element;
  title?: string;
  message?: string;
  confirmText?: string;
  cancelText?: string;
}
