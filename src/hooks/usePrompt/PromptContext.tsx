import React from 'react';
import { useCallback, useRef } from 'react';
import { useBoolean } from 'usehooks-ts';
import { Context } from './context';
import Modal from '@components/Modal';
import Button from '@components/Button';
import type { IPromptContextProps } from './prompt.types';

export const PromptContext = ({
  children,
  title = 'Perubahan belum disimpan',
  message = 'Kamu akan keluar halaman ini tanpa menyimpan perubahan. Apa kamu yakin?',
  confirmText = 'Ya, keluar',
  cancelText = 'Batal',
}: IPromptContextProps) => {
  const cbReject = useRef<() => void>();
  const cbResolve = useRef<() => void>();
  const {
    value: open,
    setFalse: setClose,
    setTrue: setOpen,
  } = useBoolean(false);

  const showPrompt = useCallback(
    () =>
      new Promise<void>((resolve, reject) => {
        cbReject.current = reject;
        cbResolve.current = resolve;
        setOpen();
      }),
    [],
  );

  const onCancel = useCallback(() => {
    if (cbReject.current) {
      cbReject.current();
    }
    setClose();
  }, []);

  const onConfirm = useCallback(() => {
    if (cbResolve.current) {
      cbResolve.current();
    }
    setClose();
  }, []);

  return (
    <Context.Provider value={showPrompt}>
      {children}
      <Modal
        show={open}
        title={title}
        size="sm"
        variant="confirmation"
        action={[
          <Button variant="primary" onClick={onCancel}>
            {cancelText}
          </Button>,
          <Button variant="outline-primary" onClick={onConfirm}>
            {confirmText}
          </Button>,
        ]}
        onClose={close}
        className="prompt-modal"
      >
        <p className="px-6">{message}</p>
      </Modal>
    </Context.Provider>
  );
};
