import { useCallback, useContext } from 'react';
import useBlocker from '@hooks/useBlocker';
import { Context } from './context';
import type { Transition } from 'history';

export const usePrompt = (when = true) => {
  const showPrompt = useContext(Context);
  const blocker = useCallback(
    (tx: Transition) => {
      showPrompt()
        .then(() => tx.retry())
        .catch(() => {
          // catch error from canceled promise
        });
    },
    [showPrompt],
  );

  return useBlocker(blocker, when);
};
