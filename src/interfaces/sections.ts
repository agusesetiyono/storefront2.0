import { IProduct } from './products';

export interface ISections {
  id?: number;
  order: number;
  status: string;
  title: string;
  type: TSectionType;
}

export type TSectionType =
  | 'banner'
  | 'slider'
  | 'categories'
  | 'catalog'
  | 'popular'
  | 'bestseller'
  | 'custom'
  | 'copywriting';

export enum ESectionType {
  BANNER = 'banner',
  SLIDER = 'slider',
  CATEGORY = 'categories',
  PRODUCT_ALL = 'catalog',
  PRODUCT_POPULAR = 'popular',
  PRODUCT_BEST_SELLER = 'bestseller',
  PRODUCT_CUSTOM = 'custom',
  TEXT = 'copywriting',
}

export interface IResponseSections {
  data: ISections[];
}

export interface IProductList {
  title: string;
  product_list: IProduct[];
}

export interface IResponseProductList {
  data: IProductList;
}

export interface IText {
  title: string;
  initialContent?: string;
  content: string;
  status?: string;
}

export interface IResponseText {
  data: IText;
}
