export interface IMeta {
  http_status: number;
  limit: number;
  offset: number;
  total: number;
}
