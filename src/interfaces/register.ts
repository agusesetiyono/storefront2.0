export interface IRegisterInfo {
  email: string;
  successMessage?: string;
}