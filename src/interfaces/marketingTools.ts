export interface ISEO {
  description: string;
  keyword: string;
  title: string;
}

export interface ISEOResponse {
  data: ISEO;
}

export interface IMetaPixel {
  pixel_id: string;
  list_event: string[];
  catalog_token: string;
}

export interface IMetaPixelResponse {
  data: IMetaPixel;
}

export interface IGoogleAnalytics {
  tracking_id: string;
}

export interface IGoogleAnalyticsResponse {
  data: IGoogleAnalytics;
}
