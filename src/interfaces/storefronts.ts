export interface IStatus {
  owner_name: string;
  storefront_type: string;
  storefront_expired_date: string;
  domain: string;
  subdomain: string;
  active_domain: string;
  version: string;
}

export interface IResponseStatus {
  data: IStatus;
}

export interface ISubscription extends IStatus {
  isActive: boolean;
  isExpired: boolean;
}

export interface ISubscriptionContext {
  subscription: ISubscription;
  refetchSubscription: () => void;
}

export interface IGetMockSubscriptionResponseParams {
  type: string;
  expiredDate: string;
  domain?: string;
  subdomain?: string;
  activeDomain?: string;
  version?: string;
}
