export type TWebBuilderStatus = string | JSX.Element | undefined;

export type TPreviewScreen = 'mobile' | 'tablet' | 'desktop';

export interface IWebBuilderContext {
  showStatus: (status: TWebBuilderStatus) => void;
  hideStatus: () => void;
  refreshPreview: () => void;
}
