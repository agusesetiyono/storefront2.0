import { IMeta } from './meta';
export interface ICoupon {
  type: string;
  code: string;
  discount_type: string;
  discount_value?: number;
  discount_percentage?: number;
  max_discount?: number;
  paid_limit?: number;
  is_limited_quota?: boolean;
  quota?: number;
  is_limited_attempt_usage?: boolean;
  attempt_usage?: number;
  start_date?: string;
  end_date?: string;
  target_customer_category: string[];
  status: string;
  id?: number;
  nominal_max?: number;
  discount?: number;
  is_code_already_used?: boolean;
}

export interface ICouponResponseStatus {
  data: ICoupon;
}
export interface ICouponsResponseStatus {
  data: ICoupon[];
  meta: IMeta;
}
