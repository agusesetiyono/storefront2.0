export interface ISlider {
  id?: number;
  image: string;
  image_file?: File;
  title: string;
  desc: string;
  url: string;
  cta: string;
  order?: number;
}

export interface IResponseSliders {
  data: ISlider[];
}

export interface IResponseSlider {
  data: ISlider;
}
