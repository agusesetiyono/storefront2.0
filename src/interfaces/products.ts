export interface IProduct {
  id: number;
  name: string;
  image?: string;
}

export interface IResponseProducts {
  data: IProduct[];
}
