export interface ISelectedTheme {
  theme_id: number;
  color: IColorTheme;
  default_color: IColorTheme;
  font?: IFontTheme;
  default_font?: IFontTheme;
  setting?: ISettingTheme;
}

export interface ISelectedThemeResponseStatus {
  data: ISelectedTheme;
}

export interface ITheme {
  id: number;
  theme_name: string;
  images: {
    large_urls: string;
    small_urls: string;
  };
}

export interface IThemeData {
  [key: number]: ITheme;
}

export interface IThemesResponseStatus {
  data: ITheme[];
}

export interface ISuccessSaveThemeResponse {
  data: {
    message: string;
  };
}

export interface IColorTheme {
  accent_color: string;
  body_bg_color: string;
  container_bg_color: string;
  footer_bg_color: string;
  header_bg_color: string;
  main_color: string;
  navbar_bg_color: string;
  widget_chat_color: string;
}

export interface IFontTheme {
  font_category?: string;
  font_color?: string;
  font_family?: string;
}

export interface ISettingTheme {
  theme?: number;
  custom_css?: string;
  favicon?: string;
  image?: string;
  image_file?: File;
  language?: string;
}
