export type QueryKeyT = [string, object | undefined, string];

interface IResponseMeta {
  http_status: number;
}

export interface IResponseSuccess<RecordType = unknown> {
  data: RecordType;
  message: string;
  meta: IResponseMeta;
}

export interface IError {
  code: number;
  field: string;
  message: string;
}

export interface IResponseError {
  errors: IError[];
  meta: IResponseMeta;
}
