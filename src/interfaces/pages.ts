export interface IPage {
  content: string;
  id: number;
  order: number;
  slug: string;
  status: string;
  title: string;
}

export interface IPageResponseStatus {
  data: IPage;
}
export interface IPagesResponseStatus {
  data: IPage[];
}
