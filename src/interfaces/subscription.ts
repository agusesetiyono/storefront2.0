export interface ISubscription {
  owner_name: string;
  storefront_type: string; //'none' | 'free_trial' | 'addons' | 'platinum';
  storefront_expired_date: string;
}

export interface IResponseSubscription {
  data: ISubscription;
}
