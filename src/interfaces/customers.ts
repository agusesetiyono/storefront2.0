export interface ICategory {
  code: string;
  name: string;
}

export interface ICategoriesStatus {
  data: ICategory[];
}
