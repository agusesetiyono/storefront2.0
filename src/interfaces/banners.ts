export interface IBanner {
  id?: number;
  image: string;
  image_file?: File;
  title: string;
  desc: string;
  url: string;
}

export interface IResponseBanners {
  data: IBanner[];
}

export interface IResponseBanner {
  data: IBanner;
}
