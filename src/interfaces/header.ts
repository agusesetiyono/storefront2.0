export interface IHeader {
  shop_name: string;
  logo?: string;
  image?: string;
  image_file?: File;
}

export interface IHeaderResponse {
  data: IHeader;
}
