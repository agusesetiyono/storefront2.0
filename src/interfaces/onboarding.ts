export enum EOnboardingSteps {
  DOMAIN = 1,
  THEME = 2,
  PRODUCT = 3,
  SHARE = 4,
}
