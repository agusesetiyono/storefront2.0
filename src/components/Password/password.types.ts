import { ChangeEvent, FocusEvent } from 'react';

export interface IPasswordProps {
  id: string;
  placeholder: string;
  label: string;
  value: string;
  onChange: (e: ChangeEvent<HTMLInputElement>) => void;
  onBlur: (e: FocusEvent<HTMLInputElement>) => void;
  error?: string;
}
