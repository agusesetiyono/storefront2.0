import React from 'react';
import { Form } from 'react-bootstrap';
import { useBoolean } from 'usehooks-ts';

import type { IPasswordProps } from './password.types';

const Password = ({
  id,
  placeholder,
  label,
  error,
  ...props
}: IPasswordProps) => {
  const { value: show, toggle: togglePassword } = useBoolean(false);

  return (
    <>
      <Form.Label htmlFor={id} className="mb-3">
        {label}
      </Form.Label>
      <div className="relative">
        <Form.Control
          type={show ? 'text' : 'password'}
          id={id}
          name={id}
          placeholder={placeholder}
          className="pr-12 without-error-icon"
          isInvalid={!!error}
          {...props}
        />
        <Form.Control.Feedback
          type="invalid"
          className="h-5 block"
          data-testid={`password-error-message-${id}`}
        >
          {error}
        </Form.Control.Feedback>
        <i
          className={`absolute top-3 right-3 cursor-pointer opacity-50 fi fi-rr-${
            show ? 'eye-crossed' : 'eye'
          }`}
          onClick={togglePassword}
        ></i>
      </div>
    </>
  );
};

export default Password;
