export interface IErrorStateProps {
  title?: string;
  content?: string;
  image?: string;
  isLoading?: boolean;
  onRetry?: () => void;
}
