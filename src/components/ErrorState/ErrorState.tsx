import React from 'react';
import { Image } from 'react-bootstrap';
import ErrorIllustration from '@assets/image/error-illustration.svg';

import Button from '@components/Button';
import type { IErrorStateProps } from './errorState.types';

const ErrorState = ({
  title = 'Gagal mengambil data',
  content = 'Terjadi suatu kesalahan, mohon hubungi customer service kami!',
  image = ErrorIllustration,
  isLoading = false,
  onRetry,
}: IErrorStateProps) => (
  <div className="flex flex-col text-center items-center py-8">
    <Image
      src={image}
      className="mb-4 mx-auto img-responsive w-[320px]"
      alt="error-illus"
    />
    <div className="max-w-[338px] opacity-60">
      <p className="text-base font-bold mb-4">{title}</p>
      <p>{content}</p>
    </div>
    {onRetry && (
      <Button onClick={onRetry} className="mt-8" isLoading={isLoading}>
        Coba Lagi
      </Button>
    )}
  </div>
);

export default ErrorState;
