export interface IEmptyStateProps {
  title?: string;
  content?: JSX.Element;
  image?: string;
}
