import React from 'react';
import { Image } from 'react-bootstrap';
import EmptyIllustration from '@assets/image/empty-illustration.svg';
import type { IEmptyStateProps } from './emptyState.types';

const EmptyState = ({
  title = 'Data tidak ditemukan',
  content = <p>Ayo buat data pertama kamu!</p>,
  image = EmptyIllustration,
}: IEmptyStateProps) => (
  <div className="flex flex-col text-center items-center py-8">
    <Image
      src={image}
      className="mb-4 mx-auto img-responsive w-[150px]"
      alt="empty-illus"
    />
    <div className="max-w-[338px] opacity-60">
      <p className="text-base font-bold mb-4">{title}</p>
      {content}
    </div>
  </div>
);

export default EmptyState;
