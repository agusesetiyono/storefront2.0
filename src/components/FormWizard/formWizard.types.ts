import { FC } from 'react';

export interface IFormWizard {
  steps: IFormWizardStep[];
  activeStep: number;
  onNext: () => void;
  onSkip: () => void;
}

export interface IFormWizardStep {
  title: string;
  form: FC<IStepProps>;
}

export interface IStepProps {
  step: number;
  active: boolean;
  completed: boolean;
  onNext: () => void;
}
