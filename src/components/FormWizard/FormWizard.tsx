import React, { FC } from 'react';
import { Button, Stack } from 'react-bootstrap';

import type { IFormWizard, IStepProps } from './formWizard.types';

const FormWizard = ({ steps, activeStep, onNext, onSkip }: IFormWizard) => {
  const isActive = (step: number) => step === activeStep;
  const isCompleted = (step: number) => step < activeStep;

  return (
    <>
      <Stack direction="horizontal" gap={2} className="justify-around">
        {steps.map(({ title }, index) => {
          const stepIndex = index + 1;
          const active = isActive(stepIndex);
          const completed = isCompleted(stepIndex);

          return (
            <Stack
              key={`form-wizard-title-${stepIndex}`}
              className={`wizard-step items-center text-center w-8 ${
                completed ? 'completed' : ''
              }`}
            >
              <span
                className={`${
                  active || completed
                    ? 'bg-blue-600 text-white completed'
                    : 'text-slate-400'
                } rounded-full w-8 h-8 leading-8 text-center text-sm font-semibold`}
              >
                {stepIndex}
              </span>
              <label
                className={`text-base text-break pt-1 ${
                  active || completed
                    ? 'font-semibold text-blue-600'
                    : 'font-medium text-black'
                }`}
              >
                {title}
              </label>
            </Stack>
          );
        })}
      </Stack>
      <Stack className="gap-11">
        {steps.map((step, index: number) => {
          const stepIndex = index + 1;
          const Step = step.form as FC<IStepProps>;
          const active = isActive(stepIndex);
          const completed = isCompleted(stepIndex);
          let style = 'form-wizard px-6 py-5 rounded-xl';
          if (active) {
            style = `${style} border border-blue-600`;
          } else if (completed) {
            style = `${style} border border-green-600 opacity-70`;
          } else {
            style = `${style} opacity-30`;
          }

          return (
            <div
              key={`form-wizard-${stepIndex}`}
              id={`form-wizard-${stepIndex}`}
              data-testid={`form-wizard-${stepIndex}`}
              className={style}
            >
              <Step
                step={stepIndex}
                active={active}
                completed={completed}
                onNext={onNext}
              />
            </div>
          );
        })}
      </Stack>
      <Button
        onClick={onSkip}
        variant="light"
        className="w-16 h-8 self-center text-xs text-blue-600 font-medium"
      >
        Lewati
      </Button>
    </>
  );
};

export default FormWizard;
