export interface IUploader {
  accept?: string;
  aspectRatio?: string;
  maxFiles?: number;
  description?: string;
  initialPreview?: string;
  modalDeleteTitle?: string;
  modalDeleteContent?: string;
  isSquare?: boolean;
  onChange?: (files: File[]) => Promise<void> | void;
}

export interface IFile extends File {
  preview: string;
}
