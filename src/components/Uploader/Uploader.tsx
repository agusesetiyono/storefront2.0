import React, { useState } from 'react';
import Dropzone from 'react-dropzone';
import { Button, Image } from 'react-bootstrap';
import { CloudUpload } from 'react-bootstrap-icons';
import useDeleteItem from '@hooks/useDeleteItem';

import type { IFile, IUploader } from './uploader.type';

const Uploader = ({
  accept,
  aspectRatio,
  maxFiles = 1,
  description,
  initialPreview,
  modalDeleteTitle = 'Hapus',
  modalDeleteContent = 'Apakah kamu yakin ingin menghapus?',
  onChange,
  isSquare,
}: IUploader) => {
  const [files, setFiles] = useState<IFile[]>([]);

  const handleDrop = (acceptedFiles: File[]) => {
    setFiles(
      acceptedFiles.map((file) =>
        Object.assign(file, {
          preview: URL.createObjectURL(file),
        }),
      ),
    );
    onChange && onChange(acceptedFiles);
  };

  const handleRemove = () => {
    setFiles([]);
    onChange && onChange([]);
  };

  const { onDelete, ModalDeleteComponent } = useDeleteItem({
    modalTitle: modalDeleteTitle,
    modalContent: modalDeleteContent,
    confirmDeleteAction: handleRemove,
  });

  const isNotEmpty = files.length || initialPreview;
  const dropZoneClass = isNotEmpty
    ? 'p-0 border-none'
    : `uploader-container${isSquare ? ' uploader-container--square' : ''}`;

  return (
    <>
      <div
        className={`flex items-center ${
          isSquare ? 'justify-center' : 'w-full'
        }`}
      >
        <Dropzone
          accept={accept}
          onDrop={handleDrop}
          maxFiles={maxFiles}
          multiple={!!(maxFiles && maxFiles > 1)}
        >
          {({ getRootProps, getInputProps }) => (
            <section className={`mr-2${isSquare ? '' : ' flex-1'}`}>
              <div {...getRootProps()} className={dropZoneClass}>
                <input {...getInputProps()} data-testid="uploader-input" />

                {initialPreview ? (
                  <Image
                    src={initialPreview}
                    thumbnail
                    className={`p-0${isSquare ? ' thumbnail-square' : ''}`}
                  />
                ) : files.length > 0 ? (
                  <>
                    {files.map((file, index) => (
                      <Image
                        src={file.preview}
                        onLoad={() => {
                          URL.revokeObjectURL(file.preview);
                        }}
                        thumbnail
                        key={`file-uploader-${index}`}
                        className="p-0"
                      />
                    ))}
                  </>
                ) : (
                  <>
                    <CloudUpload />
                    <small>
                      Drag or <a>browse</a> files here
                    </small>
                    {!!aspectRatio && (
                      <small>*Aspect Ratio {aspectRatio}</small>
                    )}
                  </>
                )}
              </div>
            </section>
          )}
        </Dropzone>
        <Button
          size="sm"
          variant="outline-primary"
          className="text-gray-700 w-8 h-8 border-none"
          onClick={() => isNotEmpty && onDelete()}
          data-testid="uploader-remove-button"
        >
          <i className="fi-sr-trash text-gray-400 text-lg hover:color[#ED7881]"></i>
        </Button>
      </div>
      {description && (
        <p className="text-xs text-center text-gray-4 mt-2 leading-5">
          {description}
        </p>
      )}
      <ModalDeleteComponent />
    </>
  );
};

export default Uploader;
