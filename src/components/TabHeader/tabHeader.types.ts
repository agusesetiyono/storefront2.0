import { ReactNode } from 'react';

export interface ITabHeader {
  title: string;
  subtitle?: string;
  subtitleClass?: string;
  href?: string;
  onLinkClick?: () => void;
  action?: ReactNode[];
  withBackButton?: boolean;
  className?: string;
}
