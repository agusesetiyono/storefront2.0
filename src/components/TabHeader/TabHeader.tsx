import React, { Fragment } from 'react';
import { Button, Stack } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import type { ITabHeader } from './tabHeader.types';

const TabHeader = ({
  title,
  subtitle,
  subtitleClass = '',
  href,
  onLinkClick,
  action,
  withBackButton,
  className = '',
}: ITabHeader) => {
  const navigate = useNavigate();

  return (
    <Stack
      direction="horizontal"
      className={`justify-between mb-8 ${className}`}
    >
      <Stack gap={2}>
        <h3 className="text-2xl font-bold">
          {withBackButton && (
            <>
              <Button
                size="sm"
                variant="outline-primary"
                className="border border-gray-400 text-gray-700 w-8 h-8 mr-3 rounded"
                onClick={() => navigate(-1)}
              >
                <i className="fi-sr-angle-small-left"></i>
              </Button>{' '}
            </>
          )}
          {title}
        </h3>
        {subtitle && (
          <p className={`text-base max-w-[320px] ${subtitleClass}`}>
            <span className="opacity-60 mr-1">{subtitle}</span>
            {href && (
              <>
                <a
                  href={href}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="underline"
                  onClick={() => onLinkClick && onLinkClick()}
                >
                  link ini
                </a>
                .
              </>
            )}
          </p>
        )}
      </Stack>
      {action && (
        <Stack direction="horizontal" className="self-end" gap={1}>
          {action.map((button, index) => {
            return <Fragment key={`action-${index}`}>{button}</Fragment>;
          })}
        </Stack>
      )}
    </Stack>
  );
};

export default TabHeader;
