import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import { XLg } from 'react-bootstrap-icons';
import BlurryLoadingImage from '@components/BlurryLoadingImage';
import type { IPropsPreview } from './theme.types';

const Preview = ({
  smallImageUrl,
  largeImageUrl,
  show,
  closePreview,
}: IPropsPreview) => {
  return (
    <>
      <Modal
        show={show}
        onHide={closePreview}
        className="preview-theme-modal"
        data-testid="preview-theme-modal"
      >
        <Modal.Body className="p-0">
          <BlurryLoadingImage
            smallImage={smallImageUrl}
            largeImage={largeImageUrl}
            alt="theme-preview"
          />
        </Modal.Body>
      </Modal>
      {show && (
        <div
          className="fixed display-flex cursor-pointer preview-theme-close-button"
          onClick={closePreview}
          data-testid="preview-theme-modal-close-button"
        >
          <span className="mr-4 text-white preview-theme-close-text">
            Close
          </span>
          <Button>
            <XLg size={24} />
          </Button>
        </div>
      )}
    </>
  );
};

export default Preview;
