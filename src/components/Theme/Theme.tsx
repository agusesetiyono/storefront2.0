import React, { useEffect } from 'react';
import { useBoolean } from 'usehooks-ts';
import { Stack } from 'react-bootstrap';
import Preview from './Preview';
import ErrorState from '@components/ErrorState';
import Button from '@components/Button';
import LoadingState from '@components/LoadingState';
import {
  useGetSelectedTheme,
  useGetThemes,
  useUpdateSelectedTheme,
} from '@hooks/api/themes';
import { sendDashboardTracker } from '@utils/tracker/dashboard';
import { sendOnboardingTracker } from '@utils/tracker/onboarding';
import { getErrorMessage } from '@utils/error';

import type { ISuccessSaveThemeResponse } from '@interfaces/themes';
import type { IPropsThemeSelector } from './theme.types';

const Theme = ({
  buttonPosition = 'bottom-5 left-7 right-7',
  thumbnailGap = 'gap-4',
  thumbnailStyle = 'p-2 mt-9',
  saveOnThumbnail = false,
  onThemeChange,
  onThemeSave,
}: IPropsThemeSelector) => {
  const {
    isFetching: isFetchingSelectedTheme,
    isError: isErrorSelectedTheme,
    savedThemeId,
    selectedThemeId,
    setSavedThemeId,
    setSelectedThemeId,
    refetch: refetchGetSelectedTheme,
  } = useGetSelectedTheme();
  const {
    isFetching: isFetchingThemes,
    isError: isErrorThemes,
    themesData,
    themes,
    refetch: refetchGetThemes,
  } = useGetThemes();
  const updateSelectedTheme = useUpdateSelectedTheme();
  const isFetching = isFetchingSelectedTheme || isFetchingThemes;
  const isError = isErrorSelectedTheme || isErrorThemes;
  const inCustomWebsiteTab = window.location.href.includes(
    '/storefront-editor/settings/custom-website',
  );

  const saveTheme = () => {
    if (!updateSelectedTheme.isLoading && selectedThemeId) {
      updateSelectedTheme.mutate(
        { theme_id: selectedThemeId },
        {
          onSuccess: (data: ISuccessSaveThemeResponse) => {
            setSavedThemeId(selectedThemeId);
            onThemeSave(data.data.message);
            if (inCustomWebsiteTab) {
              void sendDashboardTracker({ actionType: 'save_changes' });
            } else {
              void sendOnboardingTracker({
                onBoardingStep: '2',
                actionType: 'step_success',
              });
            }
          },
          onError: (error) => {
            !inCustomWebsiteTab &&
              void sendOnboardingTracker({
                onBoardingStep: '2',
                actionType: 'step_error',
                actionDetails: getErrorMessage(error),
              });
          },
        },
      );
    }
  };

  const onRetry = () => {
    isErrorSelectedTheme && refetchGetSelectedTheme();
    isErrorThemes && refetchGetThemes();
  };

  useEffect(() => {
    onThemeChange(selectedThemeId, themesData, savedThemeId);
  }, [savedThemeId, themesData, selectedThemeId]);

  const {
    value: isShowPreview,
    setTrue: showPreview,
    setFalse: closePreview,
  } = useBoolean(false);

  if (isFetching) return <LoadingState />;

  if (isError)
    return (
      <ErrorState
        content="Gagal menunjukkan list tema. Refresh untuk mencoba lagi"
        onRetry={onRetry}
      />
    );

  return (
    <>
      <div className={`grid grid-cols-2 md:grid-cols-4 ${thumbnailGap}`}>
        {themes.map(({ id, theme_name, images }) => (
          <div key={`theme-${id}`}>
            <div
              className={`${thumbnailStyle} rounded-xl cursor-pointer relative border transition-all duration-500 ${
                selectedThemeId === id
                  ? 'border-blue-600 bg-blue-50'
                  : 'border-gray-100 bg-gray-100'
              }`}
              data-testid={`theme-${theme_name}`}
              onClick={() => {
                if (updateSelectedTheme.isLoading) return;

                setSelectedThemeId(id);
                if (inCustomWebsiteTab) {
                  void sendDashboardTracker({
                    actionType: 'click_theme',
                    actionDetails: theme_name,
                  });
                } else {
                  void sendOnboardingTracker({
                    onBoardingStep: '2',
                    actionType: 'click_theme',
                    actionDetails: theme_name,
                  });
                }
              }}
            >
              <span
                style={{ backgroundImage: `url(${images.small_urls})` }}
                role="img"
                aria-label={theme_name}
                className="rounded-xl w-full theme-thumbnail"
                data-testid="theme-thumbnail"
              />
              {selectedThemeId === id && (
                <div className={`absolute ${buttonPosition}`}>
                  {saveOnThumbnail && savedThemeId !== selectedThemeId && (
                    <Button
                      className="font-light py-2 text-base block w-full mb-2"
                      size="sm"
                      onClick={(e) => {
                        saveTheme();
                        e.stopPropagation();
                      }}
                      isLoading={updateSelectedTheme.isLoading}
                    >
                      Simpan
                    </Button>
                  )}
                  <Button
                    className="font-light bg-white py-2 text-base block w-full"
                    variant="outline-primary"
                    size="sm"
                    onClick={(e) => {
                      showPreview();
                      e.stopPropagation();
                      if (inCustomWebsiteTab) {
                        void sendDashboardTracker({
                          actionType: 'preview_theme',
                          actionDetails: theme_name,
                        });
                      } else {
                        void sendOnboardingTracker({
                          onBoardingStep: '2',
                          actionType: 'preview_theme',
                          actionDetails: theme_name,
                        });
                      }
                    }}
                  >
                    Preview
                  </Button>
                </div>
              )}
            </div>
            {saveOnThumbnail && (
              <p
                className={`text-base leading-8 mt-2 capitalize${
                  savedThemeId === id ? ' text-blue-600 font-bold' : ''
                }`}
              >
                {theme_name}
              </p>
            )}
          </div>
        ))}
      </div>
      <Preview
        smallImageUrl={themesData[selectedThemeId]?.images?.small_urls}
        largeImageUrl={themesData[selectedThemeId]?.images?.large_urls}
        show={isShowPreview}
        closePreview={closePreview}
      />
      {!saveOnThumbnail && themes.length && (
        <Stack className="mt-9">
          <Button
            className="w-44 self-end"
            data-testid="save-theme-button"
            onClick={saveTheme}
            isLoading={updateSelectedTheme.isLoading}
            spinnerClass="my-1"
          >
            Pilih
          </Button>
        </Stack>
      )}
    </>
  );
};

export default Theme;
