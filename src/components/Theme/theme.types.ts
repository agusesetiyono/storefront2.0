import type { IThemeData } from '@interfaces/themes';
export interface IPropsThemeSelector {
  buttonPosition?: string;
  thumbnailGap?: string;
  thumbnailStyle?: string;
  saveOnThumbnail?: boolean;
  onThemeChange: (
    selectedThemeId: number,
    themesData: IThemeData,
    savedThemeId?: number,
  ) => void;
  onThemeSave: (message?: string) => void;
}

export interface IPropsPreview {
  smallImageUrl: string;
  largeImageUrl: string;
  show: boolean;
  closePreview: () => void;
}
