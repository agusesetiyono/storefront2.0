import { ReactNode } from 'react';

export type TModalVariant = 'normal' | 'simple' | 'confirmation';

export type TModalSize = 'sm' | 'lg' | 'xl';

export interface IModal {
  show: boolean;
  title?: string;
  variant?: TModalVariant;
  backdrop?: 'static' | true | false;
  size?: TModalSize;
  className?: string;
  children: JSX.Element;
  action?: ReactNode[];
  onClose?: () => void;
}
