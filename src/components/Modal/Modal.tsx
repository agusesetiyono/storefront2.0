import React, { Fragment } from 'react';
import { Modal as BsModal } from 'react-bootstrap';

import type { IModal } from './modal.types';

const Modal = ({
  title,
  variant = 'simple',
  children,
  action,
  className,
  backdrop,
  onClose,
  ...otherProps
}: IModal) => {
  const isConfirmation = variant === 'confirmation';

  return (
    <BsModal
      onHide={onClose}
      className={`modal-${variant} ${className}`}
      backdrop={isConfirmation ? 'static' : backdrop}
      {...otherProps}
    >
      {title && (
        <BsModal.Header closeButton={!isConfirmation}>
          <BsModal.Title>{title}</BsModal.Title>
        </BsModal.Header>
      )}

      <BsModal.Body
        className={isConfirmation ? 'text-center py-12 px-[34px]' : 'text-left'}
      >
        {children}
      </BsModal.Body>

      {action && (
        <BsModal.Footer
          className={
            isConfirmation ? 'flex justify-center p-0 pt-6' : 'text-right'
          }
        >
          {action.map((button, index) => {
            return <Fragment key={`action-${index}`}>{button}</Fragment>;
          })}
        </BsModal.Footer>
      )}
    </BsModal>
  );
};

export default Modal;
