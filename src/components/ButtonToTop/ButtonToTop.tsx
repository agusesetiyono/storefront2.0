import React, { useEffect } from 'react';
import { useBoolean } from 'usehooks-ts';
import { Button } from 'react-bootstrap';

const ButtonToTop = () => {
  const { value: visible, setFalse: hide, setTrue: show } = useBoolean(false);

  useEffect(() => {
    window.addEventListener('scroll', toggleVisible);

    return () => {
      window.removeEventListener('scroll', toggleVisible);
    };
  }, []);

  const toggleVisible = () => {
    const scrolled = document.documentElement.scrollTop;
    if (scrolled > 300) {
      show();
    } else if (scrolled <= 300) {
      hide();
    }
  };

  const scrollToTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  return (
    <Button
      className={`btn-to-top ${
        visible ? 'opacity-100 visible' : 'opacity-0 invisible'
      }`}
      onClick={scrollToTop}
    >
      <i className="fi-sr-angle-up"></i>
    </Button>
  );
};

export default ButtonToTop;
