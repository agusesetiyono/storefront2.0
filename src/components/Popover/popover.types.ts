import { ReactNode } from 'react';

export type TPopoverPlacement =
  | 'auto-start'
  | 'auto'
  | 'auto-end'
  | 'top-start'
  | 'top'
  | 'top-end'
  | 'right-start'
  | 'right'
  | 'right-end'
  | 'bottom-end'
  | 'bottom'
  | 'bottom-start'
  | 'left-end'
  | 'left'
  | 'left-start';

export type TPopoverTrigger = 'hover' | 'click' | 'focus';

export interface IPopover {
  title?: string;
  placement?: TPopoverPlacement;
  trigger?: TPopoverTrigger | TPopoverTrigger[];
  content: ReactNode;
  display?: boolean;
  children: JSX.Element;
  className?: string;
  rootClose?: boolean;
}
