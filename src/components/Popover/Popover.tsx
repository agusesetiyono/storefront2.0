import React from 'react';
import { OverlayTrigger, Popover as BsPopover } from 'react-bootstrap';

import type { IPopover } from './popover.types';

const Popover = ({
  title,
  placement = 'right',
  trigger = 'click',
  content,
  display = true,
  children,
  className = '',
  rootClose = false,
}: IPopover) => {
  return display ? (
    <OverlayTrigger
      trigger={trigger}
      placement={placement}
      overlay={
        <BsPopover id="popover-overlay" className={className}>
          {title && <BsPopover.Header as="h3">{title}</BsPopover.Header>}
          <BsPopover.Body>{content}</BsPopover.Body>
        </BsPopover>
      }
      rootClose={rootClose}
    >
      {children}
    </OverlayTrigger>
  ) : (
    children
  );
};

export default Popover;
