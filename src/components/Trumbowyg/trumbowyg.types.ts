interface ISemantic {
  [key: string]: string;
}

export interface ITrumbowygProps {
  buttons?: string[][];
  data: string;
  disabled?: boolean;
  id: string;
  name: string;
  placeholder?: string;
  removeformatPasted?: boolean;
  semantic?: ISemantic;
  svgPath?: string;
  tagsToRemove?: string[];
  onBlur?: (any) => void;
  onChange?: (any) => void;
  onClose?: (any) => void;
  onCloseFullScreen?: (any) => void;
  onFocus?: (any) => void;
  onInit?: (any) => void;
  onOpenFullScreen?: (any) => void;
  onPaste?: (any) => void;
  onResize?: (any) => void;
}
