import React, { Component } from 'react';
import 'trumbowyg';
import 'trumbowyg/dist/plugins/noembed/trumbowyg.noembed';
import { ITrumbowygProps } from './trumbowyg.types';

class Trumbowyg extends Component<ITrumbowygProps> {
  constructor(props: ITrumbowygProps) {
    super(props);
  }

  componentDidMount() {
    const {
      buttons = [
        [
          'viewHTML',
          'bold',
          'italic',
          'underline',
          'unorderedList',
          'orderedList',
        ],
        ['link', 'insertImage', 'noembed'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
      ],
      data,
      disabled = false,
      id,
      removeformatPasted = true,
      semantic = {
        div: 'div',
      },
      svgPath = '/assets/img/icons.svg',
      tagsToRemove = ['script', 'link'],
      onBlur,
      onChange,
      onClose,
      onCloseFullScreen,
      onFocus,
      onInit,
      onOpenFullScreen,
      onPaste,
      onResize,
    } = this.props;

    const trumbowygInstance = global.$(`#${id}`).trumbowyg({
      btns: buttons,
      semantic,
      removeformatPasted,
      svgPath,
      tagsToRemove,
    });

    if (onFocus) {
      trumbowygInstance.on('tbwfocus', onFocus);
    }

    if (onBlur) {
      trumbowygInstance.on('tbwblur', onBlur);
    }

    if (onInit) {
      trumbowygInstance.on('tbwinit', onInit);
    }

    if (onChange) {
      trumbowygInstance.on('tbwchange', onChange);
    }

    if (onResize) {
      trumbowygInstance.on('tbwresize', onResize);
    }

    if (onPaste) {
      trumbowygInstance.on('tbwpaste', onPaste);
    }

    if (onOpenFullScreen) {
      trumbowygInstance.on('tbwopenfullscreen', onOpenFullScreen);
    }

    if (onCloseFullScreen) {
      trumbowygInstance.on('tbwclosefullscreen', onCloseFullScreen);
    }

    if (onClose) {
      trumbowygInstance.on('tbwclose', onClose);
    }

    global.$(`#${id}`).trumbowyg('html', data);
    global.$(`#${id}`).trumbowyg(disabled === true ? 'disable' : 'enable');
  }

  shouldComponentUpdate(nextProps) {
    return (
      nextProps.data !== this.props.data ||
      nextProps.disabled !== this.props.disabled
    );
  }

  componentDidUpdate() {
    global.$(`#${this.props.id}`).trumbowyg('html', this.props.data);
    global
      .$(`#${this.props.id}`)
      .trumbowyg(this.props.disabled === true ? 'disable' : 'enable');
  }

  componentWillUnmount() {
    global.$(`#${this.props.id}`).trumbowyg('destroy');
  }

  render() {
    return (
      <textarea
        id={`${this.props.id}`}
        placeholder={this.props.placeholder}
        name={this.props.name}
      ></textarea>
    );
  }
}

export default Trumbowyg;
