import React from 'react';
import { render } from '@testing-library/react';
import jQuery from 'jquery';
global.$ = jQuery;
global.jQuery = jQuery;
document.execCommand = jest.fn();

describe('Trumbowyg', () => {
  it('Should match snapshot', async () => {
    const eventHandler = jest.fn();
    const module = await import('@components/Trumbowyg');
    const { container } = render(
      <module.default
        id="trumbowyg-editor"
        data=""
        name="trumbowyg"
        onFocus={eventHandler}
        onBlur={eventHandler}
        onInit={eventHandler}
        onChange={eventHandler}
        onResize={eventHandler}
        onPaste={eventHandler}
        onOpenFullScreen={eventHandler}
        onCloseFullScreen={eventHandler}
        onClose={eventHandler}
      />,
    );
    expect(container).toMatchSnapshot();
  });
});
