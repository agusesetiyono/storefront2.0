export interface IFormLabel {
  htmlFor?: string;
  label: string;
  description?: string;
  descriptionClass?: string;
  href?: string;
  onLinkClick?: () => void;
  className?: string;
}
