import React from 'react';
import type { IFormLabel } from './formLabel.types';

const FormLabel = ({
  htmlFor = '',
  label,
  description,
  descriptionClass = '',
  href,
  onLinkClick,
  className,
}: IFormLabel) => (
  <div className={className}>
    <label className="mb-0 block" {...(htmlFor && { htmlFor })}>
      {label}
    </label>
    {description && (
      <p className={`max-w-[465px] mb-4 ${descriptionClass}`}>
        <span className="opacity-60 mr-1">{description}</span>
        {href && (
          <>
            <a
              href={href}
              target="_blank"
              rel="noopener noreferrer"
              className="underline"
              onClick={() => onLinkClick && onLinkClick()}
            >
              link ini
            </a>
            .
          </>
        )}
      </p>
    )}
  </div>
);

export default FormLabel;
