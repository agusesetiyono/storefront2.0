import React, { useState, useMemo, useEffect } from 'react';
import { InputGroup, Form } from 'react-bootstrap';
import { RgbaStringColorPicker } from 'react-colorful';
import { colord, getFormat, extend } from 'colord';
import namesPlugin from 'colord/plugins/names';

import Button from '@components/Button';
import Popover from '@components/Popover';
import type { ISelectColorProps } from './selectColor.types';

extend([namesPlugin]);

const SelectColor = ({
  name,
  value,
  defaultColor,
  onColorChange,
  ...others
}: ISelectColorProps) => {
  const [color, setColor] = useState(value);

  const rgbaString = useMemo(() => colord(color).toRgbString(), [color]);

  useEffect(() => {
    onColorChange && onColorChange(color);
  }, [color]);

  const onColorPickerChange = (val: string) => {
    const isWithOpacity = val.includes('.');
    setColor(isWithOpacity ? val : colord(val).toHex());
  };

  const onBlur = (e) => {
    if (!defaultColor) return;
    const value: string = e.target.value;
    setColor(!getFormat(value) ? defaultColor : value);
  };

  return (
    <>
      <div className="flex gap-1" {...others}>
        <InputGroup>
          <Form.Control
            id={name}
            name={name}
            size="sm"
            value={color}
            onChange={(e) => setColor(e.target.value)}
            onBlur={onBlur}
          />
          <Popover
            rootClose
            content={
              <RgbaStringColorPicker
                color={rgbaString}
                onChange={onColorPickerChange}
              />
            }
          >
            <div className="form-control form-control-sm px-2 select-color__preview">
              <div
                className="select-color__preview-color"
                style={{ backgroundColor: color }}
              ></div>
            </div>
          </Popover>
        </InputGroup>
        {defaultColor && (
          <Button
            size="sm"
            variant="light"
            className="px-2"
            onClick={() => setColor(defaultColor)}
          >
            Default
          </Button>
        )}
      </div>
    </>
  );
};

export default SelectColor;
