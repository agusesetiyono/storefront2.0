export interface ISelectColorProps {
  name?: string;
  value: string;
  defaultColor?: string;
  onColorChange?: (color: string) => void;
}
