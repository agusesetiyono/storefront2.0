export interface IBlurryLoadingImageProps {
  smallImage: string;
  largeImage: string;
  alt: string;
}
