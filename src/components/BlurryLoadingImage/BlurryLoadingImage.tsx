import React, { useEffect, useState } from 'react';
import { useBoolean } from 'usehooks-ts';
import type { IBlurryLoadingImageProps } from './blurryLoadingImage.types';

const BlurryLoadingImage = ({
  smallImage,
  largeImage,
  alt,
}: IBlurryLoadingImageProps) => {
  const [shownImage, setShownImage] = useState(smallImage);
  const { value: isLoading, setFalse: finishLoading } = useBoolean(true);

  const fetchImage = (src: string) => {
    const loadingImage = new Image();
    loadingImage.src = src;
    loadingImage.onload = () => {
      setShownImage(loadingImage.src);
      finishLoading();
    };
  };

  useEffect(() => {
    fetchImage(largeImage);
  }, []);

  return (
    <div className="overflow-hidden">
      <img
        className={`blurry-loading-image${isLoading ? ' loading' : ''}`}
        src={shownImage}
        alt={alt}
      />
    </div>
  );
};

export default BlurryLoadingImage;
