export type DefaultRecordType = Record<string, any>;

export type TTableColumnAlign = 'left' | 'center' | 'right' | 'justify';
export type TTableSize = 'sm' | 'lg';
export interface ITableColumn<RecordType = unknown> {
  key: string;
  dataIndex: string;
  title: string;
  align?: TTableColumnAlign;
  width?: number;
  render?: (value: any, record: RecordType, index: number) => void;
}

export interface ITableRow<RecordType = unknown> {
  record: RecordType;
  rowIndex: number;
}

export interface IMeta {
  current?: number;
  pageSize?: number;
  total?: number;
}

export interface ITable<RecordType = unknown> {
  data: RecordType[];
  columns: ITableColumn<RecordType>[];
  hideHeader?: boolean;
  sortable?: boolean;
  size?: TTableSize;
  responsive?: boolean;
  striped?: boolean;
  bordered?: boolean;
  borderless?: boolean;
  isLoading?: boolean;
  isErrorFetch?: boolean;
  isPagination?: boolean;
  isFixed?: boolean;
  meta?: IMeta;
  className?: string;
  emptyState?: JSX.Element;
  errorState?: JSX.Element;
  handleSort?: (record: RecordType[]) => void;
  handleRetry?: () => void;
  handleChangePagination?: (any) => void;
}
