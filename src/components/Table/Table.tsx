import React from 'react';
import { Table as BsTable } from 'react-bootstrap';
import { ArrowsMove } from 'react-bootstrap-icons';
import {
  arrayMove,
  SortableContainer,
  SortableElement,
  SortableHandle,
  SortEnd,
} from 'react-sortable-hoc';

import Pagination from 'rc-pagination';
import localeInfo from '../../../node_modules/rc-pagination/lib/locale/id_ID';

import EmptyState from '@components/EmptyState';
import ErrorState from '@components/ErrorState';
import LoadingState from '@components/LoadingState';

import type {
  DefaultRecordType,
  ITable,
  ITableColumn,
  ITableRow,
} from './table.types';

const Table = <RecordType extends DefaultRecordType>({
  data,
  columns,
  hideHeader,
  sortable,
  size,
  isLoading = false,
  isErrorFetch,
  isPagination,
  isFixed,
  meta,
  className,
  emptyState = <EmptyState />,
  errorState,
  handleChangePagination,
  handleSort,
  handleRetry,
  ...props
}: ITable<RecordType>) => {
  const DragHandle = SortableHandle<{ rowIndex: number }>(
    ({ rowIndex }: { rowIndex: number }) => (
      <ArrowsMove
        className="table-dragger"
        data-testid={`drag-handle-${rowIndex + 1}`}
      />
    ),
  );

  const TableHeader = () => (
    <thead>
      <tr>
        {columns.map((column: ITableColumn, index: number) => (
          <th
            key={`header-column-${column.key}-${index}`}
            className={`text-${column.align || 'left'}`}
            style={{ width: column.width }}
          >
            {column.title}
          </th>
        ))}
      </tr>
    </thead>
  );

  const TableBody = SortableContainer(() => {
    if (isLoading) {
      return (
        <EmptyRow>
          <LoadingState className="py-32" />
        </EmptyRow>
      );
    } else if (isErrorFetch) {
      return (
        <EmptyRow>
          {errorState || <ErrorState onRetry={handleRetry} />}
        </EmptyRow>
      );
    } else if (data.length <= 0) {
      return <EmptyRow>{emptyState}</EmptyRow>;
    }

    return (
      <tbody>
        {data.map((record: RecordType, rowIndex: number) => (
          <TableRow
            key={`row-${rowIndex}`}
            record={record}
            index={rowIndex}
            rowIndex={rowIndex}
          />
        ))}
      </tbody>
    );
  });

  const EmptyRow = ({ children }) => (
    <tbody>
      <tr>
        <td colSpan={columns.length} className="py-10">
          {children}
        </td>
      </tr>
    </tbody>
  );

  const TableRow = SortableElement<ITableRow<RecordType>>(
    ({ record, rowIndex }: ITableRow<RecordType>) => (
      <tr style={{ verticalAlign: 'middle' }}>
        {columns.map((column: ITableColumn, columnIndex: number) => {
          const rowValue = record[column.dataIndex];
          const columnValue = column.render
            ? column.render(rowValue, record, rowIndex)
            : rowValue;
          const showDragger = columnIndex === 0 && sortable;

          return (
            <td
              key={`row-column-${column.key}-${columnIndex}`}
              className={`text-${column.align || 'left'}`}
              style={{ width: column.width }}
            >
              {showDragger ? (
                <span className="flex items-center">
                  <DragHandle rowIndex={rowIndex} />
                  {columnValue}
                </span>
              ) : (
                columnValue
              )}
            </td>
          );
        })}
      </tr>
    ),
  );

  const handleSortStart = ({ node }: { node: Element }) => {
    document.body.style.cursor = 'grabbing';
    const column =
      document.getElementsByClassName('sortable-row')[0].childNodes;
    node?.childNodes.forEach((node, index) => {
      const nodeColumn = column[index] as HTMLElement;
      nodeColumn.style.width = `${(node as HTMLElement).offsetWidth}px`;
      nodeColumn.style.padding = '8px';
      return nodeColumn;
    });
  };

  const handleSortEnd = ({ oldIndex, newIndex }: SortEnd) => {
    document.body.style.cursor = 'default';
    if (handleSort && oldIndex !== newIndex) {
      const sortedData = arrayMove(data, oldIndex, newIndex);
      handleSort(sortedData);
    }
  };

  return (
    <>
      <BsTable
        className={`${className} ${isFixed ? 'fixed-table' : ''}`}
        size={size}
        {...props}
      >
        {!hideHeader && <TableHeader />}
        <TableBody
          useDragHandle
          lockToContainerEdges
          lockAxis="y"
          helperClass="sortable-row bg-slate-100"
          onSortStart={handleSortStart}
          onSortEnd={handleSortEnd}
        />
      </BsTable>
      {isPagination && (
        <Pagination
          {...meta}
          onChange={handleChangePagination}
          className="text-center"
          locale={localeInfo}
        />
      )}
    </>
  );
};

export default Table;
