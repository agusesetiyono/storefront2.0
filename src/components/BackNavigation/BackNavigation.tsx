import React from 'react';
import { Link } from 'react-router-dom';
import type { IBackNavigationProps } from './backNavigation.types';

const BackNavigation = ({ to, text = 'Kembali' }: IBackNavigationProps) => {
  return (
    <div className="inline-block">
      <Link to={to} className="flex mb-5 items-center opacity-50">
        <span className="fi-rr-angle-small-left text-black text-3xl leading-6"></span>
        <span className="text-base text-black font-light mt-[2px]">{text}</span>
      </Link>
    </div>
  );
};

export default BackNavigation;
