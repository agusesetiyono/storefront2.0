export interface IBackNavigationProps {
  to: string;
  text?: string;
}
