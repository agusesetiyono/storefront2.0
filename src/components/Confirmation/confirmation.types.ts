export type TConfirmVariant = 'confirm' | 'success' | 'error';
export interface IConfirmation {
  show: boolean;
  variant: TConfirmVariant;
  onClose?: () => void;
}
