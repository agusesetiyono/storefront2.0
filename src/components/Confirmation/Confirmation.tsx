import React, { useState } from 'react';
import { Form, Image } from 'react-bootstrap';

import Modal from '@components/Modal';
import Button from '@components/Button';
import Callout from '@components/Callout';
import ErrorIllustration from '@assets/image/error-illustration.svg';
import MarketplaceIllustration from '@assets/image/marketplace.svg';
import storefrontIllustration from '@assets/image/storefrontIllustration.svg';

import type { IConfirmation } from './confirmation.types';

const Confirmation = ({
  variant = 'confirm',
  show = false,
  onClose,
}: IConfirmation) => {
  const [isChecked, setReadChecked] = useState<boolean>(false);
  const [isLoading, setLoading] = useState<boolean>(false);

  const handleupdate = () => {
    if (isChecked) {
      setLoading(true);
    }
  };

  return (
    <>
      {variant === 'confirm' && (
        <Modal
          show={show}
          variant="confirmation"
          action={[
            <Button
              variant="light"
              className="bg-soft-grey text-[#212529] border-0"
              onClick={onClose}
              disabled={isLoading}
              data-testid="button-confirm-cancel"
            >
              Tidak, batalkan
            </Button>,
            <Button
              onClick={handleupdate}
              disabled={!isChecked}
              isLoading={isLoading}
              className={`border-0${
                isChecked || isLoading ? ' bg-gradient' : ' is-disabled-30'
              }`}
              data-testid="button-confirm-update"
            >
              <span>
                <i className="fi fi-sr-magic-wand mr-3"></i> Ya, update
              </span>
            </Button>,
          ]}
          className="modal-confirmation--upgrade"
        >
          <div className={isLoading ? 'opacity-50' : ''}>
            <div className="flex justify-between">
              <div>
                <h3 className="text-2xl mb-6">Update ke Storefront 2.0?</h3>
                <h4 className="text-base">Nikmati manfaatnya!</h4>
                <ul className="ml-4 pl-1 font-light">
                  <li>Tampilan yang lebih terbaru</li>
                  <li>Pengaturan website yang lebih mudah</li>
                </ul>
              </div>
              <div>
                <Image
                  src={MarketplaceIllustration}
                  className="mx-auto img-responsive"
                  alt="marketplace-illus"
                />
              </div>
            </div>
            <Callout
              text="Setelah update, tidak bisa kembali ke versi lama."
              variant="info"
              size="huge"
              className="mb-4 mt-4"
            />
            <Form.Check
              label="Saya sudah membaca dan mengerti."
              id="read"
              key="read"
              checked={isChecked}
              value="read"
              name="read"
              onChange={() =>
                isChecked ? setReadChecked(false) : setReadChecked(true)
              }
              className="form-check--upgrade ml-1"
              disabled={isLoading}
            />
          </div>
        </Modal>
      )}
      {variant === 'success' && (
        <Modal
          show={show}
          variant="confirmation"
          action={[
            <Button variant="primary" size="lg" onClick={onClose}>
              Oke
            </Button>,
          ]}
          className="modal-confirmation--upgrade"
        >
          <div className="text-center">
            <Image
              src={storefrontIllustration}
              className="mb-8 mx-auto img-responsive"
              alt="welcome-illus"
            />
            <p className="text-[22px] font-bold">
              Selamat datang di Storefront 2.0!
            </p>
            <p className="text-base my-8">
              Pelajari tentang perubahan dengan klik{' '}
              <a href="" className="underline">
                link ini
              </a>
              .
            </p>
          </div>
        </Modal>
      )}
      {variant === 'error' && (
        <Modal
          show={show}
          variant="confirmation"
          action={[
            <Button variant="primary" size="lg" onClick={onClose}>
              Oke
            </Button>,
          ]}
          className="modal-confirmation--upgrade"
        >
          <div className="text-center">
            <Image
              src={ErrorIllustration}
              className="mb-8 mx-auto img-responsive w-[296px]"
              alt="error-illus"
            />
            <p className="text-[22px] font-bold">Gagal update versi</p>
            <p className="text-base my-2">Silakan coba lagi beberapa saat.</p>
          </div>
        </Modal>
      )}
    </>
  );
};

export default Confirmation;
