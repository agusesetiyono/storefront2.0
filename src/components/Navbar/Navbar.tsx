import React from 'react';
import { NavLink } from 'react-router-dom';
import {
  sendClickTabTracker,
  sendLinkTracker,
  getTabName,
} from '@utils/tracker/dashboard';

import type { Menu } from './navbar.types';

const Navbar = () => {
  const menus: Menu[] = [
    {
      url: 'domain',
      text: 'Domain',
    },
    {
      url: 'custom-website',
      text: 'Custom Website',
    },
    {
      url: 'manage-pages',
      text: 'Atur Halaman',
    },
    {
      url: 'marketing-tools',
      text: 'Marketing Tools',
    },
    {
      url: 'coupons',
      text: 'Kupon',
    },
    {
      url: '/setting_frontstore/general',
      text: 'Setting',
    },
  ];

  return (
    <div className="max-w-full h-[48px]">
      <ul className="bg-white flex border border-white rounded-xl w-full p-1 list-none whitespace-nowrap overflow-auto">
        {menus.map(({ url, text }) =>
          text === 'Setting' ? (
            <li className="w-full p-0" key={`menu-${text}`}>
              <a
                className="flex justify-center items-center rounded-lg h-base px-4 bg-white text-primary"
                href={url}
                onClick={(event) =>
                  void sendLinkTracker({
                    event,
                    url,
                    actionType: 'click_tab',
                    actionDetails: getTabName(url),
                  })
                }
              >
                <span className="font-bold text-sm">Setting</span>
              </a>
            </li>
          ) : (
            <li className="w-full p-0" key={`menu-${text}`}>
              <NavLink
                to={url}
                className={({ isActive }) =>
                  `flex justify-center items-center rounded-lg h-base px-4 ${
                    isActive ? 'bg-primary text-white' : 'bg-white text-primary'
                  }`
                }
                onClick={() => void sendClickTabTracker(url)}
              >
                <span className="font-bold text-sm">{text}</span>
              </NavLink>
            </li>
          ),
        )}
      </ul>
    </div>
  );
};

export default Navbar;
