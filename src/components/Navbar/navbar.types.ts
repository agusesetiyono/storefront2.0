export type Menu = {
  url: string;
  text: string;
};
