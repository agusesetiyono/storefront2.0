import React from 'react';

import type { IBadge } from './badge.types';

const Badge = ({
  variant,
  icon,
  isEllipsis,
  disabled,
  children,
  className = '',
}: IBadge) => {
  return (
    <span
      className={`badge badge-${variant} rounded px-1 py-1 ${
        disabled ? 'disabled' : ''
      } ${isEllipsis ? 'is-ellipsis' : ''} ${className}`}
      title={children}
    >
      {icon && <i className={`fi fi-rr-${icon}`}></i>}
      {children}
    </span>
  );
};

export default Badge;
