export type TBadgeVariant =
  | 'default'
  | 'success'
  | 'danger'
  | 'info'
  | 'warning';

export interface IBadge {
  variant: TBadgeVariant;
  icon?: string;
  isEllipsis?: boolean;
  disabled?: boolean;
  children?: string;
  className?: string;
}
