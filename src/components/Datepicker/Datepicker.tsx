import React, { useRef } from 'react';
import { Form, InputGroup } from 'react-bootstrap';
import { Calendar2Event } from 'react-bootstrap-icons';
import Flatpicker from 'react-flatpickr';
import type { IDatepickerProps } from './datepicker.types';

const Datepicker = ({
  smallLabel,
  hasValidation,
  hint,
  errorMessage,
  isInvalid,
  name,
  ...props
}: IDatepickerProps) => {
  const picker = useRef(null);

  return (
    <Flatpicker
      ref={picker}
      render={({ defaultValue, value, placeholder }, ref) => (
        <div>
          {smallLabel && <small>{smallLabel}</small>}
          <InputGroup hasValidation={hasValidation} className="datepicker">
            <Form.Control
              name={name}
              type="text"
              placeholder={placeholder || 'DD-MM-YYYY 00:00'}
              defaultValue={defaultValue}
              value={(value as string) || ''}
              isInvalid={isInvalid}
              ref={ref}
              readOnly
            />
            <InputGroup.Text
              onClick={() => (picker?.current as any)?.flatpickr?.open()}
              className="absolute z-10 cursor-pointer bg-white border-none"
            >
              <Calendar2Event className="text-blue-600" />
            </InputGroup.Text>
            {hasValidation && (
              <Form.Control.Feedback type="invalid" className="h-5 block">
                {errorMessage}
              </Form.Control.Feedback>
            )}
          </InputGroup>
          {hint && <Form.Text muted>{hint}</Form.Text>}
        </div>
      )}
      {...props}
    />
  );
};

export default Datepicker;
