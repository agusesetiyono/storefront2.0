import type { DateTimePickerProps } from 'react-flatpickr';

export interface IDatepickerProps extends DateTimePickerProps {
  smallLabel?: string;
  hasValidation?: boolean;
  hint?: string;
  errorMessage?: string;
  isInvalid?: boolean;
}
