import { ButtonProps } from 'react-bootstrap/esm/Button.d';

export interface IButtonProps extends ButtonProps {
  children: string | JSX.Element;
  isLoading?: boolean;
  spinnerClass?: string;
  className?: string;
}
