import React from 'react';
import { Button as BsButton, Spinner } from 'react-bootstrap';
import type { IButtonProps } from './button.types';

const Button = ({
  children,
  href,
  isLoading = false,
  spinnerClass = '',
  className = '',
  ...other
}: IButtonProps) => {
  return (
    <BsButton {...other} className={className}>
      {isLoading && !href ? (
        <Spinner
          as="span"
          animation="border"
          size="sm"
          role="status"
          aria-hidden="true"
          className={spinnerClass}
        />
      ) : (
        children
      )}
    </BsButton>
  );
};

export default Button;
