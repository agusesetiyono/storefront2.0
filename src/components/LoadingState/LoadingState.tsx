import React from 'react';
import { Spinner } from 'react-bootstrap';
import type { ILoadingStateProps } from './loadingState.types';

const LoadingState = ({ className }: ILoadingStateProps) => {
  return (
    <div
      className={`flex align-items-center justify-content-center pt-8 pb-12 ${className}`}
    >
      <Spinner animation="border" variant="primary" />
    </div>
  );
};

export default LoadingState;
