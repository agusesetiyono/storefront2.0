import { FormControlProps } from 'react-bootstrap/esm/FormControl.d';

export interface IInputNumberProps extends FormControlProps {
  name?: string;
  hasValidation?: boolean;
  prepend?: string;
  append?: string;
  maxValue?: number;
  errorMessage?: string;
  onChangeNumber?: (value?: number) => void;
}
