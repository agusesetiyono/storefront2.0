import React, { ChangeEvent, useEffect, useState } from 'react';
import { Form, InputGroup } from 'react-bootstrap';

import { formatNumber, formatCurrency } from '@utils/number';
import type { IInputNumberProps } from './inputNumber.types';

const InputNumber = ({
  name,
  hasValidation,
  prepend,
  append,
  maxValue = 9999999999999,
  value,
  errorMessage,
  onChangeNumber,
  ...otherProps
}: IInputNumberProps) => {
  const [formattedValue, setFormattedValue] = useState<string>(
    (value as string) || '',
  );

  useEffect(() => {
    if (value === undefined) {
      setFormattedValue('');
    } else {
      const numberValue =
        typeof value === 'number' ? value : formatNumber(value?.toString());
      setFormattedValue(formatCurrency(numberValue, false));
    }
  }, [value]);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    const numberValue = formatNumber(value);
    const isValueUndefined = numberValue === undefined;

    if (isValueUndefined || (!isValueUndefined && numberValue <= maxValue)) {
      const formattedValue = formatCurrency(numberValue, false);
      setFormattedValue(formattedValue);
      onChangeNumber && onChangeNumber(numberValue);
    }
  };

  return (
    <InputGroup hasValidation={hasValidation}>
      {prepend && <InputGroup.Text>{prepend}</InputGroup.Text>}
      <Form.Control
        type="text"
        name={name}
        value={formattedValue}
        onChange={handleChange}
        {...otherProps}
      />
      {append && <InputGroup.Text>{append}</InputGroup.Text>}
      {hasValidation && (
        <Form.Control.Feedback type="invalid" className="h-5 block">
          {errorMessage}
        </Form.Control.Feedback>
      )}
    </InputGroup>
  );
};

export default InputNumber;
