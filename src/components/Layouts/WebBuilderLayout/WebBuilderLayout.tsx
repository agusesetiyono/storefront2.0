import React, { useEffect, useState } from 'react';
import { Outlet } from 'react-router-dom';
import { Card } from 'react-bootstrap';
import { useBoolean } from 'usehooks-ts';
import IframeResizer from 'iframe-resizer-react';

import {
  DEFAULT_WEB_BUILDER_TOAST_DURATION,
  DEFAULT_WEB_BUILDER_SORT_DELAY,
} from '@utils/constants';
import useSubscription from '@hooks/useSubscription';
import type { TWebBuilderStatus, TPreviewScreen } from '@interfaces/webBuilder';

import LoadingState from '@components/LoadingState';
import NavbarWebBuilder from '@components/WebBuilder/Navbar';
import {
  sendWebBuilderTracker,
  WebBuilderAction,
} from '@utils/tracker/webBuilder';

const WebBuilderLayout = () => {
  globalThis.DEFAULT_TOAST_DURATION = DEFAULT_WEB_BUILDER_TOAST_DURATION;
  globalThis.DEFAULT_SORT_DELAY = DEFAULT_WEB_BUILDER_SORT_DELAY;

  const { subscription } = useSubscription();
  const storefrontUrl = `https://${subscription.active_domain}/`;

  const [iframeKey, setIframeKey] = useState<number>(0);
  const [status, setStatus] = useState<TWebBuilderStatus>();
  const [previewScreen, setPreviewScreen] = useState<TPreviewScreen>('mobile');
  const { value: isFinishLoad, setTrue: finishLoading } = useBoolean(false);

  const refreshPreview = () => setIframeKey(iframeKey + 1);
  const hideStatus = () => setStatus(undefined);

  const navbarProps = {
    status,
    previewScreen,
    setPreviewScreen,
  };

  useEffect(() => {
    void sendWebBuilderTracker(WebBuilderAction.Visit);
  }, []);

  return (
    <>
      <NavbarWebBuilder {...navbarProps} />
      <div className="w-[320px] fixed top-[68px] bottom-0 bg-white">
        <Outlet
          context={{
            showStatus: setStatus,
            hideStatus,
            refreshPreview,
          }}
        />
      </div>
      <div className="overflow-y-scroll px-[20px] absolute top-[68px] left-[320px] right-0 bottom-0">
        <Card
          className={`builder-preview-container w-${previewScreen}`}
          data-testid="iframe-container"
        >
          <Card.Header>
            <a href={storefrontUrl} target="_blank">
              {storefrontUrl}
            </a>
          </Card.Header>
          {!isFinishLoad && <LoadingState className="loading-state" />}
          <IframeResizer
            key={iframeKey}
            id="buyersite-iframe"
            title="Storefront Site"
            src={storefrontUrl}
            onLoad={finishLoading}
            className={!isFinishLoad ? 'invisible' : 'visible'}
          />
        </Card>
      </div>
    </>
  );
};

export default WebBuilderLayout;
