import type { Icon } from 'react-bootstrap-icons';

export interface IMarketingTab {
  title: string;
  url: string;
  icon: Icon;
}
