import React from 'react';
import { NavLink, Outlet } from 'react-router-dom';
import { Card, Col, Nav, Row } from 'react-bootstrap';
import { Search, Meta, Google } from 'react-bootstrap-icons';
import { PromptContext } from '@hooks/usePrompt';
import { sendClickTabTracker } from '@utils/tracker/dashboard';

import type { IMarketingTab } from './marketingToolsLayout.types';

const MarketingToolsLayout = () => {
  const tabs: IMarketingTab[] = [
    {
      title: 'SEO',
      url: 'seo',
      icon: Search,
    },
    {
      title: 'Meta Pixel',
      url: 'meta-pixel',
      icon: Meta,
    },
    {
      title: 'Google Analytics',
      url: 'google-analytics',
      icon: Google,
    },
  ];

  return (
    <Card className="px-24px py-0 mb-0">
      <Card.Body className="pt-0">
        <Row>
          <Col sm={2} className="py-2 px-0">
            <Nav className="tab-nav flex-column">
              {tabs.map((tab) => {
                const Icon = tab.icon;
                return (
                  <Nav.Item key={tab.url}>
                    <Nav.Link
                      as={NavLink}
                      to={tab.url}
                      className="flex align-items-center py-2 px-2 text-slate-900 cursor-pointer"
                      onClick={() =>
                        void sendClickTabTracker(`marketing-tools/${tab.url}`)
                      }
                    >
                      <Icon className="mr-2" />
                      {tab.title}
                    </Nav.Link>
                  </Nav.Item>
                );
              })}
            </Nav>
          </Col>
          <Col
            sm={10}
            className="py-5 border-l-2 border-gray-100 fixed-tab-content"
          >
            <PromptContext>
              <Outlet />
            </PromptContext>
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};

export default MarketingToolsLayout;
