import React, { useEffect } from 'react';
import { Button, Image } from 'react-bootstrap';
import { BoxArrowUpRight, Link45deg } from 'react-bootstrap-icons';
import { Outlet } from 'react-router-dom';
import { useCopyToClipboard } from 'usehooks-ts';
import successUpgradeIllustration from '@assets/image/storefrontIllustration.svg';

import useSubscription from '@hooks/useSubscription';
import useUpgrade from '@hooks/useUpgrade';
import toast from '@utils/toast';
import { sendDashboardTracker } from '@utils/tracker/dashboard';
import { sendUpgradeTracker } from '@utils/tracker/upgrade';
import { UPDATE_LEARN_MORE_LINK } from '@utils/constants';

import Navbar from '@components/Navbar';
import PageTitle from '@components/PageTitle';
import Modal from '@components/Modal';

const SettingLayout = () => {
  const [, copy] = useCopyToClipboard();
  const { subscription } = useSubscription();
  const { upgradeStatus, resetUpgrade } = useUpgrade();

  const copyDomain = () => {
    void copy(subscription.active_domain as string);
    void sendDashboardTracker({ actionType: 'copy_link' });
    toast.success(`Link berhasil di-copy!`);
  };

  useEffect(() => {
    const isNotFromGeneralSettingTab = !document.referrer.includes(
      '/setting_frontstore/general',
    );
    if (isNotFromGeneralSettingTab) {
      void sendDashboardTracker({ actionType: 'visit' });
    }
  }, []);

  const onCloseUpgradeSuccess = () => {
    void sendUpgradeTracker({ actionType: '5-a_upgrade_finish' });
    resetUpgrade();
  };

  return (
    <>
      <div className="is-sticky" data-testid="setting-layouts">
        <PageTitle title="Storefront">
          <div>
            {!!subscription.active_domain && (
              <span
                className="text-base font-light opacity-70 text-black"
                data-testid="active-domain-text"
              >
                {subscription.active_domain}
              </span>
            )}
            <Button
              onClick={copyDomain}
              variant="outline-primary"
              className="ml-6"
              data-testid="dashboard-copy-link-btn"
              disabled={!subscription.active_domain}
            >
              Copy link
              <Link45deg className="icon-base ml-2" />
            </Button>
            <Button
              href={`https://${subscription.active_domain}`}
              target="_blank"
              className="ml-6"
              data-testid="dashboard-view-storefront-btn"
              disabled={!subscription.active_domain}
              onClick={() =>
                void sendDashboardTracker({ actionType: 'to_storefront' })
              }
            >
              Lihat Storefront
              <BoxArrowUpRight className="icon-base ml-2" />
            </Button>
          </div>
        </PageTitle>
        <Navbar />
      </div>
      <Outlet />

      {upgradeStatus && (
        <Modal
          show={upgradeStatus === 'success'}
          variant="confirmation"
          action={[
            <Button variant="primary" size="lg" onClick={onCloseUpgradeSuccess}>
              Oke
            </Button>,
          ]}
          className="modal-confirmation--upgrade"
          data-testid="success-upgrade-modal"
        >
          <div className="text-center">
            <Image
              src={successUpgradeIllustration}
              className="mb-8 mx-auto img-responsive"
              alt="welcome-illus"
            />
            <p className="text-[22px] font-bold">
              Selamat datang di Storefront 2.0!
            </p>
            <p className="text-base my-8">
              Pelajari tentang perubahan dengan klik{' '}
              <a
                href={UPDATE_LEARN_MORE_LINK}
                target="_blank"
                rel="noopener noreferrer"
                className="underline"
                onClick={() =>
                  void sendUpgradeTracker({
                    actionType: '5-b_upgrade_learn_more',
                    targetUrl: UPDATE_LEARN_MORE_LINK,
                  })
                }
              >
                link ini
              </a>
              .
            </p>
          </div>
        </Modal>
      )}
    </>
  );
};

export default SettingLayout;
