import React from 'react';

import type { ICallout } from './callout.types';
const Callout = ({
  text,
  variant = 'default',
  isCenter,
  className = '',
  icon = 'fi-sr-info',
  size = 'default',
  testId,
}: ICallout) => {
  return (
    <div
      className={`flex border-radius-1 mb-2 p-3 text-[#3D3D3D] leading-5 callout callout-size--${size} callout--${variant} ${
        isCenter ? 'callout--center' : ''
      } ${className}`}
      data-testid={testId}
    >
      <div className="mr-2 callout__icon">
        <i className={icon}></i>
      </div>
      <div className="leading-5 text-sm callout__text">{text}</div>
    </div>
  );
};

export default Callout;
