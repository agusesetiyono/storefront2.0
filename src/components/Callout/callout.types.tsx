export type TCalloutVariant = 'default' | 'danger' | 'info';
export type TCalloutSize = 'default' | 'huge';
export interface ICallout {
  text?: string;
  variant?: TCalloutVariant;
  isCenter?: boolean;
  className?: string;
  icon?: string;
  size?: TCalloutSize;
  testId?: string;
}
