import React from 'react';

import Button from '@components/Button';

import type { IPageTitle } from './pageTitle.types';

const PageTitle = ({ title, withBack, children }: IPageTitle) => {
  return (
    <div className={`page-header mb-9${children ? ' page-header-top' : ''}`}>
      <div className="flex items-center">
        {withBack && (
          <Button
            size="sm"
            variant="outline-primary"
            className="border-[#AAAAAA] text-[#3F3F46] bg-white w-8 h-8 mr-3 rounded"
            data-testid="back-button"
            onClick={() => history.back()}
          >
            <i className="fi-sr-angle-small-left"></i>
          </Button>
        )}
        <h2 className="text-6xl md:h5 m-0 pt-24px pb-24px capitalize">
          {title}
        </h2>
      </div>
      {children}
    </div>
  );
};

export default PageTitle;
