export interface IPageTitle {
  title: string;
  withBack?: boolean;
  children?: JSX.Element;
}
