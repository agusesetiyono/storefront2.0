import type { TPreviewScreen, TWebBuilderStatus } from '@interfaces/webBuilder';

export interface INavbarWebBuilderProps {
  status?: TWebBuilderStatus;
  previewScreen: TPreviewScreen;
  setPreviewScreen: (screen: TPreviewScreen) => void;
}
