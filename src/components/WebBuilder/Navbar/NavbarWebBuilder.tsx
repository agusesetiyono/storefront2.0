import React from 'react';
import { Dropdown, Image } from 'react-bootstrap';
import SmartsellerLogo from '@assets/image/smartseller-logo.svg';

import type { TPreviewScreen } from '@interfaces/webBuilder';
import type { INavbarWebBuilderProps } from './navbarWebBuilder.types';

const NavbarWebBuilder = ({
  status,
  previewScreen,
  setPreviewScreen,
}: INavbarWebBuilderProps) => {
  const screenOptions: TPreviewScreen[] = ['desktop', 'mobile'];

  const getScreenOptions = (screen: TPreviewScreen) => {
    switch (screen) {
      case 'desktop':
        return (
          <span>
            <i className="fi-rr-computer opacity-50 mr-2"></i> Desktop
          </span>
        );
      default:
        return (
          <span>
            <i className="fi-rr-smartphone opacity-50 mr-2"></i> Mobile
          </span>
        );
    }
  };

  return (
    <div className="navbar navbar-inverse shadow-none border-b border-[#AAAAAA] border-solid navbar-fixed-top px-4">
      <div className="flex justify-between items-center h-full">
        <div className="hstack gap-2">
          <a href="/storefront-editor/settings/custom-website">
            <span className="fi-rr-arrow-small-left text-3xl leading-6"></span>{' '}
            Kembali ke Dashboard
          </a>
        </div>

        <div className="align-items-center flex justify-center">
          <Image src={SmartsellerLogo} className="w-[182px]" />
          <span className="align-bottom ml-2 text-black">Storefront</span>
        </div>
        <span className="hstack">
          {status}{' '}
          <Dropdown>
            <Dropdown.Toggle
              id="dropdownPreview"
              variant="default"
              size="sm"
              className="w-[156px] justify-between rounded ml-4"
              data-testid="dropdown-preview-screen"
            >
              {getScreenOptions(previewScreen)}
            </Dropdown.Toggle>

            <Dropdown.Menu>
              {screenOptions.map((screen) => (
                <Dropdown.Item
                  key={screen}
                  active={screen === previewScreen}
                  onClick={() => setPreviewScreen(screen)}
                  data-testid={screen}
                >
                  {getScreenOptions(screen)}
                </Dropdown.Item>
              ))}
            </Dropdown.Menu>
          </Dropdown>
        </span>
      </div>
    </div>
  );
};

export default NavbarWebBuilder;
