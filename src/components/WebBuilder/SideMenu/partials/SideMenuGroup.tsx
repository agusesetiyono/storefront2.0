import React, { useEffect } from 'react';
import { Form, Nav } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { useBoolean } from 'usehooks-ts';
import {
  arrayMove,
  SortableContainer,
  SortableElement,
  SortableHandle,
  SortEnd,
} from 'react-sortable-hoc';

import Popover from '@components/Popover';

import type {
  ISideMenuAction,
  ISideMenuGroup,
  ISideMenuItem,
} from '../sideMenuWebBuilder.types';

const SortAction = SortableHandle<ISideMenuAction>(
  ({ rowIndex }: ISideMenuAction) => (
    <span className="sort-icon" data-testid={`sort-action-${rowIndex + 1}`}>
      <i className="fi-sr-caret-up -mb-1.5"></i>
      <i className="fi-sr-minus"></i>
      <i className="fi-sr-caret-down -mt-1.5"></i>
    </span>
  ),
);

const ToggleAction = ({ rowIndex, isActive, onChange }: ISideMenuAction) => {
  const { value: isChecked, setValue } = useBoolean(isActive);
  useEffect(() => {
    if (isActive !== undefined && isActive !== isChecked) {
      setValue(isActive);
    }
  }, [isActive]);

  const handleChange = (e) => {
    e.preventDefault();
    onChange && onChange(e.target.checked as boolean);
  };

  return (
    <Form.Switch
      id="toggle-menu"
      checked={isChecked}
      onChange={handleChange}
      data-testid={`toggle-action-${rowIndex + 1}`}
    />
  );
};

const SideMenuItem = SortableElement<ISideMenuItem>(
  ({
    rowIndex = 0,
    groupIndex = 0,
    title,
    description,
    url,
    canSort,
    canToggle,
    canDelete,
    isActive,
    isDisabled,
    image,
    id,
    config = {
      className: '',
      showTitle: false,
      showSortAction: true,
      isCustomDelete: false,
      imageSize: 'normal',
    },
    onToggle,
    onDelete,
    onClick,
  }: ISideMenuItem) => {
    const navigate = useNavigate();
    const { className, showTitle, showSortAction, isCustomDelete, imageSize } =
      config;

    return (
      <Popover
        title={title}
        trigger={['hover', 'focus']}
        content={description}
        display={!!description}
        placement="right-start"
        className="menu-info"
      >
        <Nav.Item
          data-testid={`side-menu-item-${groupIndex}-${rowIndex}`}
          className={className}
        >
          <Nav.Link
            as="div"
            onClick={() => {
              if (url) {
                onClick && onClick(rowIndex);
                navigate(url);
              }
            }}
            className={`${
              isDisabled
                ? 'text-[#AAAAAA] cursor-default pointer-events-none'
                : ''
            } ${
              url
                ? 'text-[#1155CC] border-[#1155CC]'
                : 'text-[#AAAAAA] border-[#F4F4F4]'
            } ${image ? 'with-image py-3' : ''}`}
          >
            <span className="flex align-items-center">
              {showSortAction &&
                (canSort ? (
                  <SortAction rowIndex={rowIndex} />
                ) : (
                  <span className="w-6"></span>
                ))}
              {image && (
                <span className="rounded mr-2 border border-gray-200 opacity-100">
                  <img
                    src={image}
                    alt={title}
                    className={`${
                      imageSize === 'normal'
                        ? 'h-[28px] w-[28px]'
                        : 'h-[32px] w-[32px]'
                    } object-cover rounded`}
                  />
                </span>
              )}
              <span
                title={showTitle ? title : undefined}
                className={`${image ? 'side-menu-item__text opacity-100' : ''}`}
              >
                {title}
              </span>
            </span>
            {isCustomDelete && canDelete && (
              <i
                className="fi-sr-cross font-bold cursor-pointer"
                onClick={() => onDelete && onDelete(id)}
                data-testid={`delete-trigger-${groupIndex}-${rowIndex}`}
              ></i>
            )}
          </Nav.Link>
          <span className="menu-action">
            {canToggle && (
              <ToggleAction
                key={`toggle-${rowIndex}`}
                rowIndex={rowIndex}
                isActive={isActive}
                onChange={(value) => onToggle && onToggle(rowIndex, value)}
              />
            )}
            {!isCustomDelete && canDelete && (
              <i
                className="fi-sr-trash text-gray-400 mr-3 cursor-pointer block mt-2 hover:color[#ED7881]"
                onClick={() => onDelete && onDelete(id)}
                data-testid={`delete-trigger-${groupIndex}-${rowIndex}`}
              ></i>
            )}
          </span>
        </Nav.Item>
      </Popover>
    );
  },
);

const SideMenuNav = SortableContainer<ISideMenuGroup>(
  ({ menus, isDisabled, groupIndex, onChange, ...props }: ISideMenuGroup) => {
    const handleToggle = (index: number, checked: boolean) => {
      if (groupIndex && onChange) {
        const updatedMenu = menus.map((menu, menuIndex) =>
          index === menuIndex ? { ...menu, isActive: checked } : menu,
        );
        onChange(groupIndex, updatedMenu);
        props.onToggle && props.onToggle(index, updatedMenu);
      }
    };

    const handleClick = (index: number) => {
      props.onItemClick && props.onItemClick(index, menus);
    };

    return (
      <Nav id={`side-menu-nav-${groupIndex}`} className="flex-column">
        {menus.map((menu, index) => (
          <SideMenuItem
            {...menu}
            {...props}
            key={`${menu.title}-${index}`}
            index={index}
            rowIndex={index}
            onToggle={handleToggle}
            isDisabled={isDisabled || menu.isDisabled}
            groupIndex={groupIndex}
            onClick={handleClick}
          />
        ))}
      </Nav>
    );
  },
);

const SideMenuGroup = ({
  groupIndex = 0,
  title,
  menus,
  testid,
  ...props
}: ISideMenuGroup) => {
  const {
    value: isSorting,
    setTrue: startSort,
    setFalse: endSort,
  } = useBoolean(false);

  const handleSortStart = () => {
    document.body.style.cursor = 'grabbing';
    startSort();
  };

  const handleSortEnd = ({ oldIndex, newIndex }: SortEnd) => {
    document.body.style.cursor = 'default';
    if (props.onChange && oldIndex !== newIndex) {
      const sortedMenu = arrayMove(menus, oldIndex, newIndex);
      props.onChange(groupIndex, sortedMenu);
      props.onReorder && props.onReorder(newIndex, sortedMenu);
    }
    endSort();
  };

  return (
    <div
      className="side-menu-group"
      data-testid={testid || `side-menu-group-${groupIndex}`}
    >
      {title && <p>{title}</p>}
      <SideMenuNav
        {...props}
        groupIndex={groupIndex}
        menus={menus}
        isDisabled={isSorting}
        useDragHandle
        lockToContainerEdges
        lockAxis="y"
        helperClass="sorted-menu"
        helperContainer={() => {
          const container = document?.getElementById(
            `side-menu-nav-${groupIndex}`,
          ) as HTMLElement;
          return container;
        }}
        onSortStart={handleSortStart}
        onSortEnd={handleSortEnd}
      />
    </div>
  );
};

export default SideMenuGroup;
