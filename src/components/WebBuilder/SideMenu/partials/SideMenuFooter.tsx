import React from 'react';

import type { ISideMenuFooter } from '../sideMenuWebBuilder.types';

const SideMenuFooter = ({ action }: ISideMenuFooter) => (
  <div className="side-menu-footer border-t border-r border-[#AAAAAA] bg-white p-6 w-[320px]">
    {action}
  </div>
);

export default SideMenuFooter;
