import React from 'react';
import { Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import Button from '@components/Button';

import type { ISideMenuHeader } from '../sideMenuWebBuilder.types';

const SideMenuHeader = ({
  title,
  subtitle,
  isActive,
  backUrl,
  withToggle,
  onToggle,
}: ISideMenuHeader) => (
  <div className="side-menu-header p-6 border-b border-[#AAAAAA] bg-white">
    <div className="flex gap-2 items-center mb-3.5 justify-content-between">
      <div className="flex items-center">
        {backUrl && (
          <Link to={backUrl}>
            <Button
              size="sm"
              variant="outline-primary"
              className="border-[#AAAAAA] text-[#AAAAAA] w-8 h-8 mr-3 rounded"
              data-testid="back-button"
            >
              <i className="fi-sr-angle-small-left"></i>
            </Button>
          </Link>
        )}
        <h3 className="text-2xl font-bold">{title}</h3>
      </div>
      {withToggle && (
        <Form.Switch
          className="mr-16px"
          id="toggle-header"
          checked={isActive}
          onChange={onToggle}
        />
      )}
    </div>
    {subtitle && (
      <p className="text-base font-semibold">
        <span className="opacity-50 mr-1">{subtitle}</span>
      </p>
    )}
  </div>
);

export default SideMenuHeader;
