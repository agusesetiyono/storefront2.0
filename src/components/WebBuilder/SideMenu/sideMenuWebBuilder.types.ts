export interface ISideMenuHeader {
  title: string;
  subtitle?: string;
  isActive?: boolean;
  backUrl?: string;
  withToggle?: boolean;
  onToggle?: (e: any) => void;
}

export interface ISideMenuGroup {
  groupIndex?: number;
  title?: string;
  menus: ISideMenuItem[];
  canSort?: boolean;
  isDisabled?: boolean;
  config?: ISideMenuConfig;
  testid?: string;
  onChange?: (groupIndex: number, record: ISideMenuItem[]) => void;
  onDelete?: (id?: number) => void;
  onReorder?: (itemIndex: number, record: ISideMenuItem[]) => void;
  onToggle?: (itemIndex: number, record: ISideMenuItem[]) => void;
  onItemClick?: (itemIndex: number, record: ISideMenuItem[]) => void;
}

export interface ISideMenuItem {
  rowIndex?: number;
  groupIndex?: number;
  title: string;
  url?: string;
  description?: string;
  canSort?: boolean;
  canToggle?: boolean;
  canDelete?: boolean;
  isActive?: boolean;
  isDisabled?: boolean;
  image?: string;
  id?: number;
  config?: ISideMenuConfig;
  onToggle?: (index: number, checked: boolean) => void;
  onDelete?: (id?: number) => void;
  onClick?: (index: number) => void;
}

export interface ISideMenuConfig {
  className?: string;
  showTitle?: boolean;
  showSortAction?: boolean;
  imageSize?: 'normal' | 'large';
  isCustomDelete?: boolean;
}

export interface ISideMenuAction {
  rowIndex: number;
  show?: boolean;
  isActive?: boolean;
  onChange?: (value: boolean) => void;
}

export interface ISideMenuFooter {
  action: JSX.Element;
}

export interface ISideMenuProps {
  header: ISideMenuHeader;
  children?: JSX.Element;
  action?: JSX.Element;
  footer?: ISideMenuFooter;
  isEmpty?: boolean;
  isError?: boolean;
  isFetching?: boolean;
  onRetry?: () => void;
}
