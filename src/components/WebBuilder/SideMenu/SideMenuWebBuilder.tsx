import React, { useEffect } from 'react';

import ErrorState from '@components/ErrorState';
import LoadingState from '@components/LoadingState';

import SideMenuHeader from './partials/SideMenuHeader';
import SideMenuFooter from './partials/SideMenuFooter';
import type { ISideMenuProps } from './sideMenuWebBuilder.types';
import {
  sendWebBuilderTracker,
  WebBuilderAction,
} from '@utils/tracker/webBuilder';

const SideMenuWebBuilder = ({
  header,
  children,
  action,
  footer,
  isEmpty,
  isError,
  isFetching,
  onRetry,
}: ISideMenuProps) => {
  const pageTitle = header.title;
  const errorTitle = `Gagal mengambil data ${pageTitle}`;

  const VerticalMiddleElement = ({ children }) => (
    <div className="absolute text-center inset-x-6 top-1/2 -translate-y-2/4">
      {children}
    </div>
  );

  useEffect(() => {
    if (isError) {
      void sendWebBuilderTracker(WebBuilderAction.Error, errorTitle);
    }
  }, [isError]);

  return (
    <div className="side-menu-builder border-r border-[#AAAAAA]">
      <SideMenuHeader {...header} />
      <div className="side-menu-content p-6">
        {isFetching ? (
          <VerticalMiddleElement>
            <LoadingState />
          </VerticalMiddleElement>
        ) : isError ? (
          <VerticalMiddleElement>
            <ErrorState
              title={errorTitle}
              content="Cobalah beberapa saat lagi atau
            refresh halaman ini."
              onRetry={onRetry}
            />
          </VerticalMiddleElement>
        ) : isEmpty ? (
          <>
            {action}
            <VerticalMiddleElement>
              <p className="opacity-30">
                Belum ada {pageTitle.toLowerCase()}. Tambahkan dengan klik
                tombol di atas.
              </p>
            </VerticalMiddleElement>
          </>
        ) : (
          <>
            {action}
            {children}
          </>
        )}
      </div>
      {footer && !isError && !isEmpty && <SideMenuFooter {...footer} />}
    </div>
  );
};

export default SideMenuWebBuilder;
