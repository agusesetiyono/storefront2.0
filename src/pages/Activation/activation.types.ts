import type { ISubscription } from '@interfaces/storefronts';

export interface IConfirmProps {
  data: ISubscription;
  isSubmitting: boolean;
  onClose: () => void;
  onConfirm: () => void;
}

export interface ISuccessProps {
  data: ISubscription;
  onContinue: () => void;
}

export interface IAssertButtonInterface {
  text: string;
  className?: string;
  isExist?: boolean;
}

export interface IUpgradeText {
  type: string;
  hook: string;
}
