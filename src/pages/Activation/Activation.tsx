import React, { useState, useEffect } from 'react';
import { useSearchParams } from 'react-router-dom';
import { useBoolean } from 'usehooks-ts';
import { Button, Stack, Image, Card } from 'react-bootstrap';
import ActivationImage from '@assets/image/activation.svg';
import UpgradeStorefrontImage from '@assets/image/upgrade.svg';

import { IResponseError } from '@interfaces/query';
import { EOnboardingSteps } from '@interfaces/onboarding';
import useSubscription from '@hooks/useSubscription';
import useOnboarding from '@hooks/useOnboarding';
import { useSubmitSubscription } from '@hooks/api/storefronts';
import {
  sendActivationTracker,
  sendLinkTracker,
} from '@utils/tracker/activation';
import { sendRegistrationTracker } from '@utils/tracker/registration';
import PageTitle from '@components/PageTitle';
import Modal from '@components/Modal';

import Benefits from './partials/Benefits';
import ConfirmTrial from './partials/ConfirmTrial';
import SuccessTrial from './partials/SuccessTrial';

const Activation = () => {
  const { subscription, refetchSubscription } = useSubscription();
  const submitSubscription = useSubmitSubscription();
  const { setOnboardingStep } = useOnboarding();
  const [searchParams] = useSearchParams();
  const isFromBL = Boolean(searchParams.get('is_from_bl') === 'true');

  const { isExpired, owner_name, storefront_type } = subscription || {};

  useEffect(() => {
    void sendActivationTracker({
      actionType: '0_visit',
      isResubscribe: isExpired,
    });

    if (isFromBL && isExpired) {
      void sendRegistrationTracker({
        actionType: '8-b_visit_expiration_page',
      });
    }
  }, []);

  const {
    value: isShowModal,
    setTrue: showModal,
    setFalse: closeModal,
  } = useBoolean(false);
  const [modalType, setModalType] = useState<'confirm' | 'success'>('confirm');

  const handleFreeTrial = () => {
    showModal();
    setModalType('confirm');
    void sendActivationTracker({
      actionType: '1-a_freetrial_start',
    });
  };

  const handleConfirmFreeTrial = () => {
    showModal();
    submitSubscription.mutate(
      { class: 'freemium' },
      {
        onSuccess: () => {
          setModalType('success');
          void sendActivationTracker({
            actionType: '2-a_freetrial_popup_confirm',
          });
        },
        onError: (error) => {
          const errorResponse: IResponseError = error.response
            ?.data as IResponseError;

          void sendActivationTracker({
            actionType: '3-d_freetrial_error',
            actionDetails: errorResponse.errors[0].message,
          });
        },
      },
    );
  };

  const handleCloseModal = () => {
    closeModal();
    void sendActivationTracker({
      actionType:
        modalType === 'confirm'
          ? '2-b_freetrial_popup_close'
          : '3-c_freetrial_to_close',
    });
  };

  const handleContinueStorefront = () => {
    setOnboardingStep(EOnboardingSteps.DOMAIN);
    refetchSubscription();
  };

  const isFreeTrialExpired = Boolean(
    storefront_type === 'free_trial' && isExpired,
  );

  return (
    <>
      <PageTitle title="Storefront" />
      <div className="w-[892px] m-auto py-5">
        <Card className="pt-12 px-16 pb-12" data-testid="activation-container">
          <Stack className="align-items-center">
            <Image
              src={isExpired ? UpgradeStorefrontImage : ActivationImage}
              className={`max-w-md mt-16 pt-1 ${isExpired ? 'mb-12' : 'mb-8'}`}
            />
            {isExpired && (
              <div className="text-center">
                <h3 className="text-2xl mb-2">
                  {owner_name}, Masa{' '}
                  {isFreeTrialExpired ? 'Free Trial' : 'Subscription'}
                  -mu sudah habis.
                </h3>
                <p className="text-base font-light text-break">
                  Gimana, udah ngerasain asyiknya make Storefront? <br />
                  Yuk upgrade paketmu{' '}
                  {isFreeTrialExpired ? 'sekarang' : 'kembali'}!
                </p>
              </div>
            )}
            <div className={isExpired ? 'benefit-wrapper' : ''}>
              <Benefits small={isExpired} />
            </div>
            <Stack
              direction={isExpired ? 'vertical' : 'horizontal'}
              gap={5}
              className={`justify-content-center align-items-center ${
                isExpired ? 'mt-8' : 'mt-9'
              }`}
            >
              {isExpired ? (
                <Button
                  variant="primary"
                  href="/billing"
                  size="lg"
                  className="btn-upgrade"
                  onClick={(event) => {
                    void sendLinkTracker({
                      event,
                      url: '/billing',
                      actionType: '1-b_upgrade_plan',
                      isResubscribe: isExpired,
                    });
                  }}
                >
                  Upgrade
                </Button>
              ) : (
                <>
                  {storefront_type === 'none' && (
                    <Button variant="primary" onClick={handleFreeTrial}>
                      Free Trial Now
                    </Button>
                  )}
                  <Button
                    variant={
                      storefront_type === 'premium'
                        ? 'primary'
                        : 'outline-primary'
                    }
                    href="/billing"
                    onClick={(event) => {
                      void sendLinkTracker({
                        event,
                        url: '/billing',
                        actionType: '1-b_upgrade_plan',
                      });
                    }}
                  >
                    Upgrade Plan
                  </Button>
                </>
              )}
            </Stack>
          </Stack>
        </Card>
      </div>

      <Modal
        show={isShowModal}
        onClose={handleCloseModal}
        variant="simple"
        className="modal-custom"
      >
        {modalType === 'confirm' ? (
          <ConfirmTrial
            data={subscription}
            isSubmitting={submitSubscription.isLoading}
            onClose={handleCloseModal}
            onConfirm={handleConfirmFreeTrial}
          />
        ) : (
          <SuccessTrial
            data={subscription}
            onContinue={handleContinueStorefront}
          />
        )}
      </Modal>
    </>
  );
};

export default Activation;
