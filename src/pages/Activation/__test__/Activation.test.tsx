import axios from '@mocks/axios';
import { screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';
import { renderWithRoute, API_URL, getLastCalled } from '@utils/test-helper';
import toast from '@utils/toast';
import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';
import type { IAssertButtonInterface, IUpgradeText } from '../activation.types';

axios.onPost(`${API_URL.tracker}/e`).reply(200);

jest.mock('@utils/toast', () => ({
  danger: jest.fn(),
}));

jest.mock('@utils/tracker', () => ({
  __esModule: true,
  ...jest.requireActual('@utils/tracker'),
}));

beforeEach(() => {
  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));

  axios.onGet(`${API_URL.storefronts}/status`).reply(
    200,
    getMockSubscriptionsResponse({
      type: 'none',
      expiredDate: defaultExpiredDate,
    }),
  );
});

afterEach(() => {
  jest.useRealTimers();
});

const defaultExpiredDate = '2022-05-01T00:00:00+07:00';
const user = userEvent.setup({ delay: null });

describe('Activation Page', () => {
  it('Renders properly with default configuration', async () => {
    // Try to access restricted page
    const { container } = renderWithRoute({
      route: '/storefront-editor/settings/domain',
    });

    // Redirected to landing page
    expect(
      await screen.findByText('Buat toko online praktis dan unik'),
    ).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });

  const benefitText =
    'Buat toko online praktis dan unikTak perlu memikirkan teknis, bisa buat website dalam beberapa menit saja dengan pilihan tema yang sudah tersedia. Memudahkan branding toko mu!Jual ke lebih banyak pelangganAkses tools dan informasi penjualan seperti dashboard, SEO, google analytics, dan lainnya untuk meraih lebih banyak pelanggan dan meningkatkan kinerja bisnis mu.Pengalaman belanja lebih baikGunakan fitur untuk memudahkan pembeli berbelanja di tokomu, seperti pembayaran melalui Xendit, akses melalui smartphone maupun web, dan masih banyak lagi.PreviousNext123';

  const evn = 'ngorder_sf_activate_funnel_actions';

  const assertButtonExistence = ({
    text,
    className = 'btn-primary',
    isExist = true,
  }: IAssertButtonInterface) => {
    const button = isExist ? screen.getByText(text) : screen.queryByText(text);
    if (!isExist) {
      expect(button).not.toBeInTheDocument();
      return;
    }

    expect(button).toBeInTheDocument();
    expect(button).toHaveClass(className);
    if (['Upgrade Plan', 'Lihat Billing', 'Upgrade'].includes(text)) {
      expect(button).toHaveAttribute('href', '/billing');
    }
  };

  it('Should show free trial button for eligible user', async () => {
    renderWithRoute();

    expect(
      (await screen.findByTestId('activation-container')).textContent?.includes(
        benefitText,
      ),
    ).toBeTruthy();

    assertButtonExistence({ text: 'Free Trial Now' });
    await user.click(screen.getByText('Free Trial Now'));
    expect(getLastCalled(axios, 'post').data).toEqual(
      `{"evn":"${evn}","action_type":"1-a_freetrial_start"}`,
    );
    assertButtonExistence({
      text: 'Upgrade Plan',
      className: 'btn-outline-primary',
    });
    await user.click(screen.getByText('Upgrade Plan'));
    expect(getLastCalled(axios, 'post').data).toEqual(
      `{"evn":"${evn}","action_type":"1-b_upgrade_plan","target_url":"/billing"}`,
    );
  });

  it('Should action in benefit section correctly', async () => {
    renderWithRoute();

    expect(
      (await screen.findByTestId('activation-container')).textContent?.includes(
        benefitText,
      ),
    ).toBeTruthy();
    expect(getLastCalled(axios, 'post').data).toEqual(
      `{"evn":"${evn}","action_type":"0_visit"}`,
    );
    //slide left
    const arrowLeft = screen.getByTestId('slide-left');
    expect(arrowLeft).toBeInTheDocument();
    await user.click(arrowLeft);
    expect(getLastCalled(axios, 'post').data).toEqual(
      `{"evn":"${evn}","action_type":"slider_left","action_details":"1"}`,
    );

    //slide right
    const arrowRight = screen.getByTestId('slide-right');
    expect(arrowRight).toBeInTheDocument();
    await user.click(arrowRight);
    expect(getLastCalled(axios, 'post').data).toEqual(
      `{"evn":"${evn}","action_type":"slider_right","action_details":"3"}`,
    );

    //slide pagination
    const page2 = screen.getByTestId('button-2');
    expect(page2).toBeInTheDocument();
    await user.click(page2);
    expect(getLastCalled(axios, 'post').data).toEqual(
      `{"evn":"${evn}","action_type":"slider_number","action_details":"3:2"}`,
    );

    assertButtonExistence({ text: 'Free Trial Now' });
    assertButtonExistence({
      text: 'Upgrade Plan',
      className: 'btn-outline-primary',
    });
  });

  it('Should hide free trial button for premium user', async () => {
    axios.onGet(`${API_URL.storefronts}/status`).reply(
      200,
      getMockSubscriptionsResponse({
        type: 'premium',
        expiredDate: defaultExpiredDate,
      }),
    );

    renderWithRoute();

    expect(
      (await screen.findByTestId('activation-container')).textContent?.includes(
        benefitText,
      ),
    ).toBeTruthy();

    assertButtonExistence({ text: 'Free Trial Now', isExist: false });
    assertButtonExistence({ text: 'Upgrade Plan' });
  });

  it('Should perform use Free Trial correctly', async () => {
    renderWithRoute();

    expect(
      (await screen.findByTestId('activation-container')).textContent?.includes(
        benefitText,
      ),
    ).toBeTruthy();

    await user.click(screen.getByText('Free Trial Now'));
    expect(getLastCalled(axios, 'post').data).toEqual(
      `{"evn":"${evn}","action_type":"1-a_freetrial_start"}`,
    );

    expect(
      (await screen.findByTestId('confirm-trial-text')).textContent,
    ).toEqual(
      'Apakah anda yakin mengaktifkan Free Trial storefront? Paket ini berlaku hingga 01 Mei 2022 sejak diaktifkan.',
    );

    // Error case
    const errorMessage = 'Konfirmasi gagal, mohon dicoba beberapa saat lagi.';
    axios.onPost(`${API_URL.storefronts}/subscriptions`).replyOnce(503, {
      errors: [
        {
          message: errorMessage,
          code: 503,
        },
      ],
      meta: {
        http_status: 503,
      },
    });
    await user.click(screen.getByText('Konfirmasi'));
    await waitFor(() =>
      expect(toast.danger).toHaveBeenLastCalledWith(errorMessage),
    );
    expect(getLastCalled(axios, 'post').data).toEqual(
      `{"evn":"${evn}","action_type":"3-d_freetrial_error","action_details":"${errorMessage}"}`,
    );

    // Success case
    axios.onPost(`${API_URL.storefronts}/subscriptions`).replyOnce(200, {});
    await user.click(screen.getByText('Konfirmasi'));
    expect(
      (await screen.findByTestId('success-trial-text')).textContent,
    ).toEqual(
      'Free Trial Anda sudah aktif hingga 01 Mei 2022. Yuk, eksplor Storefront dan rasakan manfaatnya!',
    );
    expect(getLastCalled(axios, 'post').data).toEqual(
      `{"evn":"${evn}","action_type":"2-a_freetrial_popup_confirm"}`,
    );

    assertButtonExistence({
      text: 'Lihat Billing',
      className: 'btn-outline-primary',
    });
    await user.click(screen.getByText('Lihat Billing'));
    expect(getLastCalled(axios, 'post').data).toEqual(
      `{"evn":"${evn}","action_type":"3-b_freetrial_to_billing","target_url":"/billing"}`,
    );
    assertButtonExistence({ text: 'Lanjut ke Storefront' });
    await user.click(screen.getByText('Lanjut ke Storefront'));
    expect(getLastCalled(axios, 'post').data).toEqual(
      `{"evn":"${evn}","action_type":"3-a_freetrial_to_storefront","target_url":"/storefront-editor"}`,
    );
  });

  it.each(['free_trial', 'addons', 'platinum'])(
    'Should render expired (%s) landing page correctly',
    async (type) => {
      axios.onGet(`${API_URL.storefronts}/status`).reply(
        200,
        getMockSubscriptionsResponse({
          type,
          expiredDate: '2022-03-31T00:00:00+07:00',
        }),
      );

      // Try to access redirected page
      renderWithRoute({ route: '/storefront-editor/settings/domain' });

      const upgradeText: IUpgradeText =
        type === 'free_trial'
          ? { type: 'Free Trial', hook: 'sekarang' }
          : { type: 'Subscription', hook: 'kembali' };
      const expiredContent = `Ngorder Owner, Masa ${upgradeText.type}-mu sudah habis.Gimana, udah ngerasain asyiknya make Storefront? Yuk upgrade paketmu ${upgradeText.hook}!${benefitText}Upgrade`;
      // Redirected to landing page
      expect(
        (await screen.findByTestId('activation-container')).textContent,
      ).toEqual(expiredContent);

      expect(getLastCalled(axios, 'post').data).toEqual(
        `{"evn":"${evn}","action_type":"0_visit","is_resubscribe":true}`,
      );
      assertButtonExistence({ text: 'Upgrade' });
      await user.click(screen.getByText('Upgrade'));
      expect(getLastCalled(axios, 'post').data).toEqual(
        `{"evn":"${evn}","action_type":"1-b_upgrade_plan","target_url":"/billing","is_resubscribe":true}`,
      );
    },
  );

  it('Send registration tracker', async () => {
    axios.onGet(`${API_URL.storefronts}/status`).reply(
      200,
      getMockSubscriptionsResponse({
        type: 'free_trial',
        expiredDate: '2022-03-31T00:00:00+07:00',
      }),
    );
    renderWithRoute({ route: '/storefront-editor?is_from_bl=true' });
    const trackerData = JSON.stringify({
      evn: 'ngorder_sf_bukastore_funnel_action',
      action_type: '8-b_visit_expiration_page',
      bl_identity: JSON.stringify({
        user_id: '',
      }),
    });
    await waitFor(() =>
      expect(getLastCalled(axios, 'post').data).toEqual(trackerData),
    );
  });
});
