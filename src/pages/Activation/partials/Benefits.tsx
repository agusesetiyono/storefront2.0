import { sendActivationTracker } from '@utils/tracker/activation';
import React, { useState } from 'react';
import { Carousel, Button, Stack } from 'react-bootstrap';

const Benefits = ({ small }) => {
  const [activeIndex, setActiveIndex] = useState<number>(0);

  const handleSelect = (selectedIndex: number) => {
    setActiveIndex(selectedIndex);
  };

  const benefits = [
    {
      title: 'Buat toko online praktis dan unik',
      description:
        'Tak perlu memikirkan teknis, bisa buat website dalam beberapa menit saja dengan pilihan tema yang sudah tersedia. Memudahkan branding toko mu!',
    },
    {
      title: 'Jual ke lebih banyak pelanggan',
      description:
        'Akses tools dan informasi penjualan seperti dashboard, SEO, google analytics, dan lainnya untuk meraih lebih banyak pelanggan dan meningkatkan kinerja bisnis mu.',
    },
    {
      title: 'Pengalaman belanja lebih baik',
      description:
        'Gunakan fitur untuk memudahkan pembeli berbelanja di tokomu, seperti pembayaran melalui Xendit, akses melalui smartphone maupun web, dan masih banyak lagi.',
    },
  ];

  const getSliderIcon = (type: 'left' | 'right') => (
    <i
      className={`fi-sr-arrow-small-${type}`}
      onClick={() =>
        void sendActivationTracker({
          actionType: `slider_${type}`,
          actionDetails: `${activeIndex + 1}`,
          isResubscribe: small,
        })
      }
      data-testid={`slide-${type}`}
    ></i>
  );

  return (
    <div
      className={`text-center text-black mh-150 benefits-slide ${
        small ? 'benefits-slide--small' : ''
      }`}
    >
      <Carousel
        activeIndex={activeIndex}
        indicators={false}
        prevIcon={getSliderIcon('left')}
        nextIcon={getSliderIcon('right')}
        onSelect={handleSelect}
      >
        {benefits.map((benefit, index) => (
          <Carousel.Item key={`benefits-${index}`} interval={7000}>
            <h3
              className={`mb-1 ${
                small ? 'text-base' : 'text-6xl line-height-18'
              }`}
              data-testid={`slide-title-${index + 1}`}
            >
              {benefit.title}
            </h3>
            <p className={small ? 'px-3' : 'px-6'}>{benefit.description}</p>
          </Carousel.Item>
        ))}
      </Carousel>

      <Stack
        direction="horizontal"
        gap={2}
        className="mt-7 justify-content-center"
      >
        {benefits.map((_benefit, index) => (
          <Button
            key={`benefits-indicator-${index}`}
            size="sm"
            className={`font-bold py-1 ${
              activeIndex === index ? 'text-white' : 'text-blue-700'
            }`}
            variant={activeIndex === index ? 'primary' : 'dark'}
            onClick={() => {
              handleSelect(index);
              void sendActivationTracker({
                actionType: 'slider_number',
                actionDetails: `${activeIndex + 1}:${index + 1}`,
                isResubscribe: small,
              });
            }}
            data-testid={`button-${index + 1}`}
          >
            {index + 1}
          </Button>
        ))}
      </Stack>
    </div>
  );
};

export default Benefits;
