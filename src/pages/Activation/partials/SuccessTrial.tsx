import React from 'react';
import { Button, Stack, Image } from 'react-bootstrap';
import SuccessImage from '@assets/image/success.svg';

import type { ISuccessProps } from '../activation.types';
import { sendLinkTracker } from '@utils/tracker/activation';

const SuccessTrial = ({ data, onContinue }: ISuccessProps) => {
  return (
    <Stack className="align-items-center p-8 text-center">
      <Image src={SuccessImage} className="max-w-md mb-6" />
      <h4 className="text-3xl mb-6">Berhasil!</h4>
      <p data-testid="success-trial-text">
        Free Trial Anda sudah aktif hingga{' '}
        <strong>{data?.storefront_expired_date}</strong>. Yuk, eksplor
        Storefront dan rasakan manfaatnya!
      </p>

      <Stack direction="horizontal" gap={5} className="mt-9 flex">
        <Button
          variant="outline-primary"
          className="btn-block"
          href="/billing"
          onClick={(event) => {
            void sendLinkTracker({
              event,
              url: '/billing',
              actionType: '3-b_freetrial_to_billing',
            });
          }}
        >
          Lihat Billing
        </Button>
        <Button
          variant="primary"
          className="btn-block mt-0"
          onClick={(event) => {
            onContinue();
            void sendLinkTracker({
              event,
              url: '/storefront-editor',
              actionType: '3-a_freetrial_to_storefront',
            });
          }}
        >
          Lanjut ke Storefront
        </Button>
      </Stack>
    </Stack>
  );
};

export default SuccessTrial;
