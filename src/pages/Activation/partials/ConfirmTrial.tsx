import React from 'react';
import { Button, Stack, Image } from 'react-bootstrap';
import ConfirmImage from '@assets/image/confirm-free-trial.svg';

import type { IConfirmProps } from '../activation.types';

const ConfirmTrial = ({ data, onClose, onConfirm }: IConfirmProps) => (
  <Stack className="align-items-center p-5 text-center">
    <Image src={ConfirmImage} className="max-w-md mb-14" />
    <h4 className="text-3xl mb-4">Konfirmasi Paket</h4>
    <p data-testid="confirm-trial-text">
      Apakah anda yakin mengaktifkan Free Trial storefront? Paket ini berlaku
      hingga <strong>{data?.storefront_expired_date}</strong> sejak diaktifkan.
    </p>

    <Stack direction="horizontal" gap={5} className="mt-9 flex">
      <Button variant="light" className="btn-block" onClick={onClose}>
        Batal
      </Button>
      <Button variant="primary" className="btn-block mt-0" onClick={onConfirm}>
        Konfirmasi
      </Button>
    </Stack>
  </Stack>
);

export default ConfirmTrial;
