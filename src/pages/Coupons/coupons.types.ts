import type { FormikErrors } from 'formik';
import type { ICategoriesStatus } from '@interfaces/customers';
import type { ICoupon } from '@interfaces/coupons';

export interface ITableActionProps {
  data: ICoupon;
  refetchData: () => void;
}

export enum ECouponType {
  DISCOUNT = 'discount',
  FREE_SHIPPING = 'free_shipping',
}

export enum ECouponDiscountType {
  NOMINAL = 'nominal',
  PERCENTAGE = 'percentage',
}

export enum ECouponLimitOption {
  LIMITED = 'limited',
  UNLIMITED = 'unlimited',
}

export enum ECouponStatus {
  ACTIVE = 'active',
  INACTIVE = 'inactive',
}

export interface ICouponFieldProps {
  values: ICoupon;
  errors: FormikErrors<ICoupon>;
  onChange: (
    field: string,
    value: string | string[] | number | Date | boolean | undefined,
    validate?: boolean,
  ) => Promise<void>;
}

export interface ICouponTargetFieldProps extends ICouponFieldProps {
  dataCategories?: ICategoriesStatus;
}

export interface ICouponCodeFieldProps extends ICouponFieldProps {
  initialCouponCode?: string;
}

export interface IGenerateFormValue {
  values: ICoupon;
  isForSubmit: boolean;
}
