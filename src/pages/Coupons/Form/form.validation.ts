import * as Yup from 'yup';
import dayjs from 'dayjs';
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore';

import { formatCurrency } from '@utils/number';
import { ECouponDiscountType } from '../coupons.types';

dayjs.extend(isSameOrBefore);

const MIN_MONEY_VALUE = 1;
const MIN_MONEY_ERROR_MESSAGE = `Min. ${formatCurrency(
  MIN_MONEY_VALUE,
)} dan tidak boleh 0`;
const MAX_MONEY_VALUE = 999999999;
const MAX_MONEY_ERROR_MESSAGE = `Maks. ${formatCurrency(MAX_MONEY_VALUE)}`;
export const MIN_DATE_ERROR_MESSAGE = 'Masa berlaku kupon sudah lewat';
export const COUPON_ALREADY_USED_ERROR_MESSAGE =
  'Kode kupon sedang digunakan. Gunakan kode lain.';

const getMinDate = () => dayjs().second(0).millisecond(0).valueOf();
const minDateValidation = (newValue) =>
  dayjs(getMinDate()).isSameOrBefore(dayjs(newValue as Date).second(0));

export const validationForm = Yup.object().shape({
  is_code_already_used: Yup.boolean(),
  code: Yup.string()
    .min(4, 'Min. 4-10 karakter huruf atau angka')
    .max(10, 'Min. 4-10 karakter huruf atau angka')
    .required('Kode kupon wajib diisi')
    .when('is_code_already_used', (is_code_already_used, schema) =>
      schema.test({
        test: () => !is_code_already_used,
        message: COUPON_ALREADY_USED_ERROR_MESSAGE,
      }),
    ),
  discount_type: Yup.string(),
  discount_value: Yup.number().when('discount_type', {
    is: ECouponDiscountType.NOMINAL,
    then: Yup.number()
      .min(MIN_MONEY_VALUE, MIN_MONEY_ERROR_MESSAGE)
      .max(MAX_MONEY_VALUE, MAX_MONEY_ERROR_MESSAGE)
      .required('Nominal wajib diisi'),
  }),
  discount_percentage: Yup.number().when('discount_type', {
    is: ECouponDiscountType.PERCENTAGE,
    then: Yup.number()
      .min(1, 'Tidak boleh kurang dari 1%')
      .max(100, 'Tidak boleh lebih dari 100%')
      .required('Persentase wajib diisi'),
  }),
  max_discount: Yup.number().when('discount_type', {
    is: ECouponDiscountType.PERCENTAGE,
    then: Yup.number()
      .min(MIN_MONEY_VALUE, MIN_MONEY_ERROR_MESSAGE)
      .max(MAX_MONEY_VALUE, MAX_MONEY_ERROR_MESSAGE)
      .required('Maksimum potongan wajib diisi'),
  }),
  paid_limit: Yup.number()
    .max(
      MAX_MONEY_VALUE,
      `Min. ${formatCurrency(0)} hingga maks. ${formatCurrency(
        MAX_MONEY_VALUE,
      )}`,
    )
    .required('Minimal pembelian wajib diisi'),
  is_limited_quota: Yup.boolean(),
  quota: Yup.number().when('is_limited_quota', {
    is: true,
    then: Yup.number()
      .min(1, 'Jumlah kupon min. 1')
      .required('Jumlah kupon wajib diisi'),
  }),
  is_limited_attempt_usage: Yup.boolean(),
  attempt_usage: Yup.number().when('is_limited_attempt_usage', {
    is: true,
    then: Yup.number()
      .min(1, 'Batas pemakaian setiap akun min. 1')
      .required('Limit pemakaian tiap akun wajib diisi'),
  }),
  start_date: Yup.date()
    .test('minDateValidation', MIN_DATE_ERROR_MESSAGE, minDateValidation)
    .required('Masa berlaku wajib diisi'),
  end_date: Yup.date().when('start_date', () => {
    return Yup.date()
      .test('minDateValidation', MIN_DATE_ERROR_MESSAGE, minDateValidation)
      .required('Masa berlaku wajib diisi');
  }),
  target_customer_category: Yup.array().min(
    1,
    'Harus pilih min. 1 kategori customer',
  ),
  status: Yup.string(),
});
