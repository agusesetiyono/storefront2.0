import React from 'react';
import { Form, Row, Col } from 'react-bootstrap';
import dayjs from 'dayjs';
import isToday from 'dayjs/plugin/isToday';

import Datepicker from '@components/Datepicker';
import { formatDate } from '@utils/datetime';

import { ICouponFieldProps } from '@pages/Coupons/coupons.types';

dayjs.extend(isToday);

const Period = ({
  values: { start_date, end_date },
  errors,
  onChange,
}: ICouponFieldProps) => {
  let startTime = start_date ? new Date(start_date) : undefined;
  let endTime = end_date ? new Date(end_date) : undefined;

  const datetimeRangeOptions = {
    allowInvalidPreload: true,
    enableTime: true,
    time_24hr: true,
    defaultHour: new Date().getHours(),
    defaultMinute: new Date().getMinutes(),
    formatDate: (date: Date) => formatDate(date, 'DD-MM-YYYY HH:mm'),
  };

  const getMinTime = (date) =>
    dayjs(date as Date).isToday() || !date ? new Date().getTime() : undefined;

  const onChangeDate = async (field: string, date: Date) => {
    if (field === 'start_date') {
      startTime = date;
      if (!endTime || date > endTime) {
        void onChange('end_date', undefined, false);
      }
    } else if (field === 'end_date') {
      endTime = date;
    }
    await onChange(field, date?.toISOString());
  };

  return (
    <Form.Group as={Row} className="mb-10">
      <Form.Label htmlFor="duration" column sm={4} xl={3}>
        Masa Berlaku
        <small>Masa berlaku kupon</small>
      </Form.Label>
      <Col sm={6} xl={5}>
        <Row>
          <Col sm={6}>
            <Datepicker
              id="start_date"
              name="start_date"
              smallLabel="Mulai"
              options={{
                ...datetimeRangeOptions,
                minDate: 'today',
                minTime: getMinTime(startTime),
              }}
              value={startTime}
              onChange={([date]) => {
                void onChangeDate('start_date', date);
              }}
              isInvalid={!!errors.start_date}
              errorMessage={errors.start_date}
              hasValidation
            />
          </Col>
          <Col sm={6} className="pl-0">
            <Datepicker
              id="end_date"
              name="end_date"
              smallLabel="Selesai"
              options={{
                ...datetimeRangeOptions,
                minDate: startTime || 'today',
                minTime: getMinTime(endTime),
              }}
              value={endTime}
              onChange={([date]) => {
                void onChangeDate('end_date', date);
              }}
              isInvalid={!!errors.end_date}
              errorMessage={errors.end_date}
              hasValidation
            />
          </Col>
        </Row>
      </Col>
    </Form.Group>
  );
};

export default Period;
