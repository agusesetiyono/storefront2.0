import React from 'react';
import { Form, Row, Col } from 'react-bootstrap';

import InputNumber from '@components/InputNumber';

import {
  ECouponDiscountType,
  ICouponFieldProps,
} from '@pages/Coupons/coupons.types';

const Discount = ({
  values: {
    discount_type,
    discount_value,
    discount_percentage,
    max_discount,
    paid_limit,
  },
  errors,
  onChange,
}: ICouponFieldProps) => {
  const isDiscountValue = discount_type === ECouponDiscountType.NOMINAL;

  return (
    <>
      <Form.Group as={Row} className="mb-10">
        <Form.Label htmlFor="discount_type" column sm={4} xl={3}>
          Potongan atau Diskon
          <small>
            Besar potongan harga atau persentase diskon yang akan diberikan
            kepada customer
          </small>
        </Form.Label>
        <Col sm={5} xl={4}>
          <Row>
            <Col sm={4}>
              <Form.Select
                size="lg"
                name="discount_type"
                value={discount_type}
                onChange={(e) => void onChange('discount_type', e.target.value)}
                data-testid="select-discount-type"
              >
                <option value={ECouponDiscountType.NOMINAL}>Rp</option>
                <option value={ECouponDiscountType.PERCENTAGE}>%</option>
              </Form.Select>
            </Col>
            <Col sm={isDiscountValue ? 8 : 5} className="pl-0">
              {isDiscountValue ? (
                <InputNumber
                  id="discount_value"
                  data-testid="discount-value-input"
                  name="discount_value"
                  prepend="Rp"
                  value={discount_value}
                  onChangeNumber={(value) =>
                    void onChange('discount_value', value)
                  }
                  isInvalid={!!errors.discount_value}
                  errorMessage={errors.discount_value}
                  hasValidation
                />
              ) : (
                <InputNumber
                  id="discount_percentage"
                  data-testid="discount-percentage-input"
                  name="discount_percentage"
                  className="discount-percentage"
                  append="%"
                  value={discount_percentage}
                  onChangeNumber={(value) =>
                    void onChange('discount_percentage', value)
                  }
                  maxValue={9999}
                  isInvalid={!!errors.discount_percentage}
                  errorMessage={errors.discount_percentage}
                  hasValidation
                />
              )}
            </Col>
          </Row>
          {!isDiscountValue && (
            <Row className="mt-4">
              <Col sm={4} className="text-right">
                Maksimal potongan
              </Col>
              <Col sm={8} className="pl-0">
                <InputNumber
                  name="max_discount"
                  data-testid="max-discount-input"
                  prepend="Rp"
                  value={max_discount}
                  onChangeNumber={(value) =>
                    void onChange('max_discount', value)
                  }
                  isInvalid={!!errors.max_discount}
                  errorMessage={errors.max_discount}
                  hasValidation
                />
              </Col>
            </Row>
          )}
        </Col>
      </Form.Group>

      <Form.Group as={Row} className="mb-10">
        <Form.Label htmlFor="paid_limit" column sm={4} xl={3}>
          Minimum Pembelian
          <small>
            Minimum total harga produk untuk bisa menggunakan kupon. Kupon baru
            bisa dipakai jika minimum tercapai.
          </small>
        </Form.Label>
        <Col sm={5} xl={4}>
          <InputNumber
            id="paid_limit"
            name="paid_limit"
            prepend="Rp"
            value={paid_limit}
            onChangeNumber={(value) => void onChange('paid_limit', value)}
            isInvalid={!!errors.paid_limit}
            errorMessage={errors.paid_limit}
            hasValidation
          />
        </Col>
      </Form.Group>
    </>
  );
};

export default Discount;
