import React, { useEffect } from 'react';
import { Form, Row, Col } from 'react-bootstrap';
import useDebounceEffect from '@hooks/useDebounceEffect';
import { useGetCouponByCode } from '@hooks/api/coupons';

import type { ICouponCodeFieldProps } from '@pages/Coupons/coupons.types';

const Code = ({
  values: { code, is_code_already_used },
  errors,
  onChange,
  initialCouponCode,
}: ICouponCodeFieldProps) => {
  const onCodeChange = async (e) => {
    const value = e.target.value;
    const code: string = value.replace(/[\W_]+/g, '');
    await onChange('code', code?.toUpperCase());
  };

  useEffect(() => {
    if (is_code_already_used !== undefined) {
      void onChange('code', code);
    }
  }, [is_code_already_used]);

  useDebounceEffect(() => {
    if (code.length >= 4) {
      if (code === initialCouponCode) return;

      useGetCouponByCode(code)
        .then(async (data) => {
          const isAlreadyUsed = data.data.data.length > 0;
          await onChange('is_code_already_used', isAlreadyUsed);
        })
        .catch(async () => {
          await onChange('is_code_already_used', false);
        });
    } else {
      void onChange('is_code_already_used', false);
    }
  }, [code]);

  return (
    <Form.Group as={Row} className="mb-10">
      <Form.Label htmlFor="code" column sm={4} xl={3}>
        Kode Kupon
        <small>
          Buat kode kupon yang kamu inginkan dalam 4-10 huruf atau angka
        </small>
      </Form.Label>
      <Col sm={5} xl={4}>
        <Form.Control
          id="code"
          type="text"
          maxLength={10}
          name="code"
          value={code}
          onChange={(e) => void onCodeChange(e)}
          isInvalid={!!errors.code}
        />
        <Form.Control.Feedback type="invalid" className="h-5 block">
          {errors.code}
        </Form.Control.Feedback>
      </Col>
    </Form.Group>
  );
};

export default Code;
