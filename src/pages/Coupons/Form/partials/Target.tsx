import React from 'react';
import { Form, Row, Col } from 'react-bootstrap';

import { ICouponTargetFieldProps } from '@pages/Coupons/coupons.types';

const Target = ({
  values: { target_customer_category },
  errors,
  onChange,
  dataCategories,
}: ICouponTargetFieldProps) => {
  const onTargetChange = async (target: string, checked: boolean) => {
    let targets: string[] = [];
    if (checked) {
      targets = [...target_customer_category, target];
    } else {
      targets = target_customer_category;
      const index = targets.indexOf(target);
      if (index !== -1) {
        targets.splice(index, 1);
      }
    }

    await onChange('target_customer_category', targets);
  };

  return (
    <Form.Group as={Row} className="mb-10">
      <Form.Label htmlFor="code" column sm={4} xl={3}>
        Target Kupon
        <small>
          Siapa saja customer yang bisa dapat kupon? Bisa centang lebih dari
          satu
        </small>
      </Form.Label>
      <Col sm={5} xl={4}>
        {dataCategories?.data?.map((target) => (
          <Form.Check
            id={`category-${target.code}`}
            key={`category-${target.code}`}
            name="target_customer_category"
            label={target.name}
            checked={target_customer_category.includes(target.code)}
            value={target.code}
            onChange={(e) => void onTargetChange(target.code, e.target.checked)}
            isInvalid={!!errors.target_customer_category}
          />
        ))}
        <Form.Control.Feedback type="invalid" className="h-5 block">
          {errors.target_customer_category}
        </Form.Control.Feedback>
      </Col>
    </Form.Group>
  );
};

export default Target;
