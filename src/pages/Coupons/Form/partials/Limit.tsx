import React from 'react';
import { Form, Row, Col } from 'react-bootstrap';

import InputNumber from '@components/InputNumber';

import {
  ECouponLimitOption,
  ICouponFieldProps,
} from '@pages/Coupons/coupons.types';

const Limit = ({
  values: { is_limited_attempt_usage, attempt_usage },
  errors,
  onChange,
}: ICouponFieldProps) => {
  const limitOptions = [
    ECouponLimitOption.UNLIMITED,
    ECouponLimitOption.LIMITED,
  ];

  return (
    <Form.Group as={Row}>
      <Form.Label htmlFor="radioAttemptUsage" column sm={4} xl={3}>
        Batas Pemakaian Tiap Akun
        <small>Maksimal batas penggunaan kupon untuk satu akun</small>
      </Form.Label>
      <Col sm={5} xl={4}>
        <div className="coupon-form-check-container-with-text-box">
          {limitOptions.map((option) => {
            const isUnlimited = option === ECouponLimitOption.UNLIMITED;
            const checked = isUnlimited
              ? !is_limited_attempt_usage
              : is_limited_attempt_usage;
            const label = isUnlimited ? (
              'Unlimited'
            ) : is_limited_attempt_usage ? (
              <InputNumber
                name="attempt_usage"
                data-testid="attempt-usage-limited-input"
                size="sm"
                value={attempt_usage}
                onChangeNumber={(value) =>
                  void onChange('attempt_usage', value)
                }
                isInvalid={!!errors.attempt_usage}
                errorMessage={errors.attempt_usage}
                hasValidation
              />
            ) : (
              'Tentukan jumlah'
            );

            return (
              <Form.Check
                data-testid={`attempt-usage-option-${option}`}
                className="coupon-form-check"
                label={label}
                id={`attempt_usage-${option}`}
                key={`attempt_usage-${option}`}
                checked={checked}
                name="radioAttemptUsage"
                type="radio"
                onChange={() =>
                  void onChange('is_limited_attempt_usage', !isUnlimited)
                }
              />
            );
          })}
        </div>
      </Col>
    </Form.Group>
  );
};

export default Limit;
