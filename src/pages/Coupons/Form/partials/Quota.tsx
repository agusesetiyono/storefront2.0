import React from 'react';
import { Form, Row, Col } from 'react-bootstrap';

import InputNumber from '@components/InputNumber';

import {
  ECouponLimitOption,
  ICouponFieldProps,
} from '@pages/Coupons/coupons.types';

const Quota = ({
  values: { is_limited_quota, quota },
  errors,
  onChange,
}: ICouponFieldProps) => {
  const limitOptions = [
    ECouponLimitOption.UNLIMITED,
    ECouponLimitOption.LIMITED,
  ];

  return (
    <Form.Group as={Row}>
      <Form.Label htmlFor="radioQuota" column sm={4} xl={3}>
        Jumlah Kupon
        <small>Total jumlah kupon yang disediakan</small>
      </Form.Label>
      <Col sm={5} xl={4}>
        <div className="coupon-form-check-container-with-text-box">
          {limitOptions.map((option) => {
            const isUnlimited = option === ECouponLimitOption.UNLIMITED;
            const checked = isUnlimited ? !is_limited_quota : is_limited_quota;
            const label = isUnlimited ? (
              'Unlimited'
            ) : is_limited_quota ? (
              <InputNumber
                name="quota"
                data-testid="quota-limited-input"
                size="sm"
                value={quota}
                onChangeNumber={(value) => void onChange('quota', value)}
                isInvalid={!!errors.quota}
                errorMessage={errors.quota}
                hasValidation
              />
            ) : (
              'Tentukan jumlah'
            );

            return (
              <Form.Check
                data-testid={`quota-option-${option}`}
                className="coupon-form-check"
                label={label}
                id={`quota-${option}`}
                key={`quota-${option}`}
                checked={checked}
                name="radioQuota"
                type="radio"
                onChange={() => void onChange('is_limited_quota', !isUnlimited)}
              />
            );
          })}
        </div>
      </Col>
    </Form.Group>
  );
};

export default Quota;
