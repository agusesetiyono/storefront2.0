import React, { useEffect, useState } from 'react';
import { useFormik } from 'formik';
import { Card, Form, Row, Col } from 'react-bootstrap';
import { useNavigate, useParams } from 'react-router-dom';

import { ICoupon } from '@interfaces/coupons';
import { useCreateCoupon } from '@hooks/api/coupons';
import { onSuccessMutate, onErrorMutate } from '@utils/reactQuery';
import { setSecondToZero } from '@utils/datetime';
import { sendDashboardTracker } from '@utils/tracker/dashboard';

import BackNavigation from '@components/BackNavigation';
import TabHeader from '@components/TabHeader';
import Badge from '@components/Badge';
import Button from '@components/Button';
import LoadingState from '@components/LoadingState';
import ErrorState from '@components/ErrorState';

import Code from './partials/Code';
import Discount from './partials/Discount';
import Limit from './partials/Limit';
import Period from './partials/Period';
import Quota from './partials/Quota';
import Target from './partials/Target';

import { validationForm } from './form.validation';

import { useGetCategories } from '@hooks/api/customers';
import { useGetCoupon, useUpdateCoupon } from '@hooks/api/coupons';

import {
  ECouponType,
  ECouponDiscountType,
  ECouponStatus,
  ICouponFieldProps,
  IGenerateFormValue,
} from '../coupons.types';

const CouponsForm = () => {
  const COUPONS_URL = '/settings/coupons';

  const params = useParams();
  const couponId = params.id;
  const navigate = useNavigate();
  const createCoupon = useCreateCoupon();
  const updateCoupon = useUpdateCoupon(couponId);
  const [initialCouponCode, setInitialCouponCode] = useState('');

  const generateFormValue = ({
    values: {
      type,
      code,
      discount_type,
      discount_value,
      discount_percentage,
      max_discount,
      paid_limit,
      quota,
      is_limited_quota,
      attempt_usage,
      is_limited_attempt_usage,
      start_date,
      end_date,
      target_customer_category,
      status,
    },
    isForSubmit,
  }: IGenerateFormValue): ICoupon => ({
    type,
    code,
    discount_type,
    ...(discount_type === ECouponDiscountType.NOMINAL
      ? { discount_value }
      : {
          discount_percentage,
          max_discount,
        }),
    paid_limit,
    ...(isForSubmit
      ? {
          quota: is_limited_quota ? quota : -1,
          attempt_usage: is_limited_attempt_usage ? attempt_usage : -1,
        }
      : {
          ...(quota === -1
            ? { is_limited_quota: false }
            : { is_limited_quota: true, quota }),
          ...(attempt_usage === -1
            ? { is_limited_attempt_usage: false }
            : { is_limited_attempt_usage: true, attempt_usage }),
        }),
    start_date: isForSubmit
      ? setSecondToZero(start_date as string)
      : new Date(start_date as string).toISOString(),
    end_date: isForSubmit
      ? setSecondToZero(end_date as string)
      : new Date(end_date as string).toISOString(),
    target_customer_category,
    status,
  });

  const {
    values,
    errors,
    isSubmitting,
    setFieldValue,
    setValues,
    handleSubmit,
    validateField,
  } = useFormik<ICoupon>({
    initialValues: {
      type: ECouponType.DISCOUNT,
      code: '',
      discount_type: ECouponDiscountType.NOMINAL,
      is_limited_quota: false,
      is_limited_attempt_usage: false,
      target_customer_category: [],
      status: ECouponStatus.INACTIVE,
    },
    validationSchema: validationForm,
    validateOnChange: false,
    onSubmit: (values) => {
      if (createCoupon.isLoading || updateCoupon.isLoading) return;

      (couponId ? updateCoupon : createCoupon).mutate(
        generateFormValue({ values, isForSubmit: true }),
        {
          onSuccess: (data) => {
            onSuccessMutate(data);
            navigate(COUPONS_URL);
            void sendDashboardTracker({
              actionType: `${couponId ? 'edit' : 'add'}_coupon_success`,
              actionDetails: `${data.data.data.id}`,
            });
          },
          onError: onErrorMutate,
        },
      );
    },
  });

  let isLoadingCouponDetail = false;
  if (couponId) {
    const { isLoading } = useGetCoupon(
      couponId,
      (res) => {
        if (!res) return;

        const { data } = res;
        void setValues(generateFormValue({ values: data, isForSubmit: false }));
        setInitialCouponCode(data.code as string);
      },
      () => {
        navigate('/settings/coupons');
      },
    );
    isLoadingCouponDetail = isLoading;
  }

  const {
    data: dataCategories,
    isLoading: isLoadingCategories,
    isError: isErrorGetCategories,
    refetch: refetchCategories,
  } = useGetCategories({
    onSuccess: (res) => {
      if (couponId) return;

      void setFieldValue('target_customer_category', [
        res.data?.[0]?.code || '',
      ]);
    },
  });

  const { type, status } = values;
  const isActive = status === ECouponStatus.ACTIVE;
  const typeOptions = [ECouponType.DISCOUNT, ECouponType.FREE_SHIPPING];

  useEffect(() => {
    if (!isSubmitting) return;
    if (Object.keys(errors).length > 0) {
      const elementName = Object.keys(errors)?.[0];
      const element = document.getElementsByName(elementName)?.[0];
      if (!element) return;

      const elementPosition = element?.getBoundingClientRect().top || 0;
      const scrollTop = document.documentElement.scrollTop;
      const headerHeight = 303; // Navbar + Storefront settings header height
      const offsetTop = 36; // Offset height from top of card content
      if (!elementName.includes('date')) {
        element.focus();
      }
      window.scrollTo({
        top: elementPosition + scrollTop - headerHeight - offsetTop,
        behavior: 'smooth',
      });
    }
  }, [isSubmitting, errors]);

  const onFieldChange = async (
    field: string,
    value: string | string[] | number | Date | boolean,
    validate = true,
  ) => {
    await setFieldValue(field, value);
    validate && (await validateField(field));
  };

  const onStatusChange = async (e) => {
    await onFieldChange(
      'status',
      e.target.checked ? ECouponStatus.ACTIVE : ECouponStatus.INACTIVE,
    );
  };

  const fieldProps: ICouponFieldProps = {
    values,
    errors,
    onChange: onFieldChange,
  };

  return (
    <Card className="p-6">
      {isLoadingCategories || isLoadingCouponDetail ? (
        <LoadingState className="pt-12" />
      ) : isErrorGetCategories || dataCategories?.data?.length === 0 ? (
        <ErrorState onRetry={() => void refetchCategories()} />
      ) : (
        <>
          <BackNavigation to={COUPONS_URL} />
          <TabHeader title={`${couponId ? 'Edit' : 'Tambah'} Kupon`} />

          <Form noValidate onSubmit={handleSubmit}>
            <Form.Group as={Row} className="mb-10">
              <Form.Label htmlFor="type" column sm={4} xl={3}>
                Jenis Kupon
              </Form.Label>
              <Col sm={5} xl={4}>
                {typeOptions.map((couponType) => (
                  <Form.Check
                    inline
                    id={couponType}
                    key={couponType}
                    checked={type === couponType}
                    label={
                      couponType === ECouponType.DISCOUNT
                        ? 'Diskon'
                        : 'Gratis ongkir'
                    }
                    name="radioType"
                    type="radio"
                    onChange={() => void setFieldValue('type', couponType)}
                  />
                ))}
              </Col>
            </Form.Group>

            <Code {...fieldProps} initialCouponCode={initialCouponCode} />
            <Discount {...fieldProps} />
            <Quota {...fieldProps} />
            <Limit {...fieldProps} />
            <Period {...fieldProps} />
            <Target {...fieldProps} dataCategories={dataCategories} />

            <Form.Group as={Row} className="mb-10">
              <Form.Label htmlFor="status" column sm={4} xl={3}>
                Status
                <small>Hanya bisa diaktifkan dalam masa berlaku kupon</small>
              </Form.Label>
              <Col sm={5} xl={4}>
                <div className="text-center w-fit">
                  <Form.Check
                    id="status"
                    type="switch"
                    checked={isActive}
                    onChange={(e) => void onStatusChange(e)}
                  />
                  <Badge variant={isActive ? 'success' : 'default'}>
                    {isActive ? 'Aktif' : 'Nonaktif'}
                  </Badge>
                </div>
              </Col>
            </Form.Group>
            <hr className="my-6" />
            <Button
              variant="primary"
              type="submit"
              size="sm"
              isLoading={createCoupon.isLoading || updateCoupon.isLoading}
              spinnerClass="my-1"
            >
              {`${couponId ? 'Simpan' : 'Tambah'} Kupon`}
            </Button>
          </Form>
        </>
      )}
    </Card>
  );
};

export default CouponsForm;
