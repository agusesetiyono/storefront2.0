import React from 'react';
import { useState } from 'react';
import { Card, Stack, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import EmptyState from '@components/EmptyState';
import ErrorState from '@components/ErrorState';
import Table from '@components/Table';
import TabHeader from '@components/TabHeader';
import Badge from '@components/Badge';
import TableAction from './partials/TableAction';

import { formatDate, dateWithTime } from '@utils/datetime';
import { formatCurrency } from '@utils/number';
import { sendDashboardTracker } from '@utils/tracker/dashboard';

import { useGetCoupons } from '@hooks/api/coupons';

import type { ITableColumn } from '@components/Table/table.types';
import type { ICoupon } from '@interfaces/coupons';

import EmptyIllustration from '@assets/image/empty-coupons.svg';

const Coupons = () => {
  const countPerPage = 10;
  const [offset, setOffset] = useState<number>(0);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const { isFetching, isError, coupons, metaCoupons, refetchGetCoupons } =
    useGetCoupons({
      limit: countPerPage,
      offset: offset,
    });
  const isExpired = (endDate: string) =>
    Boolean(new Date(endDate) < new Date());

  const tableColumns: ITableColumn<ICoupon>[] = [
    {
      key: 'code',
      dataIndex: 'code',
      title: 'Kode Kupon',
      width: 111,
      render: (_value, record) => (
        <>
          {record.code.length > 0 && (
            <Badge
              variant="default"
              disabled={isExpired(record.end_date || '')}
              className="badge-coupon"
            >
              {record.code}
            </Badge>
          )}
        </>
      ),
    },
    {
      key: 'discount',
      dataIndex: 'discount',
      title: 'Potongan/Diskon',
      width: 150,
      render: (_value, record) => (
        <div className={isExpired(record.end_date || '') ? 'disabled' : ''}>
          <div className="font-bold">{record.discount}%</div>
          <div>Maks. {formatCurrency(record.nominal_max || 0)}</div>
        </div>
      ),
    },
    {
      key: 'paid_limit',
      dataIndex: 'paid_limit',
      title: 'Minimal Pembelian',
      width: 152,
      render: (value, record) => (
        <span className={isExpired(record.end_date || '') ? 'disabled' : ''}>
          {formatCurrency(record.paid_limit || 0)}
        </span>
      ),
    },
    {
      key: 'start_date',
      dataIndex: 'start_date',
      title: 'Masa Berlaku',
      width: 121,
      render: (_value, record) => (
        <span className={isExpired(record.end_date || '') ? 'disabled' : ''}>
          {formatDate(record.start_date || '', dateWithTime)} -{' '}
          {formatDate(record.end_date || '', dateWithTime)}
        </span>
      ),
    },
    {
      key: 'target_customer_category',
      dataIndex: 'target_customer_category',
      title: 'Kategori Customer',
      width: 249,
      render: (_value, record) => (
        <>
          {record.target_customer_category.map((target) => (
            <Badge
              key={`${record.id}-${target}`}
              variant="default"
              className="ml-2 mb-2"
              disabled={isExpired(record.end_date || '')}
              isEllipsis
            >
              {target}
            </Badge>
          ))}
        </>
      ),
    },
    {
      key: 'id',
      dataIndex: 'id',
      title: 'Status',
      width: 112,
      render: (_value, record) => (
        <TableAction data={record} refetchData={refetchGetCoupons} />
      ),
    },
  ];

  const onChangePagination = (page: number) => {
    const offset = (page - 1) * (metaCoupons?.limit || 0) || 0;
    setOffset(offset);
    setCurrentPage(page);
    window.scrollTo(0, 0);
  };
  const metaData = {
    current: currentPage,
    pageSize: countPerPage,
    total: metaCoupons?.total || 0,
  };
  return (
    <Card className="px-24px py-24px">
      <Card.Body>
        <TabHeader
          title="Pengaturan Kupon"
          action={[
            <Link
              to="create"
              onClick={() =>
                void sendDashboardTracker({ actionType: 'add_coupon' })
              }
            >
              <Button className="py-2 text-light">Tambah Kupon</Button>
            </Link>,
          ]}
        />
        <Stack className="mt-2 overflow-x-auto overflow-y-hidden">
          <Table
            className="border-0"
            size="lg"
            data={coupons}
            columns={tableColumns}
            bordered
            isFixed
            isLoading={isFetching}
            isErrorFetch={isError}
            isPagination={(metaCoupons?.total || 0) > countPerPage}
            meta={metaData}
            handleChangePagination={onChangePagination}
            errorState={
              <ErrorState
                title="Gagal mengambil data kupon"
                content="Cobalah beberapa saat lagi, atau lakukan refresh halaman."
                onRetry={refetchGetCoupons}
              />
            }
            emptyState={
              <EmptyState
                title="Belum ada kupon yang dibuat"
                image={EmptyIllustration}
                content={
                  <p>
                    Jualan jadi makin menarik dengan kupon. <br />
                    Yuk klik <b>Tambah Kupon</b> di kanan atas!
                  </p>
                }
              />
            }
          />
        </Stack>
      </Card.Body>
    </Card>
  );
};

export default Coupons;
