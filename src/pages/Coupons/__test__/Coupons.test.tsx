import axios from '@mocks/axios';
import '@testing-library/jest-dom';
import { screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';

import { renderWithRoute, API_URL } from '@utils/test-helper';
import toast from '@utils/toast';
import { sendTracker } from '@utils/tracker';

import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';
import {
  GET_COUPONS_SUCCESS_RESPONSE,
  GET_COUPONS_PAGE_2_SUCCESS_RESPONSE,
} from '@mocks/api/coupons';

jest.mock('@utils/toast', () => ({
  success: jest.fn(),
  danger: jest.fn(),
}));

axios.onGet(`${API_URL.storefronts}/status`).reply(
  200,
  getMockSubscriptionsResponse({
    type: 'platinum',
    expiredDate: '2027-05-01T00:00:00+07:00',
  }),
);

let user: UserEvent;
beforeEach(() => {
  user = userEvent.setup({ delay: null });
  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));

  localStorage.setItem('onboardingStep', '5'); // already finished onboarding

  axios.onGet(API_URL.coupons).reply(200, GET_COUPONS_SUCCESS_RESPONSE);
});

afterEach(() => {
  jest.useRealTimers();
  jest.clearAllMocks();
  localStorage.clear();
});

describe('Coupons Page', () => {
  const route = '/storefront-editor/settings/coupons';
  const defaultTrackerParams = {
    evn: 'ngorder_sf_dashboard_actions',
    active_tab: 'kupon',
  };
  
  it('Should match snapshot', async () => {
    const { container } = renderWithRoute({ route });

    expect(await screen.findByText('Pengaturan Kupon')).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });

  it('Should render sections correctly', async () => {
    renderWithRoute({ route });

    // Manage Pages section
    expect(await screen.findByText('Pengaturan Kupon')).toBeInTheDocument();
    const addButton = screen.getByRole('button', { name: 'Tambah Kupon' });
    expect(addButton).toBeInTheDocument();

    //table fetch data correctly
    expect(await screen.findByText('BIGSALE20')).toBeInTheDocument();
    expect(screen.getAllByTestId(/switch-status-coupon/i).length).toEqual(10);
    expect(screen.getByTitle('1')).toHaveClass(
      'rc-pagination-item rc-pagination-item-1 rc-pagination-item-active',
    );
    expect(screen.getByTitle('2')).toHaveClass(
      'rc-pagination-item rc-pagination-item-2',
    );

    // refetch with success response
    axios
      .onGet(API_URL.coupons, { params: { offset: 10, limit: 10 } })
      .reply(200, GET_COUPONS_PAGE_2_SUCCESS_RESPONSE);

    // click pagination
    await user.click(screen.getByTitle('2'));
    //table fetch data correctly
    expect(screen.getByTitle('2')).toHaveClass(
      'rc-pagination-item rc-pagination-item-2 rc-pagination-item-active',
    );
    expect(screen.getByTitle('1')).toHaveClass(
      'rc-pagination-item rc-pagination-item-1',
    );

    await user.click(addButton);
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'add_coupon',
    });
  });

  it('Should render empty state correctly', async () => {
    renderWithRoute({ route });

    // refetch with empty data success response
    axios.onGet(API_URL.coupons).reply(200, {
      data: [],
      meta: {
        http_status: 200,
        offset: 0,
        limit: 10,
        total: 0,
      },
    });

    //table fetch empty state correctly
    await waitFor(
      () =>
        expect(
          screen.getByText('Belum ada kupon yang dibuat'),
        ).toBeInTheDocument(),
      { timeout: 10000 },
    );
  });

  it('Should render error state correctly', async () => {
    axios.onGet(API_URL.coupons).reply(500);
    renderWithRoute({ route });

    await waitFor(
      () =>
        expect(
          screen.getByText('Gagal mengambil data kupon'),
        ).toBeInTheDocument(),
      { timeout: 10000 },
    );

    // refetch with success response
    axios.onGet(API_URL.coupons).reply(200, GET_COUPONS_SUCCESS_RESPONSE);

    await user.click(screen.getByRole('button', { name: 'Coba Lagi' }));

    //table fetch data correctly
    expect(await screen.findByText('BIGSALE20')).toBeInTheDocument();
    expect(screen.getAllByTestId(/switch-status-coupon/i).length).toEqual(10);
  });

  describe('Change status action', () => {
    const coupon = GET_COUPONS_SUCCESS_RESPONSE.data[0]

    it('Change status successfully', async () => {
      renderWithRoute({ route })
      await screen.findByText('Pengaturan Kupon')
      const message = coupon.status === 'active' ? 'Gagal mengaktifkan kupon. Silakan coba lagi.' : 'Berhasil mengaktifkan kupon'
      axios
        .onPatch(`${API_URL.coupons}/${coupon.id}/status`)
        .replyOnce(200, { message, data: { id: coupon.id } });   
      
      const statusSwitch = await screen.findByTestId(`switch-status-coupon-${coupon.id}`);
      expect(statusSwitch.hasAttribute('checked')).toEqual(coupon.status === 'active')

      await user.click(statusSwitch);
      await waitFor(() => {
        expect(toast.success).toHaveBeenLastCalledWith(message)
      });
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'toggle_coupon',
        action_details: '4'
      });
    });

    it('Disabled state: cannot change status', async () => {    
      const c = GET_COUPONS_SUCCESS_RESPONSE.data[1]  
      renderWithRoute({ route })
      await screen.findByText('Pengaturan Kupon')

      const statusSwitch = await screen.findByTestId(`switch-status-coupon-${c.id}`);
      expect(statusSwitch).toHaveAttribute('disabled')

      await user.click(statusSwitch)
      await waitFor(() => {
        expect(toast.success).not.toHaveBeenCalled()
        expect(toast.danger).not.toHaveBeenCalled()
      })
    })

    it('Change status failed', async () => {
      renderWithRoute({ route })
      await screen.findByText('Pengaturan Kupon')
      const message = 'Gagal mengaktifkan kupon. Silakan coba lagi.'
      axios.onPatch(`${API_URL.coupons}/${coupon.id}/status`).replyOnce(500, {
        errors: [
          {
            message,
          },
        ],
        meta: {
          http_status: 500,
        },
      });
      
      const statusSwitch = await screen.findByTestId(`switch-status-coupon-${coupon.id}`);
      expect(statusSwitch.hasAttribute('checked')).toEqual(coupon.status === 'active')

      await user.click(statusSwitch)
      await waitFor(() => {
        expect(toast.danger).toHaveBeenCalledWith(message)
      })
    })
  })
});
