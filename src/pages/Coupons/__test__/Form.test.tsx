import axios from '@mocks/axios';
import '@testing-library/jest-dom';
import {
  screen,
  waitFor,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';

import { renderWithRoute, API_URL, getLastCalled } from '@utils/test-helper';
import toast from '@utils/toast';
import { sendTracker } from '@utils/tracker';

import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';
import { GET_CATEGORIES_SUCCESS_RESPONSE } from '@mocks/api/customers';
import { GET_COUPON_SUCCESS_MESSAGE } from '@mocks/api/coupons';
import { ECouponDiscountType } from '@pages/Coupons/coupons.types';
import { MIN_DATE_ERROR_MESSAGE } from '../Form/form.validation';

jest.mock('@utils/toast', () => ({
  success: jest.fn(),
  danger: jest.fn(),
}));

axios.onGet(`${API_URL.storefronts}/status`).reply(
  200,
  getMockSubscriptionsResponse({
    type: 'platinum',
    expiredDate: '2022-05-01T00:00:00+07:00',
  }),
);

let user: UserEvent;
beforeEach(() => {
  user = userEvent.setup({ delay: null });
  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));

  localStorage.setItem('onboardingStep', '5'); // already finished onboarding

  axios
    .onGet(API_URL.customerCategories)
    .reply(200, GET_CATEGORIES_SUCCESS_RESPONSE);
});

afterEach(() => {
  jest.useRealTimers();
  jest.clearAllMocks();
  localStorage.clear();
});

describe('Coupon Form', () => {
  const apiErrorMessage = 'Gagal menambah kupon. Silakan coba lagi.';
  const apiSuccessMessage = 'Berhasil menambah kupon.';
  const defaultTrackerParams = {
    evn: 'ngorder_sf_dashboard_actions',
    active_tab: 'kupon',
  };

  describe('Create', () => {
    const validateMoneyField = async (
      element: HTMLElement,
      requiredErrorMessage: string,
    ) => {
      const maxMoneyErrorMessage = 'Maks. Rp 999.999.999';
      const minMoneyErrorMessage = 'Min. Rp 1 dan tidak boleh 0';

      await user.type(element, '0');
      expect(await screen.findByText(minMoneyErrorMessage)).toBeInTheDocument();
      await user.type(element, '{backspace}');
      expect(await screen.findByText(requiredErrorMessage)).toBeInTheDocument();
      await user.type(element, '9999999999');
      expect(await screen.findByText(maxMoneyErrorMessage)).toBeInTheDocument();
      await user.type(element, '{backspace}');
      await waitForElementToBeRemoved(() =>
        screen.getByText(maxMoneyErrorMessage),
      );
      expect(element).toHaveValue('999.999.999');
    };

    const route = '/storefront-editor/settings/coupons/create';
    it('Should match snapshot', async () => {
      const { container } = renderWithRoute({ route });

      expect(
        await screen.findByRole('button', { name: 'Tambah Kupon' }),
      ).toBeInTheDocument();
      expect(container).toMatchSnapshot();
    });

    it('Should show error state when failed to load category', async () => {
      axios.onGet(API_URL.customerCategories).reply(500);
      renderWithRoute({ route });

      const retryButton = await screen.findByRole('button', {
        name: 'Coba Lagi',
      });
      expect(retryButton).toBeInTheDocument();

      axios
        .onGet(API_URL.customerCategories)
        .reply(200, GET_CATEGORIES_SUCCESS_RESPONSE);
      await user.click(retryButton);
      expect(
        await screen.findByRole('button', { name: 'Tambah Kupon' }),
      ).toBeInTheDocument();
    });

    it('Should select discount type', async () => {
      renderWithRoute({ route });

      const discountRadio = await screen.findByLabelText('Diskon');
      expect(discountRadio).toBeChecked();
      const shippingRadio = screen.getByLabelText('Gratis ongkir');
      expect(shippingRadio).not.toBeChecked();
      await user.click(shippingRadio);
      expect(discountRadio).not.toBeChecked();
      expect(shippingRadio).toBeChecked();
    });

    it('Should validate coupon code', async () => {
      const minCharErrorMessage = 'Min. 4-10 karakter huruf atau angka';
      const alreadyUsedErrorMessage =
        'Kode kupon sedang digunakan. Gunakan kode lain.';
      axios
        .onGet(API_URL.coupons)
        .replyOnce(200, {
          data: [{ id: 2159, code: 'KPN11' }],
        })
        .onGet(API_URL.coupons)
        .replyOnce(500, {})
        .onGet(API_URL.coupons)
        .replyOnce(200, { data: [] });

      renderWithRoute({ route });

      const couponCodeInput = await screen.findByLabelText(/Kode Kupon/i);
      await user.type(couponCodeInput, 'KPN');
      expect(await screen.findByText(minCharErrorMessage)).toBeInTheDocument();
      await user.type(couponCodeInput, '1');
      jest.runAllTimers();
      await waitForElementToBeRemoved(() =>
        screen.getByText(minCharErrorMessage),
      );
      await user.type(couponCodeInput, '2');
      jest.runAllTimers();
      expect(
        await screen.findByText(alreadyUsedErrorMessage),
      ).toBeInTheDocument();
      await user.type(couponCodeInput, '1');
      jest.runAllTimers();
      await waitForElementToBeRemoved(() =>
        screen.getByText(alreadyUsedErrorMessage),
      );
      await user.type(couponCodeInput, '1');
      jest.runAllTimers();
      await user.type(
        couponCodeInput,
        '{backspace}{backspace}{backspace}{backspace}',
      );
      jest.runAllTimers();
      expect(await screen.findByText(minCharErrorMessage)).toBeInTheDocument();
    });

    it('Should validate discount mode (value/percentage)', async () => {
      const maxPercentageErrorMessage = 'Tidak boleh lebih dari 100%';
      renderWithRoute({ route });

      // Discount
      const discountValueInput = await screen.findByTestId(
        'discount-value-input',
      );
      await validateMoneyField(discountValueInput, 'Nominal wajib diisi');

      // Percentage
      await user.selectOptions(screen.getByTestId('select-discount-type'), [
        ECouponDiscountType.PERCENTAGE,
      ]);
      const discountPercentageInput = await screen.findByTestId(
        'discount-percentage-input',
      );
      await user.type(discountPercentageInput, '0');
      expect(
        await screen.findByText('Tidak boleh kurang dari 1%'),
      ).toBeInTheDocument();
      await user.type(discountPercentageInput, '{backspace}');
      expect(
        await screen.findByText('Persentase wajib diisi'),
      ).toBeInTheDocument();
      await user.type(discountPercentageInput, '101');
      expect(
        await screen.findByText(maxPercentageErrorMessage),
      ).toBeInTheDocument();
      await user.type(discountPercentageInput, '{backspace}');
      await waitForElementToBeRemoved(() =>
        screen.getByText(maxPercentageErrorMessage),
      );
      expect(discountPercentageInput).toHaveValue('10');

      const maxDiscountInput = screen.getByTestId('max-discount-input');
      await validateMoneyField(
        maxDiscountInput,
        'Maksimum potongan wajib diisi',
      );
    });

    it('Should validate paid limit', async () => {
      const minPurchaseErrorMessage = 'Minimal pembelian wajib diisi';
      renderWithRoute({ route });

      const paidLimitInput = await screen.findByLabelText(/Minimum Pembelian/i);
      await user.type(paidLimitInput, '9999999999');
      expect(
        await screen.findByText('Min. Rp 0 hingga maks. Rp 999.999.999'),
      ).toBeInTheDocument();
      await user.clear(paidLimitInput);
      expect(
        await screen.findByText(minPurchaseErrorMessage),
      ).toBeInTheDocument();
      await user.type(paidLimitInput, '0');
      await waitForElementToBeRemoved(() =>
        screen.getByText(minPurchaseErrorMessage),
      );
      expect(paidLimitInput).toHaveValue('0');
    });

    it.each([
      ['quota', 'Jumlah kupon min. 1', 'Jumlah kupon wajib diisi'],
      [
        'attempt-usage',
        'Batas pemakaian setiap akun min. 1',
        'Limit pemakaian tiap akun wajib diisi',
      ],
    ])(
      'Should validate %s',
      async (type, minErrorMessage, requiredErrorMessage) => {
        renderWithRoute({ route });
        const unlimitedOptionId = `${type}-option-unlimited`;
        const limitedOptionId = `${type}-option-limited`;
        const limitedInputId = `${type}-limited-input`;

        const unlimitedOption = await screen.findByTestId(unlimitedOptionId);
        expect(unlimitedOption).toBeChecked();
        const limitedOption = screen.getByTestId(limitedOptionId);
        expect(limitedOption).not.toBeChecked();

        await user.click(limitedOption);
        let limitedInput = screen.getByTestId(limitedInputId);
        await user.type(limitedInput, '0');
        expect(await screen.findByText(minErrorMessage)).toBeInTheDocument();

        await user.type(limitedInput, '{backspace}');
        expect(
          await screen.findByText(requiredErrorMessage),
        ).toBeInTheDocument();

        await user.type(limitedInput, '9999');
        await user.click(unlimitedOption);
        expect(screen.queryByTestId(limitedInputId)).not.toBeInTheDocument();

        await user.click(limitedOption);
        limitedInput = screen.getByTestId(limitedInputId);
        expect(limitedInput).toHaveValue('9.999');
      },
    );

    it('Should validate period correctly', async () => {
      renderWithRoute({ route });

      const [startDateInput, endDateInput] =
        await screen.findAllByPlaceholderText('DD-MM-YYYY 00:00');

      await user.click(startDateInput);
      await user.click(screen.getAllByLabelText('02-04-2022 00:00')[0]);
      expect(startDateInput).toHaveValue('02-04-2022 00:00');

      await user.click(endDateInput);
      await user.click(screen.getAllByLabelText('03-04-2022 00:00')[1]);
      expect(endDateInput).toHaveValue('03-04-2022 00:00');

      await user.click(startDateInput);
      await user.click(screen.getAllByLabelText('05-04-2022 00:00')[0]);
      expect(startDateInput).toHaveValue('05-04-2022 00:00');
      expect(endDateInput).toHaveValue('');
    });

    it('Should validate target correctly', async () => {
      renderWithRoute({ route });

      await user.click(await screen.findByLabelText('Pelanggan'));
      expect(
        await screen.findByText('Harus pilih min. 1 kategori customer'),
      ).toBeInTheDocument();
    });

    it('Should validate all required field', async () => {
      renderWithRoute({ route });

      await user.click(
        await screen.findByRole('button', { name: 'Tambah Kupon' }),
      );
      expect(
        await screen.findByText('Kode kupon wajib diisi'),
      ).toBeInTheDocument();
      expect(screen.getByText('Nominal wajib diisi')).toBeInTheDocument();
      expect(screen.getByText('Nominal wajib diisi')).toBeInTheDocument();
      expect(
        screen.getByText('Minimal pembelian wajib diisi'),
      ).toBeInTheDocument();
      expect(screen.getAllByText('Masa berlaku wajib diisi').length).toEqual(2);
    });

    it('Should successfully create coupon with "nominal" discount', async () => {
      axios.onGet(API_URL.coupons).reply(200, { data: [] });
      axios
        .onPost(API_URL.coupons)
        .replyOnce(422, {
          errors: [{ message: apiErrorMessage }],
          meta: { http_status: 422 },
        })
        .onPost(API_URL.coupons)
        .replyOnce(200, { message: apiSuccessMessage, data: { id: 1 } });
      renderWithRoute({ route });

      await user.type(await screen.findByLabelText(/Kode Kupon/i), 'KODE1');
      await user.type(screen.getByTestId('discount-value-input'), '10000');
      await user.type(screen.getByLabelText(/Minimum Pembelian/i), '100000');

      const [startDateInput, endDateInput] =
        screen.getAllByPlaceholderText('DD-MM-YYYY 00:00');
      await user.click(startDateInput);
      await user.click(screen.getAllByLabelText('02-04-2022 00:00')[0]);
      await user.click(endDateInput);
      await user.click(screen.getAllByLabelText('03-04-2022 00:00')[1]);

      await user.click(screen.getByLabelText('Dropshipper'));
      const saveButton = screen.getByRole('button', { name: 'Tambah Kupon' });
      await user.click(saveButton);
      await waitFor(() =>
        expect(toast.danger).toHaveBeenLastCalledWith(apiErrorMessage),
      );
      await user.click(saveButton);
      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(apiSuccessMessage),
      );
      expect(getLastCalled(axios, 'post').data).toEqual(
        '{"type":"discount","code":"KODE1","discount_type":"nominal","discount_value":10000,"paid_limit":100000,"quota":-1,"attempt_usage":-1,"start_date":"2022-04-01T17:00:00.000Z","end_date":"2022-04-02T17:00:00.000Z","target_customer_category":["N","D"],"status":"inactive"}',
      );
      expect(await screen.findByText('Pengaturan Kupon')).toBeInTheDocument();
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'add_coupon_success',
        action_details: '1',
      });
    });

    it('Should successfully create coupon with "percentage" discount', async () => {
      axios.onGet(API_URL.coupons).reply(200, { data: [] });
      axios.onPost(API_URL.coupons).reply(200, { message: apiSuccessMessage });
      renderWithRoute({ route });

      await user.click(await screen.findByLabelText('Gratis ongkir'));
      await user.type(screen.getByLabelText(/Kode Kupon/i), 'KODE2');

      await user.selectOptions(screen.getByTestId('select-discount-type'), [
        ECouponDiscountType.PERCENTAGE,
      ]);
      await user.type(
        await screen.findByTestId('discount-percentage-input'),
        '10',
      );
      await user.type(screen.getByTestId('max-discount-input'), '20000');
      await user.type(screen.getByLabelText(/Minimum Pembelian/i), '100000');

      await user.click(screen.getByTestId('quota-option-limited'));
      await user.type(screen.getByTestId('quota-limited-input'), '23');
      await user.click(screen.getByTestId('attempt-usage-option-limited'));
      await user.type(screen.getByTestId('attempt-usage-limited-input'), '4');

      const [startDateInput, endDateInput] =
        screen.getAllByPlaceholderText('DD-MM-YYYY 00:00');
      await user.click(startDateInput);
      await user.click(screen.getAllByLabelText('10-04-2022 00:00')[0]);
      await user.click(endDateInput);
      await user.click(screen.getAllByLabelText('12-04-2022 00:00')[1]);

      await user.click(screen.getByLabelText(/Status/i));
      await user.click(screen.getByRole('button', { name: 'Tambah Kupon' }));
      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(apiSuccessMessage),
      );
      expect(getLastCalled(axios, 'post').data).toEqual(
        '{"type":"free_shipping","code":"KODE2","discount_type":"percentage","discount_percentage":10,"max_discount":20000,"paid_limit":100000,"quota":23,"attempt_usage":4,"start_date":"2022-04-09T17:00:00.000Z","end_date":"2022-04-11T17:00:00.000Z","target_customer_category":["N"],"status":"active"}',
      );
    });
  });

  describe('Edit', () => {
    const route = '/storefront-editor/settings/coupons/edit/2985';
    beforeEach(() => {
      axios
        .onGet(`${API_URL.coupons}/2985`)
        .reply(200, GET_COUPON_SUCCESS_MESSAGE);
    });

    it('Should redirect to coupon list when error get coupon detail', async () => {
      axios.onGet(`${API_URL.coupons}/2985`).reply(500);
      renderWithRoute({ route });

      expect(await screen.findByText('Pengaturan Kupon')).toBeInTheDocument();
    });

    it('Should render correctly', async () => {
      renderWithRoute({ route });

      expect(await screen.findByText('Edit Kupon')).toBeInTheDocument();
      expect(
        screen.getByRole('button', { name: 'Simpan Kupon' }),
      ).toBeInTheDocument();

      expect(screen.getByLabelText(/Kode Kupon/i)).toHaveValue('9812FR');

      expect(screen.getByTestId('discount-value-input')).toHaveValue('89.000');
      expect(
        screen.queryByTestId('discount-percentage-input'),
      ).not.toBeInTheDocument();

      expect(screen.getByLabelText(/Minimum Pembelian/i)).toHaveValue(
        '2.345.612',
      );

      expect(screen.getByTestId('quota-option-unlimited')).toBeChecked();
      expect(screen.getByTestId('quota-option-limited')).not.toBeChecked();

      expect(
        screen.getByTestId('attempt-usage-option-unlimited'),
      ).not.toBeChecked();
      expect(screen.getByTestId('attempt-usage-option-limited')).toBeChecked();
      expect(screen.getByTestId('attempt-usage-limited-input')).toHaveValue(
        '87',
      );

      const [startDateInput, endDateInput] =
        screen.getAllByPlaceholderText('DD-MM-YYYY 00:00');
      expect(startDateInput).toHaveValue('29-03-2022 20:00');
      expect(endDateInput).toHaveValue('30-04-2022 12:00');

      expect(screen.getByLabelText('Pelanggan')).toBeChecked();
      expect(screen.getByLabelText('Dropshipper')).toBeChecked();
      expect(screen.getByLabelText('Reseller')).not.toBeChecked();
      expect(screen.getByLabelText('Agen')).not.toBeChecked();
      expect(screen.getByLabelText('Customer Tetap')).not.toBeChecked();
      expect(screen.getByLabelText('Distributor ASLI')).not.toBeChecked();

      expect(screen.getByLabelText(/Status/i)).toBeChecked();
    });

    it('Should successfully update coupon', async () => {
      axios
        .onPut(`${API_URL.coupons}/2985`)
        .replyOnce(422, {
          errors: [{ message: apiErrorMessage }],
          meta: { http_status: 422 },
        })
        .onPut(`${API_URL.coupons}/2985`)
        .replyOnce(200, { message: apiSuccessMessage, data: { id: 2985 } });

      renderWithRoute({ route });

      await user.type(await screen.findByTestId('discount-value-input'), '0');

      const saveButton = screen.getByRole('button', { name: 'Simpan Kupon' });
      await user.click(saveButton);

      expect(
        await screen.findByText(MIN_DATE_ERROR_MESSAGE),
      ).toBeInTheDocument();
      const [startDateInput] =
        screen.getAllByPlaceholderText('DD-MM-YYYY 00:00');
      await user.click(startDateInput);
      await user.click(screen.getAllByLabelText('05-04-2022 00:00')[0]);
      await user.click(saveButton);
      await waitFor(() =>
        expect(toast.danger).toHaveBeenLastCalledWith(apiErrorMessage),
      );
      await user.click(saveButton);
      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(apiSuccessMessage),
      );
      expect(getLastCalled(axios, 'put').data).toEqual(
        '{"type":"free_shipping","code":"9812FR","discount_type":"nominal","discount_value":890000,"paid_limit":2345612,"quota":-1,"attempt_usage":87,"start_date":"2022-04-05T13:00:00.000Z","end_date":"2022-04-30T05:00:00.000Z","target_customer_category":["N","D"],"status":"active"}',
      );
      expect(await screen.findByText('Pengaturan Kupon')).toBeInTheDocument();
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'edit_coupon_success',
        action_details: '2985',
      });
    });
  });
});
