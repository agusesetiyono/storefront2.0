import React from 'react';
import { Stack, Form, Button } from 'react-bootstrap';
import { PencilSquare } from 'react-bootstrap-icons';
import { Link } from 'react-router-dom';

import Badge from '@components/Badge';

import { onSuccessMutate, onErrorMutate } from '@utils/reactQuery';
import { sendDashboardTracker } from '@utils/tracker/dashboard';

import { useUpdateStatusCoupon } from '@hooks/api/coupons';

import { ECouponStatus, ITableActionProps } from '../coupons.types';

const TableAction = ({ data, refetchData }: ITableActionProps) => {
  const updateStatusCoupon = useUpdateStatusCoupon(data.id || 0);
  const isActive = data?.status === ECouponStatus.ACTIVE;
  const isExpired = (endDate: string) =>
    Boolean(new Date(endDate) < new Date());

  const onStatusChange = () => {
    if (data?.id) {
      updateStatusCoupon.mutate(
        {
          status: isActive ? ECouponStatus.INACTIVE : ECouponStatus.ACTIVE,
        },
        {
          onSuccess: (data) => onSuccessMutate(data, refetchData),
          onError: (error) => onErrorMutate(error, refetchData),
          onSettled: () => {
            void sendDashboardTracker({
              actionType: 'toggle_coupon',
              actionDetails: `${data.id}`,
            });
          },
        },
      );
    }
  };

  return (
    <>
      <Stack direction="horizontal" gap={4}>
        <div>
          <Form.Check
            type="switch"
            id="custom-switch"
            checked={isActive}
            data-testid={`switch-status-coupon-${data.id}`}
            onChange={onStatusChange}
            disabled={isExpired(data.end_date || '')}
          />
          <div className="text-center mt-1">
            {isActive ? (
              <Badge variant="success"> Aktif </Badge>
            ) : (
              <Badge
                variant="default"
                disabled={isExpired(data.end_date || '')}
              >
                Nonaktif
              </Badge>
            )}
          </div>
        </div>
        <Link
          to={`edit/${data.id}`}
          data-testid={`edit-link-${data.id}`}
          className="text-black"
        >
          <Button
            variant="outline-secondary"
            className="border-basic-1-opacity"
            size="lg"
          >
            <PencilSquare className="icon-base" />
          </Button>
        </Link>
      </Stack>
    </>
  );
};

export default TableAction;
