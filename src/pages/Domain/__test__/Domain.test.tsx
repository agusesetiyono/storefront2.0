import axios from '@mocks/axios';
import { screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';
import '@testing-library/jest-dom';
import { renderWithRoute, API_URL } from '@utils/test-helper';
import { sendTracker } from '@utils/tracker';
import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';
import toast from '@utils/toast';
import { LEARN_MORE_URL } from '../Domain';

jest.mock('@utils/toast', () => ({
  danger: jest.fn(),
  success: jest.fn(),
}));

let user: UserEvent;
beforeEach(() => {
  user = userEvent.setup({ delay: null });
  localStorage.setItem('onboardingStep', '5'); // finish onboarding step

  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));

  axios.onGet(`${API_URL.storefronts}/status`).reply(
    200,
    getMockSubscriptionsResponse({
      type: 'free_trial',
      expiredDate: '2022-05-01T00:00:00+07:00',
      domain: 'domain.com',
      subdomain: 'subdomain',
      activeDomain: 'domain.com',
    }),
  );
});

afterEach(() => {
  jest.useRealTimers();
  jest.clearAllMocks();
  localStorage.clear();
});

describe('Setting Domain Page', () => {
  const assertSaveButton = async () => {
    const buttonSave = await screen.findByTestId('save-domain-button');
    expect(buttonSave).toBeInTheDocument();
    return buttonSave;
  };
  const defaultTrackerParams = {
    evn: 'ngorder_sf_dashboard_actions',
    active_tab: 'domain',
  };
  const route = '/storefront-editor/settings/domain';

  it(`Renders properly with default configuration`, async () => {
    const { container } = renderWithRoute({ route });

    // Redirected to setting domain page
    expect(await screen.findByText(/Yuk, Bikin Domain!/i)).toBeInTheDocument();
    expect(
      await screen.findByText(
        'Domain adalah nama website yang diakhiri ekstensi (contoh: smartseller.co.id; .com; .id) dan diketik di kolom alamat browser.',
      ),
    ).toBeInTheDocument();

    expect(container).toMatchSnapshot();
  });

  describe('Setting subdomain field', () => {
    const assertSubdomainLabel = async () => {
      expect(await screen.findByText('Subdomain')).toBeInTheDocument();
    };

    const assertSubdomainHint = () =>
      expect(
        screen.getByText(
          'Domain website adalah {nama domain}.smartseller.co.id',
        ),
      ).toBeInTheDocument();

    const assertSubdomainField = () => {
      const fieldSubdomain = screen.getByTestId('settings-subdomain-field');
      expect(fieldSubdomain).toBeInTheDocument();
      return fieldSubdomain;
    };

    it('Should show normal state for subdomain field', async () => {
      renderWithRoute({ route });

      await assertSubdomainLabel();
      assertSubdomainHint();
      const field = assertSubdomainField();
      const button = await assertSaveButton();

      await user.clear(field);
      await user.type(field, 'halosukijan!@#');
      expect(field).toHaveValue('halosukijan');
      expect(button).not.toBeDisabled();

      await user.click(screen.getByText('link ini'));
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'learn_more',
        action_details: LEARN_MORE_URL,
      });
    });

    it('Should show error state for subdomain field', async () => {
      renderWithRoute({ route });

      await assertSubdomainLabel();
      assertSubdomainHint();
      const field = assertSubdomainField();
      const button = await assertSaveButton();

      await user.clear(field);
      await user.type(field, 'hal');
      expect(field).toHaveValue('hal');
      expect(
        screen.getByText('Minimal 4 karakter berisi huruf dan angka.'),
      ).toBeInTheDocument();
      expect(button).toBeDisabled();
    });
  });

  describe('Setting domain field', () => {
    const assertDomainLabel = async () => {
      expect(await screen.findByText('Custom Domain')).toBeInTheDocument();
    };

    const assertDomainHint = () =>
      expect(
        screen.getByText(
          'Bisa custom domain yang diinginkan, seperti .com, .co.id, atau .id',
        ),
      ).toBeInTheDocument();

    const assertDomainField = () => {
      const fieldDomain = screen.getByTestId('settings-domain-field');
      expect(fieldDomain).toBeInTheDocument();
      return fieldDomain;
    };

    it('Should show normal state for domain field', async () => {
      renderWithRoute({ route });

      await assertDomainLabel();
      assertDomainHint();
      const field = assertDomainField();
      const button = await assertSaveButton();

      await user.clear(field);
      await user.type(field, 'bukatoko.com');
      expect(field).toHaveValue('bukatoko.com');
      expect(button).not.toBeDisabled();
    });

    it('Should show error state for domain field', async () => {
      renderWithRoute({ route });

      await assertDomainLabel();
      assertDomainHint();
      const field = assertDomainField();
      const button = await assertSaveButton();

      await user.clear(field);
      await user.type(field, 'buka-toko');
      expect(field).toHaveValue('buka-toko');
      expect(
        screen.getByText(
          'Format domain tidak sesuai. Silakan tulis extension domain.',
        ),
      ).toBeInTheDocument();
      expect(button).toBeDisabled();
    });
  });

  describe('Setting domain alert info', () => {
    it('Should hide any alert if custom domain is empty', () => {
      axios.onGet(`${API_URL.storefronts}/status`).reply(
        200,
        getMockSubscriptionsResponse({
          type: 'free_trial',
          expiredDate: '2022-05-01T00:00:00+07:00',
          domain: '',
        }),
      );

      renderWithRoute({ route });

      expect(
        screen.queryByTestId('domain-alert-warning'),
      ).not.toBeInTheDocument();
      expect(
        screen.queryByTestId('domain-alert-success'),
      ).not.toBeInTheDocument();
    });

    it('Should show warning alert if custom domain is not empty and not same with active domain', async () => {
      axios.onGet(`${API_URL.storefronts}/status`).reply(
        200,
        getMockSubscriptionsResponse({
          type: 'free_trial',
          expiredDate: '2022-05-01T00:00:00+07:00',
          domain: 'domain.com',
          activeDomain: 'subdomain.smartseller.co.id',
        }),
      );

      renderWithRoute({ route });

      expect(
        await screen.findByTestId('domain-alert-warning'),
      ).toBeInTheDocument();
      expect(
        screen.queryByTestId('domain-alert-success'),
      ).not.toBeInTheDocument();
    });

    it('Should show success alert if custom domain is not empty and have same value with active domain', async () => {
      axios.onGet(`${API_URL.storefronts}/status`).reply(
        200,
        getMockSubscriptionsResponse({
          type: 'free_trial',
          expiredDate: '2022-05-01T00:00:00+07:00',
        }),
      );

      renderWithRoute({ route });

      expect(
        screen.queryByTestId('domain-alert-warning'),
      ).not.toBeInTheDocument();
      expect(
        await screen.findByTestId('domain-alert-success'),
      ).toBeInTheDocument();
    });
  });

  describe('Setting domain action save', () => {
    it('Should show success toast', async () => {
      renderWithRoute({ route });

      axios.onPost(API_URL.domains).replyOnce(200, {});

      const button = await assertSaveButton();
      await user.click(button);
      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(
          'Berhasil menyimpan perubahan',
        ),
      );
      await waitFor(() =>
        expect(sendTracker).toHaveBeenLastCalledWith({
          ...defaultTrackerParams,
          action_type: 'save_changes',
        }),
      );
    });

    it('Should show danger toast', async () => {
      axios.onGet(`${API_URL.storefronts}/status`).reply(
        200,
        getMockSubscriptionsResponse({
          type: 'free_trial',
          expiredDate: '2022-05-01T00:00:00+07:00',
          domain: '',
          subdomain: '',
          activeDomain: '',
        }),
      );

      renderWithRoute({ route });

      const errorMessage = 'Gagal menyimpan perubahan. Silakan coba lagi.';
      axios.onPost(API_URL.domains).replyOnce(500, {
        errors: [
          {
            message: errorMessage,
            code: 500,
          },
        ],
        meta: {
          http_status: 500,
        },
      });

      const button = await assertSaveButton();
      await user.click(button);
      await waitFor(() =>
        expect(toast.danger).toHaveBeenLastCalledWith(errorMessage),
      );
    });
  });

  describe('Dashboard header page title configuration', () => {
    it('Should show button', async () => {
      axios.onGet(`${API_URL.storefronts}/status`).reply(
        200,
        getMockSubscriptionsResponse({
          type: 'free_trial',
          expiredDate: '2022-05-01T00:00:00+07:00',
          domain: 'domain.com',
          activeDomain: 'subdomain.smartseller.co.id',
        }),
      );

      const { container } = renderWithRoute({ route });

      // Redirected to setting domain page
      expect(
        await screen.findByText(/Yuk, Bikin Domain!/i),
      ).toBeInTheDocument();
      expect(
        await screen.findByText(
          'Domain adalah nama website yang diakhiri ekstensi (contoh: smartseller.co.id; .com; .id) dan diketik di kolom alamat browser.',
        ),
      ).toBeInTheDocument();

      // assert isSticky class
      const SettingLayoutsDashboard = screen.getByTestId('setting-layouts');
      expect(SettingLayoutsDashboard).toHaveClass('is-sticky');

      // show text active domain
      expect(
        await screen.findByText('subdomain.smartseller.co.id'),
      ).toBeInTheDocument();
      expect(screen.getByTestId('active-domain-text')).toBeInTheDocument();

      // assert lihat storefront button
      const storefrontView = screen.getByTestId(
        'dashboard-view-storefront-btn',
      );
      expect(storefrontView).toBeInTheDocument();
      expect(storefrontView).toHaveClass('ml-6 btn');
      expect(storefrontView).toHaveAttribute(
        'href',
        'https://subdomain.smartseller.co.id',
      );
      expect(storefrontView).toHaveAttribute('target', '_blank');

      // assert copy link button
      const copyLinkBtn = screen.getByTestId('dashboard-copy-link-btn');
      expect(copyLinkBtn).toBeInTheDocument();
      expect(copyLinkBtn).toHaveClass('ml-6 btn btn-outline-primary');

      // user click "Copy Link" button
      const user = userEvent.setup({ delay: null });
      await user.click(copyLinkBtn);
      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(
          'Link berhasil di-copy!',
        ),
      );

      expect(container).toMatchSnapshot();
    });
  });

  describe('Trackers', () => {
    describe('General Tracker', () => {
      it('Should send page visit tracker', async () => {
        renderWithRoute({ route });

        expect(
          await screen.findByText(
            'Domain adalah nama website yang diakhiri ekstensi (contoh: smartseller.co.id; .com; .id) dan diketik di kolom alamat browser.',
          ),
        ).toBeInTheDocument();
        expect(sendTracker).toHaveBeenLastCalledWith({
          ...defaultTrackerParams,
          action_type: 'visit',
        });
      });

      it('Should send click tab tracker', async () => {
        renderWithRoute({ route });

        await user.click(await screen.findByText('Custom Website'));
        expect(sendTracker).toHaveBeenLastCalledWith({
          ...defaultTrackerParams,
          action_type: 'click_tab',
          action_details: 'custom_website',
        });
      });

      it('Should send copy link tracker', async () => {
        renderWithRoute({ route });

        await user.click(
          await screen.findByRole('button', { name: 'Copy link' }),
        );
        expect(sendTracker).toHaveBeenLastCalledWith({
          ...defaultTrackerParams,
          action_type: 'copy_link',
        });
      });

      it('Should send go to storefront tracker', async () => {
        renderWithRoute({ route });

        await user.click(
          await screen.findByRole('button', { name: 'Lihat Storefront' }),
        );
        expect(sendTracker).toHaveBeenLastCalledWith({
          ...defaultTrackerParams,
          action_type: 'to_storefront',
        });
      });
    });
  });
});
