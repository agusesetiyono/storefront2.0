import React, { ChangeEvent, useEffect } from 'react';
import { Alert, Button, Card, Form, InputGroup, Stack } from 'react-bootstrap';
import { ExclamationCircleFill } from 'react-bootstrap-icons';
import { useLocalStorage } from 'usehooks-ts';
import { useFormik } from 'formik';

import config from '@utils/config';
import { setFormErrors, setFormFieldError } from '@utils/form';
import { sendDashboardTracker } from '@utils/tracker/dashboard';
import useSubscription from '@hooks/useSubscription';
import useDebounceEffect from '@hooks/useDebounceEffect';
import { useValidateDomain, useSaveDomain } from '@hooks/api/domains';
import { validationDomain } from './domain.validation';
import type { IDomainForm } from './domain.types';
import toast from '@utils/toast';

export const LEARN_MORE_URL = 'https://help.smartseller.co.id/aktivasi-domain/';

const Domain = () => {
  const { subscription, refetchSubscription } = useSubscription();
  const validateDomain = useValidateDomain();
  const [showDomainInfo, setShowDomainInfo] = useLocalStorage(
    'showActiveDomainInfo',
    true,
  );
  const saveDomain = useSaveDomain();
  const isActiveCustomDomain =
    subscription.active_domain === subscription.domain;

  const formik = useFormik<IDomainForm>({
    initialValues: { subdomain: '', domain: '' },
    validateOnChange: false,
    validationSchema: validationDomain,
    onSubmit: (values: IDomainForm, { setErrors }) => {
      saveDomain.mutate(values, {
        onSuccess: () => {
          if (values.domain !== subscription.domain) {
            setShowDomainInfo(true);
          }
          setErrors({});
          refetchSubscription();
          toast.success('Berhasil menyimpan perubahan');
          void sendDashboardTracker({ actionType: 'save_changes' });
        },
        onError: (error) => {
          setFormErrors(error, setErrors);
          toast.danger('Gagal menyimpan perubahan. Silakan coba lagi.');
        },
      });
    },
  });

  const {
    values,
    errors,
    isValidating,
    setValues,
    setFieldError,
    validateField,
  } = formik;
  const { subdomain, domain } = values || {};

  useEffect(() => {
    void setValues({
      subdomain: subscription.subdomain,
      domain: subscription.domain,
    });
  }, [subscription.subdomain, subscription.domain]);

  useDebounceEffect(() => {
    if (subdomain && !errors.subdomain && !isValidating) {
      validateDomain.mutate(
        { subdomain },
        {
          onSuccess: () => setFieldError('subdomain', undefined),
          onError: (error) =>
            setFormFieldError('subdomain', error, setFieldError),
        },
      );
    }
  }, [subdomain, isValidating]);

  useDebounceEffect(() => {
    if (domain && !errors.domain && !isValidating) {
      validateDomain.mutate(
        { domain },
        {
          onSuccess: () => setFieldError('domain', undefined),
          onError: (error) => setFormFieldError('domain', error, setFieldError),
        },
      );
    }
  }, [domain, isValidating]);

  const handleChangeSubdomain = async (e: ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    const subdomain = value.replace(/[\W_]+/g, '');
    await setValues({ ...values, subdomain });
    await validateField('subdomain');
  };

  const handleChangeDomain = async (e: ChangeEvent<HTMLInputElement>) => {
    const domain = e.target.value;
    await setValues({ ...values, domain });
    await validateField('domain');
  };

  const hideDomainInfo = () => {
    setShowDomainInfo(false);
  };

  return (
    <Stack direction="horizontal" className="items-start">
      <div className="w-3/12 mr-[88px]">
        <h3 className="text-2xl font-bold">Yuk, Bikin Domain!</h3>
        <p className="text-sm opacity-60">
          Domain adalah nama website yang diakhiri ekstensi (contoh:
          smartseller.co.id; .com; .id) dan diketik di kolom alamat browser.
        </p>
      </div>
      <Card className="p-8 pr-24 w-9/12">
        <Form onSubmit={formik.handleSubmit}>
          <Stack gap={4}>
            <Form.Group controlId="validationSubdomain">
              <Form.Label aria-required>Subdomain</Form.Label>
              <InputGroup hasValidation>
                <Form.Control
                  type="text"
                  maxLength={50}
                  placeholder="Subdomain"
                  aria-describedby="siteExt"
                  name="subdomain"
                  value={subdomain}
                  onChange={(e: ChangeEvent<HTMLInputElement>) =>
                    void handleChangeSubdomain(e)
                  }
                  isInvalid={!!errors.subdomain}
                  data-testid="settings-subdomain-field"
                />
                <InputGroup.Text id="siteExt">{config.siteExt}</InputGroup.Text>
                <Form.Control.Feedback
                  type="invalid"
                  data-testid="settings-subdomain-error"
                >
                  {errors.subdomain as string}
                </Form.Control.Feedback>
              </InputGroup>
              {!errors.subdomain && (
                <Form.Text muted>
                  {`Domain website adalah {nama domain}.smartseller.co.id`}
                </Form.Text>
              )}
            </Form.Group>

            <Form.Group controlId="validationDomain">
              <Form.Label>Custom Domain</Form.Label>
              <InputGroup hasValidation>
                <Form.Control
                  type="text"
                  maxLength={50}
                  placeholder="Custom Domain"
                  name="domain"
                  value={domain}
                  onChange={(e: ChangeEvent<HTMLInputElement>) =>
                    void handleChangeDomain(e)
                  }
                  isInvalid={!!errors.domain}
                  data-testid="settings-domain-field"
                />
                <Form.Control.Feedback
                  type="invalid"
                  data-testid="settings-domain-error"
                >
                  {errors.domain as string}
                </Form.Control.Feedback>
              </InputGroup>
              {!errors.domain && (
                <Form.Text muted>
                  {`Bisa custom domain yang diinginkan, seperti .com, .co.id, atau .id`}
                </Form.Text>
              )}
            </Form.Group>

            {subscription.domain && !isActiveCustomDomain && (
              <Alert variant="warning" data-testid="domain-alert-warning">
                <Stack direction="horizontal" className="items-start" gap={3}>
                  <ExclamationCircleFill />
                  <div>
                    Segera hubungkan Nameserver kamu dengan Storefront.
                    <br /> Silakan tunggu sekitar 10 menit, lalu refresh halaman
                    ini.
                  </div>
                </Stack>
              </Alert>
            )}

            {subscription.domain && isActiveCustomDomain && (
              <Alert
                variant="success"
                show={showDomainInfo}
                data-testid="domain-alert-success"
              >
                <Stack direction="horizontal" className="items-start" gap={3}>
                  <ExclamationCircleFill />
                  <Stack className="items-start" gap={2}>
                    Kamu berhasil menghubungkan Custom Domain kamu dengan
                    Storefront.
                    <Button
                      variant="link"
                      className="font-bold p-0 leading-5 no-underline"
                      onClick={hideDomainInfo}
                    >
                      Tutup Info
                    </Button>
                  </Stack>
                </Stack>
              </Alert>
            )}

            <p>
              Pelajari tentang pengaturan <i>custom domain</i> di{' '}
              <a
                href={LEARN_MORE_URL}
                target="_blank"
                onClick={() =>
                  void sendDashboardTracker({
                    actionType: 'learn_more',
                    actionDetails: LEARN_MORE_URL,
                  })
                }
              >
                link ini
              </a>
              .
            </p>

            <Button
              type="submit"
              className="w-48"
              data-testid="save-domain-button"
              disabled={!!errors.subdomain || !!errors.domain || !subdomain}
            >
              Simpan Perubahan
            </Button>
          </Stack>
        </Form>
      </Card>
    </Stack>
  );
};

export default Domain;
