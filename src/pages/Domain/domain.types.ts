export interface IDomainForm {
  subdomain: string;
  domain?: string;
}
