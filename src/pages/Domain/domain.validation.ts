import * as Yup from 'yup';

const domainRegex =
  /^((ftp|http|https):\/\/)?(www.)?(?!.*(ftp|http|https|www.))[a-zA-Z0-9_-]+(\.[a-zA-Z]+)+((\/)[\w#]+)*(\/\w+\?[a-zA-Z0-9_]+=\w+(&[a-zA-Z0-9_]+=\w+)*)?$/gm;

export const validationDomain = Yup.object().shape({
  subdomain: Yup.string()
    .required('Minimal 4 karakter berisi huruf dan angka.')
    .min(4, 'Minimal 4 karakter berisi huruf dan angka.'),
  domain: Yup.string()
    .transform((value) => (!value ? null : value))
    .matches(
      domainRegex,
      'Format domain tidak sesuai. Silakan tulis extension domain.',
    )
    .nullable(),
});
