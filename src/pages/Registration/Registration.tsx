import React from 'react';
import './registration.scss';

import SmartSellerLogo from '@assets/image/smartseller-logo.svg';
import FooterImage from '@assets/image/registration-footer.png';

import Form from './partials/Form';
import Error from './partials/Error';

const Registration = () => {
  const isErrorToken = global.isErrorToken as boolean;

  return (
    <div
      className="flex flex-column justify-center min-h-screen bg-no-repeat bg-bottom"
      style={{ backgroundImage: `url(${FooterImage})` }}
    >
      <div className="flex align-items-center justify-center w-full flex-wrap mb-9 mt-9 px-16">
        <img
          src={SmartSellerLogo}
          className="w-[182px] mr-4"
          alt="smartseller-logo"
        />
        <h2 className="uppercase text-2xl text-[#212529] font-light tracking-[.25em]">
          Storefront
        </h2>
      </div>
      {isErrorToken ? <Error /> : <Form />}
    </div>
  );
};

export default Registration;
