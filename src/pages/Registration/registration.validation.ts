import * as Yup from 'yup';

export const validationRegistration = Yup.object().shape({
  password: Yup.string()
    .required('Password baru wajib diisi')
    .min(6, 'Password min. 6 karakter'),
  confirmPassword: Yup.string()
    .required('Konfirmasi Password wajib diisi')
    .min(6, 'Password min. 6 karakter')
    .oneOf(
      [Yup.ref('password'), null],
      'Password tidak sama. Silakan cek kembali.',
    ),
  terms: Yup.boolean().oneOf([true]),
});
