import axios from '@mocks/axios';
import '@testing-library/jest-dom';
import { screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';
import { renderWithRoute, API_URL, getLastCalled } from '@utils/test-helper';
import toast from '@utils/toast';
import { sendTracker } from '@utils/tracker';

jest.mock('@utils/toast', () => ({
  danger: jest.fn(),
}));

let user: UserEvent;
const mockEmail = 'bukastoreuser@gmail.com';
beforeEach(() => {
  user = userEvent.setup({ delay: null });

  global.CONFIG = {
    token: '',
  };
  global.isErrorToken = false;
  global.email = mockEmail;
});

describe('SmartSeller registration', () => {
  const route = '/storefront-editor/register?token=abcdef';
  const formTitle = 'Buat Password Storefront';
  const termsAndConditionText = /Dengan mendaftar, saya setuju dengan/i;
  const passwordPlaceholder = 'Minimum 6 karakter';
  const confirmPasswordPlaceholder = 'Masukkan ulang password';
  const passwordNotMatch = 'Password tidak sama. Silakan cek kembali.';
  const ctaButtonText = 'Buat Password dan Log In';
  const defaultTrackerParams = {
    evn: 'ngorder_sf_bukastore_funnel_action',
    bl_identity: JSON.stringify({
      user_id: '',
      email: mockEmail,
    }),
  };

  const assertPasswordNotMatch = async () => {
    await user.type(screen.getByPlaceholderText(passwordPlaceholder), '123456');
    await user.type(
      screen.getByPlaceholderText(confirmPasswordPlaceholder),
      '1234567',
    );
    await user.click(screen.getByLabelText(termsAndConditionText));

    expect(await screen.findByText(passwordNotMatch)).toBeInTheDocument();
  };

  it('Should render page correctly', async () => {
    const { container } = renderWithRoute({ route });
    expect(await screen.findByText(formTitle)).toBeInTheDocument();

    expect(container).toMatchSnapshot();
    expect(screen.getByLabelText('Email')).toHaveValue(mockEmail);
    await waitFor(() =>
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: '5-b_visit_registration',
      }),
    );
  });

  it('Should enable register and login button', async () => {
    renderWithRoute({ route });
    expect(await screen.findByText(formTitle)).toBeInTheDocument();

    const registerAndLoginButton = screen.getByRole('button', {
      name: ctaButtonText,
    });
    expect(registerAndLoginButton).toBeDisabled();

    await user.click(screen.getByLabelText(termsAndConditionText));
    expect(registerAndLoginButton).toBeEnabled();
  });

  it('Should have correct terms and condition link', async () => {
    renderWithRoute({ route });
    expect(await screen.findByText(formTitle)).toBeInTheDocument();

    const termsAndConditionLink = screen.getByText('syarat dan ketentuan');
    expect(termsAndConditionLink).toHaveAttribute(
      'href',
      'https://smartseller.co.id/terms',
    );
    expect(termsAndConditionLink).toHaveAttribute('target', '_blank');
  });

  it('Should show error state correctly', async () => {
    global.isErrorToken = true;
    renderWithRoute({ route });
    expect(
      await screen.findByText(
        'Silakan klik ulang tombol di halaman Storefront Bukalapak Seller Center kamu.',
      ),
    ).toBeInTheDocument();
  });

  it('Should validate password and confirm password content', async () => {
    renderWithRoute({ route });
    expect(await screen.findByText(formTitle)).toBeInTheDocument();

    await assertPasswordNotMatch();
  });

  it('Should revalidate confirm password', async () => {
    renderWithRoute({ route });
    expect(await screen.findByText(formTitle)).toBeInTheDocument();

    await assertPasswordNotMatch();

    await user.clear(screen.getByPlaceholderText(passwordPlaceholder));
    await user.type(
      screen.getByPlaceholderText(passwordPlaceholder),
      '1234567',
    );
    await user.click(screen.getByLabelText(termsAndConditionText));
    await waitFor(() => {
      expect(
        screen.getByTestId('password-error-message-confirmPassword')
          .textContent,
      ).toEqual('');
    });
  });

  it('Should validate min length password', async () => {
    renderWithRoute({ route });
    expect(await screen.findByText(formTitle)).toBeInTheDocument();

    await user.type(screen.getByPlaceholderText(passwordPlaceholder), '1234');
    await user.type(
      screen.getByPlaceholderText(confirmPasswordPlaceholder),
      '1234',
    );
    await user.click(screen.getByLabelText(termsAndConditionText));

    await waitFor(() => {
      expect(screen.getAllByText('Password min. 6 karakter').length).toEqual(2);
    });
  });

  it('Should change password input type correctly', async () => {
    renderWithRoute({ route });
    expect(await screen.findByText(formTitle)).toBeInTheDocument();

    const passwordInput = screen.getByPlaceholderText(passwordPlaceholder);
    expect(passwordInput).toHaveAttribute('type', 'password');
    const toggleTypeTrigger =
      passwordInput?.nextElementSibling?.nextElementSibling;

    if (!toggleTypeTrigger) return;
    await user.click(toggleTypeTrigger);
    expect(passwordInput).toHaveAttribute('type', 'text');
    await user.click(toggleTypeTrigger);
    expect(passwordInput).toHaveAttribute('type', 'password');
  });

  it('Should perform register user correctly', async () => {
    const errorMessage =
      'Gagal registrasi dan login. Silakan coba beberapa saat lagi.';
    const successMessage = 'Selamat, kamu berhasil registrasi dan login.';

    axios
      .onPost(API_URL.registerUser)
      .replyOnce(422, {
        errors: [{ message: errorMessage }],
        meta: { http_status: 422 },
      })
      .onPost(API_URL.registerUser)
      .replyOnce(200);

    renderWithRoute({ route });
    expect(await screen.findByText(formTitle)).toBeInTheDocument();

    await user.type(
      screen.getByPlaceholderText('Minimum 6 karakter'),
      '123456',
    );
    await user.type(
      screen.getByPlaceholderText('Masukkan ulang password'),
      '123456',
    );
    await user.click(screen.getByLabelText(termsAndConditionText));

    // error from api
    const registerAndLoginButton = screen.getByRole('button', {
      name: ctaButtonText,
    });
    await user.click(registerAndLoginButton);
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: '6-b_click_register_button',
      target_url: '/storefront-editor/onboarding',
    });
    await waitFor(() =>
      expect(toast.danger).toHaveBeenLastCalledWith(errorMessage),
    );

    // success from api
    await user.click(registerAndLoginButton);
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: '6-b_click_register_button',
      target_url: '/storefront-editor/onboarding',
    });
    expect(getLastCalled(axios, 'post').data).toEqual(
      '{"email":"bukastoreuser@gmail.com","password":"123456","confirm_password":"123456","password_token":"abcdef","terms":true}',
    );

    await waitFor(() =>
      expect(localStorage.getItem('register_storefront')).toContain(
        successMessage,
      ),
    );
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: '7_login_registration_success',
    });
  });
});
