export interface IRegistrationForm {
  password: string;
  confirmPassword: string;
  terms: boolean;
}
