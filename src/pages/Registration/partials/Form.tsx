import React, { ChangeEvent, FocusEvent, useEffect } from 'react';
import { Form } from 'react-bootstrap';
import { useSearchParams } from 'react-router-dom';
import { useFormik } from 'formik';

import FormIllustration from '@assets/image/registration-illustration.png';
import { sendRegistrationTracker } from '@utils/tracker/registration';

import Password from '@components/Password';
import Button from '@components/Button';
import { onErrorMutate } from '@utils/reactQuery';

import { useRegisterUser } from '@hooks/api/registrations';
import useRegister from '@hooks/useRegister';

import { validationRegistration } from '../registration.validation';
import type { IRegistrationForm } from '../registration.types';

const RegistrationForm = () => {
  const registerUser = useRegisterUser();
  const { successRegister } = useRegister();
  const [searchParams] = useSearchParams();

  const email = globalThis.email as string;

  useEffect(() => {
    void sendRegistrationTracker({
      actionType: '5-b_visit_registration',
    });
  }, []);

  const {
    values,
    errors,
    isSubmitting,
    handleChange,
    handleSubmit,
    setFieldError,
    setFieldValue,
    validateField,
  } = useFormik<IRegistrationForm>({
    initialValues: { password: '', confirmPassword: '', terms: false },
    validateOnChange: false,
    validationSchema: validationRegistration,
    onSubmit: ({ password, confirmPassword, terms }, { setSubmitting }) => {
      const targetUrl = '/storefront-editor/onboarding';
      void sendRegistrationTracker({
        actionType: '6-b_click_register_button',
        targetUrl,
      });
      registerUser.mutate(
        {
          email,
          password,
          confirm_password: confirmPassword,
          password_token: searchParams.get('token'),
          terms,
        },
        {
          onSuccess: () => {
            successRegister(targetUrl);
          },
          onError: (e) => {
            onErrorMutate(e);
            setSubmitting(false);
          },
        },
      );
    },
  });

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFieldError(name, '');
    void setFieldValue(name, value);
  };

  const onBlur = (e: FocusEvent<HTMLInputElement>) => {
    const { name } = e.target;
    void validateField(name);
    if (
      name === 'password' &&
      values.password.length > 5 &&
      values.confirmPassword.length > 5
    ) {
      void validateField('confirmPassword');
    }
  };

  return (
    <Form
      style={{ backgroundImage: `url(${FormIllustration})` }}
      className="bg-no-repeat registration-form-container"
      onSubmit={handleSubmit}
    >
      <div className="flex justify-center mb-14 px-16">
        <div className="p-8 bg-white rounded-xl max-w-[492px] flex-1 registration-form">
          <h1 className="text-3xl text-center mb-8">
            Buat Password Storefront
          </h1>
          <div className="mb-8">
            <Form.Label htmlFor="email" className="mb-3 text-[#AAAAAA]">
              Email
            </Form.Label>
            <Form.Control id="email" name="email" disabled value={email} />
          </div>
          <div className="mb-3">
            <Password
              id="password"
              placeholder="Minimum 6 karakter"
              label="Password baru"
              value={values.password}
              onChange={onChange}
              onBlur={onBlur}
              error={errors.password}
            />
          </div>
          <div className="mb-3">
            <Password
              id="confirmPassword"
              placeholder="Masukkan ulang password"
              label="Konfirmasi Password"
              value={values.confirmPassword}
              onChange={onChange}
              onBlur={onBlur}
              error={errors.confirmPassword}
            />
          </div>
          <div className="mb-8">
            <Form.Check id="terms">
              <Form.Check.Input
                type="checkbox"
                className="mr-0 mt-3"
                name="terms"
                onChange={handleChange}
              />
              <Form.Check.Label className="ml-2">
                Dengan mendaftar, saya setuju dengan{' '}
                <a
                  href="https://smartseller.co.id/terms"
                  target="_blank"
                  rel="noopener noreferrer"
                  className="font-bold"
                >
                  syarat dan ketentuan
                </a>{' '}
                yang berlaku.
              </Form.Check.Label>
            </Form.Check>
          </div>
          <Button
            className="w-full"
            disabled={!values.terms}
            type="submit"
            isLoading={isSubmitting}
            spinnerClass="my-1"
          >
            Buat Password dan Log In
          </Button>
        </div>
      </div>
      <div className="text-center mb-12 px-16 text-[#212529]">
        <p className="font-bold mb-3 text-xl">
          Perluas jangkauan bisnis kamu dengan Storefront.
        </p>
        <p className="text-xl">
          Mudahnya buat web toko sendiri, dilengkapi dashboard untuk kelola
          produk & stok di semua channel jualanmu.
        </p>
      </div>
    </Form>
  );
};

export default RegistrationForm;
