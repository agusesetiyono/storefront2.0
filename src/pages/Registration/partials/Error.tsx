import React from 'react';
import ErrorIllustration from '@assets/image/error-illustration.svg';

const Error = () => (
  <div className="flex justify-center flex-column lg:flex-row align-items-center gap-24 w-full my-0 lg:my-44 px-16">
    <div className="max-w-[580px] flex-1 text-center lg:text-left text-[#212529]">
      <p className="text-7xl md:text-[80px] opacity-70 font-bold mb-6">
        Ups,
      </p>
      <p className="text-7xl font-bold mb-6">
        sepertinya salah alamat.
      </p>
      <p className="text-3xl">
        Silakan klik ulang tombol di halaman Storefront Bukalapak Seller Center
        kamu.
      </p>
    </div>
    <div className="max-w-[544px]">
      <img src={ErrorIllustration} alt="error-illus" className="w-full" />
    </div>
  </div>
);

export default Error;
