import axios from '@mocks/axios';
import {
  screen,
  waitFor,
  waitForElementToBeRemoved,
  within,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';
import '@testing-library/jest-dom';
import { renderWithRoute, API_URL } from '@utils/test-helper';
import { sendTracker } from '@utils/tracker';
import { UPDATE_LEARN_MORE_LINK } from '@utils/constants';
import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';
import { faqItems } from '../partials/FAQ';

axios.onGet(`${API_URL.storefronts}/status`).reply(
  200,
  getMockSubscriptionsResponse({
    type: 'free_trial',
    expiredDate: '2022-05-01T00:00:00+07:00',
    domain: 'domain.com',
    subdomain: 'subdomain',
    activeDomain: 'domain.com',
    version: '1.0',
  }),
);

const scrollIntoViewMock = jest.fn();
global.HTMLElement.prototype.scrollIntoView = scrollIntoViewMock;

let user: UserEvent;
beforeEach(() => {
  user = userEvent.setup({ delay: null });

  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));
});

afterEach(() => {
  jest.useRealTimers();
  jest.clearAllMocks();
  localStorage.clear();
});

const evn = 'ngorder_sf_upgrade_funnel_actions';

describe('Update Version Page', () => {
  const assertButtonUpdate = async (testId: string) => {
    const btnUpdate = await screen.findByTestId(testId);
    expect(btnUpdate).toBeInTheDocument();

    return btnUpdate;
  };

  const assertLabel = (label: string) => {
    const sectionLabel = screen.getByText(label);
    expect(sectionLabel).toBeInTheDocument();
  };

  const assertTitle = (title: string) => {
    const sectionTitle = screen.getByText(title);
    expect(sectionTitle).toBeInTheDocument();
  };

  const assertSubtitle = (subtitle: string) => {
    const sectionSubtitle = screen.getByText(subtitle);
    expect(sectionSubtitle).toBeInTheDocument();
  };

  describe('Show section', () => {
    it(`Renders properly with default configuration`, async () => {
      const { container } = renderWithRoute({
        route: '/storefront-editor/update',
      });

      expect(
        await screen.findByText(
          /Bikin website lebih praktis, performa bisnis naik drastis!/i,
        ),
      ).toBeInTheDocument();
      expect(container).toMatchSnapshot();
      expect(sendTracker).toHaveBeenLastCalledWith({
        evn,
        action_type: '1_visit_sf_upgrade_landing',
      });
    });

    it('Should show main section', async () => {
      renderWithRoute({
        route: '/storefront-editor/update',
      });

      await assertButtonUpdate('btn-update-main');
      expect(
        screen.getByRole('img', { name: 'previewImage' }),
      ).toBeInTheDocument();
    });

    it('Should show dashboard section', async () => {
      renderWithRoute({
        route: '/storefront-editor/update',
      });

      await assertButtonUpdate('btn-update-tab');
      expect(
        screen.getByRole('img', { name: 'dashboardImage' }),
      ).toBeInTheDocument();

      assertLabel('Dashboard Baru');
      assertTitle('Pengaturan lebih terorganisir');
      assertSubtitle(
        'Lebih simpel hanya dengan 6 tab, Anda bisa atur segala aspek Storefront dengan mudah.',
      );
    });

    it('Should show web builder section', async () => {
      renderWithRoute({
        route: '/storefront-editor/update',
      });

      await assertButtonUpdate('btn-update-website_builder');
      expect(
        screen.getByRole('img', { name: 'webBuilderImage' }),
      ).toBeInTheDocument();

      assertLabel('Website Builder');
      assertTitle('Edit Website, Lihat Instan!');
      assertSubtitle(
        'Selagi Anda mengedit tampilan website, kamu bisa langsung lihat hasilnya di halaman yang sama.',
      );

      await assertButtonUpdate('btn-update-sections');
      expect(
        screen.getByRole('img', {
          name: 'webBuilderMenuImage',
        }),
      ).toBeInTheDocument();

      assertTitle('Tambah section, satu klik.');
      assertSubtitle(
        'Dengan sistem pengaturan lewat toggle, custom website jadi semakin mudah.',
      );

      assertTitle('Rapikan section, tinggal klik dan geser.');
      assertSubtitle('Urutkan section dengan cara drag and drop.');

      assertTitle('Edit section, langsung lihat hasilnya.');
      assertSubtitle(
        'Kamu bisa melihat tampilan website yang kamu edit langsung di halaman yang sama.',
      );
    });

    it('Should show faq section', async () => {
      renderWithRoute({
        route: '/storefront-editor/update',
      });

      await assertButtonUpdate('btn-update-faq');

      assertLabel('Pertanyaan');
      assertTitle('FAQ - Pertanyaan Seputar Produk');
      expect(screen.getByText('link ini')).toHaveAttribute(
        'href',
        UPDATE_LEARN_MORE_LINK,
      );
      expect(screen.getByText('Pelajari Detailnya')).toHaveAttribute(
        'href',
        UPDATE_LEARN_MORE_LINK,
      );

      for (const faq of faqItems) {
        const index = faqItems.indexOf(faq);
        const faqTitle = await screen.findByText(faq.question);
        expect(faqTitle).toBeInTheDocument();

        const answerTestId = `faq-body-${index}`;
        const faqToggle = screen.getByTestId(`faq-toggle-${index}`);
        await user.click(faqToggle);
        expect(screen.getByTestId(answerTestId)).toBeVisible();

        const faqButton = screen.getByTestId(`faq-button-${index}`);
        await user.click(faqButton);
        expect(screen.getByTestId(answerTestId)).toBeVisible();
      }

      expect(screen.getByText('Pelajari Detailnya')).toBeInTheDocument();
    });
  });

  describe('Upgrade action confirmation', () => {
    it.each(['main', 'tab', 'website_builder', 'sections', 'faq'])(
      'Should show %s section upgrade confirmation popup',
      async (section) => {
        renderWithRoute({
          route: '/storefront-editor/update',
        });

        const btnUpdate = await assertButtonUpdate(`btn-update-${section}`);
        await user.click(btnUpdate);
        expect(sendTracker).toHaveBeenLastCalledWith({
          evn,
          action_type: '2_click_upgrade_now',
          action_details: section,
        });

        const popupTitle = 'Update ke Storefront 2.0?';
        expect(await screen.findByText(popupTitle)).toBeInTheDocument();
        expect(screen.getByTestId('modalCallout')).toBeInTheDocument();

        const checkRead = screen.getByRole('checkbox', {
          name: 'Saya sudah membaca dan mengerti.',
        });
        const btnConfirm = screen.getByRole('button', { name: 'Ya, update' });
        expect(checkRead).not.toBeChecked();
        expect(btnConfirm).toBeDisabled();

        await user.click(checkRead);
        expect(checkRead).toBeChecked();
        expect(btnConfirm).toBeEnabled();

        const btnCancel = screen.getByRole('button', {
          name: 'Tidak, batalkan',
        });
        await user.click(btnCancel);
        expect(sendTracker).toHaveBeenLastCalledWith({
          evn,
          action_type: '3-b_upgrade_popup_cancel',
          action_details: 'true',
        });

        await waitForElementToBeRemoved(() => screen.getByText(popupTitle));
      },
    );
  });

  describe('Upgrade action submit', () => {
    it('Should success upgrade', async () => {
      renderWithRoute({
        route: '/storefront-editor/update',
      });
      axios.onPost(`${API_URL.storefronts}/upgrades`).replyOnce(200, {});

      await user.click(await assertButtonUpdate('btn-update-main'));
      await user.click(
        screen.getByRole('checkbox', {
          name: 'Saya sudah membaca dan mengerti.',
        }),
      );
      await user.click(screen.getByRole('button', { name: 'Ya, update' }));
      expect(sendTracker).toHaveBeenLastCalledWith({
        evn,
        action_type: '3-a_upgrade_popup_confirm',
      });

      await waitFor(() =>
        expect(
          screen.getByText('Selamat datang di Storefront 2.0!'),
        ).toBeInTheDocument(),
      );
      expect(sendTracker).toHaveBeenCalledWith({
        evn,
        action_type: '4-a_upgrade_success',
      });

      const successUpgradeModal = screen.getByTestId('success-upgrade-modal');
      expect(within(successUpgradeModal).getByText('link ini')).toHaveAttribute(
        'href',
        UPDATE_LEARN_MORE_LINK,
      );
      await user.click(within(successUpgradeModal).getByText('link ini'));
      expect(sendTracker).toHaveBeenLastCalledWith({
        evn,
        action_type: '5-b_upgrade_learn_more',
        target_url: UPDATE_LEARN_MORE_LINK,
      });

      await user.click(
        within(successUpgradeModal).getByRole('button', { name: 'Oke' }),
      );
      expect(sendTracker).toHaveBeenLastCalledWith({
        evn,
        action_type: '5-a_upgrade_finish',
      });
    });

    it('Should failed upgrade', async () => {
      renderWithRoute({
        route: '/storefront-editor/update',
      });
      axios.onPost(`${API_URL.storefronts}/upgrades`).replyOnce(500, {
        errors: [{ message: 'User sudah upgrade ke versi 2.0', code: 11156 }],
        meta: { http_status: 422 },
      });

      await user.click(await assertButtonUpdate('btn-update-main'));
      await user.click(
        screen.getByRole('checkbox', {
          name: 'Saya sudah membaca dan mengerti.',
        }),
      );
      await user.click(screen.getByRole('button', { name: 'Ya, update' }));

      await waitFor(() =>
        expect(screen.getByText('Gagal update versi')).toBeInTheDocument(),
      );
      expect(sendTracker).toHaveBeenLastCalledWith({
        evn,
        action_type: '4-b_upgrade-error',
        action_details: 'User sudah upgrade ke versi 2.0',
      });
    });
  });
});
