import React, { useEffect } from 'react';
import { useBoolean } from 'usehooks-ts';
import { Card, Form, Image } from 'react-bootstrap';
import PreviewImage from '@assets/image/updateVersion/preview.png';
import MarketplaceIllustration from '@assets/image/marketplace.svg';
import FailedIllustration from '@assets/image/error-illustration.svg';

import { useUpgradeVersion } from '@hooks/api/upgrade';
import useUpgrade from '@hooks/useUpgrade';
import { sendUpgradeTracker } from '@utils/tracker/upgrade';
import { getErrorMessage } from '@utils/error';

import PageTitle from '@components/PageTitle';
import ButtonToTop from '@components/ButtonToTop';
import Modal from '@components/Modal';
import Callout from '@components/Callout';
import Button from '@components/Button';

import Dashboard from './partials/Dashboard';
import WebBuilder from './partials/WebBuilder';
import FAQ from './partials/FAQ';
import ActionUpdate from './partials/ActionUpdate';

const UpdateVersion = () => {
  const upgrade = useUpgradeVersion();
  const { successUpgrade } = useUpgrade();
  const { isLoading, isError } = upgrade;

  const { value: isShow, setTrue: show, setFalse: hide } = useBoolean(false);
  const { value: isChecked, setValue: setChecked } = useBoolean(false);

  useEffect(() => {
    void sendUpgradeTracker({ actionType: '1_visit_sf_upgrade_landing' });
  }, []);

  const upgradeVersion = () => {
    void sendUpgradeTracker({ actionType: '3-a_upgrade_popup_confirm' });
    upgrade.mutate(
      {},
      {
        onSuccess: () => {
          onCloseModal();
          successUpgrade();
          void sendUpgradeTracker({ actionType: '4-a_upgrade_success' });
        },
        onError: (error) => {
          void sendUpgradeTracker({
            actionType: '4-b_upgrade-error',
            actionDetails: getErrorMessage(error),
          });
        },
      },
    );
  };

  const onCloseModal = () => {
    setChecked(false);
    upgrade.reset();
    hide();
  };

  const onCancel = () => {
    void sendUpgradeTracker({
      actionType: '3-b_upgrade_popup_cancel',
      actionDetails: `${isChecked}`,
    });
    onCloseModal();
  };

  return (
    <>
      <PageTitle title="Storefront" withBack />
      <div className="m-auto update-version">
        <Card className="pt-[99px] pb-0 px-0 relative">
          <div className="vstack align-items-center gap-11">
            <div className="text-center px-16">
              <h3 className="text-2xl font-normal leading-9">Storefront 2.0</h3>
              <h2 className="max-w-md mb-8">
                Bikin website lebih praktis, performa bisnis naik drastis!
              </h2>
              <ActionUpdate testId="btn-update-main" onUpdate={show} />
              <p className="text-base my-8">Gratis tanpa biaya apapun</p>
            </div>
            <Image
              src={PreviewImage}
              className="mb-4 mx-auto img-responsive w-[848px]"
              alt="previewImage"
            />

            <Dashboard onUpdate={show} />
            <WebBuilder onUpdate={show} />
            <FAQ onUpdate={show} />
          </div>
          <Card.Footer>
            Setelah update, tidak bisa kembali ke versi lama.
          </Card.Footer>

          <ButtonToTop />
        </Card>
      </div>

      <Modal
        show={isShow && !isError}
        variant="confirmation"
        action={[
          <Button
            variant="light"
            className="bg-soft-grey text-[#212529] border-0"
            onClick={onCancel}
            disabled={isLoading}
            data-testid="button-confirm-cancel"
          >
            Tidak, batalkan
          </Button>,
          <Button
            onClick={upgradeVersion}
            disabled={!isChecked}
            isLoading={isLoading}
            className={`border-0${
              isChecked || isLoading ? ' bg-gradient' : ' is-disabled-30'
            }`}
            data-testid="button-confirm-update"
          >
            <span>
              <i className="fi fi-sr-magic-wand mr-3"></i> Ya, update
            </span>
          </Button>,
        ]}
        className="modal-confirmation--upgrade"
      >
        <div className={isLoading ? 'opacity-50' : ''}>
          <div className="flex justify-between">
            <div>
              <h3 className="text-2xl mb-6">Update ke Storefront 2.0?</h3>
              <h4 className="text-base">Nikmati manfaatnya!</h4>
              <ul className="ml-4 pl-1 font-light">
                <li>Tampilan yang lebih terbaru</li>
                <li>Pengaturan website yang lebih mudah</li>
              </ul>
            </div>
            <div>
              <Image
                src={MarketplaceIllustration}
                className="mx-auto img-responsive"
                alt="marketplace-illus"
              />
            </div>
          </div>
          <Callout
            text="Setelah update, tidak bisa kembali ke versi lama."
            variant="info"
            size="huge"
            className="mb-4 mt-4"
            testId="modalCallout"
          />
          <Form.Check
            label="Saya sudah membaca dan mengerti."
            id="read"
            key="read"
            checked={isChecked}
            value="read"
            name="read"
            onChange={() => setChecked(!isChecked)}
            className="form-check--upgrade ml-1"
            disabled={isLoading}
          />
        </div>
      </Modal>

      <Modal
        show={isShow && isError}
        variant="confirmation"
        action={[
          <Button variant="primary" size="lg" onClick={onCloseModal}>
            Oke
          </Button>,
        ]}
        className="modal-confirmation--upgrade"
      >
        <div className="text-center">
          <Image
            src={FailedIllustration}
            className="mb-8 mx-auto img-responsive w-[296px]"
            alt="failed-illus"
          />
          <p className="text-[22px] font-bold">Gagal update versi</p>
          <p className="text-base my-2">Silakan coba lagi beberapa saat.</p>
        </div>
      </Modal>
    </>
  );
};

export default UpdateVersion;
