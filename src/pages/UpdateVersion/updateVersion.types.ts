export interface ISectionProps {
  onUpdate: () => void;
}

export interface IActionUpdateProps extends ISectionProps {
  testId: string;
}

export interface IPopupUpdateProps {
  type: 'confirm' | 'failed';
  isShow: boolean;
  onClose: () => void;
}
