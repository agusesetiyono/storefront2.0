import React, { useState } from 'react';
import { Accordion } from 'react-bootstrap';

import { UPDATE_LEARN_MORE_LINK } from '@utils/constants';

import ActionUpdate from './ActionUpdate';
import type { ISectionProps } from '../updateVersion.types';

export const faqItems = [
  {
    question:
      'Jika saya update ke Storefront versi 2.0, apakah data di Storefront lama saya akan hilang?',
    answer:
      'Tidak. Semua data yang <strong>sudah Anda simpan</strong> seperti domain, SEO, google analytics, pengaturan, banner slider, homepage, dan lain-lain akan tetap tersimpan, dan tidak akan berubah.',
  },
  {
    question: 'Apa saja yang akan berubah pada Storefront versi 2.0?',
    answer: `Versi 2.0 memudahkan Anda untuk mendesain website dengan navigasi yang lebih instan dan mudah, dengan melihat langsung perubahan yang terjadi. Klik <a href="${UPDATE_LEARN_MORE_LINK}" target="_blank">link ini</a> untuk melihat detail perubahan lainnya.`,
  },
  {
    question: 'Apakah update ini bersifat gratis?',
    answer: 'Tentu saja. Update versi ini tidak dipungut biaya atau gratis.',
  },
  {
    question: 'Mengapa saya harus update ke versi terbaru?',
    answer:
      'Storefront versi terbaru dikembangkan untuk memberikan pengalaman yang lebih memudahkan Anda sebagai pengguna. Desain website jualan dan pengaturannya lebih mudah dengan tampilan versi terbaru. Selain itu juga, Storefront versi lama tidak akan diperbaharui lagi, sehingga Anda sangat disarankan untuk segera update ke Storefront 2.0.',
  },
  {
    question:
      'Jika saya sudah melakukan update, apakah saya bisa kembali ke Storefront versi lama?',
    answer:
      'Tidak. Jika sudah update ke versi terbaru, Anda tidak akan bisa kembali ke Storefront versi lama. Anda juga tidak dapat kembali ke StoreFront versi lama dalam masa trial sekali pun.',
  },
  {
    question: 'Apakah update berbeda dengan upgrade membership?',
    answer:
      'Berbeda. Update Storefront <strong>hanya mengubah tampilan</strong> Storefront dengan teknologi dan desain terbaru. Update tersebut <strong>tidak mengubah paket membership</strong> SmartSeller baik Freemium, Premium, maupun Platinum.',
  },
];

const FAQ = ({ onUpdate }: ISectionProps) => {
  const [activeFaq, setActiveFaq] = useState<string | undefined>('0');

  const handleToggleFaq = (eventKey: string) => () => {
    if (eventKey === activeFaq) {
      setActiveFaq(undefined);
    } else {
      setActiveFaq(eventKey);
    }
  };

  return (
    <div className="section faq">
      <h5>Pertanyaan</h5>
      <h4 className="mb-8">FAQ - Pertanyaan Seputar Produk</h4>
      <Accordion activeKey={activeFaq}>
        {faqItems?.map((faq, index) => {
          const eventKey = index.toString();
          const isActive = eventKey === activeFaq;

          return (
            <Accordion.Item eventKey={eventKey} key={index}>
              <span
                className={`accordion-icon${isActive ? ' active' : ''}`}
                onClick={handleToggleFaq(eventKey)}
                data-testid={`faq-toggle-${index}`}
              >
                <i className="fi-sr-plus"></i>
              </span>
              <Accordion.Header
                onClick={handleToggleFaq(eventKey)}
                data-testid={`faq-button-${index}`}
              >
                {faq.question}
              </Accordion.Header>
              <Accordion.Body>
                <div
                  dangerouslySetInnerHTML={{ __html: faq.answer }}
                  data-testid={`faq-body-${index}`}
                ></div>
              </Accordion.Body>
            </Accordion.Item>
          );
        })}
      </Accordion>
      <div className="text-center">
        <p className="max-w-full mb-2">
          Kamu bisa melihat detail perubahan dengan klik link di bawah ini
        </p>
        <a href={UPDATE_LEARN_MORE_LINK} target="_blank" className="font-bold">
          Pelajari Detailnya
        </a>
        <br />
        <ActionUpdate testId="btn-update-faq" onUpdate={onUpdate} />
      </div>
    </div>
  );
};

export default FAQ;
