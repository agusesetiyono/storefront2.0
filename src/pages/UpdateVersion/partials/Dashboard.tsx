import React from 'react';
import { Row, Col, Image } from 'react-bootstrap';
import DashboardImage from '@assets/image/updateVersion/dashboard.png';

import ActionUpdate from './ActionUpdate';
import type { ISectionProps } from '../updateVersion.types';

const Dashboard = ({ onUpdate }: ISectionProps) => (
  <div className="section dashboard">
    <Row className="relative">
      <Col md={4} sm={12}>
        <h5>Dashboard Baru</h5>
        <h4>
          Pengaturan <br />
          lebih terorganisir
        </h4>
        <p className="text-[#F4F4F4]">
          Lebih simpel hanya dengan 6 tab, Anda bisa atur segala aspek
          Storefront dengan mudah.
        </p>
        <ActionUpdate testId="btn-update-tab" onUpdate={onUpdate} />
      </Col>
      <Col md={8} sm={12} className="absolute">
        <Image
          src={DashboardImage}
          className="img-responsive w-full"
          alt="dashboardImage"
        />
      </Col>
    </Row>
  </div>
);

export default Dashboard;
