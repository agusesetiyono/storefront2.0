import React from 'react';

import Button from '@components/Button';
import { sendUpgradeTracker } from '@utils/tracker/upgrade';

import { IActionUpdateProps } from '../updateVersion.types';

const ActionUpdate = ({ testId, onUpdate }: IActionUpdateProps) => {
  const onClick = () => {
    void sendUpgradeTracker({
      actionType: '2_click_upgrade_now',
      actionDetails: testId.replace('btn-update-', ''),
    });
    onUpdate();
  };

  return (
    <Button className="btn-update" data-testid={testId} onClick={onClick}>
      Update Sekarang
    </Button>
  );
};

export default ActionUpdate;
