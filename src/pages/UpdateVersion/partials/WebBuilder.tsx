import React from 'react';
import { Row, Col, Image } from 'react-bootstrap';
import WebBuilderImage from '@assets/image/updateVersion/webBuilder.png';
import WebBuilderMenuImage from '@assets/image/updateVersion/webBuilderMenu.png';

import ActionUpdate from './ActionUpdate';
import type { ISectionProps } from '../updateVersion.types';

const WebBuilder = ({ onUpdate }: ISectionProps) => (
  <div className="section web-builder">
    <Row>
      <Col md={5} sm={12}>
        <h5>Website Builder</h5>
        <h4>Edit Website, Lihat Instan!</h4>
        <p>
          Selagi Anda mengedit tampilan website, kamu bisa langsung lihat
          hasilnya di halaman yang sama.
        </p>
        <ActionUpdate testId="btn-update-website_builder" onUpdate={onUpdate} />
      </Col>
      <Col md={7} sm={12}>
        <Image
          src={WebBuilderImage}
          className="img-responsive w-full"
          alt="webBuilderImage"
        />
      </Col>
    </Row>
    <Row className="mt-36">
      <Col md={5} sm={12}>
        <h6 className="text-base font-bold mb-2">Tambah section, satu klik.</h6>
        <p className="text-base font-normal mb-6">
          Dengan sistem pengaturan lewat toggle, custom website jadi semakin
          mudah.
        </p>
        <h6 className="text-base font-bold mb-2">
          Rapikan section, tinggal klik dan geser.
        </h6>
        <p className="text-base font-normal mb-6">
          Urutkan section dengan cara drag and drop.
        </p>
        <h6 className="text-base font-bold mb-2">
          Edit section, langsung lihat hasilnya.
        </h6>
        <p className="text-base font-normal">
          Kamu bisa melihat tampilan website yang kamu edit langsung di halaman
          yang sama.
        </p>
        <ActionUpdate testId="btn-update-sections" onUpdate={onUpdate} />
      </Col>
      <Col md={7} sm={12}>
        <Image
          src={WebBuilderMenuImage}
          className="img-responsive w-[330px] ml-8"
          alt="webBuilderMenuImage"
          data-testid="webBuilderMenuImage"
        />
      </Col>
    </Row>
  </div>
);

export default WebBuilder;
