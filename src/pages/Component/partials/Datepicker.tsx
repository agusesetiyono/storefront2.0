import React, { useState } from 'react';
import { Stack } from 'react-bootstrap';
import { QuestionCircleFill } from 'react-bootstrap-icons';
import Datepicker from '@components/Datepicker';
import Popover from '@components/Popover';

const CompDatepicker = () => {
  const [startTime, setStartTime] = useState<Date>();
  const [endTime, setEndTime] = useState<Date>();

  const defaultDatetimeRangeOptions = {
    dateFormat: 'd-m-Y H:i',
    enableTime: true,
    time_24hr: true,
  };

  return (
    <div>
      <h3 className="text-4xl mb-2">
        Datepicker{' '}
        <Popover
          title="Datepicker Example"
          content={
            <>
              <strong>Example:</strong>{' '}
              <a href="https://flatpickr.js.org/examples/">Flatpickr example</a>
              .
            </>
          }
        >
          <QuestionCircleFill />
        </Popover>
      </h3>
      <div className="max-w-lg">
        <p>
          <strong>Basic</strong>
        </p>
        <Datepicker
          hint="Choose date!"
          placeholder="YYYY-MM-DD"
          smallLabel="From"
        />

        <p>
          <strong>Date Format</strong>
        </p>
        <Datepicker
          options={{ dateFormat: 'd/m/Y H:i', enableTime: true }}
          placeholder="DD/MM/YYYY HH:mm"
        />

        <p>
          <strong>Min and Max Date</strong>
        </p>
        <Datepicker
          options={{
            minDate: 'today',
            maxDate: (new Date() as any)?.fp_incr(7),
          }}
          placeholder="YYYY-MM-DD"
        />

        <p>
          <strong>Time Picker</strong>
        </p>
        <Datepicker
          options={{ enableTime: true, noCalendar: true, time_24hr: true }}
          placeholder="HH:mm"
        />

        <p>
          <strong>Date Range Picker</strong>
        </p>
        <Datepicker options={{ mode: 'range' }} placeholder="YYYY-MM-DD" />

        <p>
          <strong>Date Range Picker 2 Field</strong>
        </p>

        <Stack direction="horizontal" gap={2}>
          <Datepicker
            smallLabel="Mulai"
            placeholder="DD-MM-YYYY HH:mm"
            options={{
              ...defaultDatetimeRangeOptions,
              minDate: 'today',
              minTime: new Date().getTime(),
              maxDate: endTime,
            }}
            value={startTime}
            onChange={([date]) => setStartTime(date)}
          />
          <Datepicker
            smallLabel="Selesai"
            placeholder="DD-MM-YYYY HH:mm"
            options={{
              ...defaultDatetimeRangeOptions,
              minDate: startTime || 'today',
            }}
            value={endTime}
            onChange={([date]) => setEndTime(date)}
          />
        </Stack>
      </div>
    </div>
  );
};

export default CompDatepicker;
