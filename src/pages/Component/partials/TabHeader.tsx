import React from 'react';
import { Button } from 'react-bootstrap';
import TabHeader from '@components/TabHeader';

const CompTabHeader = () => (
  <div>
    <h3 className="text-4xl mb-2">Tab Header</h3>
    <TabHeader
      title="Tema Website Kamu"
      subtitle="Ini adalah tema yang lagi aktif digunakan di website kamu."
      action={[
        <Button href="#" className="w-[160px] py-2 text-light">
          Customize
        </Button>,
      ]}
    />
  </div>
);

export default CompTabHeader;
