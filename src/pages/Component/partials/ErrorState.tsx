import React from 'react';
import ErrorState from '@components/ErrorState';

const CompErrorState = () => (
  <div>
    <h3 className="text-4xl mb-2">Error state</h3>
    <ErrorState
      title="Error title"
      content="Sample error message"
      onRetry={() => {
        alert('Coba Lagi');
      }}
    />
  </div>
);

export default CompErrorState;
