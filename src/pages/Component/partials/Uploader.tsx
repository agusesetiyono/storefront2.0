import React from 'react';
import { Row, Col } from 'react-bootstrap';

import Uploader from '@components/Uploader';

const CompUploader = () => {
  return (
    <div>
      <h3 className="text-4xl mb-2">Uploader</h3>
      <Row>
        <Col md={2}>
          <Uploader aspectRatio="1:1" />
        </Col>
      </Row>
    </div>
  );
};

export default CompUploader;
