import React from 'react';
import EmptyState from '@components/EmptyState';

const CompEmptyState = () => (
  <div>
    <h3 className="text-4xl mb-2">Empty state</h3>
    <EmptyState title="Empty title" content={<p>Your data is empty!</p>} />
  </div>
);

export default CompEmptyState;
