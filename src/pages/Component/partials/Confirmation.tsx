import React, { useState } from 'react';
import { Stack } from 'react-bootstrap';
import Confirmation from '@components/Confirmation';
import Button from '@components/Button';

const CompConfirmation = () => {
  const [showModal, setShowModal] = useState<boolean>(false);
  const [showErrorModal, setErrorShowModal] = useState<boolean>(false);
  const [showSuccessModal, setSuccessShowModal] = useState<boolean>(false);
  const handleCloseModal = () => setShowModal(false);
  const handleSuccessCloseModal = () => setSuccessShowModal(false);
  const handleErrorCloseModal = () => setErrorShowModal(false);
  return (
    <div>
      <h3 className="text-4xl mb-2">Confirmation</h3>
      <Stack gap={1} direction="horizontal">
        <Button onClick={() => setShowModal(true)} variant="primary" size="sm">
          Update sekarang
        </Button>
        <Button
          onClick={() => setSuccessShowModal(true)}
          variant="primary"
          size="sm"
        >
          Success
        </Button>
        <Button
          onClick={() => setErrorShowModal(true)}
          variant="primary"
          size="sm"
        >
          Error
        </Button>
      </Stack>

      <Confirmation
        show={showModal}
        variant="confirm"
        onClose={handleCloseModal}
      />

      <Confirmation
        show={showErrorModal}
        variant="error"
        onClose={handleErrorCloseModal}
      />

      <Confirmation
        show={showSuccessModal}
        variant="success"
        onClose={handleSuccessCloseModal}
      />
    </div>
  );
};

export default CompConfirmation;
