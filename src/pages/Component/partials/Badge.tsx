import React from 'react';
import { Stack } from 'react-bootstrap';

import Badge from '@components/Badge';
import type { TBadgeVariant } from '@components/Badge/badge.types';

const CompBadge = () => (
  <div>
    <h3 className="text-4xl mb-2">Badge</h3>

    <Stack gap={1} direction="horizontal">
      {['default', 'success', 'danger', 'info', 'warning'].map((variant) => (
        <Badge key={variant} variant={variant as TBadgeVariant}>
          {variant}
        </Badge>
      ))}

      <Badge variant="default" isEllipsis>
        Badge ellipsis variant
      </Badge>

      <Badge variant="default" disabled>
        disabled
      </Badge>

      <Badge variant="success" icon="bell">
        With Icon
      </Badge>
    </Stack>
  </div>
);

export default CompBadge;
