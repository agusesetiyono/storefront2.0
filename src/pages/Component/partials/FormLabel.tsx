import React from 'react';
import FormLabel from '@components/FormLabel';

const CompFormLabel = () => (
  <div>
    <h3 className="text-4xl mb-2">Form label</h3>
    <FormLabel
      label="Form label"
      description="This is description about the label."
    />
  </div>
);

export default CompFormLabel;
