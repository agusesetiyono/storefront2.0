import React from 'react';
import LoadingState from '@components/LoadingState';

const CompLoadingState = () => (
  <div>
    <h3 className="text-4xl mb-2">Loading state</h3>
    <LoadingState />
  </div>
);

export default CompLoadingState;
