import React from 'react';
import { Form } from 'react-bootstrap';
import { Formik } from 'formik';
import * as Yup from 'yup';

import InputNumber from '@components/InputNumber';

const validationSchema = Yup.object().shape({
  price: Yup.number().max(999999999, 'Maksimal 9 digit angka').required(),
  percentage: Yup.number().max(100, 'Maksimal 100%').required(),
});

const CompInputNumber = () => {
  return (
    <div className="max-w-lg">
      <h3 className="text-4xl mb-2">Input Number</h3>
      <p>
        <strong>Default</strong>
      </p>
      <InputNumber />

      <p>
        <strong>With max value validation</strong>
      </p>
      <Formik
        validationSchema={validationSchema}
        onSubmit={(values) => alert(values)}
        initialValues={{ price: 0, percentage: 0 }}
      >
        {({ handleSubmit, setFieldValue, values, errors }) => (
          <Form noValidate onSubmit={handleSubmit}>
            <Form.Group>
              <Form.Label>Harga</Form.Label>
              <InputNumber
                name="price"
                prepend="Rp"
                value={values.price}
                onChangeNumber={(price) => setFieldValue('price', price)}
                isInvalid={!!errors.price}
                errorMessage={errors.price}
                hasValidation
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Persentase</Form.Label>
              <InputNumber
                name="percentage"
                append="%"
                value={values.percentage}
                onChangeNumber={(percentage) =>
                  setFieldValue('percentage', percentage)
                }
                isInvalid={!!errors.percentage}
                errorMessage={errors.percentage}
                hasValidation
              />
            </Form.Group>
          </Form>
        )}
      </Formik>

      <p>
        <strong>Block input above max value</strong>
      </p>
      <InputNumber hasValidation append="%" maxValue={100} errorMessage="" />
    </div>
  );
};

export default CompInputNumber;
