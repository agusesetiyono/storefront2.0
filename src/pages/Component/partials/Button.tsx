import React, { useState, useEffect } from 'react';
import Button from '@components/Button';

const CompButton = () => {
  const simulateNetworkRequest = () => {
    return new Promise((resolve) => setTimeout(resolve, 2000));
  };

  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    if (isLoading) {
      void simulateNetworkRequest().then(() => {
        setLoading(false);
      });
    }
  }, [isLoading]);

  const handleClick = () => setLoading(true);

  return (
    <div>
      <h3 className="text-4xl mb-2">Button</h3>
      <Button
        className="mr-4 w-40"
        onClick={handleClick}
        isLoading={isLoading}
        spinnerClass="my-1"
      >
        Click to load
      </Button>
    </div>
  );
};

export default CompButton;
