import React, { useState } from 'react';
import { Stack, Image, Button, Form } from 'react-bootstrap';
import StepImage from '@assets/image/activation.svg';

import FormWizard from '@components/FormWizard';
import type {
  IFormWizardStep,
  IStepProps,
} from '@components/FormWizard/formWizard.types';

const CompFormWizard = () => {
  const [activeStep, setActiveStep] = useState<number>(0);

  const handleNextStep = () => setActiveStep(activeStep + 1);
  const handleSkip = () => setActiveStep(3);

  const steps: IFormWizardStep[] = [
    {
      title: 'Step 1',
      form: Step,
    },
    {
      title: 'Step 2',
      form: Step,
    },
    {
      title: 'Step 3',
      form: Step,
    },
  ];

  return (
    <div>
      <h3 className="text-4xl mb-2">Form Wizard</h3>

      <Stack gap={3} className="w-2/3">
        <FormWizard
          steps={steps}
          activeStep={activeStep}
          onNext={handleNextStep}
          onSkip={handleSkip}
        />
      </Stack>
    </div>
  );
};

const Step = ({ step, active, completed, onNext }: IStepProps) => (
  <Stack gap={4}>
    <Stack direction="horizontal" gap={3}>
      <Image src={StepImage} className="w-40" />
      <Stack gap={2}>
        <h3 className="text-3xl">Step {step}</h3>
        {completed ? (
          <p>Success message for step {step}</p>
        ) : (
          <p>Short description for this step.</p>
        )}
      </Stack>
    </Stack>
    <p className={completed ? 'text-green-600' : 'text-gray-500'}>
      Langkah {step} {completed ? 'berhasil' : ''}
    </p>
    {active && (
      <>
        <hr />
        <Form noValidate>
          <Form.Group>
            <Form.Label>Field Step {step}</Form.Label>
            <Form.Control type="text" />
          </Form.Group>
        </Form>
        <Button onClick={onNext} className="w-36 self-end">
          Lanjutkan
        </Button>
      </>
    )}
  </Stack>
);

export default CompFormWizard;
