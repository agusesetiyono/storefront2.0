import React from 'react';
import { Button, Stack } from 'react-bootstrap';
import toast from '@utils/toast';

const CompNotification = () => (
  <div>
    <h3 className="text-4xl mb-2">Notification (Should run in ngorder2017)</h3>
    <Stack gap={1} direction="horizontal">
      {['info', 'danger', 'success', 'warning'].map((type, index) => (
        <Button
          key={`button-${index}`}
          onClick={() => toast[type](`This is an ${type} notification`)}
          variant={type}
          size="sm"
        >
          Notify {type}
        </Button>
      ))}
    </Stack>
  </div>
);

export default CompNotification;
