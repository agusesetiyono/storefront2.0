import React, { ReactNode, useState } from 'react';
import { Stack } from 'react-bootstrap';

import Modal from '@components/Modal';
import { TModalSize, TModalVariant } from '@components/Modal/modal.types';
import Button from '@components/Button';

const CompModal = () => {
  const [showModal, setShowModal] = useState<boolean>(false);

  const [modalConfig, setModalConfig] = useState<{
    size?: TModalSize;
    variant?: TModalVariant;
    action: ReactNode[];
  }>({ size: 'sm', action: [] });

  const getModalConfig = () => {
    const isConfirmation = modalConfig.variant === 'confirmation';
    if (isConfirmation) {
      return { title: 'Confirmation Modal', content: <p>Are you sure?</p> };
    }

    switch (modalConfig.size) {
      case 'sm':
        return { title: 'Small Modal', content: <p>Small modal content!</p> };
      case 'lg':
        return { title: 'Large Modal', content: <p>Large modal content!</p> };
      default:
        return {
          title: 'Default Modal',
          content: <p>Default modal content!</p>,
        };
    }
  };

  const handleModal =
    (size?: 'sm' | 'lg', variant?: TModalVariant, action?: ReactNode[]) =>
    () => {
      setModalConfig({
        size,
        variant: variant || 'simple',
        action: action || [],
      });
      setShowModal(true);
    };

  const handleCloseModal = () => setShowModal(false);

  return (
    <div>
      <h3 className="text-4xl mb-2">Modal</h3>

      <Stack gap={1} direction="horizontal">
        <Button onClick={handleModal()} variant="primary" size="sm">
          Default Modal
        </Button>
        <Button onClick={handleModal('sm')} variant="primary" size="sm">
          Small Modal
        </Button>
        <Button onClick={handleModal('lg')} variant="primary" size="sm">
          Large Modal
        </Button>
        <Button
          onClick={handleModal('lg', 'simple', [
            <Button variant="secondary" onClick={handleCloseModal} size="sm">
              Close
            </Button>,
            <Button variant="primary" onClick={handleCloseModal} size="sm">
              Save Changes
            </Button>,
          ])}
          variant="primary"
          size="sm"
        >
          With Action
        </Button>
        <Button
          onClick={handleModal('sm', 'confirmation', [
            <Button variant="primary" onClick={handleCloseModal}>
              Batal
            </Button>,
            <Button variant="outline-primary" onClick={handleCloseModal}>
              Hapus
            </Button>,
          ])}
          variant="primary"
          size="sm"
        >
          Confirmation Modal
        </Button>
      </Stack>

      <Modal
        show={showModal}
        title={getModalConfig()?.title}
        size={modalConfig.size}
        variant={modalConfig.variant}
        action={modalConfig.action}
        onClose={handleCloseModal}
      >
        {getModalConfig()?.content}
      </Modal>
    </div>
  );
};

export default CompModal;
