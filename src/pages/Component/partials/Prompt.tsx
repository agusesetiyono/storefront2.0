import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import { usePrompt } from '@hooks/usePrompt';
import Button from '@components/Button';

const CompPrompt = () => {
  const [isPromptActive, setPromptStatus] = useState(false);
  usePrompt(isPromptActive);

  return (
    <div>
      <h3 className="text-4xl mb-2">
        Prompt (status: {isPromptActive ? 'active' : 'not active'})
      </h3>
      <div className="flex">
        <Button
          onClick={() => setPromptStatus(true)}
          size="sm"
          className="mr-1"
        >
          Set prompt active
        </Button>
        <Button
          onClick={() => setPromptStatus(false)}
          size="sm"
          variant="outline-primary"
        >
          Set prompt not active
        </Button>
      </div>
      <NavLink to="/settings/domain">
        Click this link to show prompt (when active)
      </NavLink>
    </div>
  );
};

export default CompPrompt;
