import React, { useState } from 'react';
import { Button, Form, Stack } from 'react-bootstrap';

import Table from '@components/Table';
import type { ITableColumn } from '@components/Table/table.types';
import EmptyState from '@components/EmptyState';
import ErrorState from '@components/ErrorState';

const initialTableData = [
  {
    no: 1,
    firstName: 'Mark',
    lastName: 'Otto',
    username: '@mdo',
  },
  {
    no: 2,
    firstName: 'Jacob',
    lastName: 'Thornton',
    username: '@fat',
  },
  {
    no: 3,
    firstName: 'Musk',
    lastName: 'Elon',
    username: '@elon',
  },
];

const CompTable = () => {
  const [sortedData, setSortedData] = useState(initialTableData);

  const tableColumns: ITableColumn<any>[] = [
    {
      key: 'no',
      dataIndex: 'no',
      title: 'No',
      width: 80,
    },
    {
      key: 'firstName',
      dataIndex: 'firstName',
      title: 'First Name',
    },
    {
      key: 'lastName',
      dataIndex: 'lastName',
      title: 'Last Name',
    },
    {
      key: 'username',
      dataIndex: 'username',
      title: 'Username',
    },
    {
      key: 'no',
      dataIndex: 'no',
      title: 'Action',
      width: 150,
      render: (value) => (
        <Stack direction="horizontal" gap={2}>
          <Form.Check type="switch" id="custom-switch" disabled />
          <Button variant="primary" size="sm">
            Table Action {value}
          </Button>
        </Stack>
      ),
    },
  ];

  return (
    <div>
      <h3 className="text-4xl mb-2">Table</h3>
      <p>
        <strong>Default</strong>
      </p>
      <Table
        data={initialTableData}
        columns={tableColumns}
        striped
        bordered
        borderless
      />

      <p>
        <strong>Sortable</strong>
      </p>
      <Table
        data={sortedData}
        columns={tableColumns}
        sortable
        bordered
        hideHeader
        handleSort={setSortedData}
      />

      <p>
        <strong>With Empty State</strong>
      </p>
      <Table
        data={[]}
        columns={tableColumns}
        bordered
        emptyState={
          <EmptyState
            title="Halaman tidak ditemukan"
            content={
              <p>
                Ayo buat halaman pertama kamu semudah klik tombol{' '}
                <b>Tambah Halaman</b> di kanan atas!
              </p>
            }
          />
        }
      />

      <p>
        <strong>With Error State</strong>
      </p>
      <Table
        data={initialTableData}
        columns={tableColumns}
        bordered
        isErrorFetch
        errorState={
          <ErrorState
            title="Gagal mengambil data halaman"
            content="Klik tombol dibawah ini atau refresh untuk mencoba lagi"
            onRetry={() => alert('Coba lagi')}
          />
        }
      />

      <p>
        <strong>With Loading State</strong>
      </p>
      <Table
        data={initialTableData}
        columns={tableColumns}
        bordered
        isLoading
      />
    </div>
  );
};

export default CompTable;
