import React from 'react';
import { Button, Stack } from 'react-bootstrap';

import Popover from '@components/Popover';
import type { TPopoverPlacement } from '@components/Popover/popover.types';

const CompPopover = () => (
  <div>
    <h3 className="text-4xl mb-2">Popover</h3>

    <Stack gap={1} direction="horizontal">
      <>
        {['top', 'right', 'bottom', 'left'].map((placement) => (
          <Popover
            title={`Popover ${placement}`}
            key={placement}
            placement={placement as TPopoverPlacement}
            content={
              <>
                <strong>Holy guacamole!</strong> Check this info.
              </>
            }
          >
            <Button variant="primary" size="sm">
              Popover on {placement}
            </Button>
          </Popover>
        ))}

        <Popover
          title={`Popover on Hover`}
          trigger={['hover', 'focus']}
          content={
            <>
              <strong>Holy guacamole!</strong> Check this info.
            </>
          }
        >
          <Button variant="primary" size="sm">
            Popover on Hover
          </Button>
        </Popover>
      </>
    </Stack>
  </div>
);

export default CompPopover;
