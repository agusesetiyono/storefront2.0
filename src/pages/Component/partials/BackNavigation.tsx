import React from 'react';
import BackNavigation from '@components/BackNavigation';

const CompBackNavigation = () => {
  return (
    <div>
      <h3 className="text-4xl mb-2">Back Navigation</h3>
      <BackNavigation
        to="/settings/domain"
        text="Click to back to domain tab"
      />
    </div>
  );
};

export default CompBackNavigation;
