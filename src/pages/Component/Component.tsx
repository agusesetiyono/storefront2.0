import React from 'react';
import { Card, Stack } from 'react-bootstrap';

import PageTitle from '@components/PageTitle';
import { PromptContext } from '@hooks/usePrompt';

import CompButton from './partials/Button';
import CompBadge from './partials/Badge';
import CompNotification from './partials/Notification';
import CompModal from './partials/Modal';
import CompPopover from './partials/Popover';
import CompTable from './partials/Table';
import CompUploader from './partials/Uploader';
import CompDatepicker from './partials/Datepicker';
import CompValidation from './partials/Validation';
import CompInputNumber from './partials/InputNumber';
import CompFormWizard from './partials/FormWizard';
import CompErrorState from './partials/ErrorState';
import CompEmptyState from './partials/EmptyState';
import CompLoadingState from './partials/LoadingState';
import CompTabHeader from './partials/TabHeader';
import CompFormLabel from './partials/FormLabel';
import CompBackNavigation from './partials/BackNavigation';
import CompPrompt from './partials/Prompt';

const Component = () => {
  return (
    <>
      <PageTitle title="Component List" />

      <Card>
        <Stack gap={4}>
          <CompButton />
          <CompBadge />
          <CompNotification />
          <CompModal />
          <CompPopover />
          <CompTable />
          <CompUploader />
          <CompDatepicker />
          <CompValidation />
          <CompInputNumber />
          <CompFormWizard />
          <CompErrorState />
          <CompEmptyState />
          <CompLoadingState />
          <CompTabHeader />
          <CompFormLabel />
          <CompBackNavigation />
          <PromptContext>
            <CompPrompt />
          </PromptContext>
        </Stack>
      </Card>
    </>
  );
};

export default Component;
