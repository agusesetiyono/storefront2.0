import React from 'react';
import { Stack, Form, Button } from 'react-bootstrap';
import { PencilSquare, TrashFill } from 'react-bootstrap-icons';
import { useBoolean } from 'usehooks-ts';
import { Link } from 'react-router-dom';

import { onSuccessMutate, onErrorMutate } from '@utils/reactQuery';
import { sendDashboardTracker } from '@utils/tracker/dashboard';
import Modal from '@components/Modal';

import { useDeletePage, useUpdateStatusPage } from '@hooks/api/pages';

import { EPageStatus, ITableActionProps } from '../managePages.types';

const TableAction = ({ data, refetchData }: ITableActionProps) => {
  const deletePage = useDeletePage();
  const updateStatusPage = useUpdateStatusPage(data.id || 0);
  const {
    value: isConfirmDelete,
    setTrue: showConfirmDelete,
    setFalse: closeConfirmDelete,
  } = useBoolean(false);

  const onDelete = () => {
    const pageId = data?.id;
    if (pageId) {
      deletePage.mutate(pageId, {
        onSuccess: (data) => {
          onSuccessMutate(data, refetchData);
          void sendDashboardTracker({
            actionType: 'delete_page_success',
            actionDetails: `${pageId}`,
          });
          closeConfirmDelete();
        },
        onError: (error) => {
          onErrorMutate(error, refetchData);
          closeConfirmDelete();
        },
      });
    }
  };

  const onStatusChange = () => {
    if (data?.id) {
      updateStatusPage.mutate(
        {
          status:
            data?.status === EPageStatus.PUBLISHED
              ? EPageStatus.DRAFT
              : EPageStatus.PUBLISHED,
        },
        {
          onSuccess: (data) => onSuccessMutate(data, refetchData),
          onError: (error) => onErrorMutate(error, refetchData),
          onSettled: () => {
            void sendDashboardTracker({
              actionType: 'toggle_page',
              actionDetails: `${data.id}`,
            });
          },
        },
      );
    }
  };

  return (
    <>
      <Stack direction="horizontal" gap={4}>
        <Form.Check
          type="switch"
          id="custom-switch"
          checked={data.status !== EPageStatus.DRAFT}
          data-testid={`switch-status-page-${data.id}`}
          onChange={onStatusChange}
        />
        <Link
          to={`edit/${data.id}`}
          data-testid={`edit-link-${data.id}`}
          className="text-black"
        >
          <Button
            variant="outline-secondary"
            className="border-basic-1-opacity"
          >
            <PencilSquare className="icon-base" />
          </Button>
        </Link>
        <Button
          variant="outline-secondary"
          className="border-basic-1-opacity"
          data-testid={`button-delete-${data.id}`}
          onClick={showConfirmDelete}
        >
          <TrashFill className="icon-base text-red-400" />
        </Button>
      </Stack>

      <Modal
        show={isConfirmDelete}
        title="Hapus Halaman"
        variant="confirmation"
        action={[
          <Button
            variant="primary"
            onClick={closeConfirmDelete}
            data-testid="button-cancel-delete"
          >
            Batal
          </Button>,
          <Button
            variant="outline-primary"
            onClick={onDelete}
            data-testid="button-confirm-delete"
          >
            Hapus
          </Button>,
        ]}
      >
        <p>Apakah kamu yakin ingin menghapus halaman ini?</p>
      </Modal>
    </>
  );
};

export default TableAction;
