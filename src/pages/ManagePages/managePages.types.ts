import { IPage } from '@interfaces/pages';

export interface ITableActionProps {
  data: IPage;
  refetchData: () => void;
}

export enum EPageStatus {
  DRAFT = 'draft',
  PUBLISHED = 'published',
}
