import axios from '@mocks/axios';
import '@testing-library/jest-dom';
import { screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';

import { renderWithRoute, API_URL } from '@utils/test-helper';
import toast from '@utils/toast';
import { sendTracker } from '@utils/tracker';

import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';
import { GET_PAGES_SUCCESS_RESPONSE } from '@mocks/api/pages';

jest.mock('@utils/toast', () => ({
  success: jest.fn(),
  danger: jest.fn(),
}));

axios.onGet(`${API_URL.storefronts}/status`).reply(
  200,
  getMockSubscriptionsResponse({
    type: 'platinum',
    expiredDate: '2022-05-01T00:00:00+07:00',
  }),
);

let user: UserEvent;
beforeEach(() => {
  user = userEvent.setup({ delay: null });

  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));

  localStorage.setItem('onboardingStep', '5'); // already finished onboarding

  axios.onGet(`${API_URL.pages}`).reply(200, GET_PAGES_SUCCESS_RESPONSE);
});

afterEach(() => {
  jest.useRealTimers();
  jest.clearAllMocks();
  localStorage.clear();
});

describe('Manage Pages Page', () => {
  const route = '/storefront-editor/settings/manage-pages';
  const defaultTrackerParams = {
    evn: 'ngorder_sf_dashboard_actions',
    active_tab: 'atur_halaman',
  };

  it('Should match snapshot', async () => {
    const { container } = renderWithRoute({ route });

    expect(
      await screen.findByText(
        'Tambah atau edit halaman yang ingin kamu tampilkan di Storefront kamu.',
      ),
    ).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });

  it('Should render sections correctly', async () => {
    renderWithRoute({ route });

    // Manage Pages section
    expect(
      await screen.findByText(
        'Tambah atau edit halaman yang ingin kamu tampilkan di Storefront kamu.',
      ),
    ).toBeInTheDocument();
    expect(
      screen.getByRole('button', { name: 'Tambah Halaman' }),
    ).toBeInTheDocument();

    //table fetch data correctly
    expect(await screen.findByText('halamannya baru nih')).toBeInTheDocument();
    expect(screen.getAllByTestId(/switch-status-page/i).length).toEqual(10);

    //cannot add pages when data >= 10
    const message =
      'Maks. 10 halaman. Untuk tambah halaman, hapus yang lain terlebih dahulu.';
    expect(GET_PAGES_SUCCESS_RESPONSE.data.length).toEqual(10);
    await user.click(screen.getByRole('button', { name: 'Tambah Halaman' }));
    await waitFor(() => expect(toast.danger).toHaveBeenLastCalledWith(message));
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'add_page',
    });

    // have correct edit link
    expect(screen.getByTestId('edit-link-1')).toHaveAttribute(
      'href',
      '/storefront-editor/settings/manage-pages/edit/1',
    );
  });

  it('Should render empty state correctly', async () => {
    renderWithRoute({ route });

    // refetch with empty data success response
    axios.onGet(`${API_URL.pages}`).reply(200, {
      data: [],
    });

    //table fetch empty state correctly
    await waitFor(
      () =>
        expect(screen.getByText('Halaman tidak ditemukan')).toBeInTheDocument(),
      { timeout: 10000 },
    );
  });

  it('Should render error state correctly', async () => {
    axios.onGet(`${API_URL.pages}`).reply(500);
    renderWithRoute({ route });

    await waitFor(
      () =>
        expect(
          screen.getByText('Gagal mengambil data halaman'),
        ).toBeInTheDocument(),
      { timeout: 10000 },
    );

    // refetch with success response
    axios.onGet(`${API_URL.pages}`).reply(200, GET_PAGES_SUCCESS_RESPONSE);

    await user.click(screen.getByRole('button', { name: 'Coba Lagi' }));

    //table fetch data correctly
    expect(await screen.findByText('halamannya baru nih')).toBeInTheDocument();
    expect(screen.getAllByTestId(/switch-status-page/i).length).toEqual(10);
  });

  describe('Manage pages reorder action', () => {
    it('Should successfuly reorder page', async () => {
      renderWithRoute({ route });
      const successMessage = 'Berhasil memperbaharui urutan halaman.';
      axios.onPut(`${API_URL.pages}/orders`).replyOnce(200, {
        message: successMessage,
      });

      const drag1 = await screen.findByTestId('drag-handle-1');
      const drag2 = await screen.findByTestId('drag-handle-2');

      await user.pointer([
        { keys: '[MouseLeft>]', target: drag1 },
        { target: drag2 },
        { keys: '[/MouseLeft]' },
      ]);

      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(successMessage),
      );
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'drag_page',
        action_details: '2,3,4,5,6,7,8,9,10,1',
      });
    });

    it('Should failed reorder page', async () => {
      renderWithRoute({ route });
      const errorMessage =
        'Daftar halaman telah berubah. Silahkan refresh ulang halaman ini';
      axios.onPut(`${API_URL.pages}/orders`).replyOnce(500, {
        errors: [
          {
            message: errorMessage,
            code: 11127,
          },
        ],
        meta: {
          http_status: 500,
        },
      });

      const drag1 = await screen.findByTestId('drag-handle-2');
      const drag2 = await screen.findByTestId('drag-handle-1');

      await user.pointer([
        { keys: '[MouseLeft>]', target: drag1 },
        { target: drag2 },
        { keys: '[/MouseLeft]' },
      ]);

      await waitFor(() =>
        expect(toast.danger).toHaveBeenLastCalledWith(errorMessage),
      );
    });
  });

  describe('Manage pages delete action', () => {
    it('Should successfuly delete page', async () => {
      renderWithRoute({ route });
      const successMessage = 'Berhasil menghapus halaman.';
      axios.onDelete(`${API_URL.pages}/1`).replyOnce(200, {
        message: successMessage,
      });

      const buttonDelete = await screen.findByTestId('button-delete-1');
      await user.click(buttonDelete);

      expect(
        await screen.findByText(
          'Apakah kamu yakin ingin menghapus halaman ini?',
        ),
      ).toBeInTheDocument();

      await user.click(await screen.findByTestId('button-confirm-delete'));
      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(successMessage),
      );
      await waitFor(() =>
        expect(sendTracker).toHaveBeenLastCalledWith({
          ...defaultTrackerParams,
          action_type: 'delete_page_success',
          action_details: '1',
        }),
      );
    });

    it('Should failed delete page', async () => {
      renderWithRoute({ route });
      const errorMessage =
        'Daftar halaman telah berubah. Silahkan refresh ulang halaman ini';
      axios.onDelete(`${API_URL.pages}/1`).replyOnce(500, {
        errors: [
          {
            message: errorMessage,
            code: 11127,
          },
        ],
        meta: {
          http_status: 500,
        },
      });

      const buttonDelete = await screen.findByTestId('button-delete-1');
      await user.click(buttonDelete);

      expect(
        await screen.findByText(
          'Apakah kamu yakin ingin menghapus halaman ini?',
        ),
      ).toBeInTheDocument();

      await user.click(await screen.findByTestId('button-confirm-delete'));
      await waitFor(() =>
        expect(toast.danger).toHaveBeenLastCalledWith(errorMessage),
      );
    });
  });

  describe('Manage pages update status action', () => {
    it('Should successfuly update status draft to publish page', async () => {
      renderWithRoute({ route });
      const successPublishMessage = 'Berhasil publikasi halaman.';
      axios.onPatch(`${API_URL.pages}/1/status`).replyOnce(200, {
        message: successPublishMessage,
      });

      const buttonStatus = await screen.findByTestId('switch-status-page-1');
      await user.click(buttonStatus);
      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(successPublishMessage),
      );
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'toggle_page',
        action_details: '1',
      });
    });

    it('Should failed update status draft to publish page', async () => {
      renderWithRoute({ route });
      const errorPublishMessage =
        'Gagal publikasi halaman. Cobalah beberapa saat lagi.';
      axios.onPatch(`${API_URL.pages}/1/status`).replyOnce(500, {
        errors: [
          {
            message: errorPublishMessage,
            code: 11127,
          },
        ],
        meta: {
          http_status: 500,
        },
      });

      const buttonStatus = await screen.findByTestId('switch-status-page-1');
      await user.click(buttonStatus);

      await waitFor(() =>
        expect(toast.danger).toHaveBeenLastCalledWith(errorPublishMessage),
      );
    });

    it('Should successfuly update status publish to draft page', async () => {
      renderWithRoute({ route });
      const successPublishMessage = 'Berhasil menyimpan sebagai draft.';
      axios.onPatch(`${API_URL.pages}/2/status`).replyOnce(200, {
        message: successPublishMessage,
      });

      const buttonStatus = await screen.findByTestId('switch-status-page-2');
      await user.click(buttonStatus);
      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(successPublishMessage),
      );
    });

    it('Should failed update status publish to draft page', async () => {
      renderWithRoute({ route });
      const errorPublishMessage =
        'Gagal menyimpan sebagai draft. Cobalah beberapa saat lagi.';
      axios.onPatch(`${API_URL.pages}/2/status`).replyOnce(500, {
        errors: [
          {
            message: errorPublishMessage,
            code: 11127,
          },
        ],
        meta: {
          http_status: 500,
        },
      });

      const buttonStatus = await screen.findByTestId('switch-status-page-2');
      await user.click(buttonStatus);

      await waitFor(() =>
        expect(toast.danger).toHaveBeenLastCalledWith(errorPublishMessage),
      );
    });
  });
});
