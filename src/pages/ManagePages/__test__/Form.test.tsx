import React from 'react';
import '@testing-library/jest-dom';
import axios from '@mocks/axios';
import {
  screen,
  waitFor,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';
import { renderWithRoute, getLastCalled, API_URL } from '@utils/test-helper';
import toast from '@utils/toast';
import { sendTracker } from '@utils/tracker';

import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';
import {
  GET_PAGE_SUCCESS_RESPONSE,
  GET_PAGES_SUCCESS_RESPONSE,
} from '@mocks/api/pages';

jest.mock('@components/Trumbowyg', () => {
  return {
    __esModule: true,
    default: (props) => <textarea {...props} />,
  };
});

jest.mock('@utils/toast', () => ({
  success: jest.fn(),
  danger: jest.fn(),
}));

axios.onGet(`${API_URL.storefronts}/status`).reply(
  200,
  getMockSubscriptionsResponse({
    type: 'platinum',
    expiredDate: '2022-05-01T00:00:00+07:00',
  }),
);
axios.onGet(`${API_URL.pages}`).reply(200, GET_PAGES_SUCCESS_RESPONSE);

let user: UserEvent;
beforeEach(() => {
  user = userEvent.setup({ delay: null });

  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));

  localStorage.setItem('onboardingStep', '5'); // already finished onboarding

  axios.onGet(`${API_URL.pages}/4439`).reply(200, GET_PAGE_SUCCESS_RESPONSE);
});

afterEach(() => {
  jest.useRealTimers();
  jest.clearAllMocks();
  localStorage.clear();
});

describe('Create Page', () => {
  const route = '/storefront-editor/settings/manage-pages';
  const promptConfirmationMessage =
    'Kamu akan keluar halaman ini tanpa menyimpan perubahan. Apa kamu yakin?';
  const titleLabel = 'Judul halaman';
  const contentLabel = 'Isi halaman';
  const statusLabel = 'Dilihat publik';
  const createPageButton = 'Buat halaman';
  const editPageButton = 'Simpan halaman';
  const listPageSubtitle =
    'Tambah atau edit halaman yang ingin kamu tampilkan di Storefront kamu.';
  const backNavigation = 'Kembali';
  const customWebsiteTab = 'Custom Website';
  const customWebsiteTitle = 'Tema Website Kamu';
  const createPageSuccessMessage = 'Berhasil menambah halaman';
  const editPageSuccessMessage = 'Berhasil menyimpan halaman';
  const contentWithXSS = `<a href="javascript:alert('XSS1')" onmouseover="alert('XSS2')">XSS<a>`;
  const sampleErrorMessage = 'Sample error messsage';
  const defaultTrackerParams = {
    evn: 'ngorder_sf_dashboard_actions',
    active_tab: 'atur_halaman',
  };

  it('Renders properly with default configuration', async () => {
    const { container } = renderWithRoute({
      route: `${route}/create`,
    });

    expect(await screen.findByLabelText(titleLabel)).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });

  it('Should render page with default state', async () => {
    renderWithRoute({
      route: `${route}/create`,
    });

    const titleInput = await screen.findByLabelText(titleLabel);
    expect(titleInput).toBeInTheDocument();
    expect(titleInput).toHaveValue('');
    const contentInput = screen.getByLabelText(contentLabel);
    expect(contentInput).toBeInTheDocument();
    expect(contentInput).toHaveValue('');
    const statusToggle = screen.getByLabelText(statusLabel);
    expect(statusToggle).toBeInTheDocument();
    expect(statusToggle).not.toBeChecked();
    const saveButton = screen.getByRole('button', { name: createPageButton });
    expect(saveButton).toBeInTheDocument();
    expect(saveButton).toBeDisabled();
  });

  it.each([
    [
      'Should be redirected to manage page when click back',
      backNavigation,
      listPageSubtitle,
    ],
    [
      'Should be redirected to other tab when click other tab',
      customWebsiteTab,
      customWebsiteTitle,
    ],
  ])('%s', async (_, triggerText, assertionText) => {
    renderWithRoute({
      route: `${route}/create`,
    });

    expect(await screen.findByLabelText(titleLabel)).toBeInTheDocument();
    await user.click(screen.getByText(triggerText));
    expect(await screen.findByText(assertionText)).toBeInTheDocument();
  });

  it('Should hold maximum 160 chars for title input', async () => {
    renderWithRoute({
      route: `${route}/create`,
    });

    const titleInput = await screen.findByLabelText(titleLabel);
    expect(titleInput).toBeInTheDocument();
    await user.type(titleInput, 'a'.repeat(161));
    expect(titleInput).toHaveValue('a'.repeat(160));
  });

  it('Should enable and disable save button correctly', async () => {
    renderWithRoute({
      route: `${route}/create`,
    });

    const titleInput = await screen.findByLabelText(titleLabel);
    await user.type(titleInput, 'title');
    const saveButton = screen.getByRole('button', { name: createPageButton });
    expect(saveButton).toBeDisabled();
    const contentInput = screen.getByLabelText(contentLabel);
    await user.type(contentInput, '<p>content</p>');
    expect(saveButton).toBeEnabled();
  });

  it.each([
    ['go to manage page', backNavigation, listPageSubtitle],
    ['go to custom website page', customWebsiteTab, customWebsiteTitle],
  ])(
    'Should show prompt message when %s but form is not empty',
    async (_, triggerText, newPageText) => {
      renderWithRoute({
        route: `${route}/create`,
      });

      const titleInput = await screen.findByLabelText(titleLabel);
      await user.type(titleInput, 'title');
      await user.click(screen.getByText(triggerText));
      expect(
        await screen.findByText(promptConfirmationMessage),
      ).toBeInTheDocument();
      await user.click(screen.getByRole('button', { name: 'Ya, keluar' }));
      expect(await screen.findByText(newPageText)).toBeInTheDocument();
    },
  );

  it('Should cancel prompt pop up correctly', async () => {
    renderWithRoute({
      route: `${route}/create`,
    });

    const titleInput = await screen.findByLabelText(titleLabel);
    await user.type(titleInput, 'title');
    await user.click(screen.getByText(backNavigation));
    expect(
      await screen.findByText(promptConfirmationMessage),
    ).toBeInTheDocument();
    await user.click(screen.getByRole('button', { name: 'Batal' }));
    await waitForElementToBeRemoved(() =>
      screen.getByText(promptConfirmationMessage),
    );
    expect(screen.getByText(contentLabel)).toBeInTheDocument();
  });

  it('Should create page correctly', async () => {
    axios
      .onPost(API_URL.pages)
      .replyOnce(200, { message: createPageSuccessMessage, data: { id: 1 } });

    renderWithRoute({
      route: `${route}/create`,
    });

    await user.type(await screen.findByLabelText(titleLabel), 'title');
    await user.type(screen.getByLabelText(contentLabel), '<p>content</p>');
    await user.click(screen.getByLabelText(statusLabel));
    await user.click(screen.getByRole('button', { name: createPageButton }));
    await waitFor(() =>
      expect(toast.success).toHaveBeenLastCalledWith(createPageSuccessMessage),
    );
    expect(getLastCalled(axios, 'post').data).toEqual(
      '{"title":"title","content":"<p>content</p>","status":"published"}',
    );
    expect(await screen.findByText(listPageSubtitle)).toBeInTheDocument();
    await waitFor(() =>
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'add_page_success',
        action_details: '1',
      }),
    );
  });

  it('Should show error message correctly', async () => {
    axios.onPost(API_URL.pages).replyOnce(422, {
      errors: [{ message: sampleErrorMessage }],
      meta: { http_status: 422 },
    });

    renderWithRoute({
      route: `${route}/create`,
    });

    await user.type(await screen.findByLabelText(titleLabel), 'title');
    await user.type(screen.getByLabelText(contentLabel), '<p>content</p>');
    await user.click(screen.getByLabelText(statusLabel));
    await user.click(screen.getByRole('button', { name: createPageButton }));
    await waitFor(() =>
      expect(toast.danger).toHaveBeenLastCalledWith(sampleErrorMessage),
    );
  });

  it('Should sanitize page correctly', async () => {
    axios
      .onPost(API_URL.pages)
      .replyOnce(200, { message: createPageSuccessMessage });

    renderWithRoute({
      route: `${route}/create`,
    });

    await user.type(await screen.findByLabelText(titleLabel), contentWithXSS);
    await user.type(screen.getByLabelText(contentLabel), contentWithXSS);
    await user.click(screen.getByLabelText(statusLabel));
    await user.click(screen.getByRole('button', { name: createPageButton }));
    await waitFor(() =>
      expect(toast.success).toHaveBeenLastCalledWith(createPageSuccessMessage),
    );
    expect(getLastCalled(axios, 'post').data).toEqual(
      '{"title":"<a href>XSS<a>","content":"<a href>XSS<a>","status":"published"}',
    );
  });

  it('Should render edit page correctly', async () => {
    renderWithRoute({
      route: `${route}/edit/4439`,
    });

    expect(
      await screen.findByDisplayValue('Test content baru'),
    ).toBeInTheDocument();
    expect(screen.getByLabelText(contentLabel)).toHaveAttribute(
      'data',
      '<p>Test content</p>',
    );
    expect(screen.getByLabelText(statusLabel)).toBeChecked();
    expect(screen.getByRole('button', { name: editPageButton })).toBeEnabled();
  });

  it('Should success edit page', async () => {
    axios
      .onPatch(`${API_URL.pages}/4439`)
      .replyOnce(200, { message: editPageSuccessMessage, data: { id: 4439 } });

    renderWithRoute({
      route: `${route}/edit/4439`,
    });

    expect(
      await screen.findByDisplayValue('Test content baru'),
    ).toBeInTheDocument();
    await user.click(screen.getByLabelText(statusLabel));
    await user.click(screen.getByRole('button', { name: editPageButton }));
    await waitFor(() =>
      expect(toast.success).toHaveBeenLastCalledWith(editPageSuccessMessage),
    );
    expect(getLastCalled(axios, 'patch').data).toEqual(
      '{"title":"Test content baru","content":"<p>Test content</p>","status":"draft"}',
    );
    await waitFor(() =>
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'edit_page_success',
        action_details: '4439',
      }),
    );
  });

  it('Should show error toast in edit page', async () => {
    axios.onPatch(`${API_URL.pages}/4439`).replyOnce(422, {
      errors: [{ message: sampleErrorMessage }],
      meta: { http_status: 422 },
    });

    renderWithRoute({
      route: `${route}/edit/4439`,
    });

    expect(
      await screen.findByDisplayValue('Test content baru'),
    ).toBeInTheDocument();
    await user.click(screen.getByRole('button', { name: editPageButton }));
    await waitFor(() =>
      expect(toast.danger).toHaveBeenLastCalledWith(sampleErrorMessage),
    );
  });

  it('Should show prompt message when click back navigation', async () => {
    renderWithRoute({
      route: `${route}/edit/4439`,
    });

    expect(
      await screen.findByDisplayValue('Test content baru'),
    ).toBeInTheDocument();
    await user.click(screen.getByText(backNavigation));
    expect(
      await screen.findByText(promptConfirmationMessage),
    ).toBeInTheDocument();
  });

  it('Should sanitize correctly after editing title and content', async () => {
    axios
      .onPatch(`${API_URL.pages}/4439`)
      .replyOnce(200, { message: editPageSuccessMessage });

    renderWithRoute({
      route: `${route}/edit/4439`,
    });

    expect(
      await screen.findByDisplayValue('Test content baru'),
    ).toBeInTheDocument();
    await user.type(await screen.findByLabelText(titleLabel), contentWithXSS);
    await user.type(screen.getByLabelText(contentLabel), contentWithXSS);
    await user.click(screen.getByRole('button', { name: editPageButton }));
    await waitFor(() =>
      expect(toast.success).toHaveBeenLastCalledWith(editPageSuccessMessage),
    );
    expect(getLastCalled(axios, 'post').data).toEqual(
      '{"title":"<a href>XSS<a>","content":"<a href>XSS<a>","status":"published"}',
    );
  });

  it('Should redirect to list of pages when error getting page details', async () => {
    axios.onGet(`${API_URL.pages}/4440`).reply(401, {
      errors: [{ message: 'Unauthorized', code: 10001 }],
      meta: { http_status: 401 },
    });

    renderWithRoute({
      route: `${route}/edit/4440`,
    });

    await waitFor(() =>
      expect(screen.getByText(listPageSubtitle)).toBeInTheDocument(),
    );
  });
});
