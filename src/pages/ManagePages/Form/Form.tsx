import React from 'react';
import { useFormik } from 'formik';
import { Card, Form, Stack } from 'react-bootstrap';
import { useNavigate, useParams } from 'react-router-dom';
import xss, { IFilterXSSOptions, whiteList } from 'xss';
import { unescape } from 'html-escaper';

import { useCreatePage, useGetPage, useUpdatePage } from '@hooks/api/pages';
import { usePrompt } from '@hooks/usePrompt';
import { onSuccessMutate, onErrorMutate } from '@utils/reactQuery';
import { sendDashboardTracker } from '@utils/tracker/dashboard';

import Trumbowyg from '@components/Trumbowyg';
import Button from '@components/Button';
import BackNavigation from '@components/BackNavigation';
import LoadingState from '@components/LoadingState';
import { validationForm } from './form.validation';

import { EPageStatus } from '../managePages.types';

const ManagePagesForm = () => {
  const MANAGE_PAGES_URL = '/settings/manage-pages';
  const XSS_OPTIONS: IFilterXSSOptions = {
    whiteList: {
      ...whiteList,
      p: ['style'],
    },
    escapeHtml: (html: string): string => html,
  };

  const params = useParams();
  const navigate = useNavigate();
  const createPage = useCreatePage();
  const updatePage = useUpdatePage(params.id);

  const {
    values: { title, initialContent, content, status },
    errors: { content: errorContent },
    isSubmitting,
    setFieldValue,
    setValues,
    handleChange,
    handleSubmit,
  } = useFormik({
    initialValues: {
      title: '',
      initialContent: '',
      content: '',
      status: EPageStatus.DRAFT,
    },
    validationSchema: validationForm,
    onSubmit: (values, { setSubmitting }) => {
      if (createPage.isLoading) return;

      setSubmitting(true);

      (params.id ? updatePage : createPage).mutate(
        {
          title: xss(values.title, XSS_OPTIONS),
          content: xss(values.content, XSS_OPTIONS),
          status: values.status,
        },
        {
          onSuccess: (data) => {
            onSuccessMutate(data);
            navigate(MANAGE_PAGES_URL);
            void sendDashboardTracker({
              actionType: `${params.id ? 'edit' : 'add'}_page_success`,
              actionDetails: `${data.data.data.id}`,
            });
          },
          onError: (err) => {
            onErrorMutate(err);
            setSubmitting(false);
          },
        },
      );
    },
  });
  const canSubmitPage = !!title && !!content && !errorContent;
  const isFormDirty = !!title || !!content;
  usePrompt(isFormDirty && !isSubmitting);

  let isLoadingPage = false;
  if (params.id) {
    const { isLoading } = useGetPage(
      params.id,
      (res) => {
        if (!res) return;

        const {
          data: { content, title, status },
        } = res;

        void setValues({
          initialContent: unescape(content),
          content: unescape(content),
          title,
          status,
        });
      },
      () => {
        navigate('/settings/manage-pages');
      },
    );
    isLoadingPage = isLoading;
  }

  const onStatusChange = (e) => {
    void setFieldValue(
      'status',
      e.target.checked ? EPageStatus.PUBLISHED : EPageStatus.DRAFT,
    );
  };

  return (
    <Form onSubmit={handleSubmit}>
      {isLoadingPage ? (
        <Card className="p-6" data-testid="loading-state">
          <LoadingState className="pt-12" />
        </Card>
      ) : (
        <div className="flex flex-col-reverse md:grid grid-cols-12 gap-24px">
          <div className="col-span-12 md:col-span-9">
            <Card className="p-6">
              <BackNavigation to={MANAGE_PAGES_URL} />
              <Form.Group className="mb-5">
                <Form.Label aria-required className="mb-5" htmlFor="page-title">
                  Judul halaman
                </Form.Label>
                <Form.Control
                  id="page-title"
                  type="text"
                  maxLength={160}
                  name="title"
                  value={title}
                  onChange={handleChange}
                />
              </Form.Group>
              <Form.Group className={errorContent ? 'trumbowyg-error' : ''}>
                <Form.Label
                  aria-required
                  className="mb-5 whitespace-nowrap"
                  htmlFor="page-content"
                >
                  Isi halaman
                </Form.Label>
                <Trumbowyg
                  id="page-content"
                  name="content"
                  data={initialContent}
                  onChange={handleChange}
                />
                <Form.Control.Feedback type="invalid" className="text-sm mt-2">
                  {errorContent}
                </Form.Control.Feedback>
              </Form.Group>
            </Card>
          </div>
          <div className="col-span-12 md:col-span-3">
            <Card className="p-6 mb-0">
              <Stack direction="horizontal" className="justify-between mb-6">
                <Form.Label
                  htmlFor="page-status"
                  className="font-light text-base mb-0"
                >
                  Dilihat publik
                </Form.Label>
                <Form.Check
                  id="page-status"
                  type="switch"
                  checked={status === EPageStatus.PUBLISHED}
                  onChange={onStatusChange}
                />
              </Stack>
              <Button
                disabled={!canSubmitPage}
                className="w-full"
                type="submit"
                isLoading={createPage.isLoading}
                spinnerClass="my-1"
                size="sm"
              >
                {`${params.id ? 'Simpan' : 'Buat'} halaman`}
              </Button>
            </Card>
          </div>
        </div>
      )}
    </Form>
  );
};

export default ManagePagesForm;
