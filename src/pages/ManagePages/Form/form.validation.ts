import * as Yup from 'yup';

export const validationForm = Yup.object().shape({
  content: Yup.string().max(65535, 'Konten yang kamu masukkan terlalu banyak.'),
});
