import React from 'react';
import { Card, Stack, Button } from 'react-bootstrap';
import { PencilSquare } from 'react-bootstrap-icons';
import { useNavigate } from 'react-router-dom';

import toast from '@utils/toast';
import { onSuccessMutate, onErrorMutate } from '@utils/reactQuery';
import { sendDashboardTracker } from '@utils/tracker/dashboard';

import EmptyState from '@components/EmptyState';
import ErrorState from '@components/ErrorState';
import Table from '@components/Table';
import TabHeader from '@components/TabHeader';

import { useGetPages, useReorderPages } from '@hooks/api/pages';

import type { ITableColumn } from '@components/Table/table.types';
import type { IPage } from '@interfaces/pages';
import { EPageStatus } from './managePages.types';
import TableAction from './partials/TableAction';

const ManagePages = () => {
  const { isFetching, isError, pages, refetchGetPages } = useGetPages();
  const reorderPages = useReorderPages();
  const navigate = useNavigate();

  const handleSort = (sortedData: IPage[]) => {
    if (!reorderPages.isLoading) {
      const ids = sortedData?.map((page) => page.id);
      reorderPages.mutate(
        { page_ids: ids },
        {
          onSuccess: (data) => onSuccessMutate(data, refetchGetPages),
          onError: (error) => onErrorMutate(error, refetchGetPages),
          onSettled: () => {
            void sendDashboardTracker({
              actionType: 'drag_page',
              actionDetails: `${ids}`,
            });
          },
        },
      );
    }
  };
  const tableColumns: ITableColumn<IPage>[] = [
    {
      key: 'title',
      dataIndex: 'title',
      title: 'Title',
      render: (value, record) => (
        <>
          <span className="text-ellipsis truncate d-block max-w-[320px]">
            {value}
          </span>
          {record.status === EPageStatus.DRAFT && (
            <span className="italic capitalize ml-4 opacity-50">
              [{record.status}]
            </span>
          )}
        </>
      ),
    },
    {
      key: 'status',
      dataIndex: 'status',
      title: 'Status',
      align: 'right',
      render: (value) =>
        value === EPageStatus.DRAFT ? (
          <span className="text-sm text-gray-400">Tidak dilihat publik</span>
        ) : (
          <span className="text-sm"> Dilihat publik </span>
        ),
    },
    {
      key: 'id',
      dataIndex: 'id',
      title: 'Action',
      width: 150,
      render: (_value, record) => (
        <TableAction data={record} refetchData={refetchGetPages} />
      ),
    },
  ];

  const addPages = () => {
    void sendDashboardTracker({ actionType: 'add_page' });
    if (pages?.length >= 10) {
      toast.danger(
        `Maks. 10 halaman. Untuk tambah halaman, hapus yang lain terlebih dahulu.`,
      );
    } else {
      navigate('create');
    }
  };

  return (
    <Card className="px-24px py-24px">
      <Card.Body>
        <TabHeader
          title="Atur Halaman"
          subtitle="Tambah atau edit halaman yang ingin kamu tampilkan di Storefront kamu."
          action={[
            <Button onClick={addPages} className="py-2 text-light">
              Tambah Halaman <PencilSquare className="icon-base ml-2" />
            </Button>,
          ]}
        />
        <Stack className="mt-2">
          <Table
            data={pages}
            columns={tableColumns}
            sortable
            bordered={pages?.length > 0}
            borderless={isFetching || isError || pages?.length <= 0}
            hideHeader
            handleSort={handleSort}
            isLoading={isFetching}
            isErrorFetch={isError}
            errorState={
              <ErrorState
                title="Gagal mengambil data halaman"
                content="Klik tombol dibawah ini atau refresh untuk mencoba lagi"
                onRetry={refetchGetPages}
              />
            }
            emptyState={
              <EmptyState
                title="Halaman tidak ditemukan"
                content={
                  <p>
                    Ayo buat halaman pertama kamu semudah klik tombol{' '}
                    <b>Tambah Halaman</b> di kanan atas!
                  </p>
                }
              />
            }
          />
        </Stack>
      </Card.Body>
    </Card>
  );
};

export default ManagePages;
