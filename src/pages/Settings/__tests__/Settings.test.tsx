import React from 'react';
import '@testing-library/jest-dom';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { render, screen } from '@testing-library/react';
import Navbar from '@components/Navbar';

describe('Settings', () => {
  it('Should have correct url', () => {
    render(
      <Router>
        <Routes>
          <Route path="/" element={<Navbar />}></Route>
        </Routes>
      </Router>,
    );

    expect(screen.getByText('Setting').closest('a')).toHaveAttribute(
      'href',
      '/setting_frontstore/general',
    );
  });
});
