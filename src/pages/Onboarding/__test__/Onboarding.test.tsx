import axios from '@mocks/axios';
import {
  screen,
  waitForElementToBeRemoved,
  waitFor,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';
import '@testing-library/jest-dom';
import { renderWithRoute, getLastCalled, API_URL } from '@utils/test-helper';
import toast from '@utils/toast';
import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';

import {
  getMockSelectedThemeResponse,
  GET_THEMES_SUCCESS_RESPONSE,
} from '@mocks/api/themes';
import type {
  IAssertOnboardingSectionStatus,
  IAssertSectionBoxStyle,
  IAssertStepperStyle,
} from '../onboarding.types';

jest.mock('@utils/toast', () => ({
  success: jest.fn(),
}));

axios.onPost(`${API_URL.tracker}/e`).reply(200);
jest.mock('@utils/tracker', () => ({
  __esModule: true,
  ...jest.requireActual('@utils/tracker'),
}));

axios.onGet(`${API_URL.storefronts}/status`).reply(
  200,
  getMockSubscriptionsResponse({
    type: 'free_trial',
    expiredDate: '2022-05-01T00:00:00+07:00',
  }),
);
window.open = jest.fn();
const scrollIntoViewMock = jest.fn();
global.HTMLElement.prototype.scrollIntoView = scrollIntoViewMock;

let user: UserEvent;
const mockEmail = 'bukastoreuser@gmail.com';
beforeEach(() => {
  user = userEvent.setup({ delay: null });

  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));
  global.USER = { name: 'Toko Pintar', email: mockEmail };

  axios
    .onGet(`${API_URL.storefronts}/themes/me`)
    .reply(200, getMockSelectedThemeResponse());
  axios
    .onGet(`${API_URL.storefronts}/themes`)
    .reply(200, GET_THEMES_SUCCESS_RESPONSE);
});

afterEach(() => {
  jest.useRealTimers();
  jest.clearAllMocks();
  localStorage.clear();
});

// assertion method
const assertStepperStyle = ({
  stepperTittle,
  isActive,
}: IAssertStepperStyle) => {
  const stepper = screen.getByText(stepperTittle);
  expect(stepper).toHaveClass(
    isActive
      ? 'text-base text-break pt-1 font-semibold text-blue-600'
      : 'text-base text-break pt-1 font-medium text-black',
  );
  expect(stepper.previousSibling).toHaveClass(
    isActive
      ? 'bg-blue-600 text-white completed rounded-full w-8 h-8 leading-8 text-center text-sm font-semibold'
      : 'text-slate-400 rounded-full w-8 h-8 leading-8 text-center text-sm font-semibold',
  );
};

const assertOnboardingSectionStatus = ({
  elementStatusId,
  text,
  isPassed,
}: IAssertOnboardingSectionStatus) => {
  const status = screen.getByTestId(elementStatusId);
  expect(status.textContent?.trim()).toEqual(text);
  expect(status).toHaveClass(
    isPassed ? 'text-green-600' : 'mt-5 text-gray-500',
  );
};

const assertSectionBoxStyle = ({
  formWizardId,
  state,
}: IAssertSectionBoxStyle) => {
  const classes = {
    'not-active': 'opacity-30',
    active: 'border border-blue-600',
    passed: 'border border-green-600 opacity-70',
  };
  expect(screen.getByTestId(formWizardId)).toHaveClass(
    `form-wizard px-6 py-5 rounded-xl ${classes[state]}`,
  );
};
// end of global assertion method

describe('Onboarding Page', () => {
  const evn = '"ngorder_sf_onboarding_funnel_actions"';
  const storefrontLink = 'https://subdomain.smartseller.co.id';
  const route = '/storefront-editor/onboarding';

  it(`Renders properly with default configuration`, async () => {
    // Try to access restricted page
    const { container } = renderWithRoute({
      route: '/storefront-editor/settings/domain',
    });

    // Redirected to onboarding page
    expect(
      await screen.findByText(
        /Hai Ngorder Owner, Selamat datang di Storefront/i,
      ),
    ).toBeInTheDocument();
    expect(container).toMatchSnapshot();
    expect(getLastCalled(axios, 'post').data).toEqual(
      '{"evn":"ngorder_sf_onboarding_funnel_actions","onboarding_step":"1","action_type":"visit"}',
    );
  });

  describe('Product section', () => {
    const formWizardId = 'form-wizard-3';
    const elementStatusId = 'onboarding-product-status';
    const stepperTittle = 'Tampilkan Produk';

    const assertTitleAndSubtitle = async () => {
      expect(
        await screen.findByText('Tambahkan Produk ke Storefront'),
      ).toBeInTheDocument();
      expect(
        screen.getByText(
          'Pilih produk-produk yang sudah kamu upload melalui smartseller atau impor dari marketplace untuk kamu tampilkan di storefront mu.',
        ),
      ).toBeInTheDocument();
    };

    it('Should show not active state for product section', async () => {
      localStorage.setItem('onboardingStep', '2'); // still in step 2
      renderWithRoute({ route });
      await assertTitleAndSubtitle();
      assertOnboardingSectionStatus({
        elementStatusId,
        text: 'Langkah 3',
        isPassed: false,
      });
      assertSectionBoxStyle({ formWizardId, state: 'not-active' });
      assertStepperStyle({ stepperTittle, isActive: false });
      expect(
        screen.queryByTestId('daftar-product-link'),
      ).not.toBeInTheDocument();
      expect(
        screen.queryByTestId('next-product-button'),
      ).not.toBeInTheDocument();
    });

    it('Should render product section correctly', async () => {
      localStorage.setItem('onboardingStep', '3'); // step 3
      renderWithRoute({ route });

      await assertTitleAndSubtitle();
      assertOnboardingSectionStatus({
        elementStatusId,
        text: 'Langkah 3',
        isPassed: false,
      });
      assertSectionBoxStyle({ formWizardId, state: 'active' });
      assertStepperStyle({ stepperTittle, isActive: true });

      expect(await screen.findByText(/Klik menu/i)).toBeInTheDocument();
      expect(await screen.findByText(/Centang produk/i)).toBeInTheDocument();
      expect(
        await screen.findByText(/Scroll ke paling bawah halaman/i),
      ).toBeInTheDocument();
      expect(
        await screen.findByText(/Set publish privor & storefront/i),
      ).toBeInTheDocument();
      expect(await screen.findByText(/Klik tombol/i)).toBeInTheDocument();

      // assert daftar prodcut link
      const daftarProductLink = screen.getByText('Daftar produk');
      expect(daftarProductLink).toBeInTheDocument();

      // user click daftar product link
      const user = userEvent.setup({ delay: null });
      await user.click(screen.getByTestId('daftar-product-link'));
      expect(window.open).toHaveBeenCalledWith('/produk/index', '_blank');
      expect(getLastCalled(axios, 'post').data).toEqual(
        `{"evn":${evn},"onboarding_step":"3","action_type":"to_product_list","target_url":"/produk/index"}`,
      );

      // assert next daftar product
      void user.click(screen.getByTestId('next-product-button'));

      await waitForElementToBeRemoved(() =>
        screen.getByTestId('next-product-button'),
      );
      expect(localStorage.getItem('onboardingStep')).toBe('4');
      expect(getLastCalled(axios, 'post').data).toEqual(
        `{"evn":${evn},"onboarding_step":"3","action_type":"step_success"}`,
      );
    });

    it('Should show passed state for product section', async () => {
      localStorage.setItem('onboardingStep', '4'); // already in step 4
      renderWithRoute({ route });

      expect(
        await screen.findByText('Tambahkan Produk ke Storefront'),
      ).toBeInTheDocument();
      assertOnboardingSectionStatus({
        elementStatusId,
        text: 'Langkah 3 berhasil',
        isPassed: true,
      });
      assertSectionBoxStyle({ formWizardId, state: 'passed' });
      assertStepperStyle({ stepperTittle, isActive: true });
      assertStepperStyle({ stepperTittle: 'Share Link', isActive: true });
      expect(
        screen.queryByTestId('next-product-button'),
      ).not.toBeInTheDocument();
    });
  });

  describe('Domain section', () => {
    const assertDomainSectionTitle = async () => {
      const onboardingSectionTitle = await screen.findByText(
        'Tentukan nama domain',
      );
      expect(onboardingSectionTitle).toBeInTheDocument();
    };

    const assertDomainSectionSubTitle = () =>
      expect(
        screen.getByText(
          'Gunakan subdomain yang pernah kamu isi atau ubah sesuai dengan keinginanmu.',
        ),
      ).toBeInTheDocument();

    const assertDomainSectionValue = () =>
      expect(screen.getByText('subdomain')).toBeInTheDocument();

    it('Should show active state for domain section', async () => {
      localStorage.setItem('onboardingStep', '1'); // active step domain
      renderWithRoute({ route });
      await assertDomainSectionTitle();
      assertDomainSectionSubTitle();

      const onboardingDomainStatus = screen.getByTestId(
        'onboarding-domain-status',
      );
      expect(onboardingDomainStatus.textContent?.trim()).toEqual('Langkah 1');
      expect(onboardingDomainStatus).toHaveClass('mt-5 text-gray-500');

      const fieldSubdomain = screen.getByTestId('onboarding-subdomain-field');
      expect(fieldSubdomain).toBeInTheDocument();

      const buttonSave = screen.getByTestId('save-domain-button');

      expect(buttonSave).toBeInTheDocument();
      expect(screen.getByTestId('form-wizard-1')).toHaveClass(
        'form-wizard px-6 py-5 rounded-xl border border-blue-600',
      );

      await user.clear(fieldSubdomain);
      await user.type(fieldSubdomain, 'halosukijan!@#');
      expect(fieldSubdomain).toHaveValue('halosukijan');

      await user.clear(fieldSubdomain);
      await user.type(fieldSubdomain, 'hal');
      expect(fieldSubdomain).toHaveValue('hal');
      expect(
        screen.getByText('Minimal 4 karakter berisi huruf dan angka.'),
      ).toBeInTheDocument();
      expect(buttonSave).toBeDisabled();

      await user.clear(fieldSubdomain);
      await user.type(fieldSubdomain, 'subdomain');
      expect(buttonSave).not.toBeDisabled();
    });

    it('Should show finished state for domain section', async () => {
      localStorage.setItem('onboardingStep', '2'); // finished domain step
      renderWithRoute({ route });

      await assertDomainSectionTitle();
      assertDomainSectionValue();

      const onboardingDomainStatus = screen.getByTestId(
        'onboarding-domain-status',
      );
      expect(onboardingDomainStatus.textContent?.trim()).toEqual(
        'Langkah 1 berhasil',
      );
      expect(onboardingDomainStatus).toHaveClass('text-green-600');

      const fieldSubdomain = screen.queryByTestId('onboarding-subdomain-field');
      expect(fieldSubdomain).not.toBeInTheDocument();

      expect(
        screen.queryByTestId('save-domain-button'),
      ).not.toBeInTheDocument();
      expect(screen.getByTestId('form-wizard-1')).toHaveClass(
        'form-wizard px-6 py-5 rounded-xl border border-green-600 opacity-70',
      );
    });
  });

  describe('Theme section', () => {
    const formWizardId = 'form-wizard-2';
    const elementStatusId = 'onboarding-theme-status';
    const stepperTittle = 'Pilih Tema';

    const assertTitleAndSubtitle = async () => {
      expect(
        await screen.findByText('Pilih tema Storefront'),
      ).toBeInTheDocument();
      expect(
        screen.getByText(
          'Pilih tema Storefront sesuai dengan apa yang kamu inginkan.',
        ),
      ).toBeInTheDocument();
    };

    const assertSubmittedTheme = (themeName: string) => {
      expect(screen.getByLabelText(themeName)).toHaveStyle(
        `background-image: url(https://storefront-sandbox-bucket.smartseller.co.id/images/theme-${themeName}-small.png)`,
      );
    };

    const assertImage = (previewButton: HTMLElement, altText: string) => {
      expect(previewButton.parentNode?.previousSibling).toHaveAttribute(
        'aria-label',
        altText,
      );
    };

    it('Should show not active state for theme section', async () => {
      localStorage.setItem('onboardingStep', '1'); // still in step 1
      renderWithRoute({ route });

      await assertTitleAndSubtitle();
      assertOnboardingSectionStatus({
        elementStatusId,
        text: 'Langkah 2',
        isPassed: false,
      });
      assertSectionBoxStyle({ formWizardId, state: 'not-active' });
      assertStepperStyle({ stepperTittle, isActive: false });
      expect(screen.queryByTestId('save-theme-button')).not.toBeInTheDocument();
    });

    it('Should render theme section correctly', async () => {
      localStorage.setItem('onboardingStep', '2');
      renderWithRoute({ route });

      await assertTitleAndSubtitle();
      assertOnboardingSectionStatus({
        elementStatusId,
        text: 'Langkah 2',
        isPassed: false,
      });
      assertSectionBoxStyle({ formWizardId, state: 'active' });
      assertStepperStyle({ stepperTittle, isActive: true });

      // assert current selected theme
      expect(screen.getByText(/Tema dipilih/i)).toBeInTheDocument();
      expect(screen.getByText('naomi')).toBeInTheDocument();
      expect(screen.getAllByTestId('theme-thumbnail').length).toEqual(6);

      // assert current preview button position
      let previewButton = screen.getByText('Preview');
      expect(previewButton).toBeInTheDocument();
      assertImage(previewButton, 'naomi');

      // select another theme
      await user.click(screen.getByTestId('theme-salma'));
      expect(screen.queryByText('Naomi')).not.toBeInTheDocument();
      expect(screen.getByText('salma')).toBeInTheDocument();
      expect(getLastCalled(axios, 'post').data).toEqual(
        `{"evn":${evn},"onboarding_step":"2","action_type":"click_theme","action_details":"salma"}`,
      );
      previewButton = screen.getByText('Preview');
      assertImage(previewButton, 'salma');

      // submit theme
      axios.onPatch(`${API_URL.storefronts}/themes`).reply(200, {});
      await user.click(screen.getByTestId('save-theme-button'));
      jest.runAllTimers();

      // assert after submit theme
      await waitForElementToBeRemoved(() =>
        screen.getByTestId('save-theme-button'),
      );
      expect(localStorage.__STORE__['onboardingStep']).toBe('3');
      expect(scrollIntoViewMock).toHaveBeenCalled();
      expect(getLastCalled(axios, 'patch').data).toEqual('{"theme_id":5}');
      assertSubmittedTheme('salma');
      expect(getLastCalled(axios, 'post').data).toEqual(
        `{"evn":${evn},"onboarding_step":"2","action_type":"step_success"}`,
      );
    });

    it('Should show passed state for theme section', async () => {
      localStorage.setItem('onboardingStep', '3'); // already in step 3
      renderWithRoute({ route });

      expect(
        await screen.findByText('Pilih tema Storefront'),
      ).toBeInTheDocument();
      assertOnboardingSectionStatus({
        elementStatusId,
        text: 'Langkah 2 berhasil',
        isPassed: true,
      });
      assertSectionBoxStyle({ formWizardId, state: 'passed' });
      assertStepperStyle({ stepperTittle, isActive: true });
      assertStepperStyle({ stepperTittle: 'Tampilkan Produk', isActive: true });
      expect(screen.queryByTestId('save-theme-button')).not.toBeInTheDocument();
      assertSubmittedTheme('naomi');
    });

    it('Should show preview theme correctly', async () => {
      localStorage.setItem('onboardingStep', '2');
      renderWithRoute({ route });

      // show preview modal
      await user.click(await screen.findByText('Preview'));
      await waitFor(() =>
        expect(screen.getByTestId('preview-theme-modal')).toBeInTheDocument(),
      );
      expect(screen.getByAltText('theme-preview')).toHaveAttribute(
        'src',
        'https://storefront-sandbox-bucket.smartseller.co.id/images/theme-naomi-small.png',
      );
      expect(getLastCalled(axios, 'post').data).toEqual(
        `{"evn":${evn},"onboarding_step":"2","action_type":"preview_theme","action_details":"naomi"}`,
      );

      // close preview modal by clicking close button
      await user.click(screen.getByTestId('preview-theme-modal-close-button'));
      await waitForElementToBeRemoved(() =>
        screen.getByTestId('preview-theme-modal'),
      );
    });
  });

  describe('Share section', () => {
    const formWizardId = 'form-wizard-4';
    const elementStatusId = 'onboarding-share-status';
    const stepperTittle = 'Share Link';

    const assertTitleAndSubtitle = async () => {
      expect(
        await screen.findByText('Share Storefront-mu'),
      ).toBeInTheDocument();
      expect(
        screen.getByText(
          'Sebar link storefront ke sosial media atau channel lainnya, untuk menarik calon pelanggan baru dan menaikan pembelian bisnismu.',
        ),
      ).toBeInTheDocument();
    };

    it('Should show not active state for share section', async () => {
      localStorage.setItem('onboardingStep', '3'); // still in step 3
      renderWithRoute({ route });

      await assertTitleAndSubtitle();
      assertOnboardingSectionStatus({
        elementStatusId,
        text: 'Langkah 4',
        isPassed: false,
      });
      assertSectionBoxStyle({ formWizardId, state: 'not-active' });
      assertStepperStyle({ stepperTittle, isActive: false });
      expect(screen.queryByTestId('copy-link-btn')).not.toBeInTheDocument();
      expect(
        screen.queryByTestId('view-storefront-btn'),
      ).not.toBeInTheDocument();
      expect(screen.queryByTestId('done-onboarding')).not.toBeInTheDocument();
    });

    it('Should render share section correctly', async () => {
      localStorage.setItem('onboardingStep', '4'); // step 4
      renderWithRoute({ route });

      await assertTitleAndSubtitle();
      assertOnboardingSectionStatus({
        elementStatusId,
        text: 'Langkah 4',
        isPassed: false,
      });
      assertSectionBoxStyle({ formWizardId, state: 'active' });
      assertStepperStyle({ stepperTittle, isActive: true });

      // assert lihat storefront button
      const storefrontView = screen.getByTestId('view-storefront-btn');
      expect(storefrontView).toBeInTheDocument();
      expect(storefrontView).toHaveClass('ml-4 btn btn-outline-primary');

      const user = userEvent.setup({ delay: null });
      await user.click(screen.getByTestId('view-storefront-btn'));
      expect(window.open).toHaveBeenCalledTimes(1);
      expect(window.open).toHaveBeenCalledWith(storefrontLink, '_blank');
      expect(getLastCalled(axios, 'post').data).toEqual(
        `{"evn":${evn},"onboarding_step":"4","action_type":"to_storefront","target_url":"${storefrontLink}"}`,
      );

      // assert copy link button
      const copyLinkBtn = screen.getByTestId('copy-link-btn');
      expect(copyLinkBtn).toBeInTheDocument();
      expect(copyLinkBtn).toHaveClass('btn-copy-group');

      // user click "Copy Link" button
      await user.click(screen.getByTestId('copy-link-btn'));
      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(
          'Link berhasil di-copy!',
        ),
      );
      expect(getLastCalled(axios, 'post').data).toEqual(
        `{"evn":${evn},"onboarding_step":"4","action_type":"copy_link","action_details":"${storefrontLink}"}`,
      );

      // user click selesai onboarding
      void user.click(screen.getByTestId('done-onboarding'));
      await waitForElementToBeRemoved(() =>
        screen.getByTestId('done-onboarding'),
      );
      expect(global.window.location.href).toContain('/settings/domain');
      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(
          'Berhasil membuat website',
        ),
      );
    });
  });

  describe('Success register user', () => {
    it('Should show success toast', async () => {
      const successMessage = 'Selamat, kamu berhasil registrasi dan login.';
      localStorage.setItem(
        'register_storefront',
        JSON.stringify(successMessage),
      );

      renderWithRoute({ route });
      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(successMessage),
      );
    });

    it('Should send registration tracker', async () => {
      const successMessage = 'Selamat, kamu berhasil registrasi dan login.';
      localStorage.setItem(
        'register_storefront',
        JSON.stringify(successMessage),
      );

      renderWithRoute({ route });
      const trackerData = JSON.stringify({
        evn: 'ngorder_sf_bukastore_funnel_action',
        action_type: '8-a_visit_onboarding',
        bl_identity: JSON.stringify({
          user_id: '',
          email: mockEmail,
        }),
      });
      await waitFor(() =>
        expect(getLastCalled(axios, 'post').data).toEqual(trackerData),
      );
    });
  });
});
