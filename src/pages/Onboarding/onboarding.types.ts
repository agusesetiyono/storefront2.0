export interface IAssertStepperStyle {
  stepperTittle: string;
  isActive: boolean;
}

export interface IAssertOnboardingSectionStatus {
  elementStatusId: string;
  text: string;
  isPassed: boolean;
}

export interface IAssertSectionBoxStyle {
  formWizardId: string;
  state: string;
}

export interface IFormDomain {
  subdomain: string;
}
