import React, { useEffect } from 'react';
import { Card, Stack } from 'react-bootstrap';

import { sendOnboardingTracker } from '@utils/tracker/onboarding';
import toast from '@utils/toast';

import useSubscription from '@hooks/useSubscription';
import useOnboarding from '@hooks/useOnboarding';
import useRegister from '@hooks/useRegister';
import PageTitle from '@components/PageTitle';
import FormWizard from '@components/FormWizard';
import type { IFormWizardStep } from '@components/FormWizard/formWizard.types';

import FormDomain from './partials/FormDomain';
import FormTheme from './partials/FormTheme';
import FormProduct from './partials/FormProduct';
import FormShare from './partials/FormShare';
import { sendRegistrationTracker } from '@utils/tracker/registration';

const Onboarding = () => {
  const { subscription } = useSubscription();
  const { activeStep, nextOnboarding, skipOnboarding } = useOnboarding();
  const { registerMessage, resetRegisterInfo } = useRegister();

  useEffect(() => {
    void sendOnboardingTracker({
      onBoardingStep: activeStep.toString(),
      actionType: 'visit',
    });

    if (registerMessage) {
      void sendRegistrationTracker({
        actionType: '8-a_visit_onboarding',
      });

      toast.success(registerMessage);
      resetRegisterInfo();
    }
  }, []);

  const steps: IFormWizardStep[] = [
    {
      title: 'Tentukan Domain',
      form: FormDomain,
    },
    {
      title: 'Pilih Tema',
      form: FormTheme,
    },
    {
      title: 'Tampilkan Produk',
      form: FormProduct,
    },
    {
      title: 'Share Link',
      form: FormShare,
    },
  ];

  return (
    <>
      <PageTitle title="Storefront" />
      <div className="w-[892px] m-auto py-5">
        <Card className="pt-12 px-16 pb-16">
          <Stack className="align-items-center gap-11">
            <div className="text-center">
              <h3 className="text-xl font-bold">
                Hai {subscription.owner_name}, Selamat datang di Storefront
                &#128522;,
              </h3>
              <p className="text-break font-light mx-20 max-w-[600px]">
                Ikuti 4 langkah berikut untuk meluncurkan Storefront-mu! Kamu
                masih bisa ngubah setiap langkahnya kok, jadi jangan khawatir
                kalo salah!
              </p>
            </div>
            <FormWizard
              steps={steps}
              activeStep={activeStep}
              onNext={nextOnboarding}
              onSkip={skipOnboarding}
            />
          </Stack>
        </Card>
      </div>
    </>
  );
};

export default Onboarding;
