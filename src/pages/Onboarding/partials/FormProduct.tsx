import React from 'react';
import { Form, Stack, Image, Button } from 'react-bootstrap';
import StepImageProduct from '@assets/image/onboarding/product.svg';
import ChooseProduct from '@assets/image/onboarding/listproduct.svg';
import ChooseCategory from '@assets/image/onboarding/category.svg';
import ConfirmationDialog from '@assets/image/onboarding/modal.svg';
import type { IStepProps } from '@components/FormWizard/formWizard.types';
import { sendOnboardingTracker } from '@utils/tracker/onboarding';

const FormProduct = ({ step, active, completed, onNext }: IStepProps) => {
  const handleProductList = () => {
    const linkProduct = '/produk/index';
    window.open(linkProduct, '_blank');

    void sendOnboardingTracker({
      onBoardingStep: '3',
      actionType: 'to_product_list',
      targetUrl: linkProduct,
    });
  };
  return (
    <Stack className="gap-8">
      <Stack direction="horizontal" className="gap-9">
        <Image src={StepImageProduct} className="w-32" />
        <Stack gap={3}>
          <h3 className="text-3xl">Tambahkan Produk ke Storefront</h3>
          <p className="text-base max-w-[485px]">
            Pilih produk-produk yang sudah kamu upload melalui smartseller atau
            impor dari marketplace untuk kamu tampilkan di storefront mu.
          </p>
        </Stack>
      </Stack>
      <p
        className={completed ? 'text-green-600' : 'mt-5 text-gray-500'}
        data-testid="onboarding-product-status"
      >
        Langkah {step} {completed ? 'berhasil' : ''}
      </p>
      {active && (
        <>
          <hr />
          <Form noValidate>
            <Stack>
              <ol className="onboarding--order-list text-base">
                <li>
                  Klik menu <strong> Produk </strong> lalu sub menu
                  <strong> Daftar Produk </strong>
                </li>
                <li>
                  <div>
                    Centang produk yang ingin ditampilkan di Storefront, lalu
                    klik <strong>“pilih semua”</strong>
                  </div>
                  <div className="onboarding--box-img -ml-4">
                    <Image src={ChooseProduct} />
                  </div>
                </li>
                <li>
                  Scroll ke paling bawah halaman, lalu klik
                  <strong>“Pilih pengaturan”</strong>
                </li>
                <li>
                  <div>
                    Pilih <strong>“Set publish privor & storefront”</strong>
                  </div>
                  <div className="onboarding--box-img -ml-4">
                    <Image src={ChooseCategory} />
                  </div>
                </li>
                <li>
                  <div>
                    Klik tombol <strong> “atur”</strong>
                  </div>
                  <div className="onboarding--box-img -ml-4">
                    <Image src={ConfirmationDialog} />
                  </div>
                </li>
              </ol>
              <div className="onboarding--choose-product text-base mb-5">
                Coba tambahkan produk langsung di <br />
                <a
                  onClick={handleProductList}
                  className="onboarding--choose-product__link cursor-pointer"
                  target="_blank"
                  data-testid="daftar-product-link"
                >
                  Daftar produk
                </a>
              </div>
              <Button
                type="submit"
                className="self-end mr-3 w-44"
                onClick={onNext}
                data-testid="next-product-button"
              >
                Lanjutkan
              </Button>
            </Stack>
          </Form>
        </>
      )}
    </Stack>
  );
};

export default FormProduct;
