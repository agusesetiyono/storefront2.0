import React from 'react';
import { Form, InputGroup, Stack, Image, Button } from 'react-bootstrap';
import { BoxArrowUpRight, Link45deg } from 'react-bootstrap-icons';
import StepImageShare from '@assets/image/onboarding/share.svg';

import useSubscription from '@hooks/useSubscription';
import type { IStepProps } from '@components/FormWizard/formWizard.types';
import { useCopyToClipboard } from 'usehooks-ts';
import toast from '@utils/toast';
import config from '@utils/config';
import { sendOnboardingTracker } from '@utils/tracker/onboarding';

const FormShare = ({ step, active, completed, onNext }: IStepProps) => {
  const [, copy] = useCopyToClipboard();
  const { subscription } = useSubscription();
  const subdomainLink = subscription.subdomain.concat(config.siteExt as string);
  const storefront = `https://${subdomainLink}`;

  const copyDomain = () => {
    void copy(subdomainLink);
    toast.success(`Link berhasil di-copy!`);
    void sendOnboardingTracker({
      onBoardingStep: '4',
      actionType: 'copy_link',
      actionDetails: storefront,
    });
  };
  const handleStorefrontView = () => {
    window.open(storefront, '_blank');
    void sendOnboardingTracker({
      onBoardingStep: '4',
      actionType: 'to_storefront',
      targetUrl: storefront,
    });
  };
  return (
    <Stack className="gap-8">
      <Stack direction="horizontal" className="gap-9">
        <Image src={StepImageShare} className="w-32" />
        <Stack gap={3}>
          <h3 className="text-3xl">Share Storefront-mu</h3>
          <p className="text-base max-w-[485px]">
            Sebar link storefront ke sosial media atau channel lainnya, untuk
            menarik calon pelanggan baru dan menaikan pembelian bisnismu.
          </p>
        </Stack>
      </Stack>
      <p
        className={completed ? 'text-green-600' : 'mt-5 text-gray-500'}
        data-testid="onboarding-share-status"
      >
        Langkah {step} {completed ? 'berhasil' : ''}
      </p>
      {active && (
        <>
          <hr />
          <Form noValidate>
            <Stack gap={5}>
              <div className="onboarding--form-share">
                <Form.Group
                  controlId="validationSubdomain"
                  className="onboarding--form-share__fg"
                >
                  <Form.Label>Share link</Form.Label>
                  <InputGroup hasValidation>
                    <Form.Control
                      type="text"
                      placeholder={subdomainLink}
                      aria-describedby="inputGroupPrepend"
                      name="subdomain"
                      disabled
                    />
                    <Button
                      variant="outline-secondary"
                      onClick={copyDomain}
                      className="btn-copy-group"
                      data-testid="copy-link-btn"
                    >
                      Copy link
                      <Link45deg className="icon-base ml-2" />
                    </Button>
                  </InputGroup>
                </Form.Group>
                <Button
                  onClick={handleStorefrontView}
                  variant="outline-primary"
                  className="ml-4"
                  data-testid="view-storefront-btn"
                >
                  Lihat Storefront
                  <BoxArrowUpRight className="icon-base ml-2" />
                </Button>
              </div>
              <Button
                type="submit"
                className="w-44 self-end"
                onClick={onNext}
                data-testid="done-onboarding"
              >
                Selesai
              </Button>
            </Stack>
          </Form>
        </>
      )}
    </Stack>
  );
};

export default FormShare;
