import React, { ChangeEvent, useEffect } from 'react';
import { useFormik } from 'formik';
import { Form, InputGroup, Stack, Image, Button } from 'react-bootstrap';
import StepImageDomain from '@assets/image/onboarding-domain.svg';

import config from '@utils/config';
import { setFormErrors, setFormFieldError } from '@utils/form';
import useSubscription from '@hooks/useSubscription';
import useDebounceEffect from '@hooks/useDebounceEffect';
import { useValidateDomain, useSaveDomain } from '@hooks/api/domains';
import type { IStepProps } from '@components/FormWizard/formWizard.types';
import { validationDomain } from '../onboarding.validation';
import type { IFormDomain } from '../onboarding.types';
import { sendOnboardingTracker } from '@utils/tracker/onboarding';
import { IResponseError } from '@interfaces/query';

const FormDomain = ({ step, active, completed, onNext }: IStepProps) => {
  const { subscription, refetchSubscription } = useSubscription();
  const validateDomain = useValidateDomain();
  const saveDomain = useSaveDomain();

  const formik = useFormik<IFormDomain>({
    initialValues: { subdomain: '' },
    validateOnChange: false,
    validationSchema: validationDomain,
    onSubmit: (values, { setErrors }) => {
      saveDomain.mutate(values, {
        onSuccess: () => {
          setErrors({});
          refetchSubscription();
          onNext();
        },
        onError: (error) => {
          setFormErrors(error, setErrors);
          const errorResponse: IResponseError = error.response
            ?.data as IResponseError;

          void sendOnboardingTracker({
            onBoardingStep: '1',
            actionType: 'step_error',
            actionDetails: errorResponse.errors[0].message,
          });
        },
      });
    },
  });
  const {
    values,
    errors,
    isValidating,
    setValues,
    setFieldError,
    validateField,
  } = formik;
  const { subdomain } = values || {};

  useEffect(() => {
    void setValues({ subdomain: subscription.subdomain });
  }, [subscription.subdomain]);

  useDebounceEffect(() => {
    if (subdomain && !errors.subdomain && !isValidating) {
      validateDomain.mutate(
        { subdomain },
        {
          onSuccess: () => setFieldError('subdomain', undefined),
          onError: (error) =>
            setFormFieldError('subdomain', error, setFieldError),
        },
      );
    }
  }, [subdomain, isValidating]);

  const handleChangeSubdomain = async (e: ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    const subdomain = value.replace(/[\W_]+/g, '');
    await setValues({ subdomain });
    await validateField('subdomain');
  };

  return (
    <Stack className="gap-8">
      <Stack direction="horizontal" className="gap-9">
        <Image src={StepImageDomain} className="w-32" />
        <Stack gap={3}>
          <h3 className="text-3xl">Tentukan nama domain</h3>
          {completed ? (
            <div>
              <h4 className="text-2xl text-blue-600 font-light">{subdomain}</h4>
              <p className="text-gray-500">{config.siteExt}</p>
            </div>
          ) : (
            <p className="text-base max-w-[485px]">
              Gunakan subdomain yang pernah kamu isi atau ubah sesuai dengan
              keinginanmu.
            </p>
          )}
        </Stack>
      </Stack>
      <p
        className={completed ? 'text-green-600' : 'mt-5 text-gray-500'}
        data-testid="onboarding-domain-status"
      >
        Langkah {step} {completed ? 'berhasil' : ''}
      </p>
      {active && (
        <>
          <hr />
          <Form noValidate onSubmit={formik.handleSubmit}>
            <Stack gap={4}>
              <Form.Group controlId="validationSubdomain">
                <Form.Label aria-required>Subdomain</Form.Label>
                <InputGroup hasValidation>
                  <Form.Control
                    type="text"
                    maxLength={50}
                    placeholder="Subdomain"
                    aria-describedby="siteExt"
                    name="subdomain"
                    value={subdomain}
                    onChange={(e: ChangeEvent<HTMLInputElement>) =>
                      void handleChangeSubdomain(e)
                    }
                    isInvalid={!!errors.subdomain}
                    data-testid="onboarding-subdomain-field"
                  />
                  <InputGroup.Text id="siteExt">
                    {config.siteExt}
                  </InputGroup.Text>
                  <Form.Control.Feedback
                    type="invalid"
                    data-testid="onboarding-subdomain-error"
                  >
                    {errors.subdomain as string}
                  </Form.Control.Feedback>
                </InputGroup>
                {!errors.subdomain && subdomain && (
                  <Form.Text muted>
                    Jika diubah, subdomain pada private order juga akan berubah.
                  </Form.Text>
                )}
              </Form.Group>
              <Button
                type="submit"
                className="w-44 self-end"
                data-testid="save-domain-button"
                disabled={!!errors.subdomain || !subdomain}
              >
                Lanjutkan
              </Button>
            </Stack>
          </Form>
        </>
      )}
    </Stack>
  );
};

export default FormDomain;
