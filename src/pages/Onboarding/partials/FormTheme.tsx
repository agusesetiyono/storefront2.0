import React, { useState, useEffect } from 'react';
import { Form, Stack, Image } from 'react-bootstrap';

import StepImage from '@assets/image/onboarding-theme.svg';

import { useGetThemes, useGetSelectedTheme } from '@hooks/api/themes';
import Theme from '@components/Theme';

import type { IStepProps } from '@components/FormWizard/formWizard.types';
import type { ITheme, IThemeData } from '@interfaces/themes';

const FormTheme = ({ step, active, completed, onNext }: IStepProps) => {
  const { isFetching: isFetchingSelectedTheme, selectedThemeId } =
    useGetSelectedTheme();
  const { themesData, isFetching: isFetchingThemes } = useGetThemes();
  const [selectedTheme, setSelectedTheme] = useState<ITheme | null>(null);
  const [saveTheme, setSaveTheme] = useState<boolean>(false);
  const isFetching = isFetchingSelectedTheme || isFetchingThemes;

  const onThemeChange = (selectedThemeId: number, themesData: IThemeData) => {
    setSelectedTheme(themesData[selectedThemeId]);
  };

  const onThemeSave = () => {
    setSaveTheme(true);
    onNext();
  };

  useEffect(() => {
    if (completed && !isFetching && !saveTheme) {
      // update selected theme when onboarding step 2 already done before
      onThemeChange(selectedThemeId, themesData);
    }
  }, [completed, isFetching, saveTheme]);

  return (
    <Stack className="gap-8">
      <Stack direction="horizontal" className="gap-9 items-start">
        <Image src={StepImage} className="w-32" />
        <Stack gap={active ? 4 : 2}>
          <h3 className="text-3xl">Pilih tema Storefront</h3>
          {completed && !isFetching ? (
            <Stack
              direction="horizontal"
              className="justify-between items-start mt-2"
            >
              <h4 className="text-3xl text-blue-600 font-light capitalize">
                {selectedTheme?.theme_name || ''}
              </h4>
              <div className="w-[140px] -mb-11">
                <span
                  style={{
                    backgroundImage: `url(${
                      selectedTheme?.images?.small_urls || ''
                    })`,
                  }}
                  role="img"
                  aria-label={selectedTheme?.theme_name || ''}
                  className="theme-thumbnail"
                />
              </div>
            </Stack>
          ) : (
            <p className="text-base max-w-[485px]">
              Pilih tema Storefront sesuai dengan apa yang kamu inginkan.
            </p>
          )}
        </Stack>
      </Stack>
      <p
        className={completed ? 'text-green-600' : 'mt-5 text-gray-500'}
        data-testid="onboarding-theme-status"
      >
        Langkah {step} {completed ? 'berhasil' : ''}
      </p>
      {active && (
        <>
          <hr className="mb-4" />
          <Stack className="px-2">
            <Stack direction="horizontal" className="gap-8 mb-3">
              <Form.Label className="mb-0" aria-required>
                Tema dipilih
              </Form.Label>
              <p className="text-3xl font-light capitalize">
                {selectedTheme?.theme_name || ''}
              </p>
            </Stack>
            <Theme onThemeChange={onThemeChange} onThemeSave={onThemeSave} />
          </Stack>
        </>
      )}
    </Stack>
  );
};

export default FormTheme;
