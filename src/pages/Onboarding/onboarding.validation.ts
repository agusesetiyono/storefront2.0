import * as Yup from 'yup';

export const validationDomain = Yup.object().shape({
  subdomain: Yup.string()
    .required('Minimal 4 karakter berisi huruf dan angka.')
    .min(4, 'Minimal 4 karakter berisi huruf dan angka.'),
});
