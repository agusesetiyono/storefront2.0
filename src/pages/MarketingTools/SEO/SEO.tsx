import React from 'react';
import { Form } from 'react-bootstrap';
import { useFormik } from 'formik';
import xss, { IFilterXSSOptions, whiteList } from 'xss';

import { useGetSEO, useUpdateSEO } from '@hooks/api/marketingTools';
import { usePrompt } from '@hooks/usePrompt';
import { onSuccessMutate, onErrorMutate } from '@utils/reactQuery';
import { sendDashboardTracker } from '@utils/tracker/dashboard';

import TabHeader from '@components/TabHeader';
import FormLabel from '@components/FormLabel';
import Button from '@components/Button';

import { validationForm } from './seo.validation';
import type { IFormSEO } from './seo.types';

export const LEARN_MORE_URL =
  'https://help.smartseller.co.id/mengoptimasi-seo-2/';

const SEO = () => {
  const updateSEO = useUpdateSEO();
  const XSS_OPTIONS: IFilterXSSOptions = {
    whiteList: {
      ...whiteList,
      p: ['style'],
    },
    escapeHtml: (html: string): string => html,
  };

  const {
    values: { title, keyword, description },
    errors,
    dirty,
    resetForm,
    handleChange,
    handleSubmit,
  } = useFormik<IFormSEO>({
    initialValues: { title: '', keyword: '', description: '' },
    validationSchema: validationForm,
    onSubmit: ({ title, keyword, description }, { resetForm }) => {
      const reset = () =>
        resetForm({ values: { title, keyword, description } });

      updateSEO.mutate(
        {
          title: xss(title, XSS_OPTIONS),
          keyword: xss(keyword, XSS_OPTIONS),
          description: xss(description, XSS_OPTIONS),
        },
        {
          onSuccess: (data) => {
            reset();
            onSuccessMutate(data);
            void sendDashboardTracker({ actionType: 'save_changes' });
          },
          onError: (data) => {
            reset();
            onErrorMutate(data);
          },
        },
      );
    },
  });

  const { isFetching } = useGetSEO((res) => {
    if (!res) return;
    const seo: IFormSEO = res.data;
    resetForm({ values: seo });
  });

  usePrompt(dirty);

  return (
    <Form onSubmit={handleSubmit}>
      <TabHeader
        title="Pengaturan SEO (Search Engine Optimization)"
        subtitle="Pastikan halaman website kamu mudah dicari pengunjung dengan mengoptimalkan SEO. Pelajari di"
        subtitleClass="max-w-[465px]"
        href={LEARN_MORE_URL}
        onLinkClick={() =>
          void sendDashboardTracker({
            actionType: 'learn_more',
            actionDetails: LEARN_MORE_URL,
          })
        }
      />
      <div className="mb-6">
        <FormLabel
          htmlFor="title"
          label="SEO Title"
          description="Tulis judul artikel yang akan muncul di halaman hasil pencarian. Title tag akan muncul di bawah link dan di atas meta description."
        />
        <Form.Control
          id="title"
          name="title"
          value={title}
          disabled={isFetching}
          onChange={handleChange}
        />
      </div>
      <div className="mb-6">
        <FormLabel
          htmlFor="keyword"
          label="Meta Keyword"
          description="Masukkan kata kunci yang akan ditargetkan di website kamu. Pisahkan dengan tanda koma (,)."
        />
        <Form.Control
          id="keyword"
          name="keyword"
          value={keyword}
          disabled={isFetching}
          onChange={handleChange}
        />
      </div>
      <div className="mb-6">
        <FormLabel
          htmlFor="description"
          label="Meta Description"
          description="Tulis penjelasan artikel yang mengandung keyword penting dan berisi maksimum 160 karakter."
        />
        <Form.Control
          id="description"
          as="textarea"
          rows={3}
          name="description"
          value={description}
          disabled={isFetching}
          onChange={handleChange}
          isInvalid={!!errors.description}
        />
        <Form.Control.Feedback type="invalid" className="mt-2">
          {errors.description}
        </Form.Control.Feedback>
      </div>
      <hr className="mb-6" />
      <Button
        className="mb-1 w-[120px]"
        size="sm"
        type="submit"
        disabled={!!errors.description}
      >
        Simpan
      </Button>
    </Form>
  );
};

export default SEO;
