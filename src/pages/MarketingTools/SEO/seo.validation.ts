import * as Yup from 'yup';

export const validationForm = Yup.object().shape({
  description: Yup.string().max(160, 'Melebihi batas maks. 160 karakter'),
});
