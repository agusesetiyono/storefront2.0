import axios from '@mocks/axios';
import '@testing-library/jest-dom';
import {
  screen,
  waitFor,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';
import { LEARN_MORE_URL } from '../SEO';

import { renderWithRoute, API_URL, getLastCalled } from '@utils/test-helper';
import toast from '@utils/toast';
import { sendTracker } from '@utils/tracker';

import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';
import { GET_SEO_SUCCESS_RESPONSE } from '@mocks/api/marketingTools';

jest.mock('@utils/toast', () => ({
  success: jest.fn(),
  danger: jest.fn(),
}));

axios.onGet(`${API_URL.storefronts}/status`).reply(
  200,
  getMockSubscriptionsResponse({
    type: 'platinum',
    expiredDate: '2022-05-01T00:00:00+07:00',
  }),
);

let user: UserEvent;
beforeEach(() => {
  user = userEvent.setup({ delay: null });

  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));
  localStorage.setItem('onboardingStep', '5'); // already finished onboarding

  axios
    .onGet(`${API_URL.marketingTools}/seo`)
    .reply(200, GET_SEO_SUCCESS_RESPONSE);
});

afterEach(() => {
  jest.useRealTimers();
  jest.clearAllMocks();
  localStorage.clear();
});

describe('SEO Page', () => {
  const route = '/storefront-editor/settings/marketing-tools/seo';
  const promptConfirmationMessage =
    'Kamu akan keluar halaman ini tanpa menyimpan perubahan. Apa kamu yakin?';
  const pageTitle = 'Pengaturan SEO (Search Engine Optimization)';
  const titleLabel = 'SEO Title';
  const titleSubtitle =
    'Tulis judul artikel yang akan muncul di halaman hasil pencarian. Title tag akan muncul di bawah link dan di atas meta description.';
  const keywordLabel = 'Meta Keyword';
  const keywordSubtitle =
    'Masukkan kata kunci yang akan ditargetkan di website kamu. Pisahkan dengan tanda koma (,).';
  const descriptionLabel = 'Meta Description';
  const descriptionSubtitle =
    'Tulis penjelasan artikel yang mengandung keyword penting dan berisi maksimum 160 karakter.';
  const saveSEOButton = 'Simpan';
  const metaPixelTab = 'Meta Pixel';
  const metaPixelTitle = 'Pengaturan Meta Pixel & Feed';
  const contentWithXSS = `<a href="javascript:alert('XSS1')" onmouseover="alert('XSS2')">XSS<a>`;
  const successMessage = 'Berhasil menyimpan pengaturan.';
  const errorMessage =
    'Gagal menyimpan pengaturan. Cobalah beberapa saat lagi.';
  const defaultTrackerParams = {
    evn: 'ngorder_sf_dashboard_actions',
    active_tab: 'marketing_seo',
  };

  it('Renders properly with default configuration', async () => {
    const { container } = renderWithRoute({ route });

    expect(await screen.findByText(pageTitle)).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });

  it('Should render page with default state', async () => {
    renderWithRoute({ route });

    const titleInput = await screen.findByLabelText(titleLabel);
    expect(titleInput).toBeInTheDocument();
    expect(titleInput).toHaveValue('Malang shop | Tempat beli produk Malang');
    expect(screen.getByText(titleSubtitle)).toBeInTheDocument();

    const keywordInput = screen.getByLabelText(keywordLabel);
    expect(keywordInput).toBeInTheDocument();
    expect(keywordInput).toHaveValue('Jual beli Malang, belanja malang');
    expect(screen.getByText(keywordSubtitle)).toBeInTheDocument();

    const descriptionInput = screen.getByLabelText(descriptionLabel);
    expect(descriptionInput).toBeInTheDocument();
    expect(descriptionInput).toHaveValue(
      'Malang shop adalah toko online yang berdomisili di Kota Malang dan menjual produk atau barang-barang yang terkait dengan khas Malang.',
    );
    expect(screen.getByText(descriptionSubtitle)).toBeInTheDocument();

    const saveButton = screen.getByRole('button', { name: saveSEOButton });
    expect(saveButton).toBeInTheDocument();

    await user.click(screen.getByText('link ini'));
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'learn_more',
      action_details: LEARN_MORE_URL,
    });
  });

  it('Should show prompt message when click other tab', async () => {
    renderWithRoute({ route });

    expect(await screen.findByLabelText(titleLabel)).toBeInTheDocument();
    await user.type(screen.getByLabelText(titleLabel), 'Ubah Judul');

    await user.click(screen.getByText(metaPixelTab));
    expect(
      await screen.findByText(promptConfirmationMessage),
    ).toBeInTheDocument();
    await user.click(screen.getByRole('button', { name: 'Ya, keluar' }));
    expect(await screen.findByText(metaPixelTitle)).toBeInTheDocument();
  });

  it('Should cancel prompt pop up correctly', async () => {
    renderWithRoute({ route });

    expect(await screen.findByLabelText(titleLabel)).toBeInTheDocument();
    await user.type(screen.getByLabelText(titleLabel), 'Ubah Judul');

    await user.click(screen.getByText(metaPixelTab));
    expect(
      await screen.findByText(promptConfirmationMessage),
    ).toBeInTheDocument();
    await user.click(screen.getByRole('button', { name: 'Batal' }));
    await waitForElementToBeRemoved(() =>
      screen.getByText(promptConfirmationMessage),
    );
    expect(screen.getByText(pageTitle)).toBeInTheDocument();
  });

  it('Should show error state for description field', async () => {
    renderWithRoute({ route });

    const descriptionInput = await screen.findByLabelText(descriptionLabel);
    expect(descriptionInput).toBeInTheDocument();
    await user.type(descriptionInput, 'a'.repeat(161));
    expect(
      screen.getByText('Melebihi batas maks. 160 karakter'),
    ).toBeInTheDocument();
  });

  it('Should show success toast when save SEO', async () => {
    axios.onPut(`${API_URL.marketingTools}/seo`).replyOnce(200, {
      message: successMessage,
    });

    renderWithRoute({ route });

    await user.type(await screen.findByLabelText(titleLabel), 'Judul');
    await user.type(screen.getByLabelText(keywordLabel), 'Keyword');
    await user.type(screen.getByLabelText(descriptionLabel), 'Description');
    await user.click(screen.getByRole('button', { name: saveSEOButton }));
    await waitFor(() =>
      expect(toast.success).toHaveBeenLastCalledWith(successMessage),
    );
    await waitFor(() => {
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'save_changes',
      });
    });
  });

  it('Should show error toast when save SEO', async () => {
    axios.onPut(`${API_URL.marketingTools}/seo`).replyOnce(500, {
      errors: [{ message: errorMessage }],
      meta: { http_status: 500 },
    });

    renderWithRoute({ route });

    await user.type(await screen.findByLabelText(titleLabel), 'Judul');
    await user.type(screen.getByLabelText(keywordLabel), 'Keyword');
    await user.type(screen.getByLabelText(descriptionLabel), 'Description');
    await user.click(screen.getByRole('button', { name: saveSEOButton }));
    await waitFor(() =>
      expect(toast.danger).toHaveBeenLastCalledWith(errorMessage),
    );
  });

  it('Should sanitize page correctly', async () => {
    renderWithRoute({ route });

    axios.onPut(`${API_URL.marketingTools}/seo`).replyOnce(200, {
      message: successMessage,
    });

    const titleInput = await screen.findByLabelText(titleLabel);
    const keywordInput = await screen.findByLabelText(keywordLabel);
    const descriptionInput = await screen.findByLabelText(descriptionLabel);

    await user.clear(titleInput);
    await user.type(titleInput, contentWithXSS);
    await user.clear(keywordInput);
    await user.type(keywordInput, contentWithXSS);
    await user.clear(descriptionInput);
    await user.type(descriptionInput, contentWithXSS);

    await user.click(screen.getByRole('button', { name: saveSEOButton }));
    await waitFor(() =>
      expect(toast.success).toHaveBeenLastCalledWith(successMessage),
    );
    expect(getLastCalled(axios, 'put').data).toEqual(
      '{"title":"<a href>XSS<a>","keyword":"<a href>XSS<a>","description":"<a href>XSS<a>"}',
    );
  });
});
