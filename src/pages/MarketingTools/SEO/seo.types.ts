export interface IFormSEO {
  title: string;
  keyword: string;
  description: string;
}
