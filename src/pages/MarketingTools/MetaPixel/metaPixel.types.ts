export interface IMetaPixelTarget {
  label: string;
  id: string;
}

export interface IMetaPixelForm {
  pixel_id: string;
  list_event: string[];
  catalog_token: string;
}
