import '@testing-library/jest-dom';
import axios from '@mocks/axios';
import { screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';
import { renderWithRoute, API_URL, getLastCalled } from '@utils/test-helper';
import toast from '@utils/toast';
import { sendTracker } from '@utils/tracker';
import { PIXEL_ID_LEARN_MORE, FEED_LEARN_MORE } from '../MetaPixel';

import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';
import { GET_META_PIXEL_SUCCESS_RESPONSE } from '@mocks/api/marketingTools';

jest.mock('@utils/toast', () => ({
  success: jest.fn(),
  danger: jest.fn(),
}));

axios.onGet(`${API_URL.storefronts}/status`).reply(
  200,
  getMockSubscriptionsResponse({
    type: 'platinum',
    expiredDate: '2022-05-01T00:00:00+07:00',
  }),
);

let user: UserEvent;
beforeEach(() => {
  user = userEvent.setup({ delay: null });

  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));

  localStorage.setItem('onboardingStep', '5'); // already finished onboarding

  axios
    .onGet(`${API_URL.marketingTools}/meta-pixel`)
    .reply(200, GET_META_PIXEL_SUCCESS_RESPONSE);
});

afterEach(() => {
  jest.useRealTimers();
  jest.clearAllMocks();
  localStorage.clear();
});

describe('Meta Pixel', () => {
  const route = '/storefront-editor/settings/marketing-tools/meta-pixel';
  const tabTitle = 'Pengaturan Meta Pixel & Feed';
  const pixelIdLabel = 'Meta Pixel ID';
  const pixelIdSubtitle =
    'Perangkat yang sebelumnya bernama Facebook Pixel ini membantu mengumpulkan data dan memantau aktivitas pengunjung website kamu. Pelajari pada';
  const listEventLabel = 'Pasang Meta Pixel di:';
  const catalogTokenLabel = 'Feed Token';
  const catalogTokenSubtitle =
    'Aktifkan untuk menggunakan update katalog Facebook otomatis.';
  const catalogTokenPreviewLabel = 'Feed URL preview';
  const feedToggleLabel = 'Feed';
  const feedToggleSubtitle =
    'Aktifkan untuk menggunakan update katalog Facebook otomatis. Pelajari pada';
  const saveMetaPixelButton = 'Simpan';
  const successMessage = 'Berhasil menyimpan pengaturan.';
  const errorMessage =
    'Gagal menyimpan pengaturan. Cobalah beberapa saat lagi.';
  const defaultTrackerParams = {
    evn: 'ngorder_sf_dashboard_actions',
    active_tab: 'marketing_metapixel',
  };

  it('Renders properly with default configuration', async () => {
    const { container } = renderWithRoute({ route });

    expect(await screen.findByText(tabTitle)).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });

  it('Should render page with render default API state', async () => {
    renderWithRoute({ route });

    expect(await screen.findByText(tabTitle)).toBeInTheDocument();

    const pixelIdInput = await screen.findByLabelText(pixelIdLabel);
    expect(pixelIdInput).toBeInTheDocument();
    expect(pixelIdInput).toHaveValue('348704132352738');
    expect(screen.getByText(pixelIdSubtitle)).toBeInTheDocument();

    const listEventInput = screen.getByText(listEventLabel);
    expect(listEventInput).toBeInTheDocument();
    const targetHome = await screen.findByTestId('target-1');
    expect(targetHome).toBeChecked();
    const targetProductDetail = await screen.findByTestId('target-2');
    expect(targetProductDetail).toBeChecked();
    const targetCheckoutPage = await screen.findByTestId('target-3');
    expect(targetCheckoutPage).toBeChecked();

    const feedToggle = screen.getByLabelText(feedToggleLabel);
    expect(feedToggle).toBeInTheDocument();
    expect(screen.getByText(feedToggleSubtitle)).toBeInTheDocument();

    const catalogTokenInput = await screen.findByLabelText(catalogTokenLabel);
    expect(catalogTokenInput).toBeInTheDocument();
    expect(catalogTokenInput).toHaveValue('3fe5e3fcdde84ea88de985c724fa070c');
    expect(screen.getByText(catalogTokenSubtitle)).toBeInTheDocument();

    const catalogTokenPreviewInput = await screen.findByLabelText(
      catalogTokenPreviewLabel,
    );
    expect(catalogTokenPreviewInput).toBeInTheDocument();
    expect(catalogTokenPreviewInput).toHaveValue(
      'https://subdomain.smartseller.co.id/facebook/data_product/3fe5e3fcdde84ea88de985c724fa070c',
    );

    // copy token url correctly
    await user.click(screen.getByRole('button', { name: 'Copy' }));
    await waitFor(() =>
      expect(toast.success).toHaveBeenLastCalledWith(
        'Berhasil menyalin token.',
      ),
    );

    // generate token correctly
    await user.click(screen.getByRole('button', { name: 'Generate' }));
    expect(catalogTokenLabel).toEqual(expect.any(String));

    const saveButton = screen.getByRole('button', {
      name: saveMetaPixelButton,
    });
    expect(saveButton).toBeInTheDocument();

    const [pixelIdLearnMore, feedLearnMore] = screen.getAllByText('link ini');
    await user.click(pixelIdLearnMore);
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'learn_more',
      action_details: PIXEL_ID_LEARN_MORE,
    });
    await user.click(feedLearnMore);
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'learn_more',
      action_details: FEED_LEARN_MORE,
    });
  });

  it('Should show success toast when save Meta Pixel', async () => {
    axios.onPut(`${API_URL.marketingTools}/meta-pixel`).replyOnce(200, {
      message: successMessage,
    });

    renderWithRoute({ route });

    await user.clear(await screen.findByLabelText(pixelIdLabel));
    await user.type(
      await screen.findByLabelText(pixelIdLabel),
      '34870413235273891280',
    );

    const targetPayment = await screen.findByTestId('target-4');
    expect(targetPayment).not.toBeChecked();
    await user.click(targetPayment);

    await user.clear(await screen.findByLabelText(catalogTokenLabel));
    await user.type(
      screen.getByLabelText(catalogTokenLabel),
      '3fe5e3fcdde84ea88de985c724fa070c',
    );

    await user.click(screen.getByRole('button', { name: 'Simpan' }));
    await waitFor(() =>
      expect(toast.success).toHaveBeenLastCalledWith(successMessage),
    );
    expect(getLastCalled(axios, 'put').data).toEqual(
      '{"pixel_id":"34870413235273891280","list_event":["1","2","3","4"],"catalog_token":"3fe5e3fcdde84ea88de985c724fa070c"}',
    );
    await waitFor(() => {
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'save_changes',
      });
    });
  });

  it('Should show error toast when save Meta Pixel', async () => {
    axios.onPut(`${API_URL.marketingTools}/meta-pixel`).replyOnce(500, {
      errors: [{ message: errorMessage }],
      meta: { http_status: 500 },
    });

    renderWithRoute({ route });

    await user.clear(await screen.findByLabelText(pixelIdLabel));
    await user.type(
      await screen.findByLabelText(pixelIdLabel),
      '34870413235273891280',
    );

    const targetPayment = await screen.findByTestId('target-4');
    expect(targetPayment).not.toBeChecked();
    await user.click(targetPayment);

    await user.clear(await screen.findByLabelText(catalogTokenLabel));
    await user.type(
      screen.getByLabelText(catalogTokenLabel),
      '3fe5e3fcdde84ea88de985c724fa070c',
    );
    await user.click(screen.getByRole('button', { name: 'Simpan' }));
    await waitFor(() =>
      expect(toast.danger).toHaveBeenLastCalledWith(errorMessage),
    );
    expect(getLastCalled(axios, 'put').data).toEqual(
      '{"pixel_id":"34870413235273891280","list_event":["1","2","3","4"],"catalog_token":"3fe5e3fcdde84ea88de985c724fa070c"}',
    );
  });

  it('Should allow only alphanumeric and dash for Meta Pixel ID', async () => {
    renderWithRoute({ route });

    expect(await screen.findByText(tabTitle)).toBeInTheDocument();
    const metaPixelID = screen.getByLabelText(pixelIdLabel);
    await user.clear(await screen.findByLabelText(pixelIdLabel));
    await user.type(metaPixelID, 'a');
    expect(metaPixelID).toHaveValue('a');
    await user.type(metaPixelID, '1');
    expect(metaPixelID).toHaveValue('a1');
    await user.type(metaPixelID, 'B');
    expect(metaPixelID).toHaveValue('a1B');
    await user.type(metaPixelID, '}');
    expect(metaPixelID).toHaveValue('a1B');
    await user.type(metaPixelID, '-');
    expect(metaPixelID).toHaveValue('a1B-');
    await user.type(metaPixelID, ' ');
    expect(metaPixelID).toHaveValue('a1B-');
  });

  it('Should allow only alphanumeric for Catalog Token', async () => {
    renderWithRoute({ route });

    expect(await screen.findByText(tabTitle)).toBeInTheDocument();
    const catalogToken = screen.getByLabelText(catalogTokenLabel);
    await user.clear(await screen.findByLabelText(catalogTokenLabel));
    await user.type(catalogToken, 'a');
    expect(catalogToken).toHaveValue('a');
    await user.type(catalogToken, '1');
    expect(catalogToken).toHaveValue('a1');
    await user.type(catalogToken, 'B');
    expect(catalogToken).toHaveValue('a1B');
    await user.type(catalogToken, '}');
    expect(catalogToken).toHaveValue('a1B');
    await user.type(catalogToken, '-');
    expect(catalogToken).toHaveValue('a1B');
    await user.type(catalogToken, ' ');
    expect(catalogToken).toHaveValue('a1B');
  });
});
