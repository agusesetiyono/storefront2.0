import React, { useState } from 'react';
import { useBoolean, useCopyToClipboard } from 'usehooks-ts';
import { Form } from 'react-bootstrap';
import { useFormik } from 'formik';
import { ALPHA_NUMERIC_DASH_REGEX, ALPHA_NUMERIC_REGEX } from '@utils/form';
import { generateRandomString } from '@utils/token';
import toast from '@utils/toast';
import config from '@utils/config';
import { sendDashboardTracker } from '@utils/tracker/dashboard';

import TabHeader from '@components/TabHeader';
import FormLabel from '@components/FormLabel';
import Button from '@components/Button';

import useSubscription from '@hooks/useSubscription';
import { useGetMetaPixel, useUpdateMetaPixel } from '@hooks/api/marketingTools';
import { onSuccessMutate, onErrorMutate } from '@utils/reactQuery';

import type { IMetaPixelTarget, IMetaPixelForm } from './metaPixel.types';

export const PIXEL_ID_LEARN_MORE =
  'https://help.smartseller.co.id/mengaktifkan-facebook-pixel-2/';
export const FEED_LEARN_MORE =
  'https://help.smartseller.co.id/mengatur-facebook-feeds-secara-otomatis/';

const MetaPixel = () => {
  const {
    value: isShowFeed,
    toggle: toggleFeed,
    setFalse: hideFeed,
  } = useBoolean(false);
  const [, copyToClipboard] = useCopyToClipboard();

  const {
    subscription: { subdomain },
  } = useSubscription();
  const subdomainLink = subdomain.concat(config.siteExt as string);
  const feedUrl = `https://${subdomainLink}/facebook/data_product/`;
  const [feedUrlToken, setFeedUrlToken] = useState<string>(feedUrl);

  const targets: IMetaPixelTarget[] = [
    { label: 'Home', id: '1' },
    { label: 'Detail produk', id: '2' },
    { label: 'Halaman checkout', id: '3' },
    {
      label: 'Halaman setelah konfirmasi pembayaran',
      id: '4',
    },
    { label: 'Tombol tambah ke keranjang', id: '5' },
    { label: 'Tombol search', id: '7' },
  ];

  const {
    values: { pixel_id, list_event, catalog_token },
    resetForm,
    handleSubmit,
    setFieldValue,
  } = useFormik<IMetaPixelForm>({
    initialValues: {
      pixel_id: '',
      list_event: [],
      catalog_token: '',
    },
    onSubmit: ({ pixel_id, list_event, catalog_token }, { resetForm }) => {
      const reset = () =>
        resetForm({ values: { pixel_id, list_event, catalog_token } });

      updateMetaPixel.mutate(
        {
          pixel_id,
          list_event,
          catalog_token: isShowFeed ? catalog_token : '',
        },
        {
          onSuccess: (data) => {
            reset();
            !catalog_token && hideFeed();
            onSuccessMutate(data);
            void sendDashboardTracker({ actionType: 'save_changes' });
          },
          onError: (data) => {
            reset();
            onErrorMutate(data);
          },
        },
      );
    },
  });

  const { isFetching } = useGetMetaPixel((res) => {
    if (!res) return;
    const metaPixel: IMetaPixelForm = res.data;
    resetForm({ values: metaPixel });
    if (metaPixel.catalog_token !== '') {
      setFeedUrlToken(`${feedUrl}${metaPixel.catalog_token}`);
      toggleFeed();
    } else {
      generateToken();
    }
  });

  const updateMetaPixel = useUpdateMetaPixel();

  const onIdChange = (e) => {
    const value = e.target.value;
    if (value === '' || ALPHA_NUMERIC_DASH_REGEX.test(value as string)) {
      void setFieldValue('pixel_id', value);
    }
  };

  const onTokenChange = (e) => {
    const value = e.target.value;
    if (value === '' || ALPHA_NUMERIC_REGEX.test(value as string)) {
      void setFieldValue('catalog_token', e.target.value);
    }
    setFeedUrlToken(`${feedUrl}${e.target.value}`);
  };

  const generateToken = () => {
    const token = generateRandomString(32);
    void setFieldValue('catalog_token', token);
    setFeedUrlToken(`${feedUrl}${token}`);
  };

  const copyToken = () => {
    void copyToClipboard(catalog_token);
    toast.success('Berhasil menyalin token.');
  };

  const onTargetChange = async (target: string, checked: boolean) => {
    let targets: string[] = [];
    if (checked) {
      targets = [...list_event, target];
    } else {
      targets = list_event;
      const index = targets.indexOf(target);
      if (index !== -1) {
        targets.splice(index, 1);
      }
    }

    await setFieldValue('list_event', targets);
  };

  return (
    <Form onSubmit={handleSubmit}>
      <TabHeader title="Pengaturan Meta Pixel & Feed" />
      <div className="mb-6">
        <FormLabel
          htmlFor="metaPixelID"
          label="Meta Pixel ID"
          description="Perangkat yang sebelumnya bernama Facebook Pixel ini membantu mengumpulkan data dan memantau aktivitas pengunjung website kamu. Pelajari pada"
          descriptionClass="max-w-[600px]"
          href={PIXEL_ID_LEARN_MORE}
          onLinkClick={() =>
            void sendDashboardTracker({
              actionType: 'learn_more',
              actionDetails: PIXEL_ID_LEARN_MORE,
            })
          }
        />
        <Form.Control
          id="metaPixelID"
          name="id"
          value={pixel_id}
          disabled={isFetching}
          onChange={onIdChange}
        />
      </div>
      <div className="mb-6">
        <FormLabel className="mb-2" label="Pasang Meta Pixel di:" />
        {targets.map((target) => (
          <Form.Check
            label={target.label}
            id={`target-${target.id}`}
            key={`target-${target.id}`}
            className="pointer"
            checked={list_event.includes(target.id)}
            value={target.id}
            name="target"
            data-testid={`target-${target.id}`}
            onChange={(e) => void onTargetChange(target.id, e.target.checked)}
          />
        ))}
      </div>
      <hr className="mb-6" />
      <div className="mb-6">
        <div className="flex">
          <FormLabel
            htmlFor="feedToggle"
            label="Feed"
            description="Aktifkan untuk menggunakan update katalog Facebook otomatis. Pelajari pada"
            href={FEED_LEARN_MORE}
            onLinkClick={() =>
              void sendDashboardTracker({
                actionType: 'learn_more',
                actionDetails: FEED_LEARN_MORE,
              })
            }
            className="mr-2"
          />
          <Form.Check
            id="feedToggle"
            data-testid="feed-toggle"
            type="switch"
            checked={isShowFeed}
            onChange={toggleFeed}
          />
        </div>
      </div>
      {isShowFeed && (
        <>
          <div className="mb-6">
            <FormLabel
              htmlFor="feedToken"
              label="Feed Token"
              description="Aktifkan untuk menggunakan update katalog Facebook otomatis."
            />
            <Form.Control
              id="feedToken"
              name="feedToken"
              value={catalog_token}
              disabled={isFetching}
              onChange={onTokenChange}
            />
            <div className="flex mt-2">
              <Button
                size="sm"
                className="mr-2"
                variant="primary-tertiary"
                onClick={generateToken}
              >
                Generate
              </Button>
              <Button
                size="sm"
                variant="primary-tertiary"
                disabled={!catalog_token}
                onClick={copyToken}
              >
                <span className="flex">
                  <i className="fi fi-rr-copy mr-2"></i>
                  <span>Copy</span>
                </span>
              </Button>
            </div>
          </div>
          <div className="mb-6">
            <FormLabel
              htmlFor="feedUrlToken"
              className="mb-2"
              label="Feed URL preview"
            />
            <Form.Control id="feedUrlToken" disabled value={feedUrlToken} />
          </div>
        </>
      )}
      <hr className="mb-6" />
      <Button className="mb-1 w-[120px]" size="sm" type="submit">
        Simpan
      </Button>
    </Form>
  );
};

export default MetaPixel;
