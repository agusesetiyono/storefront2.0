import React from 'react';
import { useBoolean } from 'usehooks-ts';
import { Form } from 'react-bootstrap';
import { useFormik } from 'formik';
import { ALPHA_NUMERIC_DASH_REGEX } from '@utils/form';

import { useGetGoogleAnalytics, useUpdateGoogleAnalytics } from '@hooks/api/marketingTools';
import { onSuccessMutate, onErrorMutate } from '@utils/reactQuery';
import { sendDashboardTracker } from '@utils/tracker/dashboard';

import TabHeader from '@components/TabHeader';
import FormLabel from '@components/FormLabel';
import Button from '@components/Button';

import type { IGoogleAnalyticsForm } from './googleAnalytics.types';

export const TRACKING_ID_LEARN_MORE =
  'https://help.smartseller.co.id/menggunakan-google-analytics/';
export const TRACKING_ID_GOOGLE_LEARN_MORE =
  'https://support.google.com/analytics/answer/1008080#zippy=%2Cin-this-article';

const GoogleAnalytics = () => {
  const updateGoogleAnalytics = useUpdateGoogleAnalytics();
  const { value: isShowPreview, toggle: togglePreview } = useBoolean(false);

  const {
    values: { tracking_id },
    handleSubmit,
    setFieldValue,
    setValues,
    resetForm
  } = useFormik<IGoogleAnalyticsForm>({
    initialValues: {
      tracking_id: '',
    },
    onSubmit: ({ tracking_id }, { resetForm }) => {
      const reset = () =>
        resetForm({ values: { tracking_id } });

      updateGoogleAnalytics.mutate(
        {
          tracking_id,
        },
        {
          onSuccess: (data) => {
            reset();
            onSuccessMutate(data);
            void sendDashboardTracker({ actionType: 'save_changes' });
          },
          onError: (data) => {
            reset();
            onErrorMutate(data);
          },
        },
      );
    },
  });

  const onTrackingIdChange = (e) => {
    const value = e.target.value;
    if (value === '' || ALPHA_NUMERIC_DASH_REGEX.test(value as string)) {
      void setFieldValue('tracking_id', value);
    }
  };

  const { isFetching } = useGetGoogleAnalytics((res) => {
    if (!res) return;
    const tracking_id: IGoogleAnalyticsForm = res.data;
    void setValues(tracking_id);
    void resetForm({ values: tracking_id });
  });

  return (
    <Form onSubmit={handleSubmit}>
      <TabHeader title="Pengaturan Google Analytics" />
      <div className="mb-6">
        <FormLabel
          htmlFor="trackingID"
          label="Tracking ID"
          description="Masukkan tracking ID, berupa kode unik agar Google Analytics bisa mengumpulkan data website kamu. Pelajari di"
          href={TRACKING_ID_LEARN_MORE}
          onLinkClick={() =>
            void sendDashboardTracker({
              actionType: 'learn_more',
              actionDetails: TRACKING_ID_LEARN_MORE,
            })
          }
        />
        <Form.Control
          id="trackingID"
          className="max-w-[320px]"
          value={tracking_id}
          name="trackingId"
          disabled={isFetching}
          onChange={onTrackingIdChange}
        />
      </div>
      <Button size="sm" variant="primary-tertiary" onClick={togglePreview}>
        <span className="flex">
          <i
            className={`fi fi-rr-${isShowPreview ? 'cross-small' : 'eye'} mr-2`}
          ></i>
          <span>
            {isShowPreview ? 'Tutup preview' : 'Preview Tracking ID Code'}
          </span>
        </span>
      </Button>
      {isShowPreview && (
        <pre className="mt-4" data-testid="ga-preview">
          {`<!-- Global site tag (gtag.js) - Google Analytics -->\n<script async src="https://www.googletagmanager.com/gtag/js?id=${tracking_id}"></script>\n<script>\nwindow.dataLayer = window.dataLayer || [];\nfunction gtag(){dataLayer.push(arguments);}\ngtag('js', new Date());\ntag('config', '${tracking_id}');\n</script>`}
        </pre>
      )}
      <p className="font-bold mt-6">Petunjuk:</p>
      <ol className="ml-6 mb-6">
        <li>
          Untuk mendapatkan kode Tracking ID kamu, pelajari{' '}
          <a
            href={TRACKING_ID_GOOGLE_LEARN_MORE}
            target="_blank"
            rel="noopener noreferrer"
            onClick={() =>
              void sendDashboardTracker({
                actionType: 'learn_more',
                actionDetails: TRACKING_ID_GOOGLE_LEARN_MORE,
              })
            }
          >
            panduan oleh Google
          </a>
        </li>
        <li>Copy paste/salin kodenya pada kolom di atas.</li>
      </ol>
      <hr className="mb-6" />
      <Button className="mb-1 w-[120px]" size="sm" type="submit" data-testid="button-save">
        Simpan
      </Button>
    </Form>
  );
};

export default GoogleAnalytics;
