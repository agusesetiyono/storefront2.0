import '@testing-library/jest-dom';
import axios from '@mocks/axios';
import {
  screen,
  waitFor,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';
import { renderWithRoute, API_URL } from '@utils/test-helper';
import toast from '@utils/toast';
import { sendTracker } from '@utils/tracker';
import { TRACKING_ID_LEARN_MORE, TRACKING_ID_GOOGLE_LEARN_MORE } from '../GoogleAnalytics'

// API
import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';
import {
  GET_GOOGLE_ANALYTICS_DEFAULT_SUCCESS_RESPONSE,
  GET_GOOGLE_ANALYTICS_EMPTY_SUCCESS_RESPONSE,
} from '@mocks/api/marketingTools'

jest.mock('@utils/toast', () => ({
  success: jest.fn(),
  danger: jest.fn(),
}));

axios.onGet(`${API_URL.storefronts}/status`).reply(
  200,
  getMockSubscriptionsResponse({
    type: 'platinum',
    expiredDate: '2027-05-01T00:00:00+07:00',
  }),
);

let user: UserEvent;

beforeEach(() => {
  user = userEvent.setup({ delay: null });
  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));
  localStorage.setItem('onboardingStep', '5'); // already finished onboarding
  axios.onGet(`${API_URL.marketingTools}/google-analytics`).reply(200, GET_GOOGLE_ANALYTICS_EMPTY_SUCCESS_RESPONSE  );
})

afterEach(() => {
  jest.useRealTimers();
  jest.clearAllMocks();
  localStorage.clear();
});

describe('Marketing Tools - Google Analytics', () => {
  const route = '/storefront-editor/settings/marketing-tools/google-analytics';
  const apiUrl = `${API_URL.marketingTools}/google-analytics`
  const trackingIdLabel = 'Tracking ID';
  const saveButton = /Simpan/;
  const saveSuccessMessage = 'Berhasil menyimpan pengaturan.'
  const saveErrorMessage = 'Gagal menyimpan pengaturan. Cobalah beberapa saat lagi.'
  const openPreviewButton = 'Preview Tracking ID Code';
  const closePreviewButton = 'Tutup preview';
  const googleAnalyticsGuideLabel = 'panduan oleh Google';
  const googleAnalyticsGuideLink = 'https://support.google.com/analytics/answer/1008080#zippy=%2Cin-this-article';
  const trackingIdGuideLabel = 'link ini';
  const trackingIdGuideLink = 'https://help.smartseller.co.id/menggunakan-google-analytics/'
  const previewSectionTestId = 'ga-preview'
  const trackingId = 'UA-12345-12345'
  const defaultTrackerParams = {
    evn: 'ngorder_sf_dashboard_actions',
    active_tab: 'marketing_analytics',
  };

  it('Should render page with default state', async () => {
    renderWithRoute({ route });

    // tracking_id input
    const trackingIdInput = await screen.findByLabelText(trackingIdLabel)
    expect(trackingIdInput).toBeInTheDocument()
    expect(trackingIdInput).toHaveValue('')

    // preview state
    expect(screen.getByText(openPreviewButton)).toBeInTheDocument();
    expect(screen.queryByLabelText(closePreviewButton)).toBeFalsy();
    expect(screen.queryByTestId(previewSectionTestId)).toBeFalsy();

    // save button
    expect(screen.getByText(saveButton)).not.toHaveAttribute('disabled')

    // guide links
    expect(screen.getByText(trackingIdGuideLabel)).toHaveAttribute('href', trackingIdGuideLink)
    expect(screen.getByText(googleAnalyticsGuideLabel)).toHaveAttribute('href', googleAnalyticsGuideLink)

    await user.click(screen.getByText('link ini'));
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'learn_more',
      action_details: TRACKING_ID_LEARN_MORE,
    });
    await user.click(screen.getByText('panduan oleh Google'));
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'learn_more',
      action_details: TRACKING_ID_GOOGLE_LEARN_MORE,
    });
  });

  it('Should show and hide preview', async () => {
    renderWithRoute({ route });

    const trackingInput = await screen.findByLabelText(trackingIdLabel)
    await user.type(trackingInput, trackingId)

    expect(screen.getByText(openPreviewButton)).toBeInTheDocument();
    expect(screen.queryByTestId(previewSectionTestId)).toBeFalsy();

    await user.click(screen.getByText(openPreviewButton))
    expect(screen.queryByText(openPreviewButton)).not.toBeInTheDocument();
    expect(screen.getByTestId(previewSectionTestId)).toBeInTheDocument();
    expect(screen.getByTestId(previewSectionTestId)).toHaveTextContent(/id=UA-12345-12345/g)

    await user.click(screen.getByText(closePreviewButton))
    expect(screen.queryByText(openPreviewButton)).toBeInTheDocument();
    expect(screen.queryByTestId(previewSectionTestId)).not.toBeInTheDocument();
  })

  it('Should show toast when successfully save changes', async () => {
    axios.onGet(apiUrl).reply(200, GET_GOOGLE_ANALYTICS_DEFAULT_SUCCESS_RESPONSE);
    axios.onPut(apiUrl).replyOnce(200, { message: saveSuccessMessage })
    renderWithRoute({ route });

    const trackingInput = await screen.findByLabelText(trackingIdLabel)
    expect(trackingInput).toHaveValue(GET_GOOGLE_ANALYTICS_DEFAULT_SUCCESS_RESPONSE.data.tracking_id)

    // input non alphanumeric characters
    await user.type(trackingInput, '!!!')
    expect(trackingInput).toHaveValue(GET_GOOGLE_ANALYTICS_DEFAULT_SUCCESS_RESPONSE.data.tracking_id)

    await user.clear(trackingInput)
    await user.type(trackingInput, trackingId)
    expect(trackingInput).toHaveValue(trackingId)

    await user.click(screen.getByText(saveButton))
    await waitFor(() => {
      expect(toast.success).toHaveBeenCalledWith(saveSuccessMessage)
    })
    await waitFor(() => {
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'save_changes',
      });
    });
  })

  it('Should show toast when failed to save changes', async () => {
    axios.onPut(apiUrl).replyOnce(500, {
      errors: [{ message: saveErrorMessage }],
      meta: { http_status: 500 },
    });
    renderWithRoute({ route });

    const trackingInput = await screen.findByLabelText(trackingIdLabel)

    await user.clear(trackingInput)
    await user.type(trackingInput, trackingId)
    await user.click(screen.getByText(saveButton))
    await waitFor(() => {
      expect(toast.danger).toHaveBeenCalledWith(saveErrorMessage)
    })
  })

});
