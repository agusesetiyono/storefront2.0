import * as Yup from 'yup';

export const sliderValidation = Yup.object().shape({
  image: Yup.string(),
  image_file: Yup.mixed().when('image', {
    is: (image) => !image,
    then: Yup.mixed().required(),
  }),
});
