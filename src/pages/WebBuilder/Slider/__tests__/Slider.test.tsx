import '@testing-library/jest-dom';
import axios from '@mocks/axios';
import { screen, within, waitFor, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';
import {
  renderWithRoute,
  API_URL,
  getLastCalled,
  getLastCalledTimes,
} from '@utils/test-helper';
import toast from '@utils/toast';
import { sendTracker } from '@utils/tracker';

// API
import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';
import { GENERAL_SUCCESS_RESPONSE, GENERAL_ERROR_RESPONSE } from '@mocks/api';
import {
  GET_SLIDERS_SUCCESS_RESPONSE,
  REORDER_SLIDER_SUCCESS_RESPONSE,
  GET_DELETE_SLIDER_ERROR_RESPONSE,
  GET_SLIDER_SUCCESS_RESPONSE,
} from '@mocks/api/slider';

jest.mock('@utils/toast', () => ({
  success: jest.fn(),
  danger: jest.fn(),
}));

const file = new File(['hello'], 'hello.png', { type: 'image/png' });

axios.onGet(`${API_URL.storefronts}/status`).reply(
  200,
  getMockSubscriptionsResponse({
    type: 'platinum',
    expiredDate: '2027-05-01T00:00:00+07:00',
  }),
);

let user: UserEvent;

beforeEach(() => {
  user = userEvent.setup({ delay: null });
  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));
  localStorage.setItem('onboardingStep', '5'); // already finished onboarding

  axios.onGet(API_URL.sliders).reply(200, GET_SLIDERS_SUCCESS_RESPONSE);
  axios.onGet(`${API_URL.sliders}/1`).reply(200, GET_SLIDER_SUCCESS_RESPONSE);
  axios.onPost(API_URL.sliders).reply(200, GENERAL_SUCCESS_RESPONSE);
  axios.onPut(`${API_URL.sliders}/1`).reply(200, GENERAL_SUCCESS_RESPONSE);
});

afterEach(() => {
  jest.useRealTimers();
  jest.clearAllMocks();
  localStorage.clear();
  axios.history.get = [];
  axios.history.put = [];
  axios.history.post = [];
});

describe('Web builder slider', () => {
  const route =
    '/storefront-editor/settings/custom-website/builder/section/slider';
  const tabDescription =
    'Dengan Slider, kamu bisa menampilkan beberapa banner secara bergantian.';
  const addSliderButton = 'Tambah Slider';
  const firstSliderTitle = 'That one lazy dogo';
  const editSliderTitle = 'Edit Slider';
  const deleteSliderTitle = 'Hapus Slider?';
  const uploadImageLabel = 'Upload Gambar Slider';
  const deleteImageTitle = 'Hapus gambar?';
  const sliderTitleLabel = 'Headline pada Slider';
  const deleteConfirmationButton = 'Ya, hapus';
  const defaultTrackerParams = {
    evn: 'ngorder_sf_builder_actions',
    active_panel: 'section_slider',
  };

  describe('List', () => {
    it('Should render list correctly', async () => {
      renderWithRoute({ route });

      expect(await screen.findByText(tabDescription)).toBeInTheDocument();
      expect(screen.getByText(addSliderButton)).toBeInTheDocument();

      const firstSlider = screen.getByTestId('side-menu-item-0-0');
      expect(
        within(firstSlider).getByText(firstSliderTitle),
      ).toBeInTheDocument();
      expect(
        within(firstSlider).getByAltText(firstSliderTitle),
      ).toBeInTheDocument();
      expect(
        within(firstSlider).getByTestId('sort-action-1'),
      ).toBeInTheDocument();
      expect(
        within(firstSlider).getByTestId('delete-trigger-0-0'),
      ).toBeInTheDocument();

      const thirdSlider = screen.getByTestId('side-menu-item-0-2');
      expect(within(thirdSlider).getByText('Slider')).toBeInTheDocument(); // default slider title
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'visit',
      });
    });

    it('Should render empty state correctly', async () => {
      axios.onGet(API_URL.sliders).reply(200, { data: [] });
      renderWithRoute({ route });

      expect(await screen.findByText(tabDescription)).toBeInTheDocument();
      expect(
        screen.getByText(
          'Belum ada slider. Tambahkan dengan klik tombol di atas.',
        ),
      ).toBeInTheDocument();
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'visit',
      });
    });

    it('Should render error state correctly', async () => {
      const errorMessage = 'Gagal mengambil data Slider';
      axios.onGet(API_URL.sliders).reply(500, {});
      renderWithRoute({ route });

      expect(await screen.findByText(tabDescription)).toBeInTheDocument();
      expect(screen.getByText(errorMessage)).toBeInTheDocument();
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'error',
        action_details: errorMessage,
      });

      axios.onGet(API_URL.sliders).reply(200, GET_SLIDERS_SUCCESS_RESPONSE);
      await user.click(screen.getByRole('button', { name: 'Coba Lagi' }));
      expect(await screen.findByText(firstSliderTitle)).toBeInTheDocument();
    });
  });

  describe('Reorder', () => {
    beforeEach(() => {
      axios
        .onPatch(`${API_URL.sliders}/orders`)
        .reply(200, REORDER_SLIDER_SUCCESS_RESPONSE);
    });

    const orderParams = '{"slider_ids":[1,3,4,5,7,8,9,10,2,6]}';
    const triggerReorder = async (isFinishedReorder = true) => {
      const drag1 = screen.getByTestId('sort-action-1');
      const drag2 = screen.getByTestId('sort-action-2');

      await user.pointer([
        { keys: '[MouseLeft>]', target: drag2 },
        { target: drag1 },
        { keys: '[/MouseLeft]' },
      ]);
      jest.runAllTimers();

      const drag5 = screen.getByTestId('sort-action-5');
      const drag6 = screen.getByTestId('sort-action-6');

      await user.pointer([
        { keys: '[MouseLeft>]', target: drag5 },
        { target: drag6 },
        { keys: '[/MouseLeft]' },
      ]);
      isFinishedReorder && jest.runAllTimers();
    };

    it('Should reorder sliders correctly', async () => {
      renderWithRoute({ route });

      expect(await screen.findByText(firstSliderTitle)).toBeInTheDocument();
      await triggerReorder();
      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(
          REORDER_SLIDER_SUCCESS_RESPONSE.message,
        ),
      );
      expect(getLastCalled(axios, 'patch').data).toEqual(orderParams);
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'reorder_entry',
        action_details: '6',
      });
    });

    it('Should reorder sliders, even when user leave the page', async () => {
      renderWithRoute({ route });

      expect(await screen.findByText(firstSliderTitle)).toBeInTheDocument();
      await triggerReorder(false);
      await user.click(screen.getByTestId('back-button'));

      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(
          REORDER_SLIDER_SUCCESS_RESPONSE.message,
        ),
      );
      expect(getLastCalled(axios, 'patch').data).toEqual(orderParams);
      await waitFor(() =>
        expect(screen.getByText('Custom Section')).toBeInTheDocument(),
      );
    });

    it('Should reorder sliders, even when user click delete', async () => {
      renderWithRoute({ route });

      expect(await screen.findByText(firstSliderTitle)).toBeInTheDocument();
      await triggerReorder(false);
      await user.click(screen.getByTestId('delete-trigger-0-0'));

      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(
          REORDER_SLIDER_SUCCESS_RESPONSE.message,
        ),
      );
      expect(getLastCalled(axios, 'patch').data).toEqual(orderParams);
      await waitFor(() =>
        expect(screen.getByText(deleteSliderTitle)).toBeInTheDocument(),
      );
    });

    it('Should handle error when reorder', async () => {
      axios
        .onPatch(`${API_URL.sliders}/orders`)
        .reply(500, GENERAL_ERROR_RESPONSE);

      renderWithRoute({ route });

      expect(await screen.findByText(firstSliderTitle)).toBeInTheDocument();
      await triggerReorder();
      await waitFor(() =>
        expect(toast.danger).toHaveBeenLastCalledWith(
          GENERAL_ERROR_RESPONSE.errors[0].message,
        ),
      );
      expect(getLastCalledTimes(axios.history.get, API_URL.sliders)).toEqual(2);
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'error',
        action_details: GENERAL_ERROR_RESPONSE.errors[0].message,
      });
    });
  });

  describe('Delete', () => {
    it('Should delete slide correctly', async () => {
      axios
        .onDelete(`${API_URL.sliders}/1`)
        .reply(200, GENERAL_SUCCESS_RESPONSE);

      renderWithRoute({ route });

      expect(await screen.findByText(firstSliderTitle)).toBeInTheDocument();
      await user.click(screen.getByTestId('delete-trigger-0-0'));
      expect(screen.getByText(deleteSliderTitle)).toBeInTheDocument();
      await user.click(
        screen.getByRole('button', { name: deleteConfirmationButton }),
      );
      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(
          GENERAL_SUCCESS_RESPONSE.message,
        ),
      );
      expect(screen.queryByText(deleteSliderTitle)).not.toBeInTheDocument();
      expect(getLastCalledTimes(axios.history.get, API_URL.sliders)).toEqual(2);
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'delete_entry',
        action_details: '1',
      });
    });

    it.each([997, 11127])(
      'Should handle error delete slide correctly, error code: %s',
      async (code) => {
        const errorResponse = GET_DELETE_SLIDER_ERROR_RESPONSE(code);
        axios.onDelete(`${API_URL.sliders}/1`).reply(500, errorResponse);

        renderWithRoute({ route });

        expect(await screen.findByText(firstSliderTitle)).toBeInTheDocument();
        await user.click(screen.getByTestId('delete-trigger-0-0'));
        await user.click(
          screen.getByRole('button', { name: deleteConfirmationButton }),
        );
        await waitFor(() =>
          expect(toast.danger).toHaveBeenLastCalledWith(
            errorResponse.errors[0].message,
          ),
        );
        expect(screen.queryByText(deleteSliderTitle)).not.toBeInTheDocument();
        expect(getLastCalledTimes(axios.history.get, API_URL.sliders)).toEqual(
          code === 11127 ? 2 : 1,
        );
        expect(sendTracker).toHaveBeenLastCalledWith({
          ...defaultTrackerParams,
          action_type: 'error',
          action_details: errorResponse.errors[0].message,
        });
      },
    );
  });

  describe('Edit', () => {
    it('Should handle error get slider detail', async () => {
      axios.onGet(`${API_URL.sliders}/1`).reply(500, GENERAL_ERROR_RESPONSE);
      renderWithRoute({ route });

      await user.click(await screen.findByText(firstSliderTitle));
      expect(
        await screen.findByRole('button', { name: addSliderButton }),
      ).toBeInTheDocument();
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'click_entry',
        action_details: '1',
      });
    });

    it('Should edit slider correctly', async () => {
      renderWithRoute({ route });

      await user.click(await screen.findByText(firstSliderTitle));
      expect(screen.getByText(editSliderTitle)).toBeInTheDocument();
      expect(screen.getByText(tabDescription)).toBeInTheDocument();
      expect(await screen.findByText(uploadImageLabel)).toBeInTheDocument();
      expect(screen.getByLabelText(sliderTitleLabel)).toHaveValue(
        firstSliderTitle,
      );
      expect(screen.getByLabelText('Deskripsi')).toHaveValue('desc 1');
      expect(screen.getByLabelText('Tombol URL')).toHaveValue(
        'https://www.bukalapak.com',
      );
      expect(screen.getByLabelText('Teks di Tombol')).toHaveValue('cta 1');

      const saveButton = screen.getByRole('button', { name: 'Simpan' });
      expect(saveButton).toBeDisabled();
      await user.type(screen.getByLabelText(sliderTitleLabel), 'test');
      expect(saveButton).toBeEnabled();

      await user.click(saveButton);
      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(
          GENERAL_SUCCESS_RESPONSE.message,
        ),
      );
      expect(
        getLastCalledTimes(axios.history.put, `${API_URL.sliders}/1`),
      ).toEqual(1);
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'edit_entry',
        action_details: '1',
      });
    });

    it('Should delete image with confirmation pop up', async () => {
      renderWithRoute({ route });

      await user.click(await screen.findByText(firstSliderTitle));
      expect(await screen.findByText(uploadImageLabel)).toBeInTheDocument();
      await user.click(screen.getByTestId('uploader-remove-button'));
      expect(await screen.findByText(deleteImageTitle)).toBeInTheDocument();
      await user.click(
        screen.getByRole('button', { name: deleteConfirmationButton }),
      );
      await waitFor(() =>
        expect(screen.getByRole('button', { name: 'Simpan' })).toBeDisabled(),
      );
    });

    it('Should handle error correctly', async () => {
      axios.onPut(`${API_URL.sliders}/1`).reply(500, GENERAL_ERROR_RESPONSE);
      renderWithRoute({ route });

      await user.click(await screen.findByText(firstSliderTitle));
      expect(await screen.findByText(uploadImageLabel)).toBeInTheDocument();
      await user.type(screen.getByLabelText(sliderTitleLabel), 'test');
      await user.click(screen.getByRole('button', { name: 'Simpan' }));
      await waitFor(() =>
        expect(toast.danger).toHaveBeenLastCalledWith(
          GENERAL_ERROR_RESPONSE.errors[0].message,
        ),
      );
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'error',
        action_details: GENERAL_ERROR_RESPONSE.errors[0].message,
      });
    });
  });

  describe('Create', () => {
    it('Should perform create slider correctly', async () => {
      renderWithRoute({ route });

      await user.click(
        await screen.findByRole('button', { name: addSliderButton }),
      );
      expect(
        await screen.findByRole('button', { name: 'Tambah' }),
      ).toBeInTheDocument();

      const uploaderInput = screen.getByTestId('uploader-input');
      Object.defineProperty(uploaderInput, 'files', {
        value: [file],
      });
      fireEvent.drop(uploaderInput);
      await user.type(screen.getByLabelText(sliderTitleLabel), 'test');

      const addButton = screen.getByRole('button', { name: 'Tambah' });
      await waitFor(() => expect(addButton).toBeEnabled());
      await user.click(addButton);

      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(
          GENERAL_SUCCESS_RESPONSE.message,
        ),
      );
      expect(getLastCalledTimes(axios.history.post, API_URL.sliders)).toEqual(
        1,
      );
      expect(
        await screen.findByRole('button', { name: addSliderButton }),
      ).toBeInTheDocument();
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'add_entry',
      });
    });

    it('Should handle error when create slider', async () => {
      axios.onPost(API_URL.sliders).reply(500, GENERAL_ERROR_RESPONSE);
      renderWithRoute({ route });

      await user.click(
        await screen.findByRole('button', { name: addSliderButton }),
      );

      const uploaderInput = screen.getByTestId('uploader-input');
      Object.defineProperty(uploaderInput, 'files', {
        value: [file],
      });
      fireEvent.drop(uploaderInput);

      const addButton = screen.getByRole('button', { name: 'Tambah' });
      await waitFor(() => expect(addButton).toBeEnabled());
      await user.click(addButton);

      await waitFor(() =>
        expect(toast.danger).toHaveBeenLastCalledWith(
          GENERAL_ERROR_RESPONSE.errors[0].message,
        ),
      );
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'error',
        action_details: GENERAL_ERROR_RESPONSE.errors[0].message,
      });
    });
  });
});
