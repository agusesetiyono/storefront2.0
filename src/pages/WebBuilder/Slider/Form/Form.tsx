import React from 'react';
import { useFormik } from 'formik';
import { useNavigate, useParams } from 'react-router-dom';
import { Form } from 'react-bootstrap';

import useWebBuilder from '@hooks/useWebBuilder';
import { usePrompt } from '@hooks/usePrompt';
import {
  useCreateSlider,
  useGetSlider,
  useUpdateSlider,
} from '@hooks/api/webBuilder/slider';
import { onSuccessMutate, onErrorMutate } from '@utils/reactQuery';
import SideMenuWebBuilder from '@components/WebBuilder/SideMenu';
import Uploader from '@components/Uploader';
import Button from '@components/Button';
import { sliderValidation } from '../slider.validation';

import type { ISlider, IResponseSlider } from '@interfaces/sliders';
import {
  sendWebBuilderTracker,
  WebBuilderAction,
} from '@utils/tracker/webBuilder';
import { getErrorMessage } from '@utils/error';

const SliderForm = () => {
  const SLIDERS_PAGE_URL = '/settings/custom-website/builder/section/slider';
  const params = useParams();
  const sliderId = params.id;
  const navigate = useNavigate();
  const createSlider = useCreateSlider();
  const updateSlider = useUpdateSlider(sliderId);
  const { refreshPreview } = useWebBuilder();

  const {
    values,
    dirty,
    isValid,
    isSubmitting,
    handleChange,
    handleSubmit,
    setFieldValue,
    setSubmitting,
    resetForm,
  } = useFormik<ISlider>({
    initialValues: {
      image: '',
      title: '',
      desc: '',
      url: '',
      cta: '',
    },
    validationSchema: sliderValidation,
    onSubmit: ({ image_file, title, desc, url, cta }) => {
      const formData = new FormData();
      image_file && formData.append('image', image_file);
      formData.append('title', title);
      formData.append('desc', desc);
      formData.append('url', url);
      formData.append('cta', cta);

      (sliderId ? updateSlider : createSlider).mutate(formData, {
        onSuccess: (data) => {
          onSuccessMutate(data);
          refreshPreview();
          void resetForm({ values });
          void sendWebBuilderTracker(
            sliderId ? WebBuilderAction.EditEntry : WebBuilderAction.AddEntry,
            sliderId,
          );
          sliderId ? setSubmitting(false) : navigate(SLIDERS_PAGE_URL);
        },
        onError: (error) => {
          onErrorMutate(error);
          setSubmitting(false);
          void sendWebBuilderTracker(
            WebBuilderAction.Error,
            getErrorMessage(error),
          );
        },
      });
    },
  });

  usePrompt(dirty && !isSubmitting);

  let isLoadingPage = false;
  if (sliderId) {
    const { isFetching } = useGetSlider(
      sliderId,
      (res: IResponseSlider) => {
        if (!res) return;

        const {
          data: { image, title, desc, url, cta },
        } = res;
        void resetForm({ values: { image, title, desc, url, cta } });
      },
      () => {
        navigate(SLIDERS_PAGE_URL);
      },
    );
    isLoadingPage = isFetching;
  }

  return (
    <SideMenuWebBuilder
      header={{
        title: `${sliderId ? 'Edit' : 'Tambah'} Slider`,
        subtitle:
          'Dengan Slider, kamu bisa menampilkan beberapa banner secara bergantian.',
        backUrl: '/settings/custom-website/builder/section/slider',
      }}
      footer={{
        action: (
          <Button
            className="w-full"
            onClick={() => handleSubmit()}
            disabled={!isValid || !dirty}
            isLoading={isSubmitting}
            spinnerClass="my-1"
          >
            {sliderId ? 'Simpan' : 'Tambah'}
          </Button>
        ),
      }}
      isFetching={isLoadingPage}
    >
      <Form>
        <Form.Group className="mb-6">
          <Form.Label required-label="true" className="text-sm">
            Upload Gambar Slider
          </Form.Label>
          <Uploader
            maxFiles={1}
            accept=".jpg, .jpeg, .png"
            description="Format gambar JPG, JEPG, atau PNG dengan ukuran optimal 328x160 pixel."
            modalDeleteTitle="Hapus gambar?"
            modalDeleteContent="Kamu akan menghapus gambar pada <strong>Slider</strong>."
            initialPreview={values.image}
            onChange={async (files: File[]) => {
              await setFieldValue('image', '');
              await setFieldValue('image_file', files[0]);
            }}
          />
        </Form.Group>
        <Form.Group className="mb-6">
          <Form.Label
            htmlFor="title"
            required-label="false"
            className="text-sm"
          >
            Headline pada Slider
          </Form.Label>
          <Form.Control
            id="title"
            name="title"
            size="sm"
            value={values.title}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group className="mb-6">
          <Form.Label htmlFor="desc" required-label="false" className="text-sm">
            Deskripsi
          </Form.Label>
          <Form.Control
            id="desc"
            as="textarea"
            rows={5}
            name="desc"
            value={values.desc}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group className="mb-6">
          <Form.Label htmlFor="url" required-label="false" className="text-sm">
            Tombol URL
          </Form.Label>
          <Form.Control
            id="url"
            name="url"
            size="sm"
            value={values.url}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label htmlFor="cta" required-label="false" className="text-sm">
            Teks di Tombol
          </Form.Label>
          <Form.Control
            id="cta"
            name="cta"
            size="sm"
            value={values.cta}
            onChange={handleChange}
          />
        </Form.Group>
      </Form>
    </SideMenuWebBuilder>
  );
};

export default SliderForm;
