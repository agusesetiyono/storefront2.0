import React from 'react';
import { NavLink } from 'react-router-dom';

import {
  useGetSliders,
  useReorderSliders,
  useDeleteSlider,
} from '@hooks/api/webBuilder/slider';
import useDeleteItem from '@hooks/useDeleteItem';
import useUpdateBulk from '@hooks/useUpdateBulk';
import useWebBuilder from '@hooks/useWebBuilder';

import SideMenuWebBuilder from '@components/WebBuilder/SideMenu';
import SideMenuGroup from '@components/WebBuilder/SideMenu/partials/SideMenuGroup';
import Button from '@components/Button';

import type { ISideMenuItem } from '@components/WebBuilder/SideMenu/sideMenuWebBuilder.types';
import {
  sendWebBuilderTracker,
  WebBuilderAction,
} from '@utils/tracker/webBuilder';
import { getErrorMessage } from '@utils/error';

const WebBuilderSlider = () => {
  const { refreshPreview } = useWebBuilder();

  const { sideMenu, setSideMenu, isError, isFetching, refetchSliders } =
    useGetSliders();

  const { isConfirmDelete, onDelete, ModalDeleteComponent } = useDeleteItem({
    refetch: refetchSliders,
    deleteItem: useDeleteSlider,
    onRefetch: refreshPreview,
    modalTitle: 'Hapus Slider?',
    modalContent: 'Kamu akan menghapus <strong>Slider</strong>.',
    afterSuccess: (id) =>
      void sendWebBuilderTracker(WebBuilderAction.DeleteEntry, `${id}`),
    afterError: (error) =>
      void sendWebBuilderTracker(
        WebBuilderAction.Error,
        getErrorMessage(error),
      ),
  });

  const SLIDER_MENU_GROUP_ID = 0;
  const sliders = sideMenu[SLIDER_MENU_GROUP_ID].menus;
  const { startUpdate } = useUpdateBulk({
    data: sliders,
    isCancelDelay: isConfirmDelete,
    refetch: refetchSliders,
    onSuccess: refreshPreview,
    onRefetch: refreshPreview,
    onError: (error) =>
      void sendWebBuilderTracker(
        WebBuilderAction.Error,
        getErrorMessage(error),
      ),
    updateAction: () => useReorderSliders(sliders.map((menu) => menu.id)),
  });

  const onSortChanged = (_, newSort: ISideMenuItem[]) => {
    const newSideMenu = sideMenu.slice();
    newSideMenu[SLIDER_MENU_GROUP_ID] = {
      ...newSideMenu[SLIDER_MENU_GROUP_ID],
      menus: newSort,
    };
    setSideMenu(newSideMenu);
    startUpdate();
  };

  return (
    <SideMenuWebBuilder
      header={{
        title: 'Slider',
        subtitle:
          'Dengan Slider, kamu bisa menampilkan beberapa banner secara bergantian.',
        backUrl: '/settings/custom-website/builder/section',
      }}
      action={
        <NavLink to="create">
          <Button className="w-full mb-8">Tambah Slider</Button>
        </NavLink>
      }
      isEmpty={!sliders.length}
      isError={isError}
      isFetching={isFetching}
      onRetry={refetchSliders}
    >
      <>
        {sideMenu.map((group, index) => (
          <SideMenuGroup
            {...group}
            groupIndex={index}
            key={`menu-group-${index}`}
            onChange={onSortChanged}
            onDelete={onDelete}
            onReorder={(itemIndex, menuItems) =>
              void sendWebBuilderTracker(
                WebBuilderAction.ReorderEntry,
                `${menuItems[itemIndex].id}`,
              )
            }
            onItemClick={(itemIndex, menuItems) =>
              void sendWebBuilderTracker(
                WebBuilderAction.ClickEntry,
                `${menuItems[itemIndex].id}`,
              )
            }
          />
        ))}
        <ModalDeleteComponent />
      </>
    </SideMenuWebBuilder>
  );
};

export default WebBuilderSlider;
