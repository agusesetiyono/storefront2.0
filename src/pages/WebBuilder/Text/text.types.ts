export interface IText {
  title?: string;
  initialContent: string;
  content?: string;
}
