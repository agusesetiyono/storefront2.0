import React from 'react';
import '@testing-library/jest-dom';
import axios from '@mocks/axios';
import {
  screen,
  waitFor,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';
import { renderWithRoute, API_URL, getLastCalled } from '@utils/test-helper';
import toast from '@utils/toast';
import { sendTracker } from '@utils/tracker';

// API
import { GENERAL_ERROR_RESPONSE, GENERAL_SUCCESS_RESPONSE } from '@mocks/api';
import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';
import {
  GET_TEXT_EMPTY_RESPONSE,
  GET_TEXT_SUCCESS_RESPONSE,
} from '@mocks/api/text';

jest.mock('@components/Trumbowyg', () => {
  return {
    __esModule: true,
    default: (props) => <textarea {...props} />,
  };
});

jest.mock('@utils/toast', () => ({
  success: jest.fn(),
  danger: jest.fn(),
}));

axios.onGet(`${API_URL.storefronts}/status`).reply(
  200,
  getMockSubscriptionsResponse({
    type: 'platinum',
    expiredDate: '2027-05-01T00:00:00+07:00',
  }),
);

let user: UserEvent;

beforeEach(() => {
  user = userEvent.setup({ delay: null });
  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));
  localStorage.setItem('onboardingStep', '5'); // already finished onboarding

  axios.onGet(`${API_URL.text}/1`).reply(200, GET_TEXT_EMPTY_RESPONSE);
});

afterEach(() => {
  jest.useRealTimers();
  jest.clearAllMocks();
  localStorage.clear();
});

describe('Web Builder Text', () => {
  const route =
    '/storefront-editor/settings/custom-website/builder/section/text/1';
  const tabTitle = 'Teks';
  const titlePlaceholder = 'Contoh: Diskon hingga 70%';
  const contentLabel = 'Isi Konten';
  const saveButtonText = 'Simpan';
  const promptConfirmationMessage =
    'Kamu akan keluar halaman ini tanpa menyimpan perubahan. Apa kamu yakin?';
  const defaultTrackerParams = {
    evn: 'ngorder_sf_builder_actions',
    active_panel: 'section_text',
  };

  it('Should match snapshot', async () => {
    const { container } = renderWithRoute({ route });

    expect(await screen.findByText(tabTitle)).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });

  it('Should disable "Simpan" button when form is not valid', async () => {
    renderWithRoute({ route });

    expect(await screen.findByText(tabTitle)).toBeInTheDocument();
    const titleInput = screen.getByPlaceholderText(titlePlaceholder);
    expect(titleInput).toHaveValue('');
    const contentInput = screen.getByLabelText(contentLabel);
    expect(contentInput).toHaveValue('');
    const saveButton = screen.getByRole('button', { name: saveButtonText });
    expect(saveButton).toBeDisabled();

    await user.type(titleInput, 'title');
    expect(saveButton).toBeDisabled();

    await user.type(contentInput, 'content');
    expect(saveButton).toBeEnabled();
  });

  it('Should handle error when save text', async () => {
    axios.onPut(API_URL.text).reply(500, GENERAL_ERROR_RESPONSE);
    renderWithRoute({ route });

    expect(await screen.findByText(tabTitle)).toBeInTheDocument();
    await user.type(screen.getByPlaceholderText(titlePlaceholder), 'title');
    await user.type(screen.getByLabelText(contentLabel), 'content');
    const saveButton = screen.getByRole('button', { name: saveButtonText });
    await user.click(saveButton);
    await waitFor(() =>
      expect(toast.danger).toHaveBeenLastCalledWith(
        GENERAL_ERROR_RESPONSE.errors[0].message,
      ),
    );
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'error',
      action_details: GENERAL_ERROR_RESPONSE.errors[0].message,
    });
    expect(saveButton).toBeEnabled();
  });

  it('Should handle success when save text', async () => {
    axios.onPut(API_URL.text).reply(200, GENERAL_SUCCESS_RESPONSE);
    renderWithRoute({ route });

    expect(await screen.findByText(tabTitle)).toBeInTheDocument();
    await user.type(screen.getByPlaceholderText(titlePlaceholder), 'title');
    await user.type(screen.getByLabelText(contentLabel), 'content');
    const saveButton = screen.getByRole('button', { name: saveButtonText });
    await user.click(saveButton);
    await waitFor(() =>
      expect(toast.success).toHaveBeenLastCalledWith(
        GENERAL_SUCCESS_RESPONSE.message,
      ),
    );
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'edit_setting',
    });
    expect(saveButton).toBeDisabled();
    expect(getLastCalled(axios, 'put').data).toEqual(
      '{"title":"title","content":"content","id":1}',
    );
  });

  it('Should show previous data', async () => {
    axios.onGet(`${API_URL.text}/1`).reply(200, GET_TEXT_SUCCESS_RESPONSE);
    renderWithRoute({ route });

    expect(await screen.findByText(tabTitle)).toBeInTheDocument();
    expect(screen.getByPlaceholderText(titlePlaceholder)).toHaveValue(
      'Sample title',
    );
    expect(screen.getByLabelText(contentLabel)).toHaveAttribute(
      'data',
      '<p>Sample page</p>',
    );
  });

  it('Should show error state', async () => {
    const errorTitle = 'Gagal mengambil data Teks';
    axios.onGet(`${API_URL.text}/1`).reply(500, GENERAL_ERROR_RESPONSE);
    renderWithRoute({ route });

    expect(await screen.findByText(errorTitle)).toBeInTheDocument();
    axios.onGet(`${API_URL.text}/1`).reply(200, GET_TEXT_EMPTY_RESPONSE);
    await user.click(screen.getByRole('button', { name: 'Coba Lagi' }));
    expect(
      await screen.findByPlaceholderText(titlePlaceholder),
    ).toBeInTheDocument();
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'error',
      action_details: errorTitle,
    });
  });

  it('Should show block navigation pop up', async () => {
    renderWithRoute({ route });

    expect(await screen.findByText(tabTitle)).toBeInTheDocument();
    await user.type(screen.getByPlaceholderText(titlePlaceholder), 'title');

    const backButton = screen.getByTestId('back-button');
    await user.click(backButton);
    expect(
      await screen.findByText(promptConfirmationMessage),
    ).toBeInTheDocument();
    await user.click(screen.getByRole('button', { name: 'Batal' }));
    await waitForElementToBeRemoved(() =>
      screen.getByText(promptConfirmationMessage),
    );
    expect(await screen.findByText(tabTitle)).toBeInTheDocument();

    await user.click(backButton);
    await user.click(backButton);
    expect(
      await screen.findByText(promptConfirmationMessage),
    ).toBeInTheDocument();
    await user.click(screen.getByRole('button', { name: 'Ya, keluar' }));
    expect(await screen.findByText('Custom Section')).toBeInTheDocument();
  });
});
