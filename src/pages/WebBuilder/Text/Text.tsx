import React from 'react';
import { useParams } from 'react-router-dom';
import { useFormik } from 'formik';
import { Form } from 'react-bootstrap';

import { onSuccessMutate, onErrorMutate } from '@utils/reactQuery';
import { usePrompt } from '@hooks/usePrompt';
import { useGetText, useUpdateText } from '@hooks/api/webBuilder/section';
import useWebBuilder from '@hooks/useWebBuilder';

import Button from '@components/Button';
import Trumbowyg from '@components/Trumbowyg';
import SideMenuWebBuilder from '@components/WebBuilder/SideMenu';

import { IText } from '@interfaces/sections';
import { textValidation } from './text.validation';

import './custom-trumbowyg.scss';
import {
  WebBuilderAction,
  sendWebBuilderTracker,
} from '@utils/tracker/webBuilder';
import { getErrorMessage } from '@utils/error';

const TextForm = () => {
  const params = useParams();
  const textId = params.id;
  const updateText = useUpdateText();
  const { refreshPreview } = useWebBuilder();

  const {
    values: { title, initialContent },
    dirty,
    isValid,
    handleChange,
    handleSubmit,
    resetForm,
    isSubmitting,
    setSubmitting,
  } = useFormik<IText>({
    initialValues: {
      title: '',
      initialContent: '',
      content: '',
    },
    validationSchema: textValidation,
    onSubmit: (values) => {
      updateText.mutate(
        {
          title: values.title,
          content: values.content,
          id: Number(textId),
        },
        {
          onSuccess: (data) => {
            onSuccessMutate(data);
            refreshPreview();
            void resetForm({ values });
            setSubmitting(false);
            void sendWebBuilderTracker(WebBuilderAction.EditSetting);
          },
          onError: (err) => {
            onErrorMutate(err);
            setSubmitting(false);
            void sendWebBuilderTracker(
              WebBuilderAction.Error,
              getErrorMessage(err),
            );
          },
        },
      );
    },
  });

  usePrompt(dirty);

  const { isFetching, isError, refetch } = useGetText(textId, (res) => {
    if (!res) return;
    const { title, content } = res.data;
    resetForm({
      values: {
        title,
        initialContent: content,
        content,
      },
    });
  });

  const toolbarButton = [
    ['bold', 'italic', 'underline', 'viewHTML'],
    ['unorderedList', 'orderedList'],
    ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
    ['insertImage', 'noembed', 'link'],
  ];

  return (
    <SideMenuWebBuilder
      header={{
        title: 'Teks',
        subtitle:
          'Komunikasikan jualanmu dengan kata-kata menarik sekreatif mungkin.',
        backUrl: '/settings/custom-website/builder/section',
      }}
      footer={{
        action: (
          <Button
            className="w-full"
            onClick={() => handleSubmit()}
            disabled={!isValid || !dirty}
            isLoading={isSubmitting}
            spinnerClass="my-1"
          >
            Simpan
          </Button>
        ),
      }}
      isFetching={isFetching}
      isError={isError}
      onRetry={() => void refetch()}
    >
      <>
        <Form>
          <Form.Group className="mb-6">
            <Form.Label
              htmlFor="title"
              className="text-sm"
              required-label="true"
            >
              Judul
            </Form.Label>
            <Form.Control
              id="title"
              name="title"
              value={title}
              onChange={handleChange}
              placeholder="Contoh: Diskon hingga 70%"
            />
          </Form.Group>
          <Form.Group className="mb-6">
            <Form.Label
              htmlFor="content"
              className="text-sm"
              required-label="true"
            >
              Isi Konten
            </Form.Label>
            <Trumbowyg
              buttons={toolbarButton}
              id="content"
              name="content"
              data={initialContent || ''}
              onChange={handleChange}
              tagsToRemove={[]}
            />
          </Form.Group>
        </Form>
      </>
    </SideMenuWebBuilder>
  );
};

export default TextForm;
