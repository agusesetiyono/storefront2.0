import * as Yup from 'yup';

export const textValidation = Yup.object().shape({
  title: Yup.string().required(),
  content: Yup.string().required(),
});
