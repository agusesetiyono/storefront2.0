import React from 'react';
import { useFormik } from 'formik';
import { Form } from 'react-bootstrap';

import { useGetTheme, useUpdateColors } from '@hooks/api/themes';
import useWebBuilder from '@hooks/useWebBuilder';
import { onSuccessMutate, onErrorMutate } from '@utils/reactQuery';
import SideMenuWebBuilder from '@components/WebBuilder/SideMenu';
import Button from '@components/Button';

import { IColor } from './color.types';
import type { ISelectedThemeResponseStatus } from '@interfaces/themes';
import SelectColor from '@components/SelectColor';
import {
  sendWebBuilderTracker,
  WebBuilderAction,
} from '@utils/tracker/webBuilder';
import { getErrorMessage } from '@utils/error';

const ColorForm = () => {
  const defaultColor = '#000000';

  const {
    values,
    dirty,
    isValid,
    setSubmitting,
    isSubmitting,
    handleSubmit,
    setFieldValue,
    resetForm,
  } = useFormik<IColor>({
    initialValues: {
      accent_color: defaultColor,
      body_bg_color: defaultColor,
      footer_bg_color: defaultColor,
      header_bg_color: defaultColor,
      main_color: defaultColor,
      navbar_bg_color: defaultColor,
      widget_chat_color: defaultColor,
      container_bg_color: defaultColor,
      defaults: {
        accent_color: defaultColor,
        body_bg_color: defaultColor,
        footer_bg_color: defaultColor,
        header_bg_color: defaultColor,
        main_color: defaultColor,
        navbar_bg_color: defaultColor,
        widget_chat_color: defaultColor,
        container_bg_color: defaultColor,
      },
    },
    onSubmit: (values, { resetForm }) => {
      const reset = () => resetForm({ values });

      const {
        accent_color,
        body_bg_color,
        footer_bg_color,
        header_bg_color,
        main_color,
        navbar_bg_color,
        widget_chat_color,
      } = values;
      updateColor.mutate(
        {
          accent_color,
          body_bg_color,
          footer_bg_color,
          header_bg_color,
          main_color,
          navbar_bg_color,
          widget_chat_color,
        },
        {
          onSuccess: (data) => {
            reset();
            onSuccessMutate(data);
            refreshPreview();
            setSubmitting(false);
            void sendWebBuilderTracker(WebBuilderAction.EditSetting);
          },
          onError: (error) => {
            onErrorMutate(error);
            setSubmitting(false);
            void sendWebBuilderTracker(
              WebBuilderAction.Error,
              getErrorMessage(error),
            );
          },
        },
      );
    },
  });

  const { isFetching, isError, refetchSettingTheme } = useGetTheme(
    (res: ISelectedThemeResponseStatus) => {
      const {
        data: { color, default_color },
      } = res;
      void resetForm({
        values: {
          accent_color: color.accent_color || default_color.accent_color,
          body_bg_color: color.body_bg_color || default_color.body_bg_color,
          footer_bg_color:
            color.footer_bg_color || default_color.footer_bg_color,
          header_bg_color:
            color.header_bg_color || default_color.header_bg_color,
          main_color: color.main_color || default_color.main_color,
          navbar_bg_color:
            color.navbar_bg_color || default_color.navbar_bg_color,
          widget_chat_color:
            color.widget_chat_color || default_color.widget_chat_color,
          container_bg_color:
            color.container_bg_color || default_color.container_bg_color,
          defaults: {
            ...default_color,
          },
        },
      });
    },
  );

  const updateColor = useUpdateColors();

  const { refreshPreview } = useWebBuilder();

  return (
    <SideMenuWebBuilder
      header={{
        title: 'Edit Warna',
        subtitle: 'Ubah warna dari setiap aspek tokomu.',
        backUrl: '/settings/custom-website/builder/theme',
      }}
      footer={{
        action: (
          <Button
            className="w-full"
            onClick={() => handleSubmit()}
            disabled={!isValid || !dirty}
            isLoading={isSubmitting}
            spinnerClass="my-1"
          >
            Simpan
          </Button>
        ),
      }}
      isFetching={isFetching}
      isError={isError}
      onRetry={refetchSettingTheme}
    >
      <Form>
        <Form.Group className="mb-6">
          <Form.Label htmlFor="main_color" className="text-sm">
            Main Theme Color
          </Form.Label>
          <SelectColor
            name="main_color"
            value={values.main_color}
            onColorChange={(color) => void setFieldValue('main_color', color)}
            defaultColor={values.defaults.main_color}
          />
        </Form.Group>
        <Form.Group className="mb-6">
          <Form.Label htmlFor="accent_color" className="text-sm">
            Accent Theme Color
          </Form.Label>
          <SelectColor
            name="accent_color"
            value={values.accent_color}
            onColorChange={(color) => void setFieldValue('accent_color', color)}
            defaultColor={values.defaults.accent_color}
          />
        </Form.Group>
        <Form.Group className="mb-6">
          <Form.Label htmlFor="navbar_bg_color" className="text-sm">
            Navbar Background Color
          </Form.Label>
          <SelectColor
            name="navbar_bg_color"
            value={values.navbar_bg_color}
            onColorChange={(color) =>
              void setFieldValue('navbar_bg_color', color)
            }
            defaultColor={values.defaults.navbar_bg_color}
          />
        </Form.Group>
        <Form.Group className="mb-6">
          <Form.Label htmlFor="header_bg_color" className="text-sm">
            Header Background Color
          </Form.Label>
          <SelectColor
            name="header_bg_color"
            value={values.header_bg_color}
            onColorChange={(color) =>
              void setFieldValue('header_bg_color', color)
            }
            defaultColor={values.defaults.header_bg_color}
          />
        </Form.Group>
        <Form.Group className="mb-6">
          <Form.Label htmlFor="body_bg_color" className="text-sm">
            Body Background Color
          </Form.Label>
          <SelectColor
            name="body_bg_color"
            value={values.body_bg_color}
            onColorChange={(color) =>
              void setFieldValue('body_bg_color', color)
            }
            defaultColor={values.defaults.body_bg_color}
          />
        </Form.Group>
        <Form.Group className="mb-6">
          <Form.Label htmlFor="footer_bg_color" className="text-sm">
            Footer Background Color
          </Form.Label>
          <SelectColor
            name="footer_bg_color"
            value={values.footer_bg_color}
            onColorChange={(color) =>
              void setFieldValue('footer_bg_color', color)
            }
            defaultColor={values.defaults.footer_bg_color}
          />
        </Form.Group>
        <Form.Group className="mb-6">
          <Form.Label htmlFor="widget_chat_color" className="text-sm">
            Widget Color
          </Form.Label>
          <SelectColor
            name="widget_chat_color"
            value={values.widget_chat_color}
            onColorChange={(color) =>
              void setFieldValue('widget_chat_color', color)
            }
            defaultColor={values.defaults.widget_chat_color}
          />
        </Form.Group>
      </Form>
    </SideMenuWebBuilder>
  );
};

export default ColorForm;
