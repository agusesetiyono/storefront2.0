import '@testing-library/jest-dom';
import axios from '@mocks/axios';
import { screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';
import { renderWithRoute, API_URL } from '@utils/test-helper';
import toast from '@utils/toast';
import { sendTracker } from '@utils/tracker';

// API
import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';
import { getMockSelectedThemeResponse } from '@mocks/api/themes';

jest.mock('@utils/toast', () => ({
  success: jest.fn(),
  danger: jest.fn(),
}));

axios.onGet(`${API_URL.storefronts}/status`).reply(
  200,
  getMockSubscriptionsResponse({
    type: 'platinum',
    expiredDate: '2027-05-01T00:00:00+07:00',
  }),
);

let user: UserEvent;

beforeEach(() => {
  user = userEvent.setup({ delay: null });
  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));
  localStorage.setItem('onboardingStep', '5'); // already finished onboarding

  axios
    .onGet(`${API_URL.storefronts}/themes/me`)
    .reply(200, getMockSelectedThemeResponse(2));
});

afterEach(() => {
  jest.useRealTimers();
  jest.clearAllMocks();
  localStorage.clear();
});

describe('Web builder custom theme colors', () => {
  const route =
    '/storefront-editor/settings/custom-website/builder/theme/color';
  const tabTitle = 'Edit Warna';
  const tabDescription = 'Ubah warna dari setiap aspek tokomu.';
  const inputs = [
    {
      labelText: 'Main Theme Color',
      id: 'main_color',
    },
    {
      labelText: 'Accent Theme Color',
      id: 'accent_color',
    },
    {
      labelText: 'Navbar Background Color',
      id: 'navbar_bg_color',
    },
    {
      labelText: 'Header Background Color',
      id: 'header_bg_color',
    },
    {
      labelText: 'Body Background Color',
      id: 'body_bg_color',
    },
    {
      labelText: 'Footer Background Color',
      id: 'footer_bg_color',
    },
    {
      labelText: 'Widget Color',
      id: 'widget_chat_color',
    },
  ];
  const successMessage = 'Berhasil menyimpan perubahan.';
  const errorMessage = 'Gagal menyimpan perubahan. Silakan coba lagi.';
  const defaultTrackerParams = {
    evn: 'ngorder_sf_builder_actions',
    active_panel: 'theme_color',
  };

  describe('Config Theme Colors Form', () => {
    it('Renders properly with default configuration', async () => {
      const { container } = renderWithRoute({ route });

      expect(await screen.findByText(tabTitle)).toBeInTheDocument();
      expect(container).toMatchSnapshot();
    });

    it('Should render page with render default API state', async () => {
      renderWithRoute({ route });

      expect(await screen.findByText(tabTitle)).toBeInTheDocument();
      expect(screen.getByText(tabDescription)).toBeInTheDocument();

      inputs.forEach((input) => {
        const inputEl = screen.getByLabelText(input.labelText);
        expect(inputEl).toBeInTheDocument();
        expect(inputEl).toHaveValue('white');
      });

      const addButton = screen.getByRole('button', { name: 'Simpan' });
      await waitFor(() => expect(addButton).toBeDisabled());
    });

    it('Should show success toast when save', async () => {
      axios.onPatch(API_URL.themeColors).replyOnce(200, {
        message: successMessage,
      });

      renderWithRoute({ route });

      expect(await screen.findByText(tabTitle)).toBeInTheDocument();
      expect(screen.getByText(tabDescription)).toBeInTheDocument();

      const inputEl = screen.getByLabelText(inputs[0].labelText);
      await user.clear(inputEl);
      await user.type(inputEl, '#ffffff');
      expect(inputEl).toHaveValue('#ffffff');

      const addButton = screen.getByRole('button', { name: 'Simpan' });
      await waitFor(() => expect(addButton).toBeEnabled());
      await user.click(addButton);

      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(successMessage),
      );
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'edit_setting',
      });
    });

    it('Should show error toast when save', async () => {
      axios.onPatch(API_URL.themeColors).replyOnce(500, {
        errors: [{ message: errorMessage }],
        meta: { http_status: 500 },
      });

      renderWithRoute({ route });

      await user.clear(await screen.findByLabelText(inputs[0].labelText));
      await user.type(
        await screen.findByLabelText(inputs[0].labelText),
        '#ffffff',
      );

      const addButton = screen.getByRole('button', { name: 'Simpan' });
      await waitFor(() => expect(addButton).toBeEnabled());
      await user.click(addButton);

      await waitFor(() =>
        expect(toast.danger).toHaveBeenLastCalledWith(errorMessage),
      );
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'error',
        action_details: errorMessage,
      });
    });

    it('Click default to change to default color', async () => {
      renderWithRoute({ route });

      expect(
        await screen.findByLabelText(inputs[0].labelText),
      ).toBeInTheDocument();
      expect(screen.getByLabelText(inputs[0].labelText)).toHaveValue('white');
      await user.click(screen.getAllByRole('button', { name: 'Default' })[0]);
      expect(screen.getByLabelText(inputs[0].labelText)).toHaveValue('green');
    });

    it('Should set to default color if has no configuration', async () => {
      axios
        .onGet(`${API_URL.storefronts}/themes/me`)
        .reply(200, getMockSelectedThemeResponse(2, true));

      renderWithRoute({ route });

      expect(await screen.findByText(tabTitle)).toBeInTheDocument();
      expect(screen.getByText(tabDescription)).toBeInTheDocument();

      inputs.forEach((input) => {
        const inputEl = screen.getByLabelText(input.labelText);
        expect(inputEl).toBeInTheDocument();
        expect(inputEl).toHaveValue('green');
      });
    });
  });
});
