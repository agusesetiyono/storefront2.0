/* eslint-disable jest/no-conditional-expect */
import '@testing-library/jest-dom';
import axios from '@mocks/axios';
import { screen, waitFor, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';
import {
  renderWithRoute,
  API_URL,
  getLastCalled,
  getLastCalledTimes,
} from '@utils/test-helper';
import toast from '@utils/toast';
import { sendTracker } from '@utils/tracker';

// API
import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';
import { GENERAL_SUCCESS_RESPONSE, GENERAL_ERROR_RESPONSE } from '@mocks/api';
import { GET_SECTIONS_SUCCESS_RESPONSE } from '@mocks/api/section';

jest.mock('@utils/toast', () => ({
  success: jest.fn(),
  danger: jest.fn(),
}));

axios.onGet(`${API_URL.storefronts}/status`).reply(
  200,
  getMockSubscriptionsResponse({
    type: 'platinum',
    expiredDate: '2027-05-01T00:00:00+07:00',
  }),
);

let user: UserEvent;

beforeEach(() => {
  user = userEvent.setup({ delay: null });
  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));
  localStorage.setItem('onboardingStep', '5'); // already finished onboarding

  axios.onGet(API_URL.sections).reply(200, GET_SECTIONS_SUCCESS_RESPONSE);
});

afterEach(() => {
  jest.useRealTimers();
  jest.clearAllMocks();
  localStorage.clear();
  axios.history.get = [];
  axios.history.put = [];
});

describe('Web builder section', () => {
  const route = '/storefront-editor/settings/custom-website/builder/section';
  const pageDescription =
    'Tambah section atau bagian pada tampilan halaman utama.';

  const menuGroup = [
    { title: 'Bagian Atas', menus: ['Header'] },
    {
      title: 'Komponen',
      menus: [
        'Banner',
        'Slider',
        'Kategori',
        'Semua Produk',
        'Produk Populer',
        'Terlaris',
        'Custom Product List',
        'Teks',
      ],
    },
    { title: 'Bagian Bawah', menus: ['Footer'] },
  ];

  const defaultTrackerParams = {
    evn: 'ngorder_sf_builder_actions',
    active_panel: 'customize_section',
  };

  it('Should match snapshot', async () => {
    const { container } = renderWithRoute({ route });

    expect(await screen.findByText('Custom Section')).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });

  const assertMenuGroup = async () => {
    let groupIndex = 0;

    for (const group of menuGroup) {
      const sideMenuGroup = await screen.findByTestId(
        `side-menu-group-${groupIndex}`,
      );
      expect(within(sideMenuGroup).getByText(group.title)).toBeInTheDocument();

      group.menus.forEach((menu, itemIndex) => {
        const sideMenuItem = screen.getByTestId(
          `side-menu-item-${groupIndex}-${itemIndex}`,
        );
        expect(within(sideMenuItem).getByText(menu)).toBeInTheDocument();

        // check on section menu only
        if (groupIndex === 1) {
          expect(
            within(sideMenuItem).getByTestId(`sort-action-${itemIndex + 1}`),
          ).toBeInTheDocument();
          expect(
            within(sideMenuItem).getByTestId(`toggle-action-${itemIndex + 1}`),
          ).toBeInTheDocument();
        }
      });

      groupIndex += 1;
    }
  };

  describe('List', () => {
    it('Should render list correctly', async () => {
      renderWithRoute({ route });

      expect(await screen.findByText('Custom Section')).toBeInTheDocument();
      expect(screen.getByText(pageDescription)).toBeInTheDocument();
      await assertMenuGroup();
    });

    it('Should render error state correctly', async () => {
      axios.onGet(API_URL.sections).reply(500, {});
      renderWithRoute({ route });

      expect(await screen.findByText(pageDescription)).toBeInTheDocument();
      expect(
        screen.getByText('Gagal mengambil data Custom Section'),
      ).toBeInTheDocument();

      axios.onGet(API_URL.sections).reply(200, GET_SECTIONS_SUCCESS_RESPONSE);
      await user.click(screen.getByRole('button', { name: 'Coba Lagi' }));
      await assertMenuGroup();
    });
  });

  describe('Reorder', () => {
    beforeEach(() => {
      axios
        .onPut(`${API_URL.sections}/orders`)
        .reply(200, GENERAL_SUCCESS_RESPONSE);
    });

    const updateParams = {
      sections: [
        { id: 1, status: 'active' },
        { id: 3, status: 'inactive' },
        { id: 4, status: 'active' },
        { id: 5, status: 'inactive' },
        { id: 7, status: 'active' },
        { id: 8, status: 'active' },
        { id: 2, status: 'active' },
        { id: 6, status: 'active' },
      ],
    };

    const triggerReorder = async (isFinishedReorder = true) => {
      const drag1 = screen.getByTestId('sort-action-1');
      const drag2 = screen.getByTestId('sort-action-2');

      await user.pointer([
        { keys: '[MouseLeft>]', target: drag2 },
        { target: drag1 },
        { keys: '[/MouseLeft]' },
      ]);
      jest.runAllTimers();

      const drag5 = screen.getByTestId('sort-action-5');
      const drag6 = screen.getByTestId('sort-action-6');

      await user.pointer([
        { keys: '[MouseLeft>]', target: drag5 },
        { target: drag6 },
        { keys: '[/MouseLeft]' },
      ]);
      isFinishedReorder && jest.runAllTimers();
    };

    it('Should reorder sections correctly', async () => {
      renderWithRoute({ route });

      await assertMenuGroup();
      await triggerReorder();
      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(
          GENERAL_SUCCESS_RESPONSE.message,
        ),
      );
      expect(getLastCalled(axios, 'put').data).toEqual(
        JSON.stringify(updateParams),
      );
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'reorder_entry',
        action_details: 'Terlaris',
      });
    });

    it('Should reorder sections, even when user leave the page', async () => {
      renderWithRoute({ route });

      await assertMenuGroup();
      await triggerReorder(false);
      const menuTitle = menuGroup[1]?.menus[0];

      // get banner menu, exclude banner text on popover
      await user.click(screen.getAllByText(menuTitle)?.[0]);

      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(
          GENERAL_SUCCESS_RESPONSE.message,
        ),
      );
      expect(getLastCalled(axios, 'put').data).toEqual(
        JSON.stringify(updateParams),
      );
      await waitFor(() =>
        expect(screen.getByText(menuTitle)).toBeInTheDocument(),
      );
    });

    it('Should handle error when reorder', async () => {
      axios
        .onPut(`${API_URL.sections}/orders`)
        .reply(500, GENERAL_ERROR_RESPONSE);

      renderWithRoute({ route });

      await assertMenuGroup();
      await triggerReorder();
      await waitFor(() =>
        expect(toast.danger).toHaveBeenLastCalledWith(
          GENERAL_ERROR_RESPONSE.errors[0].message,
        ),
      );
      expect(getLastCalledTimes(axios.history.get, API_URL.sections)).toEqual(
        2,
      );
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'error',
        action_details: GENERAL_ERROR_RESPONSE.errors[0].message,
      });
    });
  });

  describe('Toggle', () => {
    beforeEach(() => {
      axios
        .onPut(`${API_URL.sections}/orders`)
        .reply(200, GENERAL_SUCCESS_RESPONSE);
    });

    const updateParams = {
      sections: [
        { id: 1, status: 'inactive' },
        { id: 2, status: 'inactive' },
        { id: 3, status: 'inactive' },
        { id: 4, status: 'active' },
        { id: 5, status: 'inactive' },
        { id: 6, status: 'active' },
        { id: 7, status: 'active' },
        { id: 8, status: 'active' },
      ],
    };

    const triggerToggle = async (isFinishedToggle = true) => {
      const toggle1 = screen.getByTestId('toggle-action-1');
      const toggle2 = screen.getByTestId('toggle-action-2');

      await user.click(toggle1);
      jest.runAllTimers();

      await user.click(toggle2);
      isFinishedToggle && jest.runAllTimers();
    };

    it('Should toggle section correctly', async () => {
      renderWithRoute({ route });

      await assertMenuGroup();
      await triggerToggle();
      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(
          GENERAL_SUCCESS_RESPONSE.message,
        ),
      );
      expect(getLastCalled(axios, 'put').data).toEqual(
        JSON.stringify(updateParams),
      );
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'click_toggle',
        action_details: 'Slider',
      });
    });

    it('Should toggle sections, even when user leave the page', async () => {
      renderWithRoute({ route });

      await assertMenuGroup();
      await triggerToggle(false);
      const menuTitle = menuGroup[1]?.menus[0];

      // get banner menu, exclude banner text on popover
      await user.click(screen.getAllByText(menuTitle)?.[0]);

      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(
          GENERAL_SUCCESS_RESPONSE.message,
        ),
      );
      expect(getLastCalled(axios, 'put').data).toEqual(
        JSON.stringify(updateParams),
      );
      await waitFor(() =>
        expect(screen.getByText(menuTitle)).toBeInTheDocument(),
      );
    });

    it('Should handle error when toggle', async () => {
      axios
        .onPut(`${API_URL.sections}/orders`)
        .reply(500, GENERAL_ERROR_RESPONSE);

      renderWithRoute({ route });

      await assertMenuGroup();
      await triggerToggle();
      await waitFor(() =>
        expect(toast.danger).toHaveBeenLastCalledWith(
          GENERAL_ERROR_RESPONSE.errors[0].message,
        ),
      );
      expect(getLastCalledTimes(axios.history.get, API_URL.sections)).toEqual(
        2,
      );
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'error',
        action_details: GENERAL_ERROR_RESPONSE.errors[0].message,
      });
    });
  });
});
