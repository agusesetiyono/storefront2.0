import React from 'react';

import {
  useGetSections,
  useUpdateSections,
} from '@hooks/api/webBuilder/section';
import useUpdateBulk from '@hooks/useUpdateBulk';
import useWebBuilder from '@hooks/useWebBuilder';

import type { ISideMenuItem } from '@components/WebBuilder/SideMenu/sideMenuWebBuilder.types';
import SideMenuWebBuilder from '@components/WebBuilder/SideMenu';
import SideMenuGroup from '@components/WebBuilder/SideMenu/partials/SideMenuGroup';
import {
  sendWebBuilderTracker,
  WebBuilderAction,
} from '@utils/tracker/webBuilder';
import { getErrorMessage } from '@utils/error';

const WebBuilderSection = () => {
  const { refreshPreview } = useWebBuilder();
  const { sideMenu, setSideMenu, isError, isFetching, refetchSections } =
    useGetSections();

  const SECTION_MENU_GROUP_ID = 1;
  const sections = sideMenu[SECTION_MENU_GROUP_ID]?.menus;
  const { startUpdate } = useUpdateBulk<ISideMenuItem>({
    data: sections,
    refetch: refetchSections,
    onSuccess: refreshPreview,
    onRefetch: refreshPreview,
    onError: (error) =>
      void sendWebBuilderTracker(
        WebBuilderAction.Error,
        getErrorMessage(error),
      ),
    updateAction: () =>
      useUpdateSections(
        sections.map((section) => ({
          id: section.id || 0,
          status: section.isActive ? 'active' : 'inactive',
        })),
      ),
  });

  const handleChangeMenu = (groupIndex: number, menuItems: ISideMenuItem[]) => {
    const updatedGroup = sideMenu.map((group, index) =>
      index === groupIndex ? { ...group, menus: menuItems } : group,
    );
    setSideMenu(updatedGroup);
    startUpdate();
  };

  return (
    <SideMenuWebBuilder
      header={{
        title: 'Custom Section',
        subtitle: 'Tambah section atau bagian pada tampilan halaman utama.',
        backUrl: '/settings/custom-website/builder',
      }}
      isEmpty={sideMenu.length <= 0}
      isError={isError}
      isFetching={isFetching}
      onRetry={refetchSections}
    >
      <>
        {sideMenu.map((group, index) => (
          <SideMenuGroup
            {...group}
            groupIndex={index}
            key={`menu-group-${index}`}
            onChange={handleChangeMenu}
            onReorder={(itemIndex, menuItems) =>
              void sendWebBuilderTracker(
                WebBuilderAction.ReorderEntry,
                menuItems[itemIndex].title,
              )
            }
            onToggle={(itemIndex, menuItems) =>
              void sendWebBuilderTracker(
                WebBuilderAction.ClickToggle,
                menuItems[itemIndex].title,
              )
            }
            onItemClick={(itemIndex, menuItems) =>
              void sendWebBuilderTracker(
                WebBuilderAction.ClickEntry,
                menuItems[itemIndex].title,
              )
            }
          />
        ))}
      </>
    </SideMenuWebBuilder>
  );
};

export default WebBuilderSection;
