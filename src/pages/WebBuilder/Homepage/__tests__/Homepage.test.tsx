import axios from '@mocks/axios';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';
import '@testing-library/jest-dom';
import { renderWithRoute, API_URL } from '@utils/test-helper';
import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';
import {
  getMockSelectedThemeResponse,
  GET_THEMES_SUCCESS_RESPONSE,
} from '@mocks/api/themes';
import { sendTracker } from '@utils/tracker';

jest.mock('@utils/toast', () => ({
  success: jest.fn(),
}));

axios.onGet(`${API_URL.storefronts}/status`).reply(
  200,
  getMockSubscriptionsResponse({
    type: 'platinum',
    expiredDate: '2022-05-01T00:00:00+07:00',
  }),
);

let user: UserEvent;
beforeEach(() => {
  user = userEvent.setup({ delay: null });

  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));

  localStorage.setItem('onboardingStep', '5'); // already finished onboarding
  axios
    .onGet(`${API_URL.storefronts}/themes/me`)
    .reply(200, getMockSelectedThemeResponse());
  axios
    .onGet(`${API_URL.storefronts}/themes`)
    .reply(200, GET_THEMES_SUCCESS_RESPONSE);
});

afterEach(() => {
  jest.useRealTimers();
  jest.clearAllMocks();
  localStorage.clear();
});

describe('Homepage Custom Website Builder Page', () => {
  const route = '/storefront-editor/settings/custom-website/builder';
  const defaultTrackerParams = {
    evn: 'ngorder_sf_builder_actions',
    active_panel: 'default',
  };
  it('Should match snapshot', async () => {
    const { container } = renderWithRoute({ route });

    expect(await screen.findByText('Custom Web')).toBeInTheDocument();
    container.querySelectorAll('[data-testid=dropdown-preview-screen]')[0].id =
      'dropdown-id';
    expect(container).toMatchSnapshot();
  });

  it('Should render sections correctly', async () => {
    renderWithRoute({ route });

    // Homepage website builder section
    expect(await screen.findByText('Custom Web')).toBeInTheDocument();
    expect(
      screen.getByText(
        'Atur tampilan web kamu dengan custom section dan tema.',
      ),
    ).toBeInTheDocument();
    expect(screen.getByTitle('Storefront Site')).toBeInTheDocument();

    // back to storefront dashboard section
    const backStorefrontDashboard = screen.getByText('Kembali ke Dashboard');
    expect(backStorefrontDashboard).toBeInTheDocument();
    expect(backStorefrontDashboard).toHaveAttribute(
      'href',
      '/storefront-editor/settings/custom-website',
    );

    // user click Preview screen
    await user.click(screen.getByTestId('dropdown-preview-screen'));
    await user.click(screen.getByTestId('desktop'));
    expect(screen.getByTitle('Storefront Site')).toBeInTheDocument();
    expect(screen.getByTestId('iframe-container')).toHaveClass('w-desktop');
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'visit',
    });
  });
});
