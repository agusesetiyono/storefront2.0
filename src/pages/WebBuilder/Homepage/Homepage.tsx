import React from 'react';

import type { ISideMenuGroup } from '@components/WebBuilder/SideMenu/sideMenuWebBuilder.types';
import SideMenuWebBuilder from '@components/WebBuilder/SideMenu';
import SideMenuGroup from '@components/WebBuilder/SideMenu/partials/SideMenuGroup';
import {
  sendWebBuilderTracker,
  WebBuilderAction,
} from '@utils/tracker/webBuilder';

const WebBuilderHomepage = () => {
  const menuGroup: ISideMenuGroup[] = [
    {
      menus: [
        {
          title: 'Custom Section',
          description: 'Kamu bisa mengorganisasi tampilan web dengan section.',
          url: 'section',
        },
        {
          title: 'Custom Tema',
          description: 'Kamu bisa mengubah icon, warna, dan font website',
          url: 'theme',
        },
      ],
    },
  ];

  return (
    <SideMenuWebBuilder
      header={{
        title: 'Custom Web',
        subtitle: 'Atur tampilan web kamu dengan custom section dan tema.',
      }}
    >
      <>
        {menuGroup.map((group, index) => (
          <SideMenuGroup
            {...group}
            groupIndex={index}
            key={`menu-group-${index}`}
            onItemClick={(itemIndex, menuItems) =>
              void sendWebBuilderTracker(
                WebBuilderAction.ClickEntry,
                menuItems[itemIndex].title,
              )
            }
          />
        ))}
      </>
    </SideMenuWebBuilder>
  );
};

export default WebBuilderHomepage;
