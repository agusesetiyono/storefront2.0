import * as Yup from 'yup';

export const headerValidation = Yup.object().shape({
  shop_name: Yup.string().required('Nama Toko wajib diisi'),
});
