import React from 'react';

import { useFormik } from 'formik';
import { Form } from 'react-bootstrap';
import { useBoolean } from 'usehooks-ts';

import SideMenuWebBuilder from '@components/WebBuilder/SideMenu';
import Uploader from '@components/Uploader';
import Button from '@components/Button';
import Callout from '@components/Callout';
import { headerValidation } from './header.validation';

import useWebBuilder from '@hooks/useWebBuilder';
import { usePrompt } from '@hooks/usePrompt';
import { useGetHeader, useUpdateHeader } from '@hooks/api/webBuilder/header';
import { onSuccessMutate, onErrorMutate } from '@utils/reactQuery';
import type { IHeader, IHeaderResponse } from '@interfaces/header';
import {
  sendWebBuilderTracker,
  WebBuilderAction,
} from '@utils/tracker/webBuilder';
import { getErrorMessage } from '@utils/error';

const Header = () => {
  const updateHeader = useUpdateHeader();
  const { refreshPreview } = useWebBuilder();
  const { value: initialImage, setTrue: setInitialImage } = useBoolean(false);
  const {
    value: isDeleteImage,
    setTrue: deleteImage,
    setFalse: resetImage,
  } = useBoolean(false);

  const {
    values,
    dirty,
    isValid,
    isSubmitting,
    handleChange,
    handleSubmit,
    setFieldValue,
    setSubmitting,
    resetForm,
  } = useFormik<IHeader>({
    initialValues: {
      shop_name: '',
      image: '',
    },
    validationSchema: headerValidation,
    onSubmit: ({ image_file, shop_name }) => {
      const formData = new FormData();
      image_file && formData.append('image', image_file);
      formData.append('shop_name', shop_name);
      if (isDeleteImage) {
        formData.append('delete_image', 'true');
        resetImage();
      }

      updateHeader.mutate(formData, {
        onSuccess: (data) => {
          onSuccessMutate(data);
          refreshPreview();
          void resetForm({ values });
          setSubmitting(false);
          image_file && setInitialImage();
          void sendWebBuilderTracker(WebBuilderAction.EditSetting);
        },
        onError: (error) => {
          onErrorMutate(error);
          setSubmitting(false);
          void sendWebBuilderTracker(
            WebBuilderAction.Error,
            getErrorMessage(error),
          );
        },
      });
    },
  });

  usePrompt(dirty && !isSubmitting);

  const { isFetching, isError, refetchHeader } = useGetHeader(
    (res: IHeaderResponse) => {
      if (!res) return;
      const {
        data: { logo, shop_name },
      } = res;
      setInitialImage();
      void resetForm({
        values: { logo, shop_name },
      });
    },
  );

  return (
    <SideMenuWebBuilder
      header={{
        title: `Header`,
        subtitle:
          'Header ada di posisi paling atas hompage kamu menggambarkan identitas atau profil tokomu.',
        backUrl: '/settings/custom-website/builder/section',
      }}
      footer={{
        action: (
          <Button
            className="w-full"
            onClick={() => handleSubmit()}
            disabled={!isValid || !dirty}
            isLoading={isSubmitting}
            spinnerClass="my-1"
          >
            Simpan
          </Button>
        ),
      }}
      isFetching={isFetching}
      isError={isError}
      onRetry={refetchHeader}
    >
      <Form>
        <Form.Group className="mb-8">
          <Form.Label
            htmlFor="shop_name"
            required-label="true"
            className="text-sm"
          >
            Nama Toko
          </Form.Label>
          <Form.Control
            id="shop_name"
            name="shop_name"
            value={values.shop_name}
            onChange={handleChange}
            placeholder="MyStore"
            disabled={isFetching}
          />
          <Callout
            text="Perubahan nama toko akan terlihat dalam maks. 2 jam."
            variant="info"
            isCenter
            className="mt-2"
          />
        </Form.Group>
        <Form.Group className="mb-6">
          <Form.Label required-label="false" className="text-sm">
            Upload Logo
          </Form.Label>
          <Uploader
            maxFiles={1}
            accept=".jpg, .jpeg, .png"
            description="Format logo JPG, JEPG, atau PNG dengan ukuran optimal 133x17 pixel."
            modalDeleteTitle="Hapus gambar?"
            modalDeleteContent="Kamu akan menghapus gambar pada <strong>Header</strong>."
            initialPreview={values.logo}
            // eslint-disable-next-line @typescript-eslint/no-misused-promises
            onChange={async (files: File[]) => {
              const file = files[0] || null;
              if (initialImage && !file) {
                deleteImage();
              }
              await setFieldValue('logo', '');
              await setFieldValue('image_file', file);
            }}
          />
          <Callout
            text="Logo akan menggantikan nama toko jika sudah di-upload."
            variant="info"
            isCenter
            className="mt-2"
          />
        </Form.Group>
      </Form>
    </SideMenuWebBuilder>
  );
};

export default Header;
