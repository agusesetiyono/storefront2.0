import '@testing-library/jest-dom';
import axios from '@mocks/axios';
import {
  screen,
  waitFor,
  fireEvent,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';
import { renderWithRoute, API_URL } from '@utils/test-helper';
import toast from '@utils/toast';
import { sendTracker } from '@utils/tracker';

// API
import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';
import { GET_HEADER_SUCCESS_RESPONSE } from '@mocks/api/header';

jest.mock('@utils/toast', () => ({
  success: jest.fn(),
  danger: jest.fn(),
}));

const file = new File(['hello'], 'hello.png', { type: 'image/png' });

axios.onGet(`${API_URL.storefronts}/status`).reply(
  200,
  getMockSubscriptionsResponse({
    type: 'platinum',
    expiredDate: '2027-05-01T00:00:00+07:00',
  }),
);

let user: UserEvent;

beforeEach(() => {
  user = userEvent.setup({ delay: null });
  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));
  localStorage.setItem('onboardingStep', '5'); // already finished onboarding

  axios.onGet(API_URL.header).reply(200, GET_HEADER_SUCCESS_RESPONSE);
});

afterEach(() => {
  jest.useRealTimers();
  jest.clearAllMocks();
  localStorage.clear();
});

describe('Web builder header', () => {
  const route =
    '/storefront-editor/settings/custom-website/builder/section/header';
  const tabTitle = 'Header';
  const tabDescription =
    'Header ada di posisi paling atas hompage kamu menggambarkan identitas atau profil tokomu.';
  const shopName = 'Nama Toko';
  const uploadLogo = 'Upload Logo';
  const infoLogo = 'Logo akan menggantikan nama toko jika sudah di-upload.';
  const infoShopName = 'Perubahan nama toko akan terlihat dalam maks. 2 jam.';
  const captionUpload =
    'Format logo JPG, JEPG, atau PNG dengan ukuran optimal 133x17 pixel.';
  const successMessage = 'Berhasil menyimpan perubahan.';
  const errorMessage = 'Gagal menyimpan perubahan. Silakan coba lagi.';
  const promptConfirmationMessage =
    'Kamu akan keluar halaman ini tanpa menyimpan perubahan. Apa kamu yakin?';
  const deleteImageTitle = 'Hapus gambar?';
  const defaultTrackerParams = {
    evn: 'ngorder_sf_builder_actions',
    active_panel: 'section_header',
  };

  describe('Header Form', () => {
    it('Renders properly with default configuration', async () => {
      const { container } = renderWithRoute({ route });

      expect(await screen.findByText(tabTitle)).toBeInTheDocument();
      expect(container).toMatchSnapshot();
    });

    it('Should render page with render default API state', async () => {
      renderWithRoute({ route });

      expect(await screen.findByText(tabTitle)).toBeInTheDocument();
      expect(screen.getByText(tabDescription)).toBeInTheDocument();

      const shopNameInput = await screen.findByLabelText(shopName);
      expect(shopNameInput).toBeInTheDocument();
      expect(shopNameInput).toHaveValue('Toko Smart');
      expect(screen.getByText(infoShopName)).toBeInTheDocument();

      expect(screen.getByText(uploadLogo)).toBeInTheDocument();
      expect(screen.getByText(infoLogo)).toBeInTheDocument();
      expect(screen.getByText(captionUpload)).toBeInTheDocument();

      const addButton = screen.getByRole('button', { name: 'Simpan' });
      await waitFor(() => expect(addButton).toBeDisabled());
    });

    it('Should show success toast when save', async () => {
      axios.onPatch(API_URL.header).replyOnce(200, {
        message: successMessage,
      });

      renderWithRoute({ route });

      expect(await screen.findByText(tabTitle)).toBeInTheDocument();
      expect(screen.getByText(tabDescription)).toBeInTheDocument();

      const shopNameInput = await screen.findByLabelText(shopName);
      expect(shopNameInput).toBeInTheDocument();
      expect(shopNameInput).toHaveValue('Toko Smart');
      expect(screen.getByText(infoShopName)).toBeInTheDocument();

      expect(screen.getByText(uploadLogo)).toBeInTheDocument();
      expect(screen.getByText(infoLogo)).toBeInTheDocument();
      expect(screen.getByText(captionUpload)).toBeInTheDocument();

      const uploaderInput = screen.getByTestId('uploader-input');
      Object.defineProperty(uploaderInput, 'files', {
        value: [file],
      });
      fireEvent.drop(uploaderInput);
      await user.type(screen.getByLabelText(shopName), 'test');

      const addButton = screen.getByRole('button', { name: 'Simpan' });
      await waitFor(() => expect(addButton).toBeEnabled());
      await user.click(addButton);

      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(successMessage),
      );
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'edit_setting',
      });
    });

    it('Should delete image with confirmation pop up', async () => {
      renderWithRoute({ route });

      await user.click(await screen.findByText(tabTitle));
      expect(await screen.findByText(uploadLogo)).toBeInTheDocument();
      await user.click(screen.getByTestId('uploader-remove-button'));
      expect(await screen.findByText(deleteImageTitle)).toBeInTheDocument();
      await user.click(screen.getByRole('button', { name: 'Ya, hapus' }));
      const addButton = screen.getByRole('button', { name: 'Simpan' });
      await waitFor(() => expect(addButton).toBeEnabled());
    });

    it('Should show error toast when save', async () => {
      axios.onPatch(API_URL.header).replyOnce(500, {
        errors: [{ message: errorMessage }],
        meta: { http_status: 500 },
      });

      renderWithRoute({ route });

      await user.clear(await screen.findByLabelText(shopName));
      await user.type(await screen.findByLabelText(shopName), 'Toko Jaya');

      const addButton = screen.getByRole('button', { name: 'Simpan' });
      await waitFor(() => expect(addButton).toBeEnabled());
      await user.click(addButton);

      await waitFor(() =>
        expect(toast.danger).toHaveBeenLastCalledWith(errorMessage),
      );
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'error',
        action_details: errorMessage,
      });
    });
    it('Should show prompt message when click other tab', async () => {
      renderWithRoute({ route });

      expect(await screen.findByLabelText(shopName)).toBeInTheDocument();
      await user.type(screen.getByLabelText(shopName), 'Ubah Judul');

      await user.click(await screen.findByTestId('back-button'));
      expect(
        await screen.findByText(promptConfirmationMessage),
      ).toBeInTheDocument();
      await user.click(screen.getByRole('button', { name: 'Ya, keluar' }));
      expect(await screen.findByText('Custom Section')).toBeInTheDocument();
    });
    it('Should cancel prompt pop up correctly', async () => {
      renderWithRoute({ route });

      expect(await screen.findByLabelText(shopName)).toBeInTheDocument();
      await user.type(screen.getByLabelText(shopName), 'Ubah Judul');

      await user.click(await screen.findByTestId('back-button'));
      expect(
        await screen.findByText(promptConfirmationMessage),
      ).toBeInTheDocument();
      await user.click(screen.getByRole('button', { name: 'Batal' }));
      await waitForElementToBeRemoved(() =>
        screen.getByText(promptConfirmationMessage),
      );
      expect(screen.getByText(tabTitle)).toBeInTheDocument();
    });
  });
});
