import { IProduct } from '@interfaces/products';

export interface ISelectProductProps {
  selectedProducts: IProduct[];
  onChange: (selected: IProduct) => void;
}
