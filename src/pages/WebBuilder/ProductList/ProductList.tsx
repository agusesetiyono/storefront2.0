import React from 'react';
import { useParams } from 'react-router-dom';
import { useFormik } from 'formik';
import { Form } from 'react-bootstrap';

import { usePrompt } from '@hooks/usePrompt';
import useDeleteItem from '@hooks/useDeleteItem';
import {
  useGetProductList,
  useUpdateProductList,
} from '@hooks/api/webBuilder/section';
import useWebBuilder from '@hooks/useWebBuilder';
import { onSuccessMutate, onErrorMutate } from '@utils/reactQuery';
import type { IProduct } from '@interfaces/products';
import type { IProductList, IResponseProductList } from '@interfaces/sections';

import Button from '@components/Button';
import SideMenuWebBuilder from '@components/WebBuilder/SideMenu';
import SideMenuGroup from '@components/WebBuilder/SideMenu/partials/SideMenuGroup';
import type { ISideMenuGroup } from '@components/WebBuilder/SideMenu/sideMenuWebBuilder.types';

import { productListValidation } from './productList.validation';
import SelectProduct from './partials/SelectProduct';
import {
  sendWebBuilderTracker,
  WebBuilderAction,
} from '@utils/tracker/webBuilder';
import { getErrorMessage } from '@utils/error';

const ProductListForm = () => {
  const updateProductList = useUpdateProductList();
  const { refreshPreview } = useWebBuilder();
  const params = useParams();
  const productListId = params.id;

  const {
    values: { title, product_list },
    dirty,
    isValid,
    isSubmitting,
    handleChange,
    handleSubmit,
    setFieldValue,
    setSubmitting,
    resetForm,
  } = useFormik<IProductList>({
    initialValues: {
      title: '',
      product_list: [],
    },
    validationSchema: productListValidation,
    onSubmit: ({ title, product_list }) => {
      updateProductList.mutate(
        {
          id: Number(productListId),
          title,
          product_list: product_list?.map((product) => product.id),
        },
        {
          onSuccess: (data) => {
            onSuccessMutate(data);
            refreshPreview();
            setSubmitting(false);
            void resetForm({ values: { title, product_list } });
            void sendWebBuilderTracker(WebBuilderAction.EditSetting);
          },
          onError: (error) => {
            onErrorMutate(error);
            setSubmitting(false);
            void sendWebBuilderTracker(
              WebBuilderAction.Error,
              getErrorMessage(error),
            );
          },
        },
      );
    },
  });

  const { onDelete, ModalDeleteComponent } = useDeleteItem({
    confirmDeleteAction: () => {
      void sendWebBuilderTracker(
        WebBuilderAction.DeleteEntry,
        product_list?.map((product) => product.id).join(','),
      );
      void setFieldValue('product_list', []);
    },
    modalTitle: 'Kosongkan Product List?',
    modalContent:
      'Kamu akan mengosongkan <strong>Custom Product List</strong>.',
  });

  usePrompt(dirty);

  const { isFetching, isError, refetch } = useGetProductList(
    productListId,
    (res: IResponseProductList) => {
      if (!res) return;

      const {
        data: { title, product_list },
      } = res;
      void resetForm({ values: { title, product_list } });
    },
  );

  const handleSelectProduct = (selected: IProduct) => {
    void sendWebBuilderTracker(WebBuilderAction.AddEntry, `${selected.id}`);
    void setFieldValue('product_list', [selected, ...product_list]);
  };

  const handleRemoveProduct = (id?: number) => {
    if (!id) return;

    void sendWebBuilderTracker(WebBuilderAction.DeleteEntry, `${id}`);
    const newProducts = product_list.filter((product) => product.id !== id);
    void setFieldValue('product_list', newProducts);
  };

  const productMenu: ISideMenuGroup = {
    menus: product_list.map(({ id, name, image }) => ({
      id,
      title: name,
      image: image,
      canDelete: true,
    })),
    canSort: false,
    config: {
      className: 'product-list',
      showTitle: true,
      showSortAction: false,
      isCustomDelete: true,
      imageSize: 'large',
    },
  };

  return (
    <SideMenuWebBuilder
      header={{
        title: 'Custom Product List',
        subtitle:
          'Pilih atau custom daftar produk yang ingin ditampilkan di homepage pada section ini.',
        backUrl: '/settings/custom-website/builder/section',
      }}
      footer={{
        action: (
          <Button
            className="w-full"
            onClick={() => handleSubmit()}
            disabled={!isValid || !dirty}
            isLoading={isSubmitting}
            spinnerClass="my-1"
          >
            Simpan
          </Button>
        ),
      }}
      isFetching={isFetching}
      isError={isError}
      onRetry={() => void refetch()}
    >
      <>
        <Form>
          <Form.Group className="mb-6">
            <Form.Label required-label="true" className="text-sm">
              Judul Section
            </Form.Label>
            <Form.Control
              id="title"
              name="title"
              size="sm"
              value={title}
              onChange={handleChange}
              placeholder="Contoh: Semua Produk"
            />
          </Form.Group>
          <Form.Group className="mb-6">
            <Form.Label
              required-label="true"
              className="text-sm"
              htmlFor="selectProduct"
            >
              Pilih Produk
            </Form.Label>
            <SelectProduct
              selectedProducts={product_list}
              onChange={(selected) => handleSelectProduct(selected)}
            />
            {!!product_list.length && (
              <div className="text-center w-full">
                <Button
                  size="sm"
                  variant="light"
                  onClick={() => onDelete()}
                  className="text-base text-black rounded bg-white border border-[#AAAAAA] mt-6 mb-4"
                >
                  <>
                    <i className="fi-sr-trash text-gray-400 mr-3 block mt-1"></i>{' '}
                    Kosongkan
                  </>
                </Button>

                <SideMenuGroup
                  {...productMenu}
                  testid="product-list-item"
                  onDelete={handleRemoveProduct}
                />
              </div>
            )}
          </Form.Group>
        </Form>

        <ModalDeleteComponent />
      </>
    </SideMenuWebBuilder>
  );
};

export default ProductListForm;
