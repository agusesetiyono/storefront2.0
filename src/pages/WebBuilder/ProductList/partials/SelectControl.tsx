import React from 'react';
import { ControlProps, components } from 'react-select';

import type { IProduct } from '@interfaces/products';

const SelectControl = ({
  children,
  ...props
}: ControlProps<IProduct, false>) => (
  <components.Control {...props}>
    <i className="fi-rr-search ml-3 text-[#AAAAAA]"></i> {children}
  </components.Control>
);

export default SelectControl;
