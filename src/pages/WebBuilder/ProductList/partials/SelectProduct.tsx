import React, { useState } from 'react';
import Select from 'react-select';

import type { IProduct, IResponseProducts } from '@interfaces/products';
import { useSearchProduct } from '@hooks/api/webBuilder/section';
import useDebounceEffect from '@hooks/useDebounceEffect';

import SelectOption from './SelectOption';
import SelectControl from './SelectControl';
import type { ISelectProductProps } from '../productList.types';

const SelectProduct = ({ selectedProducts, onChange }: ISelectProductProps) => {
  const [keyword, setKeyword] = useState<string>();
  const [search, setSearch] = useState<string>();
  const [selected, setSelected] = useState<IProduct | null>();
  const [productOptions, setProductOptions] = useState<IProduct[]>([]);
  const { isFetching: isSearching } = useSearchProduct(
    keyword,
    (res: IResponseProducts) => {
      if (!res) return;

      const products = res.data;
      const filteredOptions =
        selectedProducts?.length > 0
          ? products.filter(
              (option) =>
                !selectedProducts.some((product) => product.id === option.id),
            )
          : products;
      setProductOptions(filteredOptions);
    },
  );

  useDebounceEffect(() => {
    setKeyword(search);
  }, [search]);

  const handleChange = (selected: IProduct) => {
    setSelected(null);
    setProductOptions([]);
    onChange(selected);
  };

  return (
    <Select
      isSearchable
      menuIsOpen={!!search && search.length >= 3}
      name="selectProduct"
      inputId="selectProduct"
      placeholder="Cari nama produk"
      isLoading={isSearching}
      value={selected}
      options={productOptions}
      getOptionLabel={(option) => option.name}
      noOptionsMessage={() => (
        <i className="text-[#AAAAAA]">Kosong/tidak ada di Storefront</i>
      )}
      onInputChange={setSearch}
      onMenuClose={() => setProductOptions([])}
      onChange={(selected) => selected && handleChange(selected)}
      components={{
        Control: SelectControl,
        Option: SelectOption,
        DropdownIndicator: null,
      }}
      styles={{
        control: (base) => ({ ...base, cursor: 'pointer' }),
        placeholder: (base) => ({ ...base, color: '#AAAAAA' }),
        option: (base) => ({ ...base, cursor: 'pointer' }),
        input: (base) => ({ ...base, height: 30 }),
        menuList: (base) => ({
          ...base,
          height: 'auto',
          maxHeight: 230,
          padding: 0,
          zIndex: 999,
        }),
      }}
    />
  );
};

export default SelectProduct;
