import React from 'react';
import { components, OptionProps } from 'react-select';

import type { IProduct } from '@interfaces/products';

const SelectOption = ({ ...props }: OptionProps<IProduct, false>) => {
  const { name, image } = props.data;

  const search = (searchValue: string, name: string) => {
    const regex = new RegExp(`(${searchValue})`, 'gi');
    const parts = name.split(regex);

    // set text RTL to display ellipsis text
    const isRTL = parts[0]?.length >= 13;

    return (
      <span
        className="overflow-hidden text-ellipsis whitespace-nowrap"
        style={{ direction: isRTL ? 'rtl' : 'ltr' }}
      >
        {parts.map((part, index) => (
          <span
            key={index}
            className={`
                    ${
                      part.toLowerCase() === searchValue.toLowerCase()
                        ? 'text-[#ED7881] font-bold'
                        : 'text-black font-normal'
                    }`}
          >
            {part}
          </span>
        ))}
      </span>
    );
  };

  return (
    <components.Option {...props}>
      <span title={name} className="flex align-items-center">
        {image && (
          <span className="rounded mr-2 border border-gray-200">
            <img
              src={image}
              alt={name}
              className="h-[28px] w-[28px] object-cover rounded"
            />
          </span>
        )}
        {search(props.selectProps.inputValue, name)}
      </span>
    </components.Option>
  );
};

export default SelectOption;
