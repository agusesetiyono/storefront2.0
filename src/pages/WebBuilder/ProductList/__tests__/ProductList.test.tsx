import '@testing-library/jest-dom';
import axios from '@mocks/axios';

import {
  screen,
  waitFor,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';
import { renderWithRoute, API_URL, getLastCalled } from '@utils/test-helper';
import toast from '@utils/toast';
import { sendTracker } from '@utils/tracker';

// API
import { GENERAL_ERROR_RESPONSE, GENERAL_SUCCESS_RESPONSE } from '@mocks/api';
import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';
import {
  GET_PRODUCT_LIST_EMPTY_RESPONSE,
  GET_SEARCH_PRODUCT_SUCCESS_RESPONSE,
  GET_PRODUCT_LIST_SUCCESS_RESPONSE,
} from '@mocks/api/productList';

jest.mock('@utils/toast', () => ({
  success: jest.fn(),
  danger: jest.fn(),
}));

axios.onGet(`${API_URL.storefronts}/status`).reply(
  200,
  getMockSubscriptionsResponse({
    type: 'platinum',
    expiredDate: '2027-05-01T00:00:00+07:00',
  }),
);

let user: UserEvent;
const searchKeyword = 'warna';
beforeEach(() => {
  user = userEvent.setup({ delay: null });
  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));
  localStorage.setItem('onboardingStep', '5'); // already finished onboarding

  axios
    .onGet(`${API_URL.productList}/1`)
    .reply(200, GET_PRODUCT_LIST_EMPTY_RESPONSE);

  axios
    .onGet(API_URL.searchProduct, { keyword: searchKeyword })
    .reply(200, GET_SEARCH_PRODUCT_SUCCESS_RESPONSE);
});

afterEach(() => {
  jest.useRealTimers();
  jest.clearAllMocks();
  localStorage.clear();
});

describe('Web Builder Product List', () => {
  const route =
    '/storefront-editor/settings/custom-website/builder/section/product-list/1';

  const tabTitle = 'Custom Product List';
  const titlePlaceholder = 'Contoh: Semua Produk';
  const productListLabel = 'Pilih Produk';
  const productListTestId = 'product-list-item';
  const productOptionText = 'Kaos Garis-Garis';
  const saveButtonText = 'Simpan';
  const promptConfirmationMessage =
    'Kamu akan keluar halaman ini tanpa menyimpan perubahan. Apa kamu yakin?';
  const deleteAllConfirmationTitle = 'Kosongkan Product List?';
  const defaultTrackerParams = {
    evn: 'ngorder_sf_builder_actions',
    active_panel: 'section_custom_product_list',
  };

  const searchProduct = async () => {
    const productListSearch = screen.getByLabelText(productListLabel);
    expect(productListSearch).toBeInTheDocument();

    await user.click(productListSearch);
    jest.runAllTimers();
    await user.type(productListSearch, searchKeyword);
    jest.runAllTimers();

    await waitFor(() =>
      expect(screen.getByText(productOptionText)).toBeVisible(),
    );
    await user.click(screen.getByText(productOptionText));
    await waitFor(() =>
      expect(screen.getByTestId(productListTestId)).toBeVisible(),
    );
  };

  it('Should match snapshot', async () => {
    const { container } = renderWithRoute({ route });
    expect(await screen.findByText(tabTitle)).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });

  it('Should disable "Simpan" button when form is not valid', async () => {
    renderWithRoute({ route });
    expect(await screen.findByText(tabTitle)).toBeInTheDocument();

    const titleInput = screen.getByPlaceholderText(titlePlaceholder);
    expect(titleInput).toHaveValue('');

    const productListSearch = screen.getByLabelText(productListLabel);
    expect(productListSearch).toBeInTheDocument();

    const saveButton = screen.getByRole('button', { name: saveButtonText });
    expect(saveButton).toBeDisabled();

    await user.type(titleInput, 'title');
    expect(saveButton).toBeDisabled();

    await searchProduct();
    await waitFor(() => expect(saveButton).toBeEnabled());
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'add_entry',
      action_details: '5',
    });
  });

  it('Should delete selected product list', async () => {
    renderWithRoute({ route });
    expect(await screen.findByText(tabTitle)).toBeInTheDocument();

    await searchProduct();
    const deleteButton = screen.getByTestId('delete-trigger-0-0');
    await user.click(deleteButton);

    await waitFor(() => expect(deleteButton).not.toBeVisible());
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'delete_entry',
      action_details: '5',
    });
  });

  it('Should delete all selected product list', async () => {
    renderWithRoute({ route });
    expect(await screen.findByText(tabTitle)).toBeInTheDocument();

    await searchProduct();

    const deleteAllButton = screen.getByRole('button', {
      name: 'Kosongkan',
    });
    expect(deleteAllButton).toBeInTheDocument();
    await user.click(deleteAllButton);

    expect(
      await screen.findByText(deleteAllConfirmationTitle),
    ).toBeInTheDocument();

    const cancelButton = screen.getByTestId('button-cancel-delete');
    await user.click(cancelButton);
    expect(await screen.findByText(tabTitle)).toBeInTheDocument();

    await user.click(deleteAllButton);
    expect(
      await screen.findByText(deleteAllConfirmationTitle),
    ).toBeInTheDocument();
    const confirmButton = screen.getByTestId('button-confirm-delete');
    await user.click(confirmButton);
    await waitFor(() => expect(deleteAllButton).not.toBeVisible());
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'delete_entry',
      action_details: '5',
    });
  });

  it('Should handle error when save product list', async () => {
    axios.onPut(API_URL.productList).reply(500, GENERAL_ERROR_RESPONSE);

    renderWithRoute({ route });
    expect(await screen.findByText(tabTitle)).toBeInTheDocument();

    await user.type(screen.getByPlaceholderText(titlePlaceholder), 'title');
    await searchProduct();

    const saveButton = screen.getByRole('button', { name: saveButtonText });
    await waitFor(() => expect(saveButton).toBeEnabled());

    await user.click(saveButton);
    await waitFor(() =>
      expect(toast.danger).toHaveBeenLastCalledWith(
        GENERAL_ERROR_RESPONSE.errors[0].message,
      ),
    );
    expect(saveButton).toBeEnabled();
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'error',
      action_details: GENERAL_ERROR_RESPONSE.errors[0].message,
    });
  });

  it('Should handle success when save product list', async () => {
    axios.onPut(API_URL.productList).reply(200, GENERAL_SUCCESS_RESPONSE);

    renderWithRoute({ route });
    expect(await screen.findByText(tabTitle)).toBeInTheDocument();

    await user.type(screen.getByPlaceholderText(titlePlaceholder), 'title');
    await searchProduct();

    const saveButton = screen.getByRole('button', { name: saveButtonText });
    await waitFor(() => expect(saveButton).toBeEnabled());

    await user.click(saveButton);
    await waitFor(() =>
      expect(toast.success).toHaveBeenLastCalledWith(
        GENERAL_SUCCESS_RESPONSE.message,
      ),
    );
    expect(saveButton).toBeDisabled();
    expect(getLastCalled(axios, 'put').data).toEqual(
      JSON.stringify({
        id: 1,
        title: 'title',
        product_list: [5],
      }),
    );
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'edit_setting',
    });
  });

  it('Should show previous data', async () => {
    axios
      .onGet(`${API_URL.productList}/1`)
      .reply(200, GET_PRODUCT_LIST_SUCCESS_RESPONSE);

    renderWithRoute({ route });
    expect(await screen.findByText(tabTitle)).toBeInTheDocument();
    expect(screen.getByPlaceholderText(titlePlaceholder)).toHaveValue(
      'Custom Product List',
    );
    expect(screen.getByTestId(productListTestId)).toBeVisible();
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'visit',
    });
  });

  it('Should show error state', async () => {
    const errorMessage = 'Gagal mengambil data Custom Product List';
    axios.onGet(`${API_URL.productList}/1`).reply(500, GENERAL_ERROR_RESPONSE);

    renderWithRoute({ route });
    expect(await screen.findByText(errorMessage)).toBeInTheDocument();

    axios
      .onGet(`${API_URL.productList}/1`)
      .reply(200, GET_PRODUCT_LIST_EMPTY_RESPONSE);
    await user.click(screen.getByRole('button', { name: 'Coba Lagi' }));
    expect(
      await screen.findByPlaceholderText(titlePlaceholder),
    ).toBeInTheDocument();
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'error',
      action_details: errorMessage,
    });
  });

  it('Should show block navigation pop up', async () => {
    renderWithRoute({ route });
    expect(await screen.findByText(tabTitle)).toBeInTheDocument();

    await user.type(screen.getByPlaceholderText(titlePlaceholder), 'title');
    const backButton = screen.getByTestId('back-button');
    await user.click(backButton);
    expect(
      await screen.findByText(promptConfirmationMessage),
    ).toBeInTheDocument();

    await user.click(screen.getByRole('button', { name: 'Batal' }));
    await waitForElementToBeRemoved(() =>
      screen.getByText(promptConfirmationMessage),
    );
    expect(await screen.findByText(tabTitle)).toBeInTheDocument();

    await user.click(backButton);
    await user.click(backButton);
    expect(
      await screen.findByText(promptConfirmationMessage),
    ).toBeInTheDocument();

    await user.click(screen.getByRole('button', { name: 'Ya, keluar' }));
    expect(await screen.findByText('Custom Section')).toBeInTheDocument();
  });
});
