import * as Yup from 'yup';

export const productListValidation = Yup.object().shape({
  title: Yup.string().required('Judul section wajib diisi'),
  product_list: Yup.array(Yup.object()).min(1, 'Harus pilih minimal 1 produk'),
});
