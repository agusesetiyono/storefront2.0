import React from 'react';

import type { ISideMenuGroup } from '@components/WebBuilder/SideMenu/sideMenuWebBuilder.types';
import SidemenuWebBuilder from '@components/WebBuilder/SideMenu';
import SideMenuGroup from '@components/WebBuilder/SideMenu/partials/SideMenuGroup';
import {
  sendWebBuilderTracker,
  WebBuilderAction,
} from '@utils/tracker/webBuilder';

const WebBuilderTheme = () => {
  const menuGroup: ISideMenuGroup[] = [
    {
      menus: [
        {
          title: 'Pengaturan',
          url: 'config',
          description: 'Ubah tema, bahasa yang ditampilkan, dan Custom CSS.',
        },
        {
          title: 'Font',
          url: 'font',
          description:
            'Ubah jenis dan warna font sesuai kepribadian brand kamu.',
        },
        {
          title: 'Warna',
          url: 'color',
          description: 'Ubah warna dari setiap aspek tokomu.',
        },
      ],
    },
  ];

  return (
    <SidemenuWebBuilder
      header={{
        title: 'Custom Tema',
        subtitle: 'Ubah icon website, warna, dan font melalui fitur ini.',
        backUrl: '/settings/custom-website/builder',
      }}
    >
      <>
        {menuGroup.map((group, index) => (
          <SideMenuGroup
            {...group}
            groupIndex={index}
            key={`menu-group-${index}`}
            onItemClick={(itemIndex, menuItems) =>
              void sendWebBuilderTracker(
                WebBuilderAction.ClickEntry,
                menuItems[itemIndex].title,
              )
            }
          />
        ))}
      </>
    </SidemenuWebBuilder>
  );
};

export default WebBuilderTheme;
