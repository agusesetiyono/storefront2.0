import '@testing-library/jest-dom';
import axios from '@mocks/axios';
import { screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';
import { renderWithRoute, API_URL, getLastCalled } from '@utils/test-helper';
import toast from '@utils/toast';
import { sendTracker } from '@utils/tracker';

// API
import { GENERAL_ERROR_RESPONSE, GENERAL_SUCCESS_RESPONSE } from '@mocks/api';
import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';
import { getMockSelectedThemeResponse } from '@mocks/api/themes';

jest.mock('@utils/toast', () => ({
  success: jest.fn(),
  danger: jest.fn(),
}));

axios.onGet(`${API_URL.storefronts}/status`).reply(
  200,
  getMockSubscriptionsResponse({
    type: 'platinum',
    expiredDate: '2027-05-01T00:00:00+07:00',
  }),
);

let user: UserEvent;

beforeEach(() => {
  user = userEvent.setup({ delay: null });
  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));
  localStorage.setItem('onboardingStep', '5'); // already finished onboarding

  axios.onGet(API_URL.userTheme).reply(200, getMockSelectedThemeResponse());
  axios.onPatch(API_URL.updateFonts).reply(200, GENERAL_SUCCESS_RESPONSE);
});

afterEach(() => {
  jest.useRealTimers();
  jest.clearAllMocks();
  localStorage.clear();
});

describe('Web Builder Font', () => {
  const route = '/storefront-editor/settings/custom-website/builder/theme/font';
  const tabTitle = 'Edit Font';
  const saveButtonText = 'Simpan';
  const defaultTrackerParams = {
    evn: 'ngorder_sf_builder_actions',
    active_panel: 'theme_font',
  };

  it('Should match snapshot', async () => {
    const { container } = renderWithRoute({ route });

    expect(await screen.findByText(tabTitle)).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });

  it('Should update font correctly', async () => {
    renderWithRoute({ route });

    expect(await screen.findByText(tabTitle)).toBeInTheDocument();
    const saveButton = screen.getByRole('button', { name: saveButtonText });
    expect(saveButton).toBeDisabled();

    await user.click(
      screen.getByRole('button', { name: 'Encode Sans Expanded Semi Bold' }),
    );

    await user.click(await screen.findByText('Expletus Sans'));
    expect(saveButton).toBeEnabled();
    await user.click(saveButton);
    await waitFor(() =>
      expect(toast.success).toHaveBeenLastCalledWith(
        GENERAL_SUCCESS_RESPONSE.message,
      ),
    );
    expect(getLastCalled(axios, 'patch').data).toEqual(
      '{"font_family":"Expletus Sans","font_color":"#c71e1e"}',
    );
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'edit_setting',
    });
  });

  it('Should handle error update font correctly', async () => {
    axios.onPatch(API_URL.updateFonts).reply(500, GENERAL_ERROR_RESPONSE);
    renderWithRoute({ route });

    expect(await screen.findByText(tabTitle)).toBeInTheDocument();
    const colorInput = screen
      .getByTestId('font-color')
      .getElementsByTagName('input')[0];
    const colorPreview = screen
      .getByTestId('font-color')
      .getElementsByClassName('select-color__preview-color')[0];
    await user.clear(colorInput);
    await user.type(colorInput, '#757483');
    expect(colorPreview).toHaveAttribute(
      'style',
      'background-color: rgb(117, 116, 131);',
    );

    await user.click(screen.getByRole('button', { name: saveButtonText }));
    await waitFor(() =>
      expect(toast.danger).toHaveBeenLastCalledWith(
        GENERAL_ERROR_RESPONSE.errors[0].message,
      ),
    );
    expect(getLastCalled(axios, 'patch').data).toEqual(
      '{"font_family":"Encode Sans Expanded:600","font_color":"#757483"}',
    );
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'error',
      action_details: GENERAL_ERROR_RESPONSE.errors[0].message,
    });
  });

  it('Should reset font family when user click default font', async () => {
    renderWithRoute({ route });

    expect(await screen.findByText(tabTitle)).toBeInTheDocument();
    await user.click(
      screen.getByRole('button', { name: 'Encode Sans Expanded Semi Bold' }),
    );
    await user.click(await screen.findByText('Jura Medium'));
    await user.click(screen.getAllByRole('button', { name: 'Default' })[0]);
    await user.click(screen.getByRole('button', { name: saveButtonText }));

    await waitFor(() =>
      expect(toast.success).toHaveBeenLastCalledWith(
        GENERAL_SUCCESS_RESPONSE.message,
      ),
    );

    expect(getLastCalled(axios, 'patch').data).toEqual(
      '{"font_family":"Lato:400","font_color":"#c71e1e"}',
    );
  });

  it('Should reset color when user click default color', async () => {
    renderWithRoute({ route });

    expect(await screen.findByText(tabTitle)).toBeInTheDocument();
    const colorInput = screen
      .getByTestId('font-color')
      .getElementsByTagName('input')[0];
    await user.click(screen.getByRole('button', { name: saveButtonText }));
    await user.clear(colorInput);
    await user.type(colorInput, '#757483');
    await user.click(screen.getAllByRole('button', { name: 'Default' })[1]);
    await user.click(screen.getByRole('button', { name: saveButtonText }));

    await waitFor(() =>
      expect(toast.success).toHaveBeenLastCalledWith(
        GENERAL_SUCCESS_RESPONSE.message,
      ),
    );

    expect(getLastCalled(axios, 'patch').data).toEqual(
      '{"font_family":"Encode Sans Expanded:600","font_color":"#000"}',
    );
  });
});
