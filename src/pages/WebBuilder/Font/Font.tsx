import React, { useEffect } from 'react';
import { useFormik } from 'formik';
import { Form, DropdownButton } from 'react-bootstrap';
import WebFont from 'webfontloader';

import { usePrompt } from '@hooks/usePrompt';
import { useGetTheme, useUpdateFonts } from '@hooks/api/themes';
import useWebBuilder from '@hooks/useWebBuilder';

import { onSuccessMutate, onErrorMutate } from '@utils/reactQuery';
import { FONT_FAMILY_OPTIONS } from '@utils/constants';
import {
  getFontDisplayName,
  getFontProperties,
  getFontName,
} from '@utils/font';
import SideMenuWebBuilder from '@components/WebBuilder/SideMenu';
import Button from '@components/Button';
import SelectColor from '@components/SelectColor';
import FontDropdownItem from './partials/FontDropdownItem';

import { IFont } from './font.types';
import {
  sendWebBuilderTracker,
  WebBuilderAction,
} from '@utils/tracker/webBuilder';
import { getErrorMessage } from '@utils/error';

const FontForm = () => {
  const { refreshPreview } = useWebBuilder();

  useEffect(() => {
    WebFont.load({
      google: {
        families: FONT_FAMILY_OPTIONS,
      },
    });
  }, []);

  const {
    values: {
      font_family,
      font_color,
      default_font_family,
      default_font_color,
    },
    dirty,
    isValid,
    handleSubmit,
    setFieldValue,
    resetForm,
    isSubmitting,
    setSubmitting,
  } = useFormik<IFont>({
    initialValues: {
      font_family: '',
      font_color: '#000000',
      default_font_family: '',
      default_font_color: '#000000',
    },
    onSubmit: (values) => {
      updateFonts.mutate(
        {
          font_family: values.font_family,
          font_color: values.font_color,
        },
        {
          onSuccess: (data) => {
            onSuccessMutate(data);
            refreshPreview();
            void resetForm({ values });
            setSubmitting(false);
            void sendWebBuilderTracker(WebBuilderAction.EditSetting);
          },

          onError: (err) => {
            onErrorMutate(err);
            setSubmitting(false);
            void sendWebBuilderTracker(
              WebBuilderAction.Error,
              getErrorMessage(err),
            );
          },
        },
      );
    },
  });

  usePrompt(dirty);

  const fontStyle = font_family?.split(':');

  const { isFetching, isError, refetchSettingTheme } = useGetTheme((data) => {
    const fontData = data.data.font;
    const defaultFontData = data.data.default_font;
    const defaultFontFamily = `${
      defaultFontData.font_family.split(':')[0]
    }:400`;

    resetForm({
      values: {
        font_family: fontData.font_family || defaultFontFamily,
        font_color: fontData.font_color || defaultFontData.font_color,
        default_font_family: defaultFontFamily,
        default_font_color: defaultFontData.font_color,
      },
    });
  });
  const updateFonts = useUpdateFonts();

  return (
    <SideMenuWebBuilder
      header={{
        title: 'Edit Font',
        subtitle: 'Ubah jenis dan warna font sesuai kepribadian brand kamu.',
        backUrl: '/settings/custom-website/builder/theme',
      }}
      footer={{
        action: (
          <Button
            className="w-full"
            onClick={() => handleSubmit()}
            disabled={!isValid || !dirty}
            isLoading={isSubmitting}
            spinnerClass="my-1"
          >
            Simpan
          </Button>
        ),
      }}
      isFetching={isFetching}
      isError={isError}
      onRetry={refetchSettingTheme}
    >
      <Form>
        <Form.Group className="mb-6">
          <Form.Label className="text-sm">Font Family</Form.Label>
          <div className="flex gap-1">
            <DropdownButton
              className="font-dropdown"
              title={getFontDisplayName(fontStyle?.[0], fontStyle?.[1])}
              style={getFontProperties(fontStyle?.[0], fontStyle?.[1])}
            >
              {FONT_FAMILY_OPTIONS.map((fontFamily) => {
                const fontInfo = fontFamily.split(':');
                const fontName: string = getFontName(fontInfo?.[0]);
                const fontStyles = fontInfo?.[1]?.split(',');

                return fontStyles?.length ? (
                  fontStyles.map((style: string) => (
                    <FontDropdownItem
                      key={`${fontName}:${style}`}
                      font={fontName}
                      style={style}
                      onClick={() => {
                        void setFieldValue(
                          'font_family',
                          `${fontName}:${style}`,
                        );
                      }}
                      isSelected={font_family === `${fontName}:${style}`}
                    />
                  ))
                ) : (
                  <FontDropdownItem
                    key={fontName}
                    font={fontName}
                    onClick={() => {
                      void setFieldValue('font_family', fontName);
                    }}
                    isSelected={font_family === fontName}
                  />
                );
              })}
            </DropdownButton>
            <Button
              size="sm"
              variant="light"
              className="px-2"
              onClick={() =>
                void setFieldValue('font_family', default_font_family)
              }
            >
              Default
            </Button>
          </div>
        </Form.Group>
        <Form.Group className="mb-6">
          <Form.Label className="text-sm">Font Color</Form.Label>
          <SelectColor
            value={font_color}
            onColorChange={(color) => void setFieldValue('font_color', color)}
            defaultColor={default_font_color}
            data-testid="font-color"
          />
        </Form.Group>
      </Form>
    </SideMenuWebBuilder>
  );
};

export default FontForm;
