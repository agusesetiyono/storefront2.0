export interface IFont {
  font_family: string;
  font_color: string;
  default_font_family: string;
  default_font_color: string;
}

export interface IPropsFontDropdownItem {
  onClick: () => void;
  font: string;
  style?: string;
  isSelected: boolean;
}
