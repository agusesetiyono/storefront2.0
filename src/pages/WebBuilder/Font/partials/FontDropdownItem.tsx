import React from 'react';
import { Dropdown } from 'react-bootstrap';
import { getFontDisplayName, getFontProperties } from '@utils/font';

import type { IPropsFontDropdownItem } from '../font.types';

const FontDropdownItem = ({
  onClick,
  font,
  style,
  isSelected,
}: IPropsFontDropdownItem) => {
  const value = style ? `${font}:${style}` : font;

  return (
    <Dropdown.Item
      value={value}
      style={getFontProperties(font, style)}
      onClick={onClick}
      className="flex items-center"
    >
      <span className="flex-1">{getFontDisplayName(font, style)}</span>
      {isSelected && <i className="fi-rr-check text-xs text-right"></i>}
    </Dropdown.Item>
  );
};

export default FontDropdownItem;
