import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import { Form } from 'react-bootstrap';
import { useBoolean } from 'usehooks-ts';

import useWebBuilder from '@hooks/useWebBuilder';
import { usePrompt } from '@hooks/usePrompt';
import SideMenuWebBuilder from '@components/WebBuilder/SideMenu';
import Uploader from '@components/Uploader';
import Button from '@components/Button';

import { useGetThemes, useGetTheme, useUpdateTheme } from '@hooks/api/themes';
import { onSuccessMutate, onErrorMutate } from '@utils/reactQuery';
import type {
  ISettingTheme,
  ISelectedThemeResponseStatus,
} from '@interfaces/themes';
import type { ITheme, IThemeData } from '@interfaces/themes';
import {
  WebBuilderAction,
  sendWebBuilderTracker,
} from '@utils/tracker/webBuilder';
import { getErrorMessage } from '@utils/error';

const ConfigForm = () => {
  const updateSettingTheme = useUpdateTheme();
  const { refreshPreview } = useWebBuilder();
  const { value: initialImage, setTrue: setInitialImage } = useBoolean(false);
  const {
    value: isDeleteImage,
    setTrue: deleteImage,
    setFalse: resetImage,
  } = useBoolean(false);
  const { themesData } = useGetThemes();
  const [selectedTheme, setSelectedTheme] = useState<ITheme | null>(null);
  const onThemeChange = (selectedThemeId: number, themesData: IThemeData) => {
    const themeName = themesData[selectedThemeId];
    themeName && setSelectedTheme(themeName);
  };
  const {
    values,
    dirty,
    isValid,
    isSubmitting,
    handleChange,
    handleSubmit,
    setFieldValue,
    setSubmitting,
    resetForm,
  } = useFormik<ISettingTheme>({
    initialValues: {
      language: '',
      favicon: '',
      custom_css: '',
    },
    onSubmit: ({ image_file, language, custom_css }) => {
      const formData = new FormData();
      image_file && formData.append('image', image_file);
      language && formData.append('language', language);
      custom_css && formData.append('custom_css', custom_css);
      if (isDeleteImage) {
        formData.append('delete_image', 'true');
        resetImage();
      }

      updateSettingTheme.mutate(formData, {
        onSuccess: (data) => {
          onSuccessMutate(data);
          refreshPreview();
          void resetForm({ values });
          setSubmitting(false);
          image_file && setInitialImage();
          void sendWebBuilderTracker(WebBuilderAction.EditSetting);
        },
        onError: (error) => {
          onErrorMutate(error);
          setSubmitting(false);
          void sendWebBuilderTracker(
            WebBuilderAction.Error,
            getErrorMessage(error),
          );
        },
      });
    },
  });

  usePrompt(dirty && !isSubmitting);

  const languageOption = [
    {
      value: 'id',
      label: 'Indonesia',
    },
    {
      value: 'en',
      label: 'English',
    },
  ];
  const { isFetching, isError, refetchSettingTheme } = useGetTheme(
    (res: ISelectedThemeResponseStatus) => {
      if (!res) return;
      const {
        data: { theme_id, setting },
      } = res;
      setInitialImage();
      void resetForm({
        values: {
          theme: theme_id,
          custom_css: setting?.custom_css,
          favicon: setting?.favicon,
          language: setting?.language,
        },
      });
    },
  );

  useEffect(() => {
    // selected theme
    onThemeChange(values.theme || 0, themesData);
  }, [values.theme, themesData]);

  return (
    <SideMenuWebBuilder
      header={{
        title: 'Pengaturan',
        subtitle: 'Ubah tema, bahasa yang ditampilkan, dan Custom CSS.',
        backUrl: '/settings/custom-website/builder/theme',
      }}
      footer={{
        action: (
          <Button
            className="w-full"
            onClick={() => handleSubmit()}
            disabled={!isValid || !dirty}
            isLoading={isSubmitting}
            spinnerClass="my-1"
          >
            Simpan
          </Button>
        ),
      }}
      isFetching={isFetching}
      isError={isError}
      onRetry={refetchSettingTheme}
    >
      <Form>
        <Form.Group className="mb-6">
          <Form.Label className="text-sm mb-0" htmlFor="theme-name">
            Tema
          </Form.Label>
          <p className="text-sm text-capitalize" data-testid="theme-name">
            {selectedTheme?.theme_name || ''}
          </p>
        </Form.Group>
        <Form.Group className="mb-6">
          <Form.Label className="text-sm" htmlFor="language">
            Bahasa
          </Form.Label>
          <Form.Select
            id="language"
            name="language"
            value={values.language}
            onChange={(e) => void setFieldValue('language', e.target.value)}
          >
            {languageOption.map((language) => (
              <option
                key={language.value}
                value={language.value}
                data-testid={`test-lang-${language.value}`}
              >
                {language.label}
              </option>
            ))}
          </Form.Select>
        </Form.Group>
        <Form.Group className="mb-6">
          <Form.Label className="text-sm" required-label="false">
            Favicon
          </Form.Label>
          <Uploader
            maxFiles={1}
            accept=".jpg, .jpeg, .png"
            description="Format gambar JPG, JEPG, atau PNG dengan ukuran optimal 32x32 pixel."
            modalDeleteTitle="Hapus gambar?"
            modalDeleteContent="Kamu akan menghapus gambar icon website."
            initialPreview={values.favicon}
            onChange={async (files: File[]) => {
              const file = files[0] || null;
              if (initialImage && !file) {
                deleteImage();
              }
              await setFieldValue('favicon', '');
              await setFieldValue('image_file', file);
            }}
            isSquare
          />
        </Form.Group>
        <Form.Group className="mb-6">
          <Form.Label
            htmlFor="custom_css"
            className="text-sm"
            required-label="false"
          >
            Custom CSS
          </Form.Label>
          <Form.Control
            id="custom_css"
            as="textarea"
            rows={8}
            name="custom_css"
            value={values.custom_css}
            onChange={handleChange}
            disabled={isFetching}
          />
        </Form.Group>
      </Form>
    </SideMenuWebBuilder>
  );
};

export default ConfigForm;
