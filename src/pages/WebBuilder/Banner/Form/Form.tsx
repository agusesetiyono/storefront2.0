import React from 'react';
import { useFormik } from 'formik';
import { useNavigate, useParams } from 'react-router-dom';
import { Form } from 'react-bootstrap';

import useWebBuilder from '@hooks/useWebBuilder';
import { usePrompt } from '@hooks/usePrompt';
import {
  useCreateBanner,
  useGetBanner,
  useUpdateBanner,
} from '@hooks/api/webBuilder/banner';
import { onSuccessMutate, onErrorMutate } from '@utils/reactQuery';
import SideMenuWebBuilder from '@components/WebBuilder/SideMenu';
import Uploader from '@components/Uploader';
import Button from '@components/Button';
import { bannerValidation } from '../banner.validation';

import type { IBanner, IResponseBanner } from '@interfaces/banners';
import {
  sendWebBuilderTracker,
  WebBuilderAction,
} from '@utils/tracker/webBuilder';
import { getErrorMessage } from '@utils/error';

const BannerForm = () => {
  const BANNERS_PAGE_URL = '/settings/custom-website/builder/section/banner';
  const params = useParams();
  const bannerId = params.id;
  const navigate = useNavigate();
  const createBanner = useCreateBanner();
  const updateBanner = useUpdateBanner(bannerId);
  const { refreshPreview } = useWebBuilder();

  const {
    values,
    dirty,
    isValid,
    isSubmitting,
    handleChange,
    handleSubmit,
    setFieldValue,
    setSubmitting,
    resetForm,
  } = useFormik<IBanner>({
    initialValues: {
      image: '',
      title: '',
      desc: '',
      url: '',
    },
    validationSchema: bannerValidation,
    onSubmit: ({ image_file, title, desc, url }) => {
      const formData = new FormData();
      image_file && formData.append('image', image_file);
      formData.append('title', title);
      formData.append('desc', desc);
      formData.append('url', url);

      (bannerId ? updateBanner : createBanner).mutate(formData, {
        onSuccess: (data) => {
          onSuccessMutate(data);
          refreshPreview();
          void resetForm({ values });
          void sendWebBuilderTracker(
            bannerId ? WebBuilderAction.EditEntry : WebBuilderAction.AddEntry,
            bannerId,
          );
          bannerId ? setSubmitting(false) : navigate(BANNERS_PAGE_URL);
        },
        onError: (error) => {
          onErrorMutate(error);
          setSubmitting(false);
          void sendWebBuilderTracker(
            WebBuilderAction.Error,
            getErrorMessage(error),
          );
        },
      });
    },
  });

  usePrompt(dirty && !isSubmitting);

  let isLoadingPage = false;
  if (bannerId) {
    const { isFetching } = useGetBanner(
      bannerId,
      (res: IResponseBanner) => {
        if (!res) return;

        const {
          data: { image, title, desc, url },
        } = res;
        void resetForm({ values: { image, title, desc, url } });
      },
      () => {
        navigate(BANNERS_PAGE_URL);
      },
    );
    isLoadingPage = isFetching;
  }

  return (
    <SideMenuWebBuilder
      header={{
        title: `${bannerId ? 'Edit' : 'Tambah'} Banner`,
        subtitle:
          'Dengan Banner, kamu bisa menampilkan promosi menarik berupa gambar dan teks.',
        backUrl: '/settings/custom-website/builder/section/banner',
      }}
      footer={{
        action: (
          <Button
            className="w-full"
            onClick={() => handleSubmit()}
            disabled={!isValid || !dirty}
            isLoading={isSubmitting}
            spinnerClass="my-1"
          >
            {bannerId ? 'Simpan' : 'Tambah'}
          </Button>
        ),
      }}
      isFetching={isLoadingPage}
    >
      <Form>
        <Form.Group className="mb-6">
          <Form.Label required-label="true" className="text-sm">
            Upload Gambar Banner
          </Form.Label>
          <Uploader
            maxFiles={1}
            accept=".jpg, .jpeg, .png"
            description="Format gambar JPG, JEPG, atau PNG dengan ukuran optimal 328x160 pixel."
            modalDeleteTitle="Hapus gambar?"
            modalDeleteContent="Kamu akan menghapus gambar pada <strong>Banner</strong>."
            initialPreview={values.image}
            onChange={async (files: File[]) => {
              await setFieldValue('image', '');
              await setFieldValue('image_file', files[0] || null);
            }}
          />
        </Form.Group>
        <Form.Group className="mb-6">
          <Form.Label
            htmlFor="title"
            required-label="false"
            className="text-sm"
          >
            Headline di Banner
          </Form.Label>
          <Form.Control
            id="title"
            name="title"
            size="sm"
            value={values.title}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group className="mb-6">
          <Form.Label htmlFor="desc" required-label="false" className="text-sm">
            Teks
          </Form.Label>
          <Form.Control
            id="desc"
            as="textarea"
            rows={3}
            name="desc"
            value={values.desc}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group className="mb-6">
          <Form.Label htmlFor="url" required-label="false" className="text-sm">
            URL
          </Form.Label>
          <Form.Control
            id="url"
            name="url"
            type="url"
            size="sm"
            value={values.url}
            onChange={handleChange}
          />
        </Form.Group>
      </Form>
    </SideMenuWebBuilder>
  );
};

export default BannerForm;
