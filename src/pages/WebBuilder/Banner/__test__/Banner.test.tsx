import '@testing-library/jest-dom';
import axios from '@mocks/axios';
import { screen, within, waitFor, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';
import {
  renderWithRoute,
  API_URL,
  getLastCalledTimes,
} from '@utils/test-helper';
import toast from '@utils/toast';
import { sendTracker } from '@utils/tracker';

// API
import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';
import { GENERAL_SUCCESS_RESPONSE, GENERAL_ERROR_RESPONSE } from '@mocks/api';
import {
  GET_BANNERS_SUCCESS_RESPONSE,
  GET_BANNER_SUCCESS_RESPONSE,
  GET_DELETE_BANNER_ERROR_RESPONSE,
} from '@mocks/api/banner';

jest.mock('@utils/toast', () => ({
  success: jest.fn(),
  danger: jest.fn(),
}));

const file = new File(['hello'], 'hello.png', { type: 'image/png' });

axios.onGet(`${API_URL.storefronts}/status`).reply(
  200,
  getMockSubscriptionsResponse({
    type: 'platinum',
    expiredDate: '2027-05-01T00:00:00+07:00',
  }),
);

let user: UserEvent;

beforeEach(() => {
  user = userEvent.setup({ delay: null });
  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));
  localStorage.setItem('onboardingStep', '5'); // already finished onboarding

  axios.onGet(API_URL.banners).reply(200, GET_BANNERS_SUCCESS_RESPONSE);
  axios.onPost(API_URL.banners).reply(200, GENERAL_SUCCESS_RESPONSE);
  axios
    .onGet(`${API_URL.banners}/4794`)
    .reply(200, GET_BANNER_SUCCESS_RESPONSE);
  axios.onPut(`${API_URL.banners}/4794`).reply(200, GENERAL_SUCCESS_RESPONSE);
});

afterEach(() => {
  jest.useRealTimers();
  jest.clearAllMocks();
  localStorage.clear();
  axios.history.get = [];
  axios.history.put = [];
  axios.history.post = [];
});

describe('Web builder banner', () => {
  const route =
    '/storefront-editor/settings/custom-website/builder/section/banner';
  const tabDescription =
    'Dengan Banner, kamu bisa menampilkan promosi menarik berupa gambar dan teks.';
  const addButton = 'Tambah Banner';
  const firstItemTitle = 'This is the title';
  const editBannerTitle = 'Edit Banner';
  const uploadImageLabel = 'Upload Gambar Banner';
  const bannerTitleLabel = 'Headline di Banner';
  const deleteImageTitle = 'Hapus gambar?';
  const deleteBannerTitle = 'Hapus Banner?';
  const deleteConfirmationButton = 'Ya, hapus';
  const defaultTrackerParams = {
    evn: 'ngorder_sf_builder_actions',
    active_panel: 'section_banner',
  };

  describe('List', () => {
    it('Should render list correctly', async () => {
      renderWithRoute({ route });

      expect(await screen.findByText(tabDescription)).toBeInTheDocument();
      expect(screen.getByText(addButton)).toBeInTheDocument();

      const firstBanner = screen.getByTestId('side-menu-item-0-0');
      expect(within(firstBanner).getByText(firstItemTitle)).toBeInTheDocument();
      expect(
        within(firstBanner).getByAltText(firstItemTitle),
      ).toBeInTheDocument();
      expect(
        within(firstBanner).getByTestId('delete-trigger-0-0'),
      ).toBeInTheDocument();

      const secondBanner = screen.getByTestId('side-menu-item-0-1');
      expect(within(secondBanner).getByText('Banner')).toBeInTheDocument(); // default banner title
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'visit',
      });
    });

    it('Should render empty state correctly', async () => {
      axios.onGet(API_URL.banners).reply(200, { data: [] });
      renderWithRoute({ route });

      expect(await screen.findByText(tabDescription)).toBeInTheDocument();
      expect(
        screen.getByText(
          'Belum ada banner. Tambahkan dengan klik tombol di atas.',
        ),
      ).toBeInTheDocument();
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'visit',
      });
    });

    it('Should render error state correctly', async () => {
      const errorMessage = 'Gagal mengambil data Banner';
      axios.onGet(API_URL.banners).reply(500, {});
      renderWithRoute({ route });

      expect(await screen.findByText(tabDescription)).toBeInTheDocument();
      expect(screen.getByText(errorMessage)).toBeInTheDocument();

      axios.onGet(API_URL.banners).reply(200, GET_BANNERS_SUCCESS_RESPONSE);
      await user.click(screen.getByRole('button', { name: 'Coba Lagi' }));
      expect(await screen.findByText(firstItemTitle)).toBeInTheDocument();
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'error',
        action_details: errorMessage,
      });
    });
  });

  describe('Delete', () => {
    it('Should delete banner correctly', async () => {
      axios
        .onDelete(`${API_URL.banners}/4794`)
        .reply(200, GENERAL_SUCCESS_RESPONSE);

      renderWithRoute({ route });

      expect(await screen.findByText(firstItemTitle)).toBeInTheDocument();
      await user.click(screen.getByTestId('delete-trigger-0-0'));
      expect(screen.getByText(deleteBannerTitle)).toBeInTheDocument();
      await user.click(
        screen.getByRole('button', { name: deleteConfirmationButton }),
      );
      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(
          GENERAL_SUCCESS_RESPONSE.message,
        ),
      );
      expect(screen.queryByText(deleteImageTitle)).not.toBeInTheDocument();
      expect(getLastCalledTimes(axios.history.get, API_URL.banners)).toEqual(2);
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'delete_entry',
        action_details: '4794',
      });
    });

    it.each([997, 11127])(
      'Should handle error delete slide correctly, error code: %s',
      async (code) => {
        const errorResponse = GET_DELETE_BANNER_ERROR_RESPONSE(code);
        axios.onDelete(`${API_URL.banners}/4794`).reply(500, errorResponse);

        renderWithRoute({ route });

        expect(await screen.findByText(firstItemTitle)).toBeInTheDocument();
        await user.click(screen.getByTestId('delete-trigger-0-0'));
        await user.click(
          screen.getByRole('button', { name: deleteConfirmationButton }),
        );
        await waitFor(() =>
          expect(toast.danger).toHaveBeenLastCalledWith(
            errorResponse.errors[0].message,
          ),
        );
        expect(screen.queryByText(deleteImageTitle)).not.toBeInTheDocument();
        expect(getLastCalledTimes(axios.history.get, API_URL.banners)).toEqual(
          code === 11127 ? 2 : 1,
        );
        expect(sendTracker).toHaveBeenLastCalledWith({
          ...defaultTrackerParams,
          action_type: 'error',
          action_details: errorResponse.errors[0].message,
        });
      },
    );
  });

  describe('Edit', () => {
    it('Should handle error get banner detail', async () => {
      axios.onGet(`${API_URL.banners}/4794`).reply(500, GENERAL_ERROR_RESPONSE);
      renderWithRoute({ route });

      await user.click(await screen.findByText(firstItemTitle));
      expect(
        await screen.findByRole('button', { name: addButton }),
      ).toBeInTheDocument();
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'click_entry',
        action_details: '4794',
      });
    });

    it('Should edit banner correctly', async () => {
      renderWithRoute({ route });

      await user.click(await screen.findByText(firstItemTitle));
      expect(screen.getByText(editBannerTitle)).toBeInTheDocument();
      expect(screen.getByText(tabDescription)).toBeInTheDocument();
      expect(await screen.findByText(uploadImageLabel)).toBeInTheDocument();
      expect(screen.getByLabelText(bannerTitleLabel)).toHaveValue(
        firstItemTitle,
      );
      expect(screen.getByLabelText('Teks')).toHaveValue('Ini adalah deskripsi');
      expect(screen.getByLabelText('URL')).toHaveValue(
        'http://app.smartseller.co.id',
      );

      const saveButton = screen.getByRole('button', { name: 'Simpan' });
      expect(saveButton).toBeDisabled();
      await user.type(screen.getByLabelText(bannerTitleLabel), 'test');
      expect(saveButton).toBeEnabled();

      await user.click(saveButton);
      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(
          GENERAL_SUCCESS_RESPONSE.message,
        ),
      );
      expect(
        getLastCalledTimes(axios.history.put, `${API_URL.banners}/4794`),
      ).toEqual(1);
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'edit_entry',
        action_details: '4794',
      });
    });

    it('Should delete image with confirmation pop up', async () => {
      renderWithRoute({ route });

      await user.click(await screen.findByText(firstItemTitle));
      expect(await screen.findByText(uploadImageLabel)).toBeInTheDocument();
      await user.click(screen.getByTestId('uploader-remove-button'));
      expect(await screen.findByText(deleteImageTitle)).toBeInTheDocument();
      await user.click(
        screen.getByRole('button', { name: deleteConfirmationButton }),
      );
      await waitFor(() =>
        expect(screen.getByRole('button', { name: 'Simpan' })).toBeDisabled(),
      );
    });

    it('Should handle error correctly', async () => {
      axios.onPut(`${API_URL.banners}/4794`).reply(500, GENERAL_ERROR_RESPONSE);
      renderWithRoute({ route });

      await user.click(await screen.findByText(firstItemTitle));
      expect(await screen.findByText(uploadImageLabel)).toBeInTheDocument();
      await user.type(screen.getByLabelText(bannerTitleLabel), 'test');
      await user.click(screen.getByRole('button', { name: 'Simpan' }));
      await waitFor(() =>
        expect(toast.danger).toHaveBeenLastCalledWith(
          GENERAL_ERROR_RESPONSE.errors[0].message,
        ),
      );
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'error',
        action_details: GENERAL_ERROR_RESPONSE.errors[0].message,
      });
    });
  });

  describe('Create', () => {
    it('Should perform create banner correctly', async () => {
      renderWithRoute({ route });
      await user.click(await screen.findByRole('button', { name: addButton }));
      expect(
        await screen.findByRole('button', { name: 'Tambah' }),
      ).toBeInTheDocument();
      const uploaderInput = screen.getByTestId('uploader-input');
      Object.defineProperty(uploaderInput, 'files', {
        value: [file],
      });
      fireEvent.drop(uploaderInput);
      await user.type(screen.getByLabelText(bannerTitleLabel), 'test');
      const ctaButton = screen.getByRole('button', { name: 'Tambah' });
      await waitFor(() => expect(ctaButton).toBeEnabled());
      await user.click(ctaButton);
      await waitFor(() =>
        expect(toast.success).toHaveBeenLastCalledWith(
          GENERAL_SUCCESS_RESPONSE.message,
        ),
      );
      expect(getLastCalledTimes(axios.history.post, API_URL.banners)).toEqual(
        1,
      );
      expect(
        await screen.findByRole('button', { name: addButton }),
      ).toBeInTheDocument();
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'add_entry',
      });
    });

    it('Should handle error when create banner', async () => {
      axios.onPost(API_URL.banners).reply(500, GENERAL_ERROR_RESPONSE);
      renderWithRoute({ route });
      await user.click(await screen.findByRole('button', { name: addButton }));
      const uploaderInput = screen.getByTestId('uploader-input');
      Object.defineProperty(uploaderInput, 'files', {
        value: [file],
      });
      fireEvent.drop(uploaderInput);
      const ctaButton = screen.getByRole('button', { name: 'Tambah' });
      await waitFor(() => expect(ctaButton).toBeEnabled());
      await user.click(ctaButton);
      await waitFor(() =>
        expect(toast.danger).toHaveBeenLastCalledWith(
          GENERAL_ERROR_RESPONSE.errors[0].message,
        ),
      );
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'error',
        action_details: GENERAL_ERROR_RESPONSE.errors[0].message,
      });
    });
  });
});
