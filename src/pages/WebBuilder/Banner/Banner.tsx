import React from 'react';
import { NavLink } from 'react-router-dom';

import { useGetBanners, useDeleteBanner } from '@hooks/api/webBuilder/banner';
import useDeleteItem from '@hooks/useDeleteItem';
import useWebBuilder from '@hooks/useWebBuilder';

import SideMenuWebBuilder from '@components/WebBuilder/SideMenu';
import SideMenuGroup from '@components/WebBuilder/SideMenu/partials/SideMenuGroup';
import Button from '@components/Button';
import {
  sendWebBuilderTracker,
  WebBuilderAction,
} from '@utils/tracker/webBuilder';
import { getErrorMessage } from '@utils/error';

const WebBuilderBanner = () => {
  const { sideMenu, isError, isFetching, refetchBanners } = useGetBanners();

  const { refreshPreview } = useWebBuilder();
  const { onDelete, ModalDeleteComponent } = useDeleteItem({
    refetch: refetchBanners,
    deleteItem: useDeleteBanner,
    onRefetch: refreshPreview,
    modalTitle: 'Hapus Banner?',
    modalContent: 'Kamu akan menghapus <strong>Banner</strong>.',
    afterSuccess: (id) => {
      void sendWebBuilderTracker(WebBuilderAction.DeleteEntry, `${id}`);
    },
    afterError: (error) => {
      void sendWebBuilderTracker(
        WebBuilderAction.Error,
        getErrorMessage(error),
      );
    },
  });

  return (
    <SideMenuWebBuilder
      header={{
        title: 'Banner',
        subtitle:
          'Dengan Banner, kamu bisa menampilkan promosi menarik berupa gambar dan teks.',
        backUrl: '/settings/custom-website/builder/section',
      }}
      action={
        <NavLink to="create">
          <Button className="w-full mb-8">Tambah Banner</Button>
        </NavLink>
      }
      isEmpty={!sideMenu[0].menus.length}
      isError={isError}
      isFetching={isFetching}
      onRetry={refetchBanners}
    >
      <>
        {sideMenu.map((group, index) => (
          <SideMenuGroup
            {...group}
            groupIndex={index}
            key={`menu-group-${index}`}
            onDelete={onDelete}
            onItemClick={(itemIndex, menuItems) =>
              void sendWebBuilderTracker(
                WebBuilderAction.ClickEntry,
                `${menuItems[itemIndex].id}`,
              )
            }
          />
        ))}
        <ModalDeleteComponent />
      </>
    </SideMenuWebBuilder>
  );
};

export default WebBuilderBanner;
