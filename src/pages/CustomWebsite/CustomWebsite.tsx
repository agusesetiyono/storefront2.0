import React, { useState } from 'react';
import { Button, Stack, Card } from 'react-bootstrap';

import toast from '@utils/toast';
import { sendLinkTracker } from '@utils/tracker/dashboard';
import type { IThemeData } from '@interfaces/themes';

import TabHeader from '@components/TabHeader';
import Theme from '@components/Theme';

const CustomWebsite = () => {
  const WEBSITE_BUILDER_URL =
    '/storefront-editor/settings/custom-website/builder';
  const [isSelectedThemeSaved, setIsSelectedThemeSaved] = useState(false);
  const [selectedThemeName, setSelectedThemeName] = useState('');

  const onThemeChange = (
    selectedThemeId: number,
    themesData: IThemeData,
    savedThemeId: number,
  ) => {
    setIsSelectedThemeSaved(savedThemeId === selectedThemeId);
    setSelectedThemeName(themesData[selectedThemeId]?.theme_name || '');
  };

  return (
    <Card className="px-24px py-24px">
      <Card.Body>
        <TabHeader
          title="Tema Website Kamu"
          subtitle="Ini adalah tema yang lagi aktif digunakan di website kamu."
          action={[
            <Button
              href={WEBSITE_BUILDER_URL}
              className="w-[160px] py-2 text-light"
              onClick={(event) =>
                void sendLinkTracker({
                  event,
                  url: WEBSITE_BUILDER_URL,
                  actionType: 'to_website_builder',
                })
              }
            >
              Custom sekarang
            </Button>,
          ]}
        />
        <hr className="mb-8" />
        <Stack direction="horizontal" className="gap-48px">
          <div>
            <h2 className="text-3xl mb-3">Ubah Tema</h2>
            <p className="text-base max-w-[300px] opacity-70">
              Pilih tema yang kamu inginkan untuk website-mu.
            </p>
          </div>
          <h3
            className={`text-6xl font-light ml-8 capitalize${
              isSelectedThemeSaved ? ' text-blue-600' : ''
            }`}
            data-testid="selected-theme-name"
          >
            {selectedThemeName}
          </h3>
        </Stack>
        <Theme
          buttonPosition="bottom-6 left-8 right-8"
          thumbnailGap="gap-x-9"
          thumbnailStyle="mt-8 p-3"
          saveOnThumbnail
          onThemeChange={onThemeChange}
          onThemeSave={(message) => toast.success(message)}
        />
      </Card.Body>
    </Card>
  );
};

export default CustomWebsite;
