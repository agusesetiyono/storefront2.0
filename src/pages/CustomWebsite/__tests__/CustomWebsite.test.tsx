import axios from '@mocks/axios';
import {
  screen,
  waitFor,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup';
import '@testing-library/jest-dom';
import { renderWithRoute, API_URL } from '@utils/test-helper';
import toast from '@utils/toast';
import { sendTracker } from '@utils/tracker';
import { getMockSubscriptionsResponse } from '@mocks/api/storefronts';
import {
  getMockSelectedThemeResponse,
  GET_THEMES_SUCCESS_RESPONSE,
} from '@mocks/api/themes';

jest.mock('@utils/toast', () => ({
  success: jest.fn(),
}));

axios.onGet(`${API_URL.storefronts}/status`).reply(
  200,
  getMockSubscriptionsResponse({
    type: 'platinum',
    expiredDate: '2022-05-01T00:00:00+07:00',
  }),
);

let user: UserEvent;
beforeEach(() => {
  user = userEvent.setup({ delay: null });

  jest.useFakeTimers();
  jest.setSystemTime(new Date('2022-04-01T00:00:00+07:00'));

  localStorage.setItem('onboardingStep', '5'); // already finished onboarding
  axios
    .onGet(`${API_URL.storefronts}/themes/me`)
    .reply(200, getMockSelectedThemeResponse());
  axios
    .onGet(`${API_URL.storefronts}/themes`)
    .reply(200, GET_THEMES_SUCCESS_RESPONSE);
});

afterEach(() => {
  jest.useRealTimers();
  jest.clearAllMocks();
  localStorage.clear();
});

const assertButtonInThumbnail = (button: HTMLElement, altText: string) => {
  expect(button.parentNode?.previousSibling).toHaveAttribute(
    'aria-label',
    altText,
  );
};

const assertActiveTheme = (themeName: string) => {
  const selectedThemeName = screen.getByTestId('selected-theme-name');
  expect(selectedThemeName.textContent).toEqual(themeName);
  expect(selectedThemeName).toHaveClass('text-blue-600');
  expect(screen.getByTestId(`theme-${themeName}`)).toHaveClass(
    'border-blue-600 bg-blue-50',
  );
  assertButtonInThumbnail(
    screen.getByRole('button', { name: 'Preview' }),
    themeName,
  );
  expect(
    screen.queryByRole('button', { name: 'Simpan' }),
  ).not.toBeInTheDocument();
  expect(screen.getAllByText(themeName)[1]).toHaveClass(
    'text-blue-600 font-bold',
  );
};

describe('Custom Website Page', () => {
  const route = '/storefront-editor/settings/custom-website';
  const defaultTrackerParams = {
    evn: 'ngorder_sf_dashboard_actions',
    active_tab: 'custom_website',
  };

  it('Should match snapshot', async () => {
    const { container } = renderWithRoute({ route });

    expect(await screen.findByText('Tema Website Kamu')).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });

  it('Should render sections correctly', async () => {
    renderWithRoute({ route });

    // Customize website section
    expect(await screen.findByText('Tema Website Kamu')).toBeInTheDocument();
    expect(
      screen.getByText(
        'Ini adalah tema yang lagi aktif digunakan di website kamu.',
      ),
    ).toBeInTheDocument();
    expect(
      screen.getByRole('button', { name: 'Custom sekarang' }),
    ).toBeInTheDocument();

    // Theme section
    expect(screen.getByText('Ubah Tema')).toBeInTheDocument();
    expect(
      screen.getByText('Pilih tema yang kamu inginkan untuk website-mu.'),
    ).toBeInTheDocument();
    expect(screen.getAllByTestId('theme-thumbnail').length).toEqual(6);
    assertActiveTheme('naomi');

    await user.click(
      await screen.findByRole('button', { name: 'Custom sekarang' }),
    );
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'to_website_builder',
    });
  });

  it('Should change & save selected theme correctly', async () => {
    renderWithRoute({ route });

    expect(await screen.findByText('Tema Website Kamu')).toBeInTheDocument();
    // select another theme
    await user.click(screen.getByTestId('theme-salma'));
    const selectedThemeName = screen.getByTestId('selected-theme-name');
    expect(selectedThemeName.textContent).toEqual('salma');
    expect(selectedThemeName).not.toHaveClass('text-blue-600');
    assertButtonInThumbnail(
      screen.getByRole('button', { name: 'Preview' }),
      'salma',
    );
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'click_theme',
      action_details: 'salma',
    });
    const saveButton = screen.getByRole('button', { name: 'Simpan' });
    assertButtonInThumbnail(saveButton, 'salma');
    expect(screen.getAllByText('salma')[1]).not.toHaveClass(
      'text-blue-600 font-bold',
    );

    // save theme
    const message = 'Berhasil mengganti Tema!';
    axios.onPatch(`${API_URL.storefronts}/themes`).reply(200, { message });
    await user.click(saveButton);
    await waitFor(() =>
      expect(toast.success).toHaveBeenLastCalledWith(message),
    );
    await waitFor(() => {
      expect(sendTracker).toHaveBeenLastCalledWith({
        ...defaultTrackerParams,
        action_type: 'save_changes',
      });
    });
    assertActiveTheme('salma');
  });

  it.each(['/themes', '/themes/me'])(
    'Should render error state correctly, when error get %s',
    async (url) => {
      axios.onGet(`${API_URL.storefronts}${url}`).reply(500);
      renderWithRoute({ route });

      await waitFor(
        () =>
          expect(
            screen.getByText(
              'Gagal menunjukkan list tema. Refresh untuk mencoba lagi',
            ),
          ).toBeInTheDocument(),
        { timeout: 10000 },
      );

      // refetch with success response
      axios
        .onGet(`${API_URL.storefronts}/themes/me`)
        .reply(200, getMockSelectedThemeResponse());
      axios
        .onGet(`${API_URL.storefronts}/themes`)
        .reply(200, GET_THEMES_SUCCESS_RESPONSE);
      await user.click(screen.getByRole('button', { name: 'Coba Lagi' }));
      const thumbnails = await screen.findAllByTestId('theme-thumbnail');
      expect(thumbnails.length).toEqual(6);
    },
  );

  it('Should cannot select other theme when saving current selected theme', async () => {
    renderWithRoute({ route });

    expect(await screen.findByText('Tema Website Kamu')).toBeInTheDocument();
    await user.click(screen.getByTestId('theme-salma'));
    void user.click(screen.getByRole('button', { name: 'Simpan' }));
    void user.click(screen.getByTestId('theme-fixy'));
    expect(screen.getByTestId('selected-theme-name').textContent).toEqual(
      'salma',
    );
  });

  it('Should show preview theme correctly', async () => {
    renderWithRoute({ route });

    // show preview modal
    await user.click(await screen.findByText('Preview'));
    await waitFor(() =>
      expect(screen.getByTestId('preview-theme-modal')).toBeInTheDocument(),
    );
    expect(screen.getByAltText('theme-preview')).toHaveAttribute(
      'src',
      'https://storefront-sandbox-bucket.smartseller.co.id/images/theme-naomi-small.png',
    );
    expect(sendTracker).toHaveBeenLastCalledWith({
      ...defaultTrackerParams,
      action_type: 'preview_theme',
      action_details: 'naomi',
    });

    // close preview modal by clicking modal backdrop
    await user.click(screen.getByRole('dialog'));
    await waitForElementToBeRemoved(() =>
      screen.getByTestId('preview-theme-modal'),
    );
  });
});
