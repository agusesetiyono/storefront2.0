export const GENERAL_ERROR_RESPONSE = {
  errors: [
    { message: 'Gagal menyimpan perubahan. Silakan coba lagi.', code: 997 },
  ],
  meta: { http_status: 500 },
};

export const GENERAL_SUCCESS_RESPONSE = {
  message: 'Berhasil menyimpan perubahan',
  meta: {
    http_status: 200,
  },
};
