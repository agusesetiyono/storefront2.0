export const GET_CATEGORIES_SUCCESS_RESPONSE = {
  data: [
    { code: 'N', name: 'Pelanggan' },
    { code: 'D', name: 'Dropshipper' },
    { code: 'Y', name: 'Reseller' },
    { code: '1', name: 'Agen' },
    { code: '2', name: 'Customer Tetap' },
    { code: '3', name: 'Distributor ASLI' },
  ],
  meta: { http_status: 200 },
};
