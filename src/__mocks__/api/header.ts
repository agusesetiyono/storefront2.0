export const GET_HEADER_SUCCESS_RESPONSE = {
  data: {
    logo: 'https://akcdn.detik.net.id/community/media/visual/2022/08/01/lionel-messi-1.jpeg?w=200&q=90',
    shop_name: 'Toko Smart',
  },
  meta: { http_status: 200 },
};
