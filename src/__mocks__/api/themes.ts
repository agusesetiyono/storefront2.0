import type { ISelectedThemeResponseStatus } from '@interfaces/themes';

export const getMockSelectedThemeResponse = (
  themeId = 2, emptyColors = false
): ISelectedThemeResponseStatus => ({
  data: {
    theme_id: themeId,
    color: {
      accent_color: emptyColors ? '' : 'white',
      body_bg_color: emptyColors ? '' : 'white',
      container_bg_color: emptyColors ? '' : 'white',
      footer_bg_color: emptyColors ? '' : 'white',
      header_bg_color: emptyColors ? '' : 'white',
      main_color: emptyColors ? '' : 'white',
      navbar_bg_color: emptyColors ? '' : 'white',
      widget_chat_color: emptyColors ? '' : 'white',
    },
    default_color: {
      accent_color: 'green',
      body_bg_color: 'green',
      container_bg_color: 'green',
      footer_bg_color: 'green',
      header_bg_color: 'green',
      main_color: 'green',
      navbar_bg_color: 'green',
      widget_chat_color: 'green',
    },
    font: {
      font_category: 'sans-serif',
      font_color: '#c71e1e',
      font_family: 'Encode Sans Expanded:600',
    },
    default_font: {
      font_category: 'sans-serif',
      font_color: '#000',
      font_family: 'Lato',
    },
    setting: {
      custom_css: 'test',
      favicon:
        'https://akcdn.detik.net.id/community/media/visual/2022/08/01/lionel-messi-1.jpeg?w=200&q=90',
      language: 'id',
    },
  },
});

export const GET_THEMES_SUCCESS_RESPONSE = {
  data: [
    {
      id: 1,
      theme_name: 'default',
      images: {
        large_urls:
          'https://storefront-sandbox-bucket.smartseller.co.id/images/theme-default-large.png',
        small_urls:
          'https://storefront-sandbox-bucket.smartseller.co.id/images/theme-default-small.png',
      },
    },
    {
      id: 2,
      theme_name: 'naomi',
      images: {
        large_urls:
          'https://storefront-sandbox-bucket.smartseller.co.id/images/theme-naomi-large.png',
        small_urls:
          'https://storefront-sandbox-bucket.smartseller.co.id/images/theme-naomi-small.png',
      },
    },
    {
      id: 3,
      theme_name: 'orenji',
      images: {
        large_urls:
          'https://storefront-sandbox-bucket.smartseller.co.id/images/theme-orenji-large.png',
        small_urls:
          'https://storefront-sandbox-bucket.smartseller.co.id/images/theme-orenji-small.png',
      },
    },
    {
      id: 4,
      theme_name: 'eliza',
      images: {
        large_urls:
          'https://storefront-sandbox-bucket.smartseller.co.id/images/theme-eliza-large.png',
        small_urls:
          'https://storefront-sandbox-bucket.smartseller.co.id/images/theme-eliza-small.png',
      },
    },
    {
      id: 5,
      theme_name: 'salma',
      images: {
        large_urls:
          'https://storefront-sandbox-bucket.smartseller.co.id/images/theme-salma-large.png',
        small_urls:
          'https://storefront-sandbox-bucket.smartseller.co.id/images/theme-salma-small.png',
      },
    },
    {
      id: 7,
      theme_name: 'fixy',
      images: {
        large_urls:
          'https://storefront-sandbox-bucket.smartseller.co.id/images/theme-fixy-large.png',
        small_urls:
          'https://storefront-sandbox-bucket.smartseller.co.id/images/theme-fixy-small.png',
      },
    },
  ],
  meta: {
    http_status: 200,
  },
};
