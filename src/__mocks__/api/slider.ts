export const GET_SLIDERS_SUCCESS_RESPONSE = {
  data: [
    {
      id: 1,
      image:
        'https://akcdn.detik.net.id/community/media/visual/2022/08/01/lionel-messi-1.jpeg?w=200&q=90',
      title: 'That one lazy dogo',
      desc: 'desc 1',
      url: 'https://www.bukalapak.com',
      cta: 'cta 1',
      order: 1,
    },
    {
      id: 2,
      image:
        'https://akcdn.detik.net.id/community/media/visual/2022/08/01/lionel-messi-1.jpeg?w=200&q=90',
      title: 'The lazy dog in the park',
      desc: 'desc 2',
      url: 'https://www.bukalapak.com',
      cta: 'cta 2',
      order: 2,
    },
    {
      id: 3,
      image:
        'https://akcdn.detik.net.id/community/media/visual/2022/08/01/lionel-messi-1.jpeg?w=200&q=90',
      title: '',
      desc: 'desc 3',
      url: 'https://www.bukalapak.com',
      cta: 'cta 3',
      order: 3,
    },
    {
      id: 4,
      image:
        'https://akcdn.detik.net.id/community/media/visual/2022/08/01/lionel-messi-1.jpeg?w=200&q=90',
      title: 'title 4',
      desc: 'desc 4',
      url: 'https://www.bukalapak.com',
      cta: 'cta 4',
      order: 4,
    },
    {
      id: 5,
      image:
        'https://akcdn.detik.net.id/community/media/visual/2022/08/01/lionel-messi-1.jpeg?w=200&q=90',
      desc: 'desc 5',
      url: 'https://www.bukalapak.com',
      cta: 'cta 5',
      order: 5,
    },
    {
      id: 6,
      image:
        'https://akcdn.detik.net.id/community/media/visual/2022/08/01/lionel-messi-1.jpeg?w=200&q=90',
      desc: 'desc 6',
      url: 'https://www.bukalapak.com',
      cta: 'cta 6',
      order: 6,
    },
    {
      id: 7,
      image:
        'https://akcdn.detik.net.id/community/media/visual/2022/08/01/lionel-messi-1.jpeg?w=200&q=90',
      desc: 'desc 7',
      url: 'https://www.bukalapak.com',
      cta: 'cta 7',
      order: 7,
    },
    {
      id: 8,
      image:
        'https://akcdn.detik.net.id/community/media/visual/2022/08/01/lionel-messi-1.jpeg?w=200&q=90',
      desc: 'desc 8',
      url: 'https://www.bukalapak.com',
      cta: 'cta 8',
      order: 8,
    },
    {
      id: 9,
      image:
        'https://akcdn.detik.net.id/community/media/visual/2022/08/01/lionel-messi-1.jpeg?w=200&q=90',
      desc: 'desc 9',
      url: 'https://www.bukalapak.com',
      cta: 'cta 9',
      order: 9,
    },
    {
      id: 10,
      image:
        'https://akcdn.detik.net.id/community/media/visual/2022/08/01/lionel-messi-1.jpeg?w=200&q=90',
      desc: 'desc 10',
      url: 'https://www.bukalapak.com',
      cta: 'cta 10',
      order: 10,
    },
  ],
};

export const GET_SLIDER_SUCCESS_RESPONSE = {
  data: {
    id: 1,
    image:
      'https://akcdn.detik.net.id/community/media/visual/2022/08/01/lionel-messi-1.jpeg?w=200&q=90',
    title: 'That one lazy dogo',
    desc: 'desc 1',
    url: 'https://www.bukalapak.com',
    cta: 'cta 1',
    order: 1,
  },
};

export const REORDER_SLIDER_SUCCESS_RESPONSE = {
  message: 'Berhasil memperbaharui urutan halaman',
  meta: {
    http_status: 200,
  },
};

export const GET_DELETE_SLIDER_ERROR_RESPONSE = (code = 997) => ({
  errors: [{ message: 'Gagal hapus slider. Silakan coba lagi.', code }],
  meta: { http_status: 500 },
});
