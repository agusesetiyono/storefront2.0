export const GET_PRODUCT_LIST_SUCCESS_RESPONSE = {
  data: {
    id: 12313,
    title: 'Custom Product List',
    product_list: [
      {
        id: 1,
        name: 'Kemeja Polos Banget Guys',
        image: 'https://cf.shopee.co.id/file/75a9377cb95797a27bc3c39be9300633',
      },
      {
        id: 2,
        name: 'Tas Punggung Pria',
        image:
          'https://image-cdn.smartseller.co.id/7/products/tas-punggung-pria-murah-1627256998575.webp?width=500',
      },
      {
        id: 3,
        name: 'Sandal Gunung',
        image:
          'https://image-cdn.smartseller.co.id/7/products/sandal-murce-1627256838294.webp?width=500',
      },
    ],
  },
};

export const GET_PRODUCT_LIST_EMPTY_RESPONSE = {
  data: {
    id: 0,
    title: '',
    product_list: [],
  },
};

export const GET_SEARCH_PRODUCT_SUCCESS_RESPONSE = {
  data: [
    {
      id: 5,
      name: 'Kaos Garis-Garis Warna Kuning',
      image:
        'https://image-cdn.smartseller.co.id/7/products/kaos-polo-1612272377164.jfif?width=500',
    },
    {
      id: 6,
      name: 'Kaos Polos Banget Warna Hitam',
      image: 'https://s3.bukalapak.com/img/37064928592/large/data.jpeg',
    },
  ],
};
