export const GET_TEXT_SUCCESS_RESPONSE = {
  data: {
    title: 'Sample title',
    content: '<p>Sample page</p>',
  },
};

export const GET_TEXT_EMPTY_RESPONSE = {
  data: {
    title: '',
    content: '',
  },
};
