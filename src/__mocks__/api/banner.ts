export const GET_BANNERS_SUCCESS_RESPONSE = {
  data: [
    {
      id: 4794,
      image:
        'https://ngorder-1.sgp1.digitaloceanspaces.com/91501/storefront/banner/banner-1659593570139.png',
      title: 'This is the title',
      desc: 'Ini adalah deskripsi',
      url: 'http://app.smartseller.co.id',
    },
    {
      id: 4793,
      image:
        'https://ngorder-1.sgp1.digitaloceanspaces.com/91501/storefront/banner/banner-1659593501462.png',
      title: '',
      desc: '',
      url: '',
    },
  ],
  meta: {
    http_status: 200,
  },
};

export const GET_BANNER_SUCCESS_RESPONSE = {
  data: {
    id: 4794,
    image:
      'https://ngorder-1.sgp1.digitaloceanspaces.com/91501/storefront/banner/banner-1659593570139.png',
    title: 'This is the title',
    desc: 'Ini adalah deskripsi',
    url: 'http://app.smartseller.co.id',
  },
};

export const GET_DELETE_BANNER_ERROR_RESPONSE = (code = 997) => ({
  errors: [{ message: 'Gagal hapus banner. Silakan coba lagi.', code }],
  meta: { http_status: 500 },
});
