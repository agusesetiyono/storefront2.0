import type {
  IResponseStatus,
  IGetMockSubscriptionResponseParams,
} from '@interfaces/storefronts';

export const getMockSubscriptionsResponse = ({
  type,
  expiredDate,
  domain,
  subdomain,
  activeDomain,
  version,
}: IGetMockSubscriptionResponseParams): IResponseStatus => ({
  data: {
    owner_name: 'Ngorder Owner',
    storefront_type: type,
    storefront_expired_date: expiredDate,
    domain: domain || 'domain.com',
    subdomain: subdomain || 'subdomain',
    active_domain: activeDomain || 'domain.com',
    version: version || '2.0',
  },
});
