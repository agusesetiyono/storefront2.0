export const GET_COUPONS_SUCCESS_RESPONSE = {
  data: [
    {
      id: 4,
      code: 'BIGSALE20',
      discount: 32767,
      nominal_max: 0,
      paid_limit: 1000091,
      start_date: '2022-03-01T00:00:00+07:00',
      end_date: '2023-07-01T00:00:00+07:00',
      target_customer_category: [],
      status: 'active',
    },
    {
      id: 5,
      code: 'INIPROMO2',
      discount: 50,
      nominal_max: 10000,
      paid_limit: 20000,
      start_date: '2020-04-01T00:00:00+07:00',
      end_date: '2020-05-31T02:30:00+07:00',
      target_customer_category: [
        {
          code: 'N',
          name: 'Pelanggan',
        },
        {
          code: 'D',
          name: 'Dropshipper',
        },
      ],
      status: 'inactive',
    },
    {
      id: 6,
      code: 'PROMO444',
      discount: 0,
      nominal_max: 50000,
      paid_limit: 10000,
      start_date: '2019-04-03T00:00:00+07:00',
      end_date: '2020-04-30T01:00:00+07:00',
      target_customer_category: [
        {
          code: 'N',
          name: 'Pelanggan',
        },
        {
          code: 'D',
          name: 'Dropshipper',
        },
        {
          code: 'Y',
          name: 'Reseller',
        },
      ],
      status: 'inactive',
    },
    {
      id: 46,
      code: 'MAUDISKON1',
      discount: 0,
      nominal_max: 9000,
      paid_limit: 1000,
      start_date: '2019-04-04T00:00:00+07:00',
      end_date: '2022-05-08T00:00:00+07:00',
      target_customer_category: [
        {
          code: 'N',
          name: 'Pelanggan',
        },
        {
          code: 'D',
          name: 'Dropshipper',
        },
        {
          code: 'Y',
          name: 'Reseller',
        },
        {
          code: '2',
          name: 'Customer Tetap',
        },
        {
          code: '3',
          name: 'Distributor ASLI',
        },
      ],
      status: 'inactive',
    },
    {
      id: 71,
      code: 'DISKON200',
      discount: 0,
      nominal_max: 200000,
      paid_limit: 1500000,
      start_date: '2019-11-18T00:00:00+07:00',
      end_date: '2019-12-05T00:00:00+07:00',
      target_customer_category: [
        {
          code: 'Y',
          name: 'Reseller',
        },
      ],
      status: 'inactive',
    },
    {
      id: 76,
      code: 'DISC1',
      discount: 0,
      nominal_max: 100000,
      paid_limit: 50000,
      start_date: '2019-04-04T09:30:00+07:00',
      end_date: '2019-04-20T00:00:00+07:00',
      target_customer_category: [
        {
          code: 'N',
          name: 'Pelanggan',
        },
        {
          code: '1',
          name: 'Agen',
        },
      ],
      status: 'inactive',
    },
    {
      id: 106,
      code: 'DISKON20RB',
      discount: 0,
      nominal_max: 20000,
      paid_limit: 100000,
      start_date: '2019-11-26T01:00:00+07:00',
      end_date: '2019-11-27T21:27:00+07:00',
      target_customer_category: [
        {
          code: 'N',
          name: 'Pelanggan',
        },
        {
          code: 'D',
          name: 'Dropshipper',
        },
        {
          code: 'Y',
          name: 'Reseller',
        },
        {
          code: '1',
          name: 'Agen',
        },
        {
          code: '2',
          name: 'Customer Tetap',
        },
        {
          code: '3',
          name: 'Distributor ASLI',
        },
      ],
      status: 'inactive',
    },
    {
      id: 111,
      code: 'KUPON50',
      discount: 50,
      nominal_max: 1000000,
      paid_limit: 500000,
      start_date: '2019-04-01T00:00:00+07:00',
      end_date: '2019-04-05T00:00:00+07:00',
      target_customer_category: [
        {
          code: 'N',
          name: 'Pelanggan',
        },
        {
          code: 'D',
          name: 'Dropshipper',
        },
        {
          code: 'Y',
          name: 'Reseller',
        },
        {
          code: '1',
          name: 'Agen',
        },
        {
          code: '2',
          name: 'Customer Tetap',
        },
      ],
      status: 'inactive',
    },
    {
      id: 126,
      code: 'BAMBANG',
      discount: 400,
      nominal_max: 1000000,
      paid_limit: 0,
      start_date: '2019-04-01T00:00:00+07:00',
      end_date: '2019-04-30T00:00:00+07:00',
      target_customer_category: [
        {
          code: 'N',
          name: 'Pelanggan',
        },
        {
          code: 'D',
          name: 'Dropshipper',
        },
        {
          code: 'Y',
          name: 'Reseller',
        },
        {
          code: '1',
          name: 'Agen',
        },
        {
          code: '2',
          name: 'Customer Tetap',
        },
      ],
      status: 'inactive',
    },
    {
      id: 131,
      code: 'KUPON5',
      discount: 5,
      nominal_max: 25000,
      paid_limit: 100000,
      start_date: '2019-04-06T00:00:00+07:00',
      end_date: '2019-04-13T14:10:00+07:00',
      target_customer_category: [
        {
          code: 'Y',
          name: 'Reseller',
        },
      ],
      status: 'inactive',
    },
  ],
  meta: {
    http_status: 200,
    offset: 0,
    limit: 10,
    total: 134,
  },
};

export const GET_COUPONS_PAGE_2_SUCCESS_RESPONSE = {
  data: [
    {
      id: 341,
      code: 'BAWANG',
      discount: 0,
      nominal_max: 5000,
      paid_limit: 0,
      start_date: '2019-05-13T00:00:00+07:00',
      end_date: '2019-05-31T00:00:00+07:00',
      target_customer_category: [
        {
          code: 'N',
          name: 'Pelanggan',
        },
        {
          code: 'D',
          name: 'Dropshipper',
        },
        {
          code: 'Y',
          name: 'Reseller',
        },
        {
          code: '1',
          name: 'Agen',
        },
        {
          code: '2',
          name: 'Customer Tetap',
        },
      ],
      status: 'inactive',
    },
    {
      id: 396,
      code: 'LARIZA',
      discount: 0,
      nominal_max: 5000,
      paid_limit: 200000,
      start_date: '2019-06-17T08:00:00+07:00',
      end_date: '2019-06-22T12:00:00+07:00',
      target_customer_category: [],
      status: 'inactive',
    },
    {
      id: 401,
      code: 'TESTING1',
      discount: 0,
      nominal_max: 50000,
      paid_limit: 100000,
      start_date: '2019-06-18T00:00:00+07:00',
      end_date: '2019-06-30T00:00:00+07:00',
      target_customer_category: [],
      status: 'inactive',
    },
    {
      id: 406,
      code: 'TESTING1',
      discount: 0,
      nominal_max: 10000,
      paid_limit: 101010,
      start_date: '2019-06-18T00:00:00+07:00',
      end_date: '2019-06-19T00:00:00+07:00',
      target_customer_category: [],
      status: 'inactive',
    },
    {
      id: 411,
      code: 'ROLAS12',
      discount: 0,
      nominal_max: 24000,
      paid_limit: 100000,
      start_date: '2019-12-12T06:00:00+07:00',
      end_date: '2019-12-14T00:00:00+07:00',
      target_customer_category: [
        {
          code: 'N',
          name: 'Pelanggan',
        },
        {
          code: 'D',
          name: 'Dropshipper',
        },
        {
          code: 'Y',
          name: 'Reseller',
        },
      ],
      status: 'inactive',
    },
    {
      id: 416,
      code: 'LARIZ',
      discount: 0,
      nominal_max: 5000,
      paid_limit: 200000,
      start_date: '2019-06-17T08:00:00+07:00',
      end_date: '2019-06-22T12:00:00+07:00',
      target_customer_category: [
        {
          code: 'Y',
          name: 'Reseller',
        },
        {
          code: '1',
          name: 'Agen',
        },
      ],
      status: 'inactive',
    },
    {
      id: 421,
      code: 'LARISS',
      discount: 10,
      nominal_max: 10000,
      paid_limit: 200000,
      start_date: '2019-06-17T00:00:00+07:00',
      end_date: '2019-06-18T00:00:00+07:00',
      target_customer_category: [
        {
          code: '1',
          name: 'Agen',
        },
      ],
      status: 'inactive',
    },
    {
      id: 538,
      code: 'FORCUST',
      discount: 0,
      nominal_max: 20000,
      paid_limit: 100000,
      start_date: '2019-09-24T16:00:00+07:00',
      end_date: '2019-09-30T00:00:00+07:00',
      target_customer_category: [
        {
          code: 'N',
          name: 'Pelanggan',
        },
      ],
      status: 'inactive',
    },
    {
      id: 545,
      code: 'NEWDISC',
      discount: 0,
      nominal_max: 25000,
      paid_limit: 100000,
      start_date: '2019-10-10T00:00:00+07:00',
      end_date: '2020-10-31T00:00:00+07:00',
      target_customer_category: [
        {
          code: 'N',
          name: 'Pelanggan',
        },
        {
          code: 'D',
          name: 'Dropshipper',
        },
        {
          code: 'Y',
          name: 'Reseller',
        },
        {
          code: '1',
          name: 'Agen',
        },
        {
          code: '2',
          name: 'Customer Tetap',
        },
        {
          code: '3',
          name: 'Distributor ASLI',
        },
      ],
      status: 'inactive',
    },
    {
      id: 578,
      code: 'BERKAH11',
      discount: 20,
      nominal_max: 50000,
      paid_limit: 300000,
      start_date: '2019-12-03T00:00:00+07:00',
      end_date: '2019-12-12T00:00:00+07:00',
      target_customer_category: [
        {
          code: 'N',
          name: 'Pelanggan',
        },
      ],
      status: 'inactive',
    },
  ],
  meta: {
    http_status: 200,
    offset: 10,
    limit: 10,
    total: 134,
  },
};

export const GET_COUPON_SUCCESS_MESSAGE = {
  data: {
    attempt_usage: 87,
    code: '9812FR',
    discount_percentage: 0,
    discount_type: 'nominal',
    discount_value: 89000,
    end_date: '2022-04-30T12:00:00+07:00',
    id: 2985,
    max_discount: 0,
    paid_limit: 2345612,
    quota: -1,
    start_date: '2022-03-29T20:00:00+07:00',
    status: 'active',
    target_customer_category: ['N', 'D'],
    type: 'free_shipping',
  },
  meta: { http_status: 200 },
};
