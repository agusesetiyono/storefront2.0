export const GET_SEO_SUCCESS_RESPONSE = {
  data: {
    title: 'Malang shop | Tempat beli produk Malang',
    keyword: 'Jual beli Malang, belanja malang',
    description:
      'Malang shop adalah toko online yang berdomisili di Kota Malang dan menjual produk atau barang-barang yang terkait dengan khas Malang.',
  },
  meta: { http_status: 200 },
};

export const GET_META_PIXEL_SUCCESS_RESPONSE = {
  data: {
    pixel_id: '348704132352738',
    list_event: ['1', '2', '3'],
    catalog_token: '3fe5e3fcdde84ea88de985c724fa070c',
  },
  meta: { http_status: 200 },
};

export const GET_GOOGLE_ANALYTICS_DEFAULT_SUCCESS_RESPONSE = {
  data: {
    tracking_id: 'UA-4353-1233',
  },
  meta: {
    http_status: 200,
  },
};

export const GET_GOOGLE_ANALYTICS_EMPTY_SUCCESS_RESPONSE = {
  data: {
    tracking_id: '',
  },
  meta: {
    http_status: 200,
  },
};
