import _ from 'lodash';
import MockAdapter from 'axios-mock-adapter';
import { AxiosStatic } from 'axios';

const axios: AxiosStatic = jest.requireActual('axios');
jest.unmock('axios');

const mock = new MockAdapter(axios);
const mockAxios: MockAdapter = _.assignIn(axios, mock);

export default mockAxios;
